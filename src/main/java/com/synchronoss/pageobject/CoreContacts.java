package com.synchronoss.pageobject;

import java.awt.event.KeyEvent;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.synchronoss.core.Base;
import com.synchronoss.core.Logging;
import com.synchronoss.core.PropertyHelper;
import com.synchronoss.core.Tools;

public class CoreContacts extends PageObjectBase {

	/**
	 * @Method Name: AddAdderssBook
	 * @Author: Amanda Kang
	 * @Created On: 08/24/2014
	 * @Description: Adds a new address book to contacts
	 * @parameter1: String addressbook - address book name
	 */

	public void addAddressBook(String addressbook) {
		Logging.info("Adding new address book: " + addressbook);
		String contacts_addressbook_name_input = PropertyHelper.coreContacts
				.getPropertyValue("contacts_addressbook_name_input");

		try {
			Tools.setDriverDefaultValues();
			this.clickContactsToolBarButton("Add address book");

			Logging.info("Clearing texts from address book name input");
			Base.base.getDriver().findElement(By.xpath(contacts_addressbook_name_input)).clear();

			Logging.info("Entering new address book name: " + addressbook);
			Base.base.getDriver().findElement(By.xpath(contacts_addressbook_name_input)).sendKeys(addressbook);
			Base.base.getDriver().findElement(By.xpath(contacts_addressbook_name_input)).sendKeys(Keys.ENTER);
			this.clickSaveOrCancelGroupButton("Save");

			this.waitForLoadMaskDismissed();

		} catch (WebDriverException ex) {
			Logging.error("Could not add new address book " + addressbook);
			throw ex;
		}
	}

	/**
	 * @Method Name: getAddressBookUserNumber
	 * @Author: Vivian Xie
	 * @Created On: 09/12/2016
	 * @Description: get addressbook user number
	 * @parameter1: String addressBookName - addressBook name
	 */
	public int getAddressBookUserNumber(String addressBookName) {
		Logging.info("starting to get user number in addressbook " + addressBookName);
		addressBookName = addressBookName.toLowerCase();
		addressBookName = addressBookName.replace("-", "");
		String contacts_addressbook_userNo = PropertyHelper.coreContacts.getPropertyValue("contacts_addressbook_userNo")
				.replace("+variable+", addressBookName);
		int currentNo = 0;
		try {
			String scurrentNo = Base.base.getDriver().findElement(By.xpath(contacts_addressbook_userNo)).getText();
			currentNo = Integer.parseInt(scurrentNo);
			Logging.info("There are " + currentNo + " users in  adressbook " + addressBookName);
		} catch (WebDriverException ex) {
			Logging.info("Could not found element in getAddressBookUserNumber");
			throw ex;
		}
		return currentNo;
	}

	/**
	 * @Method Name: inputGroupName
	 * @Author: Vivian Xie
	 * @Created On: 09/12/2016
	 * @Description: Input group name after clicking "Add A group button "
	 * @parameter1: String groupName - group name the newly created group name
	 */

	public void inputGroupName(String groupName) {
		Logging.info("Adding new group: " + groupName);
		String contacts_addgroup_name_input = PropertyHelper.coreContacts
				.getPropertyValue("contacts_addgroup_name_input");

		try {
			Tools.setDriverDefaultValues();
			Logging.info("Clearing texts from group name input");
			Base.base.getDriver().findElement(By.xpath(contacts_addgroup_name_input)).clear();
			Logging.info("Entering new  group name: " + groupName);
			Base.base.getDriver().findElement(By.xpath(contacts_addgroup_name_input)).sendKeys(groupName);
			Base.base.getDriver().findElement(By.xpath(contacts_addgroup_name_input)).sendKeys(Keys.ENTER);

		} catch (WebDriverException ex) {
			Logging.error("Could not input groupName " + groupName);
			throw ex;
		}
	}

	/**
	 * @Method Name: getGroupUserNumber
	 * @Author: Vivian Xie
	 * @Created On: 09/12/2016
	 * @Description: get group user number
	 * @parameter1: String groupName - group name
	 * @return user number
	 */

	public int getGroupUserNumber(String groupName) {
		String contact_group_user_no = PropertyHelper.coreContacts.getPropertyValue("contact_group_user_no")
				.replace("+variable+", groupName);
		int number = 0;
		try {
			Tools.setDriverDefaultValues();
			String currentNo = Base.base.getDriver().findElement(By.xpath(contact_group_user_no)).getText();
			Logging.info("There are " + currentNo + " users in Group " + groupName);
			number = Integer.parseInt(currentNo);
		} catch (WebDriverException ex) {
			Logging.error("Could not find elememt in getGroupUserNumber");
		}

		return number;

	}

	/**
	 * @Method Name: verifyConactMobileValue
	 * @Author: Vivian Xie
	 * @Created On: 09/12/2016
	 * @Description: assert if the mobile exists on contact detail page
	 * @parameter1: String mobile -- the expect mobile number
	 */

	public boolean verifyConactMobileValue(String mobile) {
		Logging.info("asserting contact mobile  " + mobile);
		String contact_mobile = PropertyHelper.coreContacts.getPropertyValue("contact_mobile").replace("+variable+",
				mobile);
		boolean result = false;
		try {
			Base.base.getDriver().findElement(By.xpath(contact_mobile));
			result = true;
		} catch (WebDriverException ex) {
			Logging.error("Could not verify contact mobile value");
			throw ex;
		}
		return result;
	}

	/**
	 * Get contact main info value on contact detail view
	 * 
	 * @Author Vivian Xie
	 * @Created May/5/2017
	 * @parameter fieldName -- value can be "Mobile", "Phone", "Email"
	 */
	public ArrayList<String> getConactMainInfoValueInContactDetailView(String fieldName) {
		Logging.info("Starting to get contact " + fieldName + " value in contact detail view");
		String contact_filed_in_contact_detail_view = PropertyHelper.coreContacts
				.getPropertyValue("contact_filed_in_contact_detail_view")
				.replace("+variable+", fieldName.toLowerCase());
		ArrayList<String> result = new ArrayList<String>();
		try {
			List<WebElement> webElements = Base.base.getDriver()
					.findElements(By.xpath(contact_filed_in_contact_detail_view));
			for (int i = 0; i < webElements.size(); i++) {
				String fieldValue = webElements.get(i).getAttribute("innerHTML");
				result.add(fieldValue);
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not get contact " + fieldName + " value");
			throw ex;
		}
		return result;
	}

	/**
	 * @Method Name: addUserToGroup
	 * @Author: Vivian Xie
	 * @Created On: 09/12/2016
	 * @Description: Adds a user to group
	 * @parameter1: String userName, user name;String groupName - group name
	 */
	public void addUserToGroup(String userName, String groupName) {

		Logging.info("adding User " + userName + " to Group " + groupName);
		String contact_select_group_name = PropertyHelper.coreContacts.getPropertyValue("contact_select_group_name")
				.replace("+variable+", groupName);
		System.out.println(contact_select_group_name);
		try {
			System.out.println("clicking group" + groupName);
			Base.base.getDriver().findElement(By.xpath(contact_select_group_name)).click();
			;
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not add new group " + groupName);
			throw ex;
		}
	}

	/**
	 * @Method Name: deleteGroup
	 * @Author: Vivian Xie
	 * @Created On: 09/12/2016
	 * @Description: Delete the specific group
	 * @parameter1: String groupName - group name
	 */
	public void deleteGroup(String groupName) {
		Logging.info("Deleting group: " + groupName);
		String contact_group_in_list = PropertyHelper.coreContacts.getPropertyValue("contact_group_in_list")
				.replace("+variable+", groupName);
		System.out.println(contact_group_in_list);
		Logging.info(contact_group_in_list);

		String contact_group_delete_icon = PropertyHelper.coreContacts.getPropertyValue("contact_group_delete_icon");

		try {
			Tools.moveFocusToElementByXpath(contact_group_in_list);
			WebElement delete = Base.base.getDriver().findElement(By.xpath(contact_group_delete_icon));
			delete.click();
			clickButtonOnMessageBox("Yes");
			Logging.info("Delete group successful " + groupName);
		} catch (WebDriverException ex) {
			Logging.info("Could not delete group " + groupName);
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jun 24, 2015<br>
	 * <b>Description</b><br>
	 * Calculate the number of user created Address Book
	 * 
	 */
	public int calculateUserCreatedAddressBookNumber() {
		Logging.info("Calculating the number of user created Address Book");
		String contacts_user_created_address_book_list = PropertyHelper.coreContacts
				.getPropertyValue("contacts_user_created_address_book_list");
		int result = 0;

		try {
			Tools.setDriverDefaultValues();
			result = Base.base.getDriver().findElements(By.xpath(contacts_user_created_address_book_list)).size();

			Logging.info("The number of user created Address Book is: " + result);
		} catch (WebDriverException ex) {
			Logging.error("Could not calculate the number of user created Address Book");
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>:Young Zhao <br>
	 * <b>Date</b>: Jun 24, 2015 <br>
	 * <b>Description</b><br>
	 * Clean up all the user created address books in the address book list, except
	 * "Default" and "auto-complete" books.
	 * 
	 */
	public void cleanupAllUserCreatedAddressBook() {

		Logging.info("cleaning up all the user created Address Book On Conatct Pannel");

		String contacts_addressbook_hoverpanel_icon_delete = PropertyHelper.coreContacts
				.getPropertyValue("contacts_addressbook_hoverpanel_icon_delete");
		String contacts_user_created_address_book_list = PropertyHelper.coreContacts
				.getPropertyValue("contacts_user_created_address_book_list");

		int noOfAddressBook = this.calculateUserCreatedAddressBookNumber();

		try {
			for (int i = noOfAddressBook; i > 0; i--) {
				Logging.info("Deleting address book, index: " + i);
				Tools.moveFocusToElementByXpath(contacts_user_created_address_book_list);
				Base.base.getDriver().findElement(By.xpath(contacts_addressbook_hoverpanel_icon_delete)).click();

				this.clickButtonOnMessageBox("Yes");
				this.waitForLoadMaskDismissed();
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not clean up all the user created Address Book On Conatct Pannel");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>:Young Zhao <br>
	 * <b>Date</b>: Jun 29, 2015 <br>
	 * <b>Description</b><br>
	 * Click on a contacts address book
	 * 
	 */
	public void clickOnAddressBook(String bookname) {
		Logging.info("Clicking on address book : " + bookname);

		String contact_list_folder = PropertyHelper.coreContacts.getPropertyValue("contacts_list_folder")
				.replace("+variable+", bookname);

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(contact_list_folder)).click();
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on address book in contact list: " + contact_list_folder);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>:Young Zhao <br>
	 * <b>Date</b>: Jul 28, 2015 <br>
	 * <b>Description</b><br>
	 * Click on "Default" address book
	 * 
	 */
	public void clickOnDefaultAddressBook() {
		Logging.info("Clicking on DEFAULT address book");

		String contacts_default_addressbook = PropertyHelper.coreContacts
				.getPropertyValue("contacts_default_addressbook");

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(contacts_default_addressbook)).click();
			this.waitForLoadMaskDismissed();

		} catch (WebDriverException ex) {
			Logging.error("Could not click on DEFAULT address book in contact list");
			throw ex;
		}
	}

	public void clickOnDefautltAddbook() {
		Logging.info("Clicking on DEFAULT address book");

		String contacts_default_addressbook = PropertyHelper.coreContacts
				.getPropertyValue("contacts_default_addbook");

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(contacts_default_addressbook)).click();
			this.waitForLoadMaskDismissed();

		} catch (WebDriverException ex) {
			Logging.error("Could not click on DEFAULT address book in contact list");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>:Young Zhao <br>
	 * <b>Date</b>: Jun 29, 2015 <br>
	 * <b>Description</b><br>
	 * enter the value in contacts search text box
	 * 
	 */
	public void enterValueInSearchTextBox(String searchValue) {

		Logging.info("Entering value in search text box: " + searchValue);
		try {
			String contacts_search_text_box = PropertyHelper.coreContacts.getPropertyValue("contacts_search_text_box");

			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(contacts_search_text_box)).clear();
			Base.base.getDriver().findElement(By.xpath(contacts_search_text_box)).sendKeys(searchValue);
			this.waitForLoadMaskDismissed();

		} catch (WebDriverException ex) {
			Logging.error("Could not enter text in search text box: " + searchValue);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jun 29, 2015<br>
	 * <b>Description</b><br>
	 * Calculate the number of Contacts Search results
	 * 
	 */
	public int calculateContactsSearchResults() {
		Logging.info("Calculating the number of contacts search results");
		String contacts_search_result_list = PropertyHelper.coreContacts
				.getPropertyValue("contacts_search_result_list");
		int result = 0;

		try {
			Tools.setDriverDefaultValues();
			result = Base.base.getDriver().findElements(By.xpath(contacts_search_result_list)).size();

			Logging.info("The number of Address Book is: " + result);
		} catch (WebDriverException ex) {
			Logging.error("Could not calculate the number of Address Book");
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jun 29, 2015<br>
	 * <b>Description</b><br>
	 * click on the specified contacts toolbar button.
	 * 
	 */
	public void clickContactsToolBarButton(String button) {
		Logging.info("Clicking on contacts toolbar button: " + button);

		button = button.trim();
		String contacts_toolbar_button = PropertyHelper.coreContacts.getPropertyValue("contacts_toolbar_button_in_more_menu")
				.replace("+variable+", button);
		String contacts_toolbar_more_button = PropertyHelper.coreContacts.getPropertyValue("contacts_toolbar_button")
				.replace("+variable+", "More");
		String contacts_toolbar_more_button_addContact = PropertyHelper.coreContacts.getPropertyValue("contacts_Add")
				.replace("+variable+", "Add");
		String contacts_toolbar_new_button= PropertyHelper.coreContacts.getPropertyValue("contacts_toolbar_button")
				.replace("+variable+", "New");
		String contacts_toolbar_button_in_more_menu = PropertyHelper.coreContacts
				.getPropertyValue("contacts_toolbar_button_in_more_menu").replace("+variable+", button);
		String contacts_tooltip_context_mask = PropertyHelper.coreContacts
				.getPropertyValue("contacts_tooltip_context_mask");
		String contacts_toolbar_button_new_menu = PropertyHelper.coreContacts
				.getPropertyValue("contacts_toolbar_button_new_menu");

		try {
			Tools.setDriverDefaultValues();
			String contacts_toolbar_active_button;
			// Scroll toolbar More button into view.
			if (button.equals("Add contact") || button.equals("Add group")) {
				Tools.scrollElementIntoViewByXpath(contacts_toolbar_new_button);
				contacts_toolbar_active_button = contacts_toolbar_new_button;
			} else {
				Tools.scrollElementIntoViewByXpath(contacts_toolbar_more_button);
				contacts_toolbar_active_button = contacts_toolbar_more_button;
			}

			boolean buttonDisplayed;
			try {
				// To speed up hidden button searching.
				Base.base.getDriver().manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
				buttonDisplayed = Base.base.getDriver().findElement(By.xpath(contacts_toolbar_button)).isDisplayed();
			} catch (NoSuchElementException ex) {
				buttonDisplayed = false;
			} finally {
				// Set driver timeout to default.
				Tools.setDriverDefaultValues();
			}

			if (buttonDisplayed) {
				Base.base.getDriver().findElement(By.xpath(contacts_toolbar_button)).click();
			} else {
				Base.base.getDriver().findElement(By.xpath(contacts_toolbar_active_button)).click();
				Tools.scrollElementIntoViewByXpath(contacts_toolbar_button_in_more_menu);
				// wait for the context tool tip to go away before clicking
				//Tools.waitUntilElementNotDisplayedByXpath(contacts_tooltip_context_mask, this.timeoutMedium);
				Base.base.getDriver().findElement(By.xpath(contacts_toolbar_button_in_more_menu)).click();
			}
			this.waitForLoadMaskDismissed();

		} catch (WebDriverException ex) {
			Logging.error("Could not click on contacts toolbar button: " + button);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Oct 13, 2014<br>
	 * <b>Description</b><br>
	 * verify if contacts list is empty
	 * 
	 */
	public boolean verifyContactsListIsEmpty() {
		Logging.info("Verifying if contacts list is empty");

		String contacts_empty_list_texts = PropertyHelper.coreContacts.getPropertyValue("contacts_empty_list_texts");
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(contacts_empty_list_texts));
			result = true;

		} catch (WebDriverException ex) {
			// Do nothing.
		}

		Logging.info("Contacts list is empty: " + result);
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jun 30, 2015<br>
	 * <b>Description</b><br>
	 * click on "Select all" button in contact list.
	 * 
	 */
	public void selectAllContactInList() {
		Logging.info("Selecting all contacts in contact list");

		String contacts_select_all_contacts = PropertyHelper.coreContacts
				.getPropertyValue("contacts_select_all_contacts");

		try {
			Tools.setDriverDefaultValues();

			boolean enabled = Base.base.getDriver().findElement(By.xpath(contacts_select_all_contacts))
					.getAttribute("class").contains("cb-checked");

			if (!enabled) {
				Base.base.getDriver().findElement(By.xpath(contacts_select_all_contacts)).click();
			}

		} catch (WebDriverException ex) {
			Logging.error("Could not select all contacts in contact list");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Dec 04, 2014<br>
	 * <b>Description</b><br>
	 * remove all contacts from list by selecting all and then click delete
	 * 
	 */
	public void removeAllContactsInList() {
		Logging.info("Removing all contacts from list.");

		if (this.verifyContactsListIsEmpty()) {
			return;
		}

		try {
			for (int i = 0; i < 5; i++) {
				this.selectAllContactInList();
				this.clickContactsToolBarButton("Delete");
				this.clickButtonOnMessageBox("Yes");

				this.waitForLoadMaskDismissed();

				if (this.verifyContactsListIsEmpty()) {
					Logging.info("All contacts have been removed from list");
					break;
				} else if (i == 4) {
					Logging.info("Fail to remove all contacts from list; maximum attempt reached");
				}
			}

		} catch (WebDriverException ex) {
			Logging.error("Could not remove all contacts from list");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jun 30, 2015<br>
	 * <b>Description</b><br>
	 * clean up all contacts
	 * 
	 */
	public void cleanupAllContacts() {
		Logging.info("Cleaning up all contacts in contacts page...");

		try {
			// remove all custom addressbooks
			this.cleanupAllUserCreatedAddressBook();

			// remove all contacts in Default book
			this.clickOnDefaultAddressBook();
			this.removeAllContactsInList();

			// remove all contacts in "Auto-complete"
			this.clickOnAddressBook("Auto-complete");
			this.removeAllContactsInList();
		} catch (WebDriverException ex) {
			Logging.error("Could not cleanup all contacts in contact list");
			throw ex;
		}

	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jun 30, 2015<br>
	 * <b>Description</b><br>
	 * enter "Name" information for a contact
	 * 
	 * <b>Parameters</b><br>
	 * String fieldname - field name. One of the following buttons should be
	 * provided: - firstname - lastname - middlename - nickname
	 */
	public void enterContactNameInfo(String fieldname, String data) {
		Logging.info("Filling data in NAME field :  " + data);

		String contacts_field_for_name = PropertyHelper.coreContacts.getPropertyValue("contacts_field_for_name")
				.replace("+variable+", fieldname);

		try {
			Tools.setDriverDefaultValues();

			Tools.scrollElementIntoViewByXpath(contacts_field_for_name);
			Base.base.getDriver().findElement(By.xpath(contacts_field_for_name)).clear();
			Base.base.getDriver().findElement(By.xpath(contacts_field_for_name)).sendKeys(data);

		} catch (WebDriverException ex) {
			Logging.error("Could not enter text in : " + fieldname + " field");
			throw ex;
		}
	}

	/**
	 * enter main information for a contact
	 * 
	 * @param fieldname
	 *            -- value can be "Email","Mobile", "Phone", "Address" ,"Personal"
	 * @param data
	 *            - the field value to be input
	 * @author Young Zhao
	 * @create June 30 2015
	 */
	public void enterContactMainInfo(String fieldname, String data) {
		Logging.info("Filing data in MAIN field :  " + data);

		String contacts_field_main = PropertyHelper.coreContacts.getPropertyValue("contacts_field_main")
				.replace("+variable+", fieldname);

		try {
			Tools.setDriverDefaultValues();

			Tools.scrollElementIntoViewByXpath(contacts_field_main);
			Base.base.getDriver().findElement(By.xpath(contacts_field_main)).clear();
			Base.base.getDriver().findElement(By.xpath(contacts_field_main)).sendKeys(data);

		} catch (WebDriverException ex) {
			Logging.error("Could not enter text in : " + fieldname + " field");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Sept 19 , 2016<br>
	 * <b>Description</b><br>
	 * click input for birthday or anniversary for a contact <b>parameter:
	 */
	public void clickContactPersonalDate() {
		Logging.info("Inputing contact's personal date ");

		String contact_field_for_pesonal_date = PropertyHelper.coreContacts
				.getPropertyValue("contact_field_for_pesonal_date");

		try {
			Tools.setDriverDefaultValues();

			Tools.scrollElementIntoViewByXpath(contact_field_for_pesonal_date);
			Base.base.getDriver().findElement(By.xpath(contact_field_for_pesonal_date)).click();

		} catch (WebDriverException ex) {
			Logging.error("element not found in clickContactPersonalDate");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>:Oct 10, 2016<br>
	 * <b>Description</b><br>
	 * click Merge or Cancel button when merging contacts <b>parameter: buttonName
	 * -- can be "Merge" or "Cancle"
	 * 
	 */
	public void clickContactMergeButton(String buttonName) {
		Logging.info("Clicking " + buttonName + " button on contact merge page ");

		String contact_merge_button = PropertyHelper.coreContacts.getPropertyValue("contact_merge_button")
				.replace("+variable+", buttonName);

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(contact_merge_button)).click();

		} catch (WebDriverException ex) {
			Logging.error("Cound not click " + buttonName + " button in merge view");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 01, 2015<br>
	 * <b>Description</b><br>
	 * click "Save" button in contact edit window
	 * 
	 */
	public void clickContactSave() {
		Logging.info("Clicking Save button on Add contact window");

		String contact_add_save = PropertyHelper.coreContacts.getPropertyValue("contact_add_save");
		//String quicktips_text1 = PropertyHelper.pageObjectBaseFile.getPropertyValue("quicktips_text_show_advanced_search_pane");
		String contacts_detail_view_header_bar = PropertyHelper.coreContacts
				.getPropertyValue("contacts_detail_view_header_bar");

		try {
			Tools.setDriverDefaultValues();

			// Workaround code: waits for quick tips bar "Click to show advanced
			// search pane"
			// dismissed before clicking Save button.
			//this.waitForQuicktipsDismissed(quicktips_text1);
			Tools.moveFocusToElementByXpath(contacts_detail_view_header_bar);

			Base.base.getDriver().findElement(By.xpath(contact_add_save)).click();
			this.waitForLoadMaskDismissed();

		} catch (WebDriverException ex) {
			Logging.error("Could not click on Save button on add contact window");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 02, 2015<br>
	 * <b>Description</b><br>
	 * verify a contact display in contact list or not
	 * 
	 * <b>Parameters</b><br>
	 * String firstname - the first name of contact
	 */
	public boolean verifyContactInList(String firstname) {

		Logging.info("Verifying contact dispay in contact list");

		String contact_in_list = PropertyHelper.coreContacts.getPropertyValue("contact_in_list").replace("+variable+",
				firstname);

		boolean result = false;

		try {
			Tools.setDriverDefaultValues();

			// try to find the contact
			Base.base.getDriver().findElement(By.xpath(contact_in_list));

			result = true;

		} catch (WebDriverException ex) {
			Logging.info("Contact is not found in contact list");
			// throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 06, 2015<br>
	 * <b>Description</b><br>
	 * verify a contact display in contact list or not
	 * 
	 * <b>Parameters</b><br>
	 * String email - the email address of contact
	 */
	public boolean verifyContactInListByEmail(String email) {

		Logging.info("Verifying contact dispay in contact list");

		String contact_in_list_email = PropertyHelper.coreContacts.getPropertyValue("contact_in_list_email")
				.replace("+variable+", email);
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();

			// try to find the contact
			Base.base.getDriver().findElement(By.xpath(contact_in_list_email));
			Logging.info("Contact: " + email + " is found in contact list");
			result = true;

		} catch (WebDriverException ex) {
			Logging.info("Contact: " + email + " is not found in contact list");
			// throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 02, 2015<br>
	 * <b>Description</b><br>
	 * click on a contact actions icon by its first name
	 * 
	 * <b>Parameters</b><br>
	 * String firstname - the firstname of contact String icon - icon to be clicked
	 * on. One of the following buttons should be provided: - compose - invite -
	 * delete
	 */
	public void clickOnContactActionsIconByFirstName(String firstname, String icon) {
		Logging.info("Clicking on actions icon: " + icon + " on contact: " + firstname);

		String contact_in_list = PropertyHelper.coreContacts.getPropertyValue("contact_in_list").replace("+variable+",
				firstname);
		String contacts_actions_icon = PropertyHelper.coreContacts.getPropertyValue("contacts_actions_icon")
				.replace("+variable+", icon);

		try {
			Tools.setDriverDefaultValues();

			// Move focus to the contact.
			Tools.moveFocusToElementByXpath(contact_in_list);

			Base.base.getDriver().findElement(By.xpath(contacts_actions_icon)).click();

		} catch (WebDriverException ex) {
			Logging.error("Could not click on actions icon: " + icon + " with contact: " + firstname);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 06, 2015<br>
	 * <b>Description</b><br>
	 * click on a contact actions icon by its first name
	 * 
	 * <b>Parameters</b><br>
	 * String email - the email address of contact String icon - icon to be clicked
	 * on. One of the following buttons should be provided: - compose - invite -
	 * delete
	 */


	public void clickOnContactsBtn(String email, String icon) {
		Logging.info("Clicking on actions icon: " + icon + " on contact: " + email);

		String contact_list = PropertyHelper.coreContacts.getPropertyValue("contacts_list")
				.replace("+variable+", email);
		String contacts_icon = PropertyHelper.coreContacts.getPropertyValue("contact_in_list_email")
				.replace("+variable+", email);

		try {
			Tools.setDriverDefaultValues();

			// Move focus to the contact.
			Tools.moveFocusToElementByXpath(contact_list);

			// click on action icon
			Base.base.getDriver().findElement(By.xpath(contacts_icon)).click();

		} catch (WebDriverException ex) {
			Logging.error("Could not click on actions icon: " + icon + " with contact: " + email);
			throw ex;
		}
	}



	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 02, 2015<br>
	 * <b>Description</b><br>
	 * delete a contact in list by its first name
	 * 
	 * <b>Parameters</b><br>
	 * String firstname - the firstname of contact
	 */
	public void deleteContactInList(String firstname) {
		Logging.info("Deleting contact: " + firstname + " in Contacts list");

		try {
			Tools.setDriverDefaultValues();

			// delete a contact in list
			this.clickOnContactActionsIconByFirstName(firstname, "delete");
			this.clickButtonOnMessageBox("Yes");
			this.waitForLoadMaskDismissed();

		} catch (WebDriverException ex) {
			Logging.error("Could not delete the contact: " + firstname);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 02, 2015<br>
	 * <b>Description</b><br>
	 * click on toolbar button on Contact Preview window
	 * 
	 * <b>Parameters</b><br>
	 * String button - button name. One of the following buttons should be provided:
	 * - Send message - Edit - Delete - Add to group - Send as vCard - Invite to
	 * event
	 */
	public void clickContactDetailViewToolbarButton(String button) {

		Logging.info("Clicking button on contact detail view : " + button);

		String replacement = null;

		switch (button.toLowerCase()) {
		case "send message":
			replacement = "compose";
			break;

		case "edit":
			replacement = "edit";
			break;

		case "delete":
			replacement = "delete";
			break;

		case "add to group":
			replacement = "addtogroup";
			break;

		case "send as vcard":
			replacement = "sendvcard";
			break;

		case "invite to event":
			replacement = "inviteevent";
			break;

		default:
			throw new IllegalArgumentException("Invalid button name for contact detail view toolbar");
		}

		String contacts_detail_view_header_bar = PropertyHelper.coreContacts
				.getPropertyValue("contacts_detail_view_header_bar");
		String contacts_detail_view_toolbar_button = PropertyHelper.coreContacts
				.getPropertyValue("contacts_detail_view_toolbar_button").replace("+variable+", replacement);
		String contacts_detail_view_toolbar_more_button = PropertyHelper.coreContacts
				.getPropertyValue("contacts_detail_view_toolbar_more_button");
		String contacts_detail_view_toolbar_menuitem = PropertyHelper.coreContacts
				.getPropertyValue("contacts_detail_view_toolbar_menuitem").replace("+variable+", replacement);

		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(contacts_detail_view_header_bar);

			boolean buttonVisible = Base.base.getDriver().findElement(By.xpath(contacts_detail_view_toolbar_button))
					.isDisplayed();

			if (buttonVisible) {
				Base.base.getDriver().findElement(By.xpath(contacts_detail_view_toolbar_button)).click();
			} else {
				// Open 'More' menu then click on item.
				Base.base.getDriver().findElement(By.xpath(contacts_detail_view_toolbar_more_button)).click();
				Base.base.getDriver().findElement(By.xpath(contacts_detail_view_toolbar_menuitem)).click();
			}
			this.waitForLoadMaskDismissed();

		} catch (WebDriverException ex) {
			Logging.error("Could not click on button on contact detail view: " + button);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 03, 2015<br>
	 * <b>Description</b><br>
	 * click a contact display in contact list
	 * 
	 * <b>Parameters</b><br>
	 * String firstname - the first name of contact
	 */
	public void clickContactInListByName(String firstname) {

		Logging.info("Clicking on contact " + firstname + " in contact list");

		String contact_in_list = PropertyHelper.coreContacts.getPropertyValue("contact_in_list").replace("+variable+",
				firstname);

		try {
			Tools.setDriverDefaultValues();

			this.waitForLoadMaskDismissed();

			// try to find the contact
			Base.base.getDriver().findElement(By.xpath(contact_in_list)).click();

		} catch (WebDriverException ex) {
			Logging.info("Contact is not found in contact list");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 03, 2015<br>
	 * <b>Description</b><br>
	 * click a contact display in contact list
	 * 
	 * <b>Parameters</b><br>
	 * String firstname - the first name of contact
	 */
	public void clickContactInListByEmail(String email) {

		Logging.info("Clicking on contact in contact list");

		String contact_in_list_email = PropertyHelper.coreContacts.getPropertyValue("contact_in_list_email")
				.replace("+variable+", email);

		try {
			Tools.setDriverDefaultValues();

			this.waitForLoadMaskDismissed();

			// try to find the contact
			Base.base.getDriver().findElement(By.xpath(contact_in_list_email)).click();

		} catch (WebDriverException ex) {
			Logging.info("Contact is not found in contact list");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Oct 10, 2016<br>
	 * <b>Description</b><br>
	 * verify "Name" information input for a contact
	 * 
	 * <b>Parameters</b><br>
	 * String fieldname - field name. One of the following buttons should be
	 * provided: - firstname - lastname - middlename - nickname
	 */
	public boolean verifyContactNameInfo(String fieldname, String data) {
		Logging.info("Verifying data in NAME field :  " + data);

		String contacts_field_for_name = PropertyHelper.coreContacts.getPropertyValue("contacts_field_for_name")
				.replace("+variable+", fieldname);
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();

			Tools.scrollElementIntoViewByXpath(contacts_field_for_name);
			String nameString = Base.base.getDriver().findElement(By.xpath(contacts_field_for_name))
					.getAttribute("value");
			if (nameString.equals(data)) {
				result = true;
			}

		} catch (WebDriverException ex) {
			Logging.error("Could not enter text in : " + fieldname + " field");
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Oct 10, 2016<br>
	 * <b>Description</b><br>
	 * verify main information for a contact
	 * 
	 */
	public boolean verifyContactMainInfo(String fieldname, String data) {
		Logging.info("Filing data in MAIN field :  " + data);

		String contacts_field_main = PropertyHelper.coreContacts.getPropertyValue("contacts_field_main")
				.replace("+variable+", fieldname);
		boolean result = false;
		try {
			Tools.setDriverDefaultValues();

			Tools.scrollElementIntoViewByXpath(contacts_field_main);
			String nmainInfo = Base.base.getDriver().findElement(By.xpath(contacts_field_main)).getAttribute("value");
			if (nmainInfo.equals(data)) {
				result = true;
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not enter text in : " + fieldname + " field");
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 03, 2015<br>
	 * <b>Description</b><br>
	 * verify address book name exist or not
	 * 
	 * <b>Parameters</b><br>
	 * String bookname - the addressbook name
	 */
	public boolean verifyAddressBookName(String bookname) {
		Logging.info("Verifying address book exists in list: " + bookname);

		boolean result = false;
		String contacts_addressbook_name = PropertyHelper.coreContacts.getPropertyValue("contacts_addressbook_name")
				.replace("+variable+", bookname);

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(contacts_addressbook_name)).getText();
			Logging.info("Address book found in list: " + bookname);
			result = true;
		} catch (NoSuchElementException ex) {
			Logging.error("Could not find address book: " + bookname);
		}
		return result;
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Oct 10, 2016<br>
	 * <b>Description</b><br>
	 * verify if contact's birthday has been correctly saved
	 * 
	 * <b>Parameters</b><br>
	 * LocalDate birthday - contact's expect birthday, String dateFormatterURI --the
	 * date formatter
	 */
	public boolean verifyContactBirthday(LocalDate birthday, String dateFormatterURI) {
		Logging.info("Verifying if contact's birthday has been correctly saved. birthday:" + birthday);

		boolean result = false;
		String contacts_birthday_value = PropertyHelper.coreContacts.getPropertyValue("contacts_birthday_value");
		try {
			Tools.setDriverDefaultValues();
			String sBirthday = Base.base.getDriver().findElement(By.xpath(contacts_birthday_value)).getText();
			Logging.info("The actual birthday is " + sBirthday);
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormatterURI);
			String expectBirthday = birthday.format(formatter);
			if (sBirthday.indexOf(expectBirthday) == -1) {
				return false;
			}
			result = true;
		} catch (NoSuchElementException ex) {
			Logging.error("Could not verify contact's birthday");
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jun 24, 2015<br>
	 * <b>Description</b><br>
	 * Edit an exist Address Book
	 * 
	 */
	public void editAdderssBook(String bookname1, String bookname2) {
		Logging.info("Editing AddressBook name from " + bookname1 + " to " + bookname2);

		String contacts_addressbook_rename_input = PropertyHelper.coreContacts
				.getPropertyValue("contacts_addressbook_rename_input");

		try {
			Tools.setDriverDefaultValues();

			this.clickOnAddressBook(bookname1);
			this.clickContactsToolBarButton("Edit address book");

			Logging.info("Clearing texts from address book name input");
			Base.base.getDriver().findElement(By.xpath(contacts_addressbook_rename_input)).clear();

			Logging.info("Entering new address book name: " + bookname2);
			Base.base.getDriver().findElement(By.xpath(contacts_addressbook_rename_input)).sendKeys(bookname2);
			Base.base.getDriver().findElement(By.xpath(contacts_addressbook_rename_input)).sendKeys(Keys.ENTER);

			this.waitForLoadMaskDismissed();

		} catch (WebDriverException ex) {
			Logging.error("Could not edit AddressBook " + bookname1);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jun 24, 2015<br>
	 * <b>Description</b><br>
	 * Delete an exist Address Book
	 * 
	 */
	public void deleteAdderssBook(String bookname) {
		Logging.info("Deleting an existing AddressBook name " + bookname);

		try {
			Tools.setDriverDefaultValues();

			this.clickOnAddressBook(bookname);
			this.clickContactsToolBarButton("Delete address book");
			this.clickButtonOnMessageBox("Yes");
			this.waitForLoadMaskDismissed();

		} catch (WebDriverException ex) {
			Logging.error("Could not edit AddressBook " + bookname);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 08, 2015<br>
	 * <b>Description</b><br>
	 * click on "Move to" button in toolbar "More", to move contact to another
	 * addressbook.
	 * 
	 * <b>Parameters</b><br>
	 * String bookname - the target addressbook name
	 */
	public void clickOnMoveTo(String bookname) {
		Logging.info("Clicking on Move To button to move contact into: " + bookname);

		bookname = bookname.trim();
		String contacts_toolbar_more_button = PropertyHelper.coreContacts.getPropertyValue("contacts_toolbar_button")
				.replace("+variable+", "More");
		String contacts_move_button_in_more_menu = PropertyHelper.coreContacts
				.getPropertyValue("contacts_toolbar_button_in_more_menu").replace("+variable+", "Move to");
		String contacts_target_addressbook_in_more_menu = PropertyHelper.coreContacts
				.getPropertyValue("contacts_toolbar_button_in_more_menu").replace("+variable+", bookname);
		String contacts_tooltip_context_mask = PropertyHelper.coreContacts
				.getPropertyValue("contacts_tooltip_context_mask");

		try {
			Tools.setDriverDefaultValues();
			// Scroll toolbar More button into view.
			Tools.scrollElementIntoViewByXpath(contacts_toolbar_more_button);
			Base.base.getDriver().findElement(By.xpath(contacts_toolbar_more_button)).click();

			// Click on "Move to" button
			Tools.waitUntilElementNotDisplayedByXpath(contacts_tooltip_context_mask, this.timeoutMedium);
			Tools.scrollElementIntoViewByXpath(contacts_move_button_in_more_menu);
			Tools.moveFocusToElementByXpath(contacts_move_button_in_more_menu);

			// click on target address book
			Base.base.getDriver().findElement(By.xpath(contacts_target_addressbook_in_more_menu)).click();
			this.waitForLoadMaskDismissed();

		} catch (WebDriverException ex) {
			Logging.error("Could not move the contact into: " + bookname);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 09, 2015<br>
	 * <b>Description</b><br>
	 * Click on the email link in contact preview window
	 * 
	 * <b>Parameters</b><br>
	 * String emailID - email ID of contact
	 */
	public void clickContactEmailLinkInContactPreviewWindow(String emailID) {

		Logging.info("Clicking on the email link in contact preview window.");

		String contacts_email_link = PropertyHelper.coreContacts.getPropertyValue("contacts_email_link")
				.replace("+variable+", emailID);

		try {
			Tools.waitUntilElementDisplayedByXpath(contacts_email_link, this.timeoutMedium);
			Base.base.getDriver().findElement(By.xpath(contacts_email_link)).click();

		} catch (WebDriverException ex) {
			Logging.error("Could not click on email link on contact preview window");
			throw ex;
		}
	}

	/**
	 * @Method Name: verifyEmailExisting
	 * @Author: Vivian Xie
	 * @Created On: 09/14/2016
	 * @Description: Assert if the eamil address exists on the Add New Contact Page
	 * @parameter1: String emailAddress -- the expected email address
	 */
	public boolean verifyEmailExisting(String emailAddress) {
		Logging.info("Asserting if the eamil address" + emailAddress + " exists on the Add New Contact Page");

		String contact_email_address_input = PropertyHelper.coreContacts.getPropertyValue("contact_email_address_input")
				.replace("+variable+", emailAddress);
		boolean result = false;

		try {
			Tools.waitUntilElementDisplayedByXpath(contact_email_address_input, this.timeoutMedium);
			Logging.info("EmailAddress " + emailAddress + "is on the page");
			result = true;

		} catch (WebDriverException ex) {
			Logging.error("Could not click on email link on contact preview window");
			throw ex;
		}
		return result;
	}

	/**
	 * @Method Name: verifyContacthasBeenAdded
	 * @Author: Vivian Xie
	 * @Created On: 09/14/2016
	 * @Description: Assert if the eamil address exists on the Add New Contact Page
	 * @parameter1: String firstName - contact's expected firstname String
	 *              emailAddress--contact's expected emailAddress
	 */
	public boolean verifyContacthasBeenAdded(String firstName, String emailAddress) {
		Logging.info(
				"Asserting if user " + firstName + " email " + emailAddress + "exists on the Add New Contact Page");

		String contact_first_name = PropertyHelper.coreContacts.getPropertyValue("contact_first_name")
				.replace("+variable+", firstName);
		String contact_email_address = PropertyHelper.coreContacts.getPropertyValue("contact_email_address")
				.replace("+variable+", emailAddress);
		boolean result = false;
		try {
			Tools.waitUntilElementDisplayedByXpath(contact_first_name, this.timeoutMedium);
			Logging.info("User " + firstName + " is on the page");
			Tools.waitUntilElementDisplayedByXpath(contact_email_address, this.timeoutMedium);
			Logging.info("EmailAddress " + emailAddress + " is on the page");
			result = true;

		} catch (WebDriverException ex) {
			Logging.error("Do not found the contact");
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 11, 2016<br>
	 * <b>Description</b><br>
	 * Click save/cancel button in photo avatar view
	 * 
	 * <b>Parameters</b><br>
	 * String buttonName - the button name in avatar editor view
	 */
	public void clickChangePhotoButton(String buttonName) {
		String contacts_photo_change_action_button = null;
		if (buttonName.equalsIgnoreCase(DELETE)) {
			contacts_photo_change_action_button = PropertyHelper.coreContacts
					.getPropertyValue("contact_photo_delete_button");
		} else
			contacts_photo_change_action_button = PropertyHelper.coreContacts
					.getPropertyValue("contact_photo_change_button");
		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(contacts_photo_change_action_button);
			Tools.moveFocusToElementByXpath(contacts_photo_change_action_button);
			Tools.waitFor(10, TimeUnit.SECONDS);
			Base.base.getDriver().findElement(By.xpath(contacts_photo_change_action_button)).click();
			Tools.waitFor(5, TimeUnit.SECONDS);
		} catch (WebDriverException ex) {
			ex.printStackTrace();
		}
		Logging.info("Click " + buttonName + " photo button");
	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 11, 2016<br>
	 * <b>Description</b><br>
	 * Click save/cancel button in photo avatar view
	 * 
	 * <b>Parameters</b><br>
	 * String buttonName - the button name in avatar editor view
	 */
	public void clickContactPhotoEditorActionButton(String buttonName) {
		String contacts_edit_avatar_cancel = PropertyHelper.coreContacts.getPropertyValue("contact_edit_avatar_cancel");
		String contacts_edit_avatar_save = PropertyHelper.coreContacts.getPropertyValue("contact_edit_avatar_save");
		String contacts_upload_photo_progress_bar = PropertyHelper.coreContacts
				.getPropertyValue("contact_upload_photo_progress_bar");
		String contacts_edit_avatar_header = PropertyHelper.coreContacts.getPropertyValue("contact_edit_avatar_header");
		try {
			// waiting for the avatar view displayed
			Tools.waitUntilElementDisplayedByXpath(contacts_edit_avatar_header, 10);
			Tools.setDriverDefaultValues();
			if (buttonName.equalsIgnoreCase(SAVE))
				Tools.clickOnElementByXpath(contacts_edit_avatar_save);
			else if (buttonName.equalsIgnoreCase(CANCEL))
				Tools.clickOnElementByXpath(contacts_edit_avatar_cancel);

			// waiting for the upload bar disappear
			Tools.waitUntilElementNotDisplayedByXpath(contacts_upload_photo_progress_bar, 10);
		} catch (WebDriverException ex) {
			throw ex;
		}
		Logging.info("Click " + buttonName + " button in contact avatar editor view ");

	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Nov 11, 2016<br>
	 * <b>Description</b><br>
	 * verify if the contact photo section contains picture
	 * 
	 * @return true - the contact photo is a user customersized photo, false - the
	 *         contact photo is a default photo
	 */
	public boolean verifyContactPhoto() {

		boolean result = false;
		String contacts_addressbook_name = PropertyHelper.coreContacts.getPropertyValue("contact_photo_section");

		try {
			Tools.setDriverDefaultValues();
			String elementSrc = Base.base.getDriver().findElement(By.xpath(contacts_addressbook_name))
					.getAttribute("src");
			if (elementSrc.startsWith("http") && elementSrc.contains("%"))
				result = true;
			else if (elementSrc.contains("resources") && elementSrc.contains("icon_blank_contact"))
				result = false;
		} catch (NoSuchElementException ex) {
			Logging.error("Could not find element of contact photo: ");
		}
		Logging.info("Verifying if the contact photo section contains picture " + result);

		return result;

	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 12, 2016<br>
	 * <b>Description</b><br>
	 * Zoom in / Zoom out avatar picture
	 * 
	 * <b>Parameters</b><br>
	 * String zoomSize - zoom in / zoom out
	 */
	public void moveContactPhotoEditorActionButton(String zoomSize) {
		String contact_edit_avatar_zoom_button = PropertyHelper.coreContacts
				.getPropertyValue("contact_edit_avatar_zoom_button");
		try {
			Tools.setDriverDefaultValues();
			if (zoomSize.equalsIgnoreCase("zoomout"))
				Tools.dragAndDropSourceToTargetByXpath(contact_edit_avatar_zoom_button, 2000, 0);
			else if (zoomSize.equalsIgnoreCase("zoomin"))
				Tools.dragAndDropSourceToTargetByXpath(contact_edit_avatar_zoom_button, -2000, 0);
		} catch (WebDriverException ex) {
			throw ex;
		}

	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 12, 2016<br>
	 * <b>Description</b><br>
	 * Verify the Zoom in / Zoom out avatar picture size and controller
	 * 
	 * <b>Parameters</b><br>
	 * String zoomSize - zoom in / zoom out
	 */
	public boolean verifyTheAvatarChangesSize(String zoomSize) {
		boolean result = false;
		String contact_avatar_editor_track = PropertyHelper.coreContacts
				.getPropertyValue("contact_avatar_editor_track");
		String contact_avatar_editor_controller = PropertyHelper.coreContacts
				.getPropertyValue("contact_avatar_editor_controller");

		try {
			Tools.setDriverDefaultValues();
			String sizeChanges = Base.base.getDriver().findElement(By.xpath(contact_avatar_editor_track))
					.getAttribute("style");
			String controllerPosition = Base.base.getDriver().findElement(By.xpath(contact_avatar_editor_controller))
					.getAttribute("style");
			if (zoomSize.equalsIgnoreCase("zoomout"))
				result = sizeChanges.contains("width: 100%") && controllerPosition.contains("left: 100%");
			else if (zoomSize.equalsIgnoreCase("zoomin"))
				result = sizeChanges.contains("width: 0%") && controllerPosition.contains("left: 0%");
		} catch (NoSuchElementException ex) {
			ex.printStackTrace();
			throw ex;
		}
		Logging.info("Verifying if the contact avatar change size to " + zoomSize + " sucessfuly " + result);
		return result;
	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 14, 2016<br>
	 * <b>Description</b><br>
	 * Click the photo of the contact to add photo
	 */
	public void clickContactPhoto() {
		String contact_photo_edit_section = PropertyHelper.coreContacts.getPropertyValue("contact_photo_edit_section");
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(contact_photo_edit_section)).click();
			/*
			 * Robotil rb = new Robotil(Base.base.getIpOfNode(), 6666);
			 * rb.pressKey(KeyEvent.VK_SHIFT); rb.pressKey(KeyEvent.VK_TAB);
			 * rb.releaseKey(KeyEvent.VK_TAB); rb.releaseKey(KeyEvent.VK_SHIFT);
			 * rb.pressKey(KeyEvent.VK_RIGHT); Thread.sleep(100);
			 * rb.pressKey(KeyEvent.VK_ENTER); Thread.sleep(100);
			 */
		} catch (WebDriverException ex) {
			ex.printStackTrace();
		}

		Logging.info("Click the photo section in edit contact view");

	}

	/**
	 * @Method Name: clickSaveGroupButton
	 * @Author: Vivian Xie
	 * @Created On: 11/14/2016
	 * @Description: Click Save or Cancel button when adding a group or adding an
	 *               address book
	 */
	public void clickSaveOrCancelGroupButton(String buttonName) {
		Logging.info("Clicking " + buttonName + " button when creating a group/adding an addess book");

		String contact_save_or_cancel_group_button = PropertyHelper.coreContacts
				.getPropertyValue("contact_save_or_cancel_group_button").replace("+variable+", buttonName);
		Logging.info(contact_save_or_cancel_group_button);
		try {
			Base.base.getDriver().findElement(By.xpath(contact_save_or_cancel_group_button)).click();

		} catch (WebDriverException ex) {
			Logging.error("Could not find " + buttonName + " buttnon");
			throw ex;
		}

	}

	/**
	 * @Method Name: deleteAllGroup
	 * @Author: Vivian Xie
	 * @Created On: 11/14/2016
	 * @Description: Click save button when adding a group
	 */
	public void deleteAllGroup() {
		Logging.info("deleting all the groups");

		String contact_all_group = PropertyHelper.coreContacts.getPropertyValue("contact_all_group");
		ArrayList<WebElement> groups = null;
		try {
			groups = (ArrayList<WebElement>) Base.base.getDriver().findElements(By.xpath(contact_all_group));
			String grouName = "";
			Logging.info(String.valueOf(groups.size()));
			while (groups.size() != 0) {
				try {
					grouName = groups.get(0).getText();
				} catch (Exception e) {
					e.printStackTrace();
				}
				Logging.info("grouName=" + grouName);
				this.deleteGroup(grouName.trim());
				Tools.waitFor(1, TimeUnit.SECONDS);
				groups = (ArrayList<WebElement>) Base.base.getDriver().findElements(By.xpath(contact_all_group));
			}
			this.waitForLoadMaskDismissed();
			groups = (ArrayList<WebElement>) Base.base.getDriver().findElements(By.xpath(contact_all_group));
		} catch (WebDriverException ex) {
			Logging.error("Could not find groups");
			throw ex;
		}

	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Nov 17, 2016<br>
	 * <b>Description</b><br>
	 * verify if the contact photo section in contact edit view contains picture
	 * 
	 * @return true - the contact photo is a user customersized photo, false - the
	 *         contact photo is a default photo
	 */
	public boolean verifyContactPhotoInContactEditView() {

		boolean result = false;
		String contact_photo_edit_section = PropertyHelper.coreContacts.getPropertyValue("contact_photo_edit_section");

		try {
			Tools.setDriverDefaultValues();
			String elementSrc = Base.base.getDriver().findElement(By.xpath(contact_photo_edit_section))
					.getAttribute("src");
			if (elementSrc.startsWith("http") && elementSrc.contains("%"))
				result = true;
			else if (elementSrc.contains("resources") && elementSrc.contains("icon_blank_contact"))
				result = false;
		} catch (NoSuchElementException ex) {
			Logging.error("Could not find element of contact photo: ");
		}
		Logging.info("Verify if the contact photo edit contains picture " + result);

		return result;

	}

	/**
	 * Upload a file in windows dialog
	 * 
	 * @Author: Vivian Xie
	 * @Created On: Mar/7th/2016
	 * @param filePath
	 *            -- The file to be upload
	 */
	public void uploadFileInWindowsDialog(String filePath) {
		String ip = Base.base.getIpOfNode();
		String hubName = Base.hubName;
		String browser = Base.getBrowserName();
		String command = "C:\\Automation\\uploadFile.exe " + browser + " " + filePath;
		Logging.info(command);
		String nodeOsuser = PropertyHelper.HubConfig.getPropertyValue("hub_" + hubName + "_node_osuser");
		Logging.info("nodeOsuser:" + nodeOsuser);
		String nodeOspassword = PropertyHelper.HubConfig.getPropertyValue("hub_" + hubName + "_node_ospassword");
		if (nodeOsuser != null && !nodeOsuser.trim().equals("null")) {
			Tools.sendSSHCommandToRunCommand(ip, nodeOsuser, nodeOspassword, command);
		} else {

			// following code is used for debugging in local
			Logging.info("Upload in local environment");
			Runtime rn = Runtime.getRuntime();
			try {
				rn.exec(command);
			} catch (Exception e) {
				System.out.println("Error win exec!");
			}
		}
	}
	
	public void uploadFileInWindowsDialogUseAut() {
		String ip = Base.base.getIpOfNode();
		String hubName = Base.hubName;
		String browser = Base.getBrowserName();
		String command = "C:\\Automation\\media\\test.exe";
		Logging.info(command);
		String nodeOsuser = PropertyHelper.HubConfig.getPropertyValue("hub_" + hubName + "_node_osuser");
		Logging.info("nodeOsuser:" + nodeOsuser);
		String nodeOspassword = PropertyHelper.HubConfig.getPropertyValue("hub_" + hubName + "_node_ospassword");
	
		if (nodeOsuser != null && !nodeOsuser.trim().equals("null")) {
			
			Tools.sendSSHCommandToRunCommand(ip, nodeOsuser, nodeOspassword, command);
			
			
		} else {

			// following code is used for debugging in local
			Logging.info("Upload in local environment");
			Runtime rn = Runtime.getRuntime();
			try {
				rn.exec(command);
			} catch (Exception e) {
				System.out.println("Error win exec!");
			}
		}
	}

	/**
	 * get all the contacts specified name types in the list
	 * 
	 * @Author Vivian Xie
	 * @Created April/27th/2017
	 * @param nameType
	 *            -- value can be firstname, middlename or lastname. This function
	 *            only works when both fist name and last name exist and the names
	 *            don't contains space
	 */
	public ArrayList<String> getAllContactsNamesInTheList(String nameType) {
		Logging.info("Starting to get " + nameType + " of all the contacts in the list. This function only works"
				+ " when both fist name and last name exist and the names ( first name, middle name , last name) don't contains space ");
		String contacts_names_in_list = PropertyHelper.coreContacts.getPropertyValue("contacts_names_in_list");

		ArrayList<String> result = new ArrayList<String>();
		ArrayList<String> names = new ArrayList<String>();
		String aUserName[] = null;
		try {
			List<WebElement> nameElement = Base.base.getDriver().findElements(By.xpath(contacts_names_in_list));

			for (int i = 0; i < nameElement.size(); i++) {
				String nameString = ((WebElement) nameElement.get(i)).getAttribute("innerHTML");
				names.add(nameString);
			}

		} catch (WebDriverException ex) {
			throw ex;
		}

		for (int i = 0; i < names.size(); i++) {
			aUserName = ((String) names.get(i)).split(" ");

			if (aUserName.length < 2) {
				Logging.info("Both fist name and last name should exist.");
				return null;
			} else if (aUserName.length > 3) {
				Logging.info("The names contains space, and could not get deal with the date");
				return null;
			} else {
				if (nameType.equals("firstname")) {
					result.add(aUserName[0]);
				} else if (nameType.equals("lastname")) {
					int len = aUserName.length;
					result.add(aUserName[len - 1]);
				}
			}
		}

		return result;
	}

	/**
	 * click on contact sort button
	 * 
	 * @Author Vivian Xie
	 * @Created May/2nd/2017
	 */
	public void clickContactSortButton() {
		Logging.info("Clicking on contact sort button");

		String contact_sort_button = PropertyHelper.coreContacts.getPropertyValue("contact_sort_button");

		try {
			Base.base.getDriver().findElement(By.xpath(contact_sort_button)).click();

		} catch (NoSuchElementException ex) {
			Logging.error("Could not click on contact sort button");
			throw ex;
		}
	}

	/**
	 * Check if contact sort menu is checked
	 * 
	 * @Author Vivian Xie
	 * @return true if checked, else false
	 * @Created May/2nd/2017
	 */
	public boolean isContactSortMenuChecked(String menuItem) {
		Logging.info("Starting to check if contact sort menu " + menuItem + " is checked");
		String contact_sort_menu = PropertyHelper.coreContacts.getPropertyValue("contact_sort_menu")
				.replace("+variable+", menuItem);
		String classString = null;
		boolean result = false;
		try {
			classString = Base.base.getDriver().findElement(By.xpath(contact_sort_menu)).getAttribute("class");
		} catch (NoSuchElementException ex) {
			Logging.error("Could not check if contact sort button is checked");
		}
		result = classString.contains("-checked");
		Logging.info("contact sort menu " + menuItem + " is checked :" + result);
		return result;
	}

	/**
	 * Chick on the specified contact sort menu
	 * 
	 * @Author Vivian Xie
	 * @Created May/2nd/2017
	 */
	public void clickOnContactSortMenu(String menuItem) {
		Logging.info("Starting to click on contact sort menu " + menuItem);
		String contact_sort_menu = PropertyHelper.coreContacts.getPropertyValue("contact_sort_menu")
				.replace("+variable+", menuItem);
		try {
			Base.base.getDriver().findElement(By.xpath(contact_sort_menu)).click();
		} catch (NoSuchElementException ex) {
			Logging.error("Could not click on contact sort button");
			throw ex;
		}
	}

	/**
	 * click on contact group
	 * 
	 * @Author Vivian Xie
	 * @Created May/2nd/2017
	 */
	public void clickOnContactGroup(String groupName) {
		Logging.info("Starting to click on contact group: " + groupName);
		String contact_group_in_list = PropertyHelper.coreContacts.getPropertyValue("contact_group_in_list")
				.replace("+variable+", groupName);
		try {
			Base.base.getDriver().findElement(By.xpath(contact_group_in_list)).click();
		} catch (NoSuchElementException ex) {
			Logging.error("Could not click on contact group");
			throw ex;
		}
	}

	/**
	 * click on duplicated contact line
	 * 
	 * @Author Vivian Xie
	 * @param duplicateString
	 *            -- part of the text in duplicated contact line
	 * @Created May/5th/2017
	 */
	public void clickOnDuplicatedContactLine(String duplicateString) {
		Logging.info("Starting to click on duplicated contact line: " + duplicateString);
		String contacts_duplicate_line = PropertyHelper.coreContacts.getPropertyValue("contacts_duplicate_line")
				.replace("+variable+", duplicateString);
		try {
			Base.base.getDriver().findElement(By.xpath(contacts_duplicate_line)).click();
		} catch (NoSuchElementException ex) {
			Logging.error("Could not click on duplicated contact line");
			throw ex;
		}
	}

	/**
	 * get the given field value of a specify contact in merge view
	 * 
	 * @Author Vivian Xie
	 * @param contactName
	 *            -- part of or full of the contact name
	 * @param fieldName
	 *            -- the name of the field. This function get the value of the field
	 * @Created May/5th/2017
	 */
	public String getContactFieldValueInMergeView(String contactName, String fieldName) {
		Logging.info("Starting to get contact" + contactName + ", fieldName:" + fieldName + " value");
		String contact_merge_view_detail = PropertyHelper.coreContacts.getPropertyValue("contact_merge_view_detail")
				.replace("+variable1+", contactName).replace("+variable2+", fieldName.toLowerCase());
		String filedValue = null;
		try {
			filedValue = Base.base.getDriver().findElement(By.xpath(contact_merge_view_detail))
					.getAttribute("innerHTML");
		} catch (NoSuchElementException ex) {
			Logging.error("Could not click on duplicated contact line");
			throw ex;
		}
		return filedValue;
	}

	/**
	 * click on the given contact main field type input box
	 * 
	 * @Author Vivian Xie
	 * @param fieldName
	 *            -- the main field name, value can be "Email", "Mobile" ,"Phone"
	 * @Created May/5th/2017
	 */
	public void clickOnContactMainFieldTypeInputBox(String fieldName) {

		Logging.info("Starting to click on " + fieldName + " type input box ");
		String contacts_field_main_type_combine_box = PropertyHelper.coreContacts
				.getPropertyValue("contacts_field_main_type_combine_box").replace("+variable+", fieldName);
		try {
			Base.base.getDriver().findElement(By.xpath(contacts_field_main_type_combine_box)).click();
		} catch (NoSuchElementException ex) {
			Logging.error("Could not click on filed " + fieldName + "type input box");
			throw ex;
		}
	}

	/**
	 * click on the specified contact main field type option
	 * 
	 * @Author Vivian Xie
	 * @param optionText
	 *            -- the option to be select, value can be "Home", "Work", "Other"
	 * @Created May/5th/2017
	 */
	public void clickOnContactMainFieldTypeOption(String optionText) {

		Logging.info("Starting to click on " + optionText + " option ");
		String contacts_field_main_type_option = PropertyHelper.coreContacts
				.getPropertyValue("contacts_field_main_type_option").replace("+variable+", optionText);
		try {
			Base.base.getDriver().findElement(By.xpath(contacts_field_main_type_option)).click();
		} catch (NoSuchElementException ex) {
			Logging.error("Could not click option " + optionText);
			throw ex;
		}
	}

	/**
	 * Verifiy if the duplicated contact number displayed in contact duplicate view
	 * is correct.
	 * 
	 * @Author Vivian Xie
	 * @param duplicateLineText
	 *            -- the text in the specific duplicate line
	 * @param expectedNumber
	 *            -- the expected duplicate number
	 * @return true if the duplicate number is correct, false if not.
	 * @Created May/5th/2017
	 */
	public boolean verfiyDuplicateNumberInContactDuplicateView(String duplicateLineText, int expectedNumber) {

		Logging.info("Starting to verify if duplicate contact number is " + expectedNumber + " in line :"
				+ duplicateLineText);
		boolean result = false;
		String contact_duplicate_number = PropertyHelper.coreContacts.getPropertyValue("contact_duplicate_number")
				.replace("+variable+", duplicateLineText);
		String textString = null;
		try {
			textString = Base.base.getDriver().findElement(By.xpath(contact_duplicate_number))
					.getAttribute("innerHTML");
		} catch (NoSuchElementException ex) {
			Logging.error("Could not get duplicate contact nunber");
			throw ex;
		}
		if (textString.trim().equals("(" + expectedNumber + ")"))
			result = true;
		Logging.info("The actual number is:" + textString + ".  The number is as expected :" + result);
		return result;
	}

	/**
	 * Click on the add button of the main field
	 * 
	 * @Author Charlie Li
	 * @param fieldTitle
	 *            -- the main field name, value can be "Email", "Mobile" ,"Phone" ,
	 *            "Address"
	 * @Created September/06/2017
	 */
	public void clickContactsFieldAddButton(String fieldTitle) {
		Logging.info("Starting to click on " + fieldTitle + " button");
		String contacts_field_main_button_add = PropertyHelper.coreContacts
				.getPropertyValue("contacts_field_main_button_add").replace("+variable+", fieldTitle);
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(contacts_field_main_button_add)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click " + fieldTitle + " button");
		}
	}

	/**
	 * Enter values to multiple text fields
	 * 
	 * @Author Charlie Li
	 * @param fieldTitle
	 *            -- the main field name, value can be "Email", "Mobile" ,"Phone" ,
	 *            "Address"
	 * @param valus
	 *            -- List<String>, the values of multiple fields
	 * @param fieldName
	 *            -- the field name, value can be "Home", "Work", "Other", "City",
	 *            "State", "Zip" and "Country"
	 * @Created September/06/2017
	 */
	public void typeInMultipleField(String FieldTitle, List<String> values, String fieldName) {
		Logging.info("Starting to input values to " + fieldName + " fields");
		String contacts_addressfield = PropertyHelper.coreContacts.getPropertyValue("contacts_addressfield")
				.replace("+variable1+", FieldTitle).replace("+variable2+", fieldName);
		try {
			List<WebElement> fieldList = Base.base.getDriver().findElements(By.xpath(contacts_addressfield));
			for (int i = 0, size = fieldList.size(); i < size; i++) {
				fieldList.get(i).sendKeys(values.get(i));
			}
		} catch (WebDriverException ex) {
			Logging.error("error");
		}
	}

	/**
	 * Enter values to multiple textarea fields
	 * 
	 * @Author Charlie Li
	 * @param valus
	 *            -- List<String>, the values of multiple fields
	 * @param fieldName
	 *            -- the field name, value can be "Street"
	 * @Created September/06/2017
	 */
	public void typeInMultipleAddressStreet(List<String> values, String fieldName) {
		Logging.info("Starting to input values to " + fieldName + " fields");
		String contacts_address_textarea = PropertyHelper.coreContacts.getPropertyValue("contacts_address_textarea")
				.replace("+variable+", fieldName);
		try {
			List<WebElement> fieldList = Base.base.getDriver().findElements(By.xpath(contacts_address_textarea));
			for (int i = 0, size = fieldList.size(); i < size; i++) {
				fieldList.get(i).sendKeys(values.get(i));
			}
		} catch (WebDriverException ex) {
			Logging.error("error");
		}
	}

	/**
	 * click on the given contact main field type input box
	 * 
	 * @Author Charlie Li
	 * @param fieldName
	 *            -- the main field name, value can be "Email", "Mobile" ,"Phone" ,
	 *            "Address"
	 * @param number
	 *            -- the position of multiple fields
	 * @Created September/06/2017
	 */
	public void clickOnContactMainFieldTypeInputBox(String fieldName, int number) {

		Logging.info("Starting to click on " + fieldName + " type input box ");
		String contacts_field_main_type_combine_box = PropertyHelper.coreContacts
				.getPropertyValue("contacts_field_main_type_combine_box").replace("+variable+", fieldName);
		try {
			List<WebElement> mainTypes = Base.base.getDriver()
					.findElements(By.xpath(contacts_field_main_type_combine_box));
			if (mainTypes.size() > number) {
				mainTypes.get(number).click();
			}
		} catch (NoSuchElementException ex) {
			Logging.error("Could not click on filed " + fieldName + "type input box");
			throw ex;
		}
	}

	/**
	 * Verifying contact address in contact detail view
	 * 
	 * @Author Charlie Li
	 * @param address
	 *            -- the contact address
	 * @Created September/07/2017
	 */
	public boolean verifyContactAddressInDetailView(String FieldTitle, String address) {
		Logging.info("Verifying contact address in contact detail view, address: " + address);

		String contacts_address_in_preview = PropertyHelper.coreContacts.getPropertyValue("contacts_address_in_preview")
				.replace("+variable1+", FieldTitle).replace("+variable2+", address);
		
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();

			// try to find the contact address
			Base.base.getDriver().findElement(By.xpath(contacts_address_in_preview));

			result = true;

		} catch (WebDriverException ex) {
			Logging.info("Contact address is not found in contact detail view");
		}
		return result;
	}
}
