package com.synchronoss.pageobject;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.synchronoss.core.Base;
import com.synchronoss.core.Logging;
import com.synchronoss.core.PropertyHelper;
import com.synchronoss.core.Tools;

public class CoreEmailComposer extends PageObjectBase {
  public static String ATTACHMENT = "attachment";
  public static String LINK = "link";
  public static String CLOUD = "cloud";
  public static String LOCAL = "local";

    // ####################################################################
    // ----------------------- CLICKING METHODS ---------------------------
    // ####################################################################
	
    /**
     * @Method Name: clickComposerToolBarButton
     * @Author: Jerry Zhang
     * @Created On: 07/28/2014
     * @Description: This method will click on the specified email composer 
     * 				toolbar button.
     * @Parameter1: String button - button name
     * 				One of the following item should be provided:
     * 				- Send
     * 				- Save
     * 				- Discard
     */
	public void clickComposerToolBarButton(String button) {
		Logging.info("Clicking on email composer toolbar button: " + button);
		button = button.trim();
		String email_composer_toolbar_button;
		
    if (button.equalsIgnoreCase("save")) {
      email_composer_toolbar_button =
          PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_save_button");
    } else {
      email_composer_toolbar_button = PropertyHelper.coreEmailComposer
          .getPropertyValue("email_composer_toolbar_button").replace("+variable+", button);
    }
		try {
			//Tools.setDriverDefaultValues();			
			Tools.waitUntilElementDisplayedByXpath(email_composer_toolbar_button, this.timeoutMedium);
			Tools.scrollElementIntoViewByXpath(email_composer_toolbar_button);				
			
	    	Base.base.getDriver().findElement(By.xpath(email_composer_toolbar_button)).click();
	    	this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on email composer toolbar button: " + button);
			throw ex;
		}
	}
	
	
  /**
   * @Method Name: clickComposerHeaderToolBarButton
   * @Author: Fiona
   * @Created On: 08/09/2017
   * @Description: This method will click on the header of specified email composer toolbar button.
   * @Parameter1: String button - button name One of the following item should be provided: - Send -
   *              Save - attach
   */
  public void clickComposerHeaderToolBarButton(String button) {
    Logging.info("Clicking on email composer header toolbar button: " + button);
    button = button.trim();
    String headerButton = null;

    switch (button.toLowerCase()) {
      case "save":
        headerButton =
            PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_save_button");
        break;
      case "send":
        headerButton =
            PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_header_send_button");
        break;
    }
    try {
      // Tools.setDriverDefaultValues();
      Tools.waitUntilElementDisplayedByXpath(headerButton, this.timeoutMedium);
      Tools.scrollElementIntoViewByXpath(headerButton);

      Base.base.getDriver().findElement(By.xpath(headerButton)).click();
      this.waitForLoadMaskDismissed();
    } catch (WebDriverException ex) {
      Logging.error("Could not click on email composer toolbar button: " + button);
      throw ex;
    }
  }
	
	
  /**
   * @Method Name: clickComposerToolBarButton
   * @Author: Jerry Zhang
   * @Created On: 07/28/2014
   * @Description: This method will click on the specified email composer toolbar button.
   * @Parameter1: String button - button name One of the following item should be provided: - Send -
   *              Save - Discard
   * @Parameter1: ifWaitForLoadMaskDismiss -- wait for load mask dismiss if the value is true
   */
  public void clickComposerToolBarButton(String button, boolean ifWaitForLoadMaskDismiss) {
    Logging.info("Clicking on email composer toolbar button: " + button);
    button = button.trim();
    String email_composer_toolbar_button = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_composer_toolbar_button").replace("+variable+", button);

    try {
      // Tools.setDriverDefaultValues();
      Tools.waitUntilElementDisplayedByXpath(email_composer_toolbar_button, this.timeoutMedium);
      Tools.scrollElementIntoViewByXpath(email_composer_toolbar_button);

      Base.base.getDriver().findElement(By.xpath(email_composer_toolbar_button)).click();
      if (ifWaitForLoadMaskDismiss == true)
        this.waitForLoadMaskDismissed();
    } catch (WebDriverException ex) {
      Logging.error("Could not click on email composer toolbar button: " + button);
      throw ex;
    }
  }

/*    *//**
     * @Method Name: clickOnCcButton
     * @Author: Jerry Zhang
     * @Created On: 07/28/2014
     * @Description: This method will click on Cc button of email composer panel.
     *//*
	public void clickOnCcButton() {
		Logging.info("Clicking on email composer Cc button");
		String email_composer_cc_button = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_cc_button");
		
		try {
			Tools.setDriverDefaultValues();
	    	Base.base.getDriver().findElement(By.xpath(email_composer_cc_button)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on email composer Cc button");
			throw ex;
		}
	}
	*/
	
    // ####################################################################
    // ------------------------- TYPING METHODS ---------------------------
    // ####################################################################
	
	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Jun 17, 2015<br>
	 * <b>Description</b><br>
	 * Types recipient into the specified field with maximum 5 times of validation 
	 * for the input text. This method is aim to eliminate any potential failure of 
	 * recipient inputting.
	 * 
	 * @param field field to input
	 * @param text input text of recipient
	 * @return void
	 */
	private void typeRecipientAndEliminateInvalidBubble(String field, String text) {
		Logging.info("Typing in email composer " + field + " field with text: " + text);
		String email_composer_recipient_input = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_recipient_input")
				.replace("+variable+", field);
		String email_composer_recipient_invalid_bubble_remove = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_recipient_invalid_bubble_remove")
				.replace("+variable+", field);
		boolean recipientValid = false;
		int count = 0;
		
		Tools.setDriverDefaultValues();
		try {
			while (!recipientValid && count < 5) {
				Base.base.getDriver().findElement(By.xpath(email_composer_recipient_input))
						.sendKeys(text);
			    // move focus out of input field
			    Base.base.getDriver().findElement(By.xpath(email_composer_recipient_input)).sendKeys(Keys.TAB);
				
			    // To speed up invalid bubble locating. All consequent WebDriver wait timeout 
				// in the loop will be affected. Reset to default values in 'finally' block.
				Base.base.getDriver().manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
				
				List<WebElement> bubbleRemoveIcons = Base.base.getDriver()
						.findElements(By.xpath(email_composer_recipient_invalid_bubble_remove));
				if (bubbleRemoveIcons.size() != 0) {
					Logging.info("Invalid recipient found, removing ...");
					for (WebElement icon : bubbleRemoveIcons) {
						icon.click();
					}
				} else {
					recipientValid = true;
				}
				count++;
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not type text in " + field + " field");
		} finally {
			Tools.setDriverDefaultValues();
		}
		
		if (!recipientValid) {
			Logging.warn("Recipient could not be correctly typed into " + field 
					+ " field after " + count + " tries");
		}
	}
	
	/**
	 * <b>Author</b>:Young Zhao <br>
	 * <b>Date</b>: Jul 06, 2015 <br>
	 * <b>Description</b><br>
	 * Verify displayed name in field
	 * 
	 */
	public boolean verifyRecipientInField(String value) {
		Logging.info("verifying value in field: "+value);

		String email_composer_to_field_checkvalue = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_to_field_checkvalue").replace("+variable+", value);
		String email_composer_findfield = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_findfield").replace("+variable+", value);
		
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			
			Base.base.getDriver().findElement(By.xpath(email_composer_to_field_checkvalue));
			String fieldName = Base.base.getDriver().findElement(By.xpath(email_composer_findfield)).getText();
			Logging.info(value + " is found in " + fieldName + " field");
			result = true;
			
		} catch (WebDriverException ex) {
			Logging.error("Could not find the expected value in any field");
			throw ex;
		}
		return result;
	}

  /**
   * Verify if specific recipient is in To/Cc/Bcc field
   * 
   * @author Vivian Xie
   * @Create May/17th/2017
   * @param fieldName -- field name, value can be "To" ,"Cc" and "Bcc
   * @param value -- the expected value of the field
   * 
   */
  public boolean verifyRecipientInField(String fieldName, String value) {
    Logging.info("verifying if value " + value + " is in field " + fieldName);

    String email_composer_to_field_checkvalue = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_composer_to_field_checkvalue").replace("+variable+", value);
    String email_composer_findfield = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_composer_findfield").replace("+variable+", value);
    String actualfieldName = null;
    boolean result = false;

    try {
      Tools.setDriverDefaultValues();
      Base.base.getDriver().findElement(By.xpath(email_composer_to_field_checkvalue));
      actualfieldName =
          Base.base.getDriver().findElement(By.xpath(email_composer_findfield)).getText();
      Logging.info("actualfieldName="+actualfieldName);
    } catch (WebDriverException ex) {
      Logging.error("Could not find the expected value in any field");
      throw ex;
    }
    if (actualfieldName.contains(fieldName))
      result = true;
    Logging.info(value + " is found in " + fieldName + " field:" + result);
    return result;
  }
	
    /**
     * @Method Name: typeInToFieldWithValidation
     * @Author: Jerry Zhang
     * @Created On: 07/28/2014
     * @Description: This method will type text to email composer's To field,
     * and validate its correctness.
     * @parameter: String text - text to type in.
     */
	public void typeInToFieldWithValidation(String text) {
		this.typeRecipientAndEliminateInvalidBubble("To", text);
	}
	
	
    /**
     * @Method Name: typeInCcFieldWithValidation
     * @Author: Jerry Zhang
     * @Created On: 07/28/2014
     * @Description: This method will type text to email composer's Cc field,
     * and validate its correctness.
     * @parameter: String text - text to type in.
     */
	public void typeInCcFieldWithValidation(String text) {
		this.typeRecipientAndEliminateInvalidBubble("Cc", text);
	}
	
	
    /**
     * @Method Name: typeInBccFieldWithValidation
     * @Author: Jerry Zhang
     * @Created On: 07/28/2014
     * @Description: This method will type text to email composer's Bcc field,
     * and validate its correctness.
     * @parameter: String text - text to type in.
     */
	public void typeInBccFieldWithValidation(String text) {
		this.typeRecipientAndEliminateInvalidBubble("Bcc", text);
	}
	
	
    /**
     * @Method Name: typeInSubjectField
     * @Author: Jerry Zhang
     * @Created On: 07/28/2014
     * @Description: This method will type text to email composer's Subject field.
     * @parameter: String text - text to type in.
     */
	public void typeInSubjectField(String text) {
		Logging.info("Typing in email composer Subject field with text: " + text);
		String email_composer_subject_input = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_subject_input");
		String email_composer_window_header_text = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_window_header_text");
		
		try {
			//Tools.setDriverDefaultValues();
			// Click composer window header first to dismiss potential auto-suggest box.
		  
	    	Base.base.getDriver().findElement(By.xpath(email_composer_window_header_text)).click();
	    	Base.base.getDriver().findElement(By.xpath(email_composer_subject_input)).sendKeys(text);
		} catch (WebDriverException ex) {
			Logging.error("Could not type in email composer Subject field");
			throw ex;
		}
	}
	
	 /**
     * @Method Name: clearMailSubjectField
     * @Author: Vivian Xie
     * @Created On: Nov/25/2016
     * @Description: This method will clear text in email composer's Subject field.
     * @parameter: String text - text to type in.
     */
    public void clearMailSubjectField() {
        Logging.info("Clearing email composer Subject field" );
        String email_composer_subject_input = PropertyHelper.coreEmailComposer
                .getPropertyValue("email_composer_subject_input");
        String email_composer_window_header_text = PropertyHelper.coreEmailComposer
                .getPropertyValue("email_composer_window_header_text");
        
        try {
            Tools.setDriverDefaultValues();
            // Click composer window header first to dismiss potential auto-suggest box.
            Base.base.getDriver().findElement(By.xpath(email_composer_window_header_text)).click();
            Base.base.getDriver().findElement(By.xpath(email_composer_subject_input)).clear();
        } catch (WebDriverException ex) {
            Logging.error("Could not clear email composer Subject field");
            throw ex;
        }
    }
	
    /**
     * @Method Name: typeInMessageBody
     * @Author: Jerry Zhang
     * @Created On: 07/29/2014
     * @Description: This method will type text to email composer's body.
     * @parameter: String text - text to type in.
     */
	public void typeInMessageBody(String text) {
		Logging.info("Typing in email composer body: " + text);
		String email_composer_iframe_outer = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_iframe_outer");
		String email_composer_html_body = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_html_body");
		String email_composer_plain_textarea = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_plain_textarea");
		boolean isRichText;
		
		try {
	    	//Tools.setDriverDefaultValues();
	    	
	    	// determine whether composer body in HTML format
	    	isRichText = Base.base.getDriver().findElement(By.xpath(email_composer_iframe_outer))
	    			.isDisplayed();
	    	Logging.info("Is email composer body in HTML format: " + isRichText);
	    	
	    	if (isRichText) {
		   
		    	if (!Tools.isChrome()) {
			    	// Sometimes, editor in iframe will not be focused, click on the up-left corner to focus.
		    		// Note: this extra clicking may fail on Chrome. Get Chrome excluded.
			    	WebElement bodyUpLeftPoint = Base.base.getDriver().findElement(By.xpath(email_composer_html_body));
			    	new Actions(Base.base.getDriver()).moveToElement(bodyUpLeftPoint, 1, 1).click().build().perform();
		    	}
		    	
		    	// Type text in body.
				//Base.base.getDriver().findElement(By.xpath(email_composer_html_body)).click();
		    	Logging.info("click");
				//Tools.waitFor(2000, TimeUnit.MILLISECONDS);

				WebElement element=Base.base.getDriver().findElement(By.xpath(email_composer_html_body));
				((JavascriptExecutor)Base.base.getDriver()).executeScript("arguments[0].click();", element);

		    	//Base.base.getDriver().findElement(By.xpath(email_composer_html_body)).sendKeys(text);
		    
	    	} else {
		    	// Sometimes, editor in iframe will not be focused, click to focus.
		    	Base.base.getDriver().findElement(By.xpath(email_composer_plain_textarea)).click();
	    		Base.base.getDriver().findElement(By.xpath(email_composer_plain_textarea)).sendKeys(text);
	    	}
		} catch (WebDriverException ex) {
			Logging.error("Could not type in email composer body");
			throw ex;
		} 
	}
	
	
	/**
     * @Method Name: clearMessageBody
     * @Author: Vivia Xie
     * @Created On: Nov/25/2016
     * @Description: This method will clear text to email composer's body.
     * @parameter: String text - text to type in.
     */
    public void clearMessageBody() {
        Logging.info("Clearing composer body:" );
        String email_composer_iframe_outer = PropertyHelper.coreEmailComposer
                .getPropertyValue("email_composer_iframe_outer");
        String email_composer_html_body = PropertyHelper.coreEmailComposer
                .getPropertyValue("email_composer_html_body");
        String email_composer_plain_textarea = PropertyHelper.coreEmailComposer
                .getPropertyValue("email_composer_plain_textarea");
        boolean isRichText;
        
        try {
            Tools.setDriverDefaultValues();
            
            // determine whether composer body in HTML format
            isRichText = Base.base.getDriver().findElement(By.xpath(email_composer_iframe_outer))
                    .isDisplayed();
            Logging.info("Is email composer body in HTML format: " + isRichText);
            
            if (isRichText) {
                if (!Tools.isChrome()) {
                    // Sometimes, editor in iframe will not be focused, click on the up-left corner to focus.
                    // Note: this extra clicking may fail on Chrome. Get Chrome excluded.
                    WebElement bodyUpLeftPoint = Base.base.getDriver().findElement(By.xpath(email_composer_html_body));
                    new Actions(Base.base.getDriver()).moveToElement(bodyUpLeftPoint, 1, 1).click().build().perform();
                }
                
                // Type text in body.
                Base.base.getDriver().findElement(By.xpath(email_composer_html_body)).clear();
            } else {
                // Sometimes, editor in iframe will not be focused, click to focus.
                Base.base.getDriver().findElement(By.xpath(email_composer_plain_textarea)).click();
                Base.base.getDriver().findElement(By.xpath(email_composer_plain_textarea)).clear();
            }
        } catch (WebDriverException ex) {
            Logging.error("Could not clear email composer body");
            throw ex;
        } finally {
            // Switch to default window
            Tools.setDriverDefaultValues();
        }
    }
    
    /**
     * @Method Name: clickOnMessageBody
     * @Author: Vivia Xie
     * @Created On: Dec/20/2016
     * @Description: This method will click on email composer's body.
     */
    public void clickOnMessageBody() {
        Logging.info("Clicking on composer body:" );
        String email_composer_iframe_outer = PropertyHelper.coreEmailComposer
                .getPropertyValue("email_composer_iframe_outer");
        String email_composer_html_body = PropertyHelper.coreEmailComposer
                .getPropertyValue("email_composer_html_body");
        String email_composer_plain_textarea = PropertyHelper.coreEmailComposer
                .getPropertyValue("email_composer_plain_textarea");
        boolean isRichText;
        
        try {
            Tools.setDriverDefaultValues();
            
            // determine whether composer body in HTML format
            isRichText = Base.base.getDriver().findElement(By.xpath(email_composer_iframe_outer))
                    .isDisplayed();
            Logging.info("Is email composer body in HTML format: " + isRichText);
            
            if (isRichText) {
                if (!Tools.isChrome()) {
                    // Sometimes, editor in iframe will not be focused, click on the up-left corner to focus.
                    // Note: this extra clicking may fail on Chrome. Get Chrome excluded.
                    WebElement bodyUpLeftPoint = Base.base.getDriver().findElement(By.xpath(email_composer_html_body));
                    new Actions(Base.base.getDriver()).moveToElement(bodyUpLeftPoint, 1, 1).click().build().perform();
                }
                
                // Type text in body.
                Base.base.getDriver().findElement(By.xpath(email_composer_html_body)).click();
            } else {
                // Sometimes, editor in iframe will not be focused, click to focus.
                Base.base.getDriver().findElement(By.xpath(email_composer_plain_textarea)).click();
            }
        } catch (WebDriverException ex) {
            Logging.error("Could not click on email composer body");
            throw ex;
        } finally {
            // Switch to default window
            Tools.setDriverDefaultValues();
        }
    }
    
    /**
     * @Method Name: verifyAttachmentUploadedByName
     * @Author: Jerry zhang
     * @Created On: 08/05/2014
     * @Description: This method will verify whether the expected attachment 
     * 				is uploaded to mail composer.
     * @Parameter1: String filename - expected file name
     * @Return: boolean - true/false
     */
    public boolean verifyAttachmentUploadedByName(String filename) {
    	Logging.info("Verifying if attachment uploaded: " + filename);
    	
    	filename = filename.trim();
    	String email_composer_attach_name = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_attach_name");
    	String email_composer_upload_progress_bar = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_upload_progress_bar");
    	
    	boolean result = false;
    	try {	
    		Tools.setDriverDefaultValues();
	    	
	    	// Wait for uploading finished.
	    	Tools.waitUntilElementNotDisplayedByXpath(email_composer_upload_progress_bar, this.timeoutMedium);
	    	
	    	List<WebElement> elements = Base.base.getDriver()
	    			.findElements(By.xpath(email_composer_attach_name));
	    	
	    	for (WebElement name : elements) {
	    		if (name.getText().contains(filename)) {
	    			result = true;
	    			break;
	    		}
	    	}
	    	
	    	if (result) {
		    	Logging.info("Found attachment: " + filename);
	    	} else {
		    	Logging.info("Not found attachment: " + filename);
	    	}    		
    	} catch (WebDriverException ex) {
	    	Logging.info("Could not verify uploaded attachment");
		}
    	return result;
    }
    
    
  /**
   * @Method Name: attachFile
   * @Author: Jerry zhang
   * @Created On: 08/10/2015
   * @Description: This method will attach specified file to the message being composed.
   * @parameter: String filename - file name.
   */
  public void attachFile(String filename) {
    Logging.info("Attaching file: " + filename);

    String email_composer_attach_file_input =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_attach_file_input");
    String email_composer_upload_progress_bar =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_upload_progress_bar");

    // All win7 systems have media folder here
    String fullpath = "C:\\Automation\\media\\" + filename;
    Logging.info(fullpath);
    WebElement element =
        Base.base.getDriver().findElement(By.xpath(email_composer_attach_file_input));
    JavascriptExecutor j = (JavascriptExecutor) Base.base.getDriver();
    j.executeScript(
        "arguments[0].setAttribute('style', 'display: block; height: 1px; width: 1px; visibility: visible');",
        element);


    try {
      Tools.setDriverDefaultValues();
      Base.base.getDriver().findElement(By.xpath(email_composer_attach_file_input))
          .sendKeys(fullpath);
      Tools.waitUntilElementNotDisplayedByXpath(email_composer_upload_progress_bar,
          this.timeoutMedium);
      Logging.info("File attached successfully");

    } catch (NoSuchElementException ex) {
      Logging.error("Could not attach file: " + filename);
      throw ex;
    }
  }
	
	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 08, 2015<br>
	 * <b>Description</b><br>
	 * click the minimize or close button on the right corner of the compose window
	 * 
	 * <b>Parameters</b><br>
	 * String button - button name. One of the following buttons should be provided:
	 * 			- close
	 * 			- minimize
	 */
	public void clickMinimizeOrCloseComposeEmailWindow(String button) {
		Logging.info("Clicking on "+button+" on the compose email pop up window");
		String email_composer_minimize_or_close_btn = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_minimize_or_close_btn").replace("+variable+", button);
		String email_load_mask = PropertyHelper.coreEmailFile
				.getPropertyValue("email_load_mask");
		try {
			Tools.setDriverDefaultValues();
	    	
	    	Tools.waitUntilElementNotDisplayedByXpath(email_load_mask, this.timeoutMedium);
	    	Tools.scrollElementIntoViewByXpath(email_composer_minimize_or_close_btn);
	    	Base.base.getDriver().findElement(By.xpath(email_composer_minimize_or_close_btn)).click();
			
		} catch (WebDriverException ex) {
			Logging.error("Could not found the "+ button);
			throw ex;
		}
	}
	
	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 13, 2015<br>
	 * <b>Description</b><br>
	 * types recipient into the specified field, ended with "key", without any validation check.
	 * 
	 * <b>Parameters</b><br>
	 * String field - field name. One of the following buttons should be provided:
	 * 			- To
	 * 			- Cc
	 * 			- Bcc
	 * String text - input text of recipient
	 * Keys key - the key which should be ended with. One of the following keys should be provided:
	 * 			- Keys.ENTER
	 * 			- Keys.TAB
	 * 			- Keys.SEMICOLON
	 * 			- ","
	 */
	public void typeRecipientWithoutValidation(String field, String text, CharSequence key) {
		Logging.info("Typing in email composer " + field + " field with text: " + text);
		String email_composer_recipient_input = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_recipient_input")
				.replace("+variable+", field);
		
		//Tools.setDriverDefaultValues();
		try {
			if (key.equals(Keys.ENTER)) {
				Base.base.getDriver().findElement(By.xpath(email_composer_recipient_input))
				.sendKeys(text);
				
				Tools.waitFor(2000, TimeUnit.MILLISECONDS);
			    Base.base.getDriver().findElement(By.xpath(email_composer_recipient_input))
				.sendKeys(Keys.ENTER);
			} else {
				Base.base.getDriver().findElement(By.xpath(email_composer_recipient_input))
				.sendKeys(text + key);
			}	
			
			Tools.waitFor(500, TimeUnit.MILLISECONDS);
		} catch (WebDriverException ex) {
			Logging.error("Could not type text in " + field + " field");
		} finally {
			Tools.setDriverDefaultValues();
		}
	}
	
	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 14, 2015<br>
	 * <b>Description</b><br>
	 * click on Cc button of email composer panel
	 */
	public void clickOnBccButton() {
		Logging.info("Clicking on email composer Bcc button");
		String email_composer_bcc_button = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_bcc_button");
		
		try {
			Tools.setDriverDefaultValues();
	    	Base.base.getDriver().findElement(By.xpath(email_composer_bcc_button)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on email composer Bcc button");
			throw ex;
		}
	}
	
	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 14, 2015<br>
	 * <b>Description</b><br>
	 * verify there are any invalid email address in any field
	 */
	public boolean verifyInvalidRecipient() {
		Logging.info("Verifying there are any invalid recipient in field");

		String email_composer_invalid_recipient = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_invalid_recipient");
		
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			
			Base.base.getDriver().findElement(By.xpath(email_composer_invalid_recipient));
			List<WebElement> invalidEmails = Base.base.getDriver().findElements(By.xpath(email_composer_invalid_recipient));

			if ( invalidEmails.size() != 0 ) {
				for (WebElement email : invalidEmails) {
					String address = email.getAttribute("title");
					Logging.info("Invalid recipient found: " + address);
				}
				result = true;
			}
			
		} catch (WebDriverException ex) {
			throw ex;
		}
		return result;
	} 
	
	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 14, 2015<br>
	 * <b>Description</b><br>
	 * verify there are any invalid email address in any field
	 */
	public boolean verifyInvalidRecipientByName(String recipient) {
		Logging.info("Verifying " + recipient + " is an invalid recipient");

		String email_composer_invalid_recipient_by_name = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_invalid_recipient_by_name").replace("+variable+", recipient);
		
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			
			Base.base.getDriver().findElement(By.xpath(email_composer_invalid_recipient_by_name));
			result = true;
			
		} catch (WebDriverException ex) {
			// no need to throw this exception
			// throw ex;
		}
		Logging.info(recipient + " is an invalid recipient: " + result);
		return result;
	} 
	
	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 24, 2015<br>
	 * <b>Description</b><br>
	 * click on "x" icon of email in any field to remove this email
	 * 
	 * <b>Parameters</b><br>
	 * String email - email displayed name in any field. 
	 */
	public void removeSingleRecipientsByName(String email) {
		Logging.info("Clicking on \"x\" icon of " + email + " to remove it");
		String email_composer_single_email_remove_bubble = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_single_email_remove_bubble").replace("+variable+", email);
		
		try {
			Tools.setDriverDefaultValues();
	    	Base.base.getDriver().findElement(By.xpath(email_composer_single_email_remove_bubble)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on \"x\" icon of " + email + " to remove it");
			throw ex;
		}
	}
	
	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 24, 2015<br>
	 * <b>Description</b><br>
	 * click on "x" icon of all email in any field to remove them
	 * 
	 */
	public void cleanupAllRecipients() {
		Logging.info("click on \"x\" icon of all email in any field to remove them");
		String email_composer_remove_bubble = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_remove_bubble");
		try {
			List<WebElement> bubbleRemoveIcons = Base.base.getDriver()
					.findElements(By.xpath(email_composer_remove_bubble));
			if (bubbleRemoveIcons.size() != 0) {
				for (WebElement icon : bubbleRemoveIcons) {
					icon.click();
				}
			} else {
				Logging.info("there is no any email to remove");
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not click on \"x\" icon to remove email");
			throw ex;
		}
	}
	
	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 27, 2015<br>
	 * <b>Description</b><br>
	 * click on recipient-input button in email composer panel to launch contactpicker
	 * <b>Parameters</b><br>
	 * String button - button name. One of the following buttons should be provided:
	 * 			- To
	 * 			- Cc
	 * 			- Bcc
	 */
	public void clickRecipientInputButton(String button) {
		Logging.info("Clicking on " + button + " button to launch contactpicker");
		String email_composer_recipient_input_button = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_recipient_input_button").replace("+variable+", button);
		boolean buttonDisplayed;
		
		try {
			Tools.setDriverDefaultValues();
			buttonDisplayed = Base.base.getDriver()
	    			.findElement(By.xpath(email_composer_recipient_input_button)).isDisplayed();
			
			// open Cc/Bcc input field
			if (!buttonDisplayed) {
				if (button == "Cc") {
					//this.clickOnCcButton();
				} else if (button == "Bcc") {
					this.clickOnBccButton();
				}
			}
			
			WebElement recipientInputButton =  Base.base.getDriver().findElement(By.xpath(email_composer_recipient_input_button));
	    	new Actions(Base.base.getDriver()).moveToElement(recipientInputButton).moveByOffset(-20, 5).click().build().perform();
		} catch (WebDriverException ex) {
			Logging.error("Could not Click on " + button + " button to launch contactpicker");
			throw ex;
		}
	}
	
	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 27, 2015<br>
	 * <b>Description</b><br>
	 * click on Add/Cancel button in contactpicker
	 * <b>Parameters</b><br>
	 * String button - button name. One of the following buttons should be provided:
	 * 			- Add
	 * 			- Cancel
	 */
	public void clickButtonInContactpicker(String button) {
		Logging.info("Clicking on " + button + " button in contactpicker");
		String email_composer_contactpicker_button = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_contactpicker_button").replace("+variable+", button);
		
		try {
			Tools.setDriverDefaultValues();
	    	Base.base.getDriver().findElement(By.xpath(email_composer_contactpicker_button)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on " + button + " button in contactpicker");
			throw ex;
		}
	}
	
	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 27, 2015<br>
	 * <b>Description</b><br>
	 * select contact in contactpicker
	 * <b>Parameters</b><br>
	 * String fname - first name of contact
	 */
	public void selectContactInContactpicker(String fname) {
		Logging.info("Selecting " + fname + " in contactpicker");
		String email_composer_contactpicker_contact = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_contactpicker_contact").replace("+variable+", fname);
		
		try {
			Tools.setDriverDefaultValues();
	    	Base.base.getDriver().findElement(By.xpath(email_composer_contactpicker_contact)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not select " + fname + " in contactpicker");
			throw ex;
		}
	}
	
	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Aug 04, 2015<br>
	 * <b>Description</b><br>
	 * send ctrl+A to the email context body
	 */
	public void selectAllInHtmlMessageBody() {
		String email_composer_iframe_outer = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_iframe_outer");
		String email_composer_iframe_inner = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_iframe_inner");
		       
		Logging.info("Selecting all text in email contents with ctrl+A");
		       
		try {
			Tools.setDriverDefaultValues();
			
			// Switch into outer iframe.
			WebElement outerFrame = Base.base.getDriver()
					.findElement(By.xpath(email_composer_iframe_outer));
			Base.base.getDriver().switchTo().frame(outerFrame);
			    	
			// Switch into inner iframe.
			WebElement innerFrame = Base.base.getDriver()
					.findElement(By.xpath(email_composer_iframe_inner));
			Base.base.getDriver().switchTo().frame(innerFrame);
			Actions action = new Actions(Base.base.getDriver()); 
			action.keyDown(Keys.CONTROL).sendKeys("a").perform();
		}catch (WebDriverException ex) {
			Logging.error("Selecting all text fails");
			throw ex;			    		
		} finally {
			// Switch to default window
			Tools.setDriverDefaultValues();
		}
	}
	
	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Aug 04, 2015<br>
	 * <b>Description</b><br>
	 * click on the specified text format button in HTML composer
	 * <b>Parameters</b><br>
	 * String button - button name. One of the following buttons should be provided:
	 * 			- Bold
	 * 			- Italic
	 * 			- Underline
	 * 			- Text color
	 * 			- Text background color
	 * 			- Insert image
	 *          - Bulleted list
	 *          - Numbered list
	 *          - Decrease indent
	 *          - Increase indent
	 *          - Align left
	 *          - Align center
	 *          - Align right
	 *          - Justify
	 *          - Add or remove link
	 *          - Local Image
	 *          - Emoji
	 */
	public void clickFormatButtonInHtmlComposer(String button) {
		Logging.info("Clicking on format button in HTML composer: " + button);
		
		button = button.trim();
		
		String email_composer_format_button = PropertyHelper.coreEmailComposer
					.getPropertyValue("email_composer_format_button")
					.replace("+variable+", button);
         if (button.equals("Text color")){
           email_composer_format_button = PropertyHelper.coreEmailComposer
               .getPropertyValue("email_composer_format_text_color");       
        }
		try {
	    	
	    	Tools.waitUntilElementDisplayedByXpath(email_composer_format_button, timeoutLong);
	    	Base.base.getDriver().findElement(By.xpath(email_composer_format_button)).click(); 	
		} catch (WebDriverException ex) {
			Logging.error("Could not click on format button in HTML composer");
			throw ex;
		} 
		Tools.waitFor(2, TimeUnit.SECONDS);
	}
  /**
   * @Method Name: verifyFormatButtonIsActiveInHtmlComposer
   * @Author: Vivian Xie
   * @Created On: Dec/30/2016
   * @Description: This method will verify if the format button is active in rich mail tool bar .
   * @param: button -- the button name, One of the following buttons should be provided: - Bold -
   *         Italic - Underline - Bulleted list - Numbered list - Align left - Align center - Align
   *         right - Justify
   * @return: true if selected , false if not
   */
  public boolean verifyFormatButtonIsActiveInHtmlComposer(String button) {
    Logging.info("verify the format button " + button + " is selected in rich mail tool bar ");
    button = button.trim();
    boolean result = false;
    String email_composer_format_button = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_composer_format_button").replace("+variable+", button);
    String buttonClass = null;
    try {
      Tools.waitUntilElementDisplayedByXpath(email_composer_format_button, timeoutLong);
      buttonClass = Base.base.getDriver().findElement(By.xpath(email_composer_format_button))
          .getAttribute("class");
    } catch (WebDriverException ex) {
      Logging.error("Could not find on format button in HTML composer");
      throw ex;
    }
    Logging.info(buttonClass);
    if (buttonClass.contains(" active")) result = true;
    Logging.info("the format button " + button + " is selected in rich mail tool bar " + result );
    return result;
  }
  /**
   * @Method Name: verifyColorSelectedInColorPicker
   * @Author: Vivian Xie
   * @Created On: Dec/30/2016
   * @Description: This method will verify if the specific color is selected in color picker .
   * @param: colorPickerTyle -- value can be "Text color" and "Text background color"   
   *         color -- the specific color
   * @return: true if the color is correct , false if not
   */
  public boolean verifyColorSelectedInColorPicker(String colorPickerTyle, Color color) {
    Logging.info("verify the color" + color.toString() + " is selected in "+ colorPickerTyle +" colorPicker");
    String colorString = Tools.color2RgbString(color);
    boolean result = false;
    String email_composer_colorpicker_color = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_composer_colorpicker_color").replace("+variable+", colorPickerTyle);
    String pickerStyle = null;
    try {
      Tools.waitUntilElementDisplayedByXpath(email_composer_colorpicker_color, timeoutLong);
      Logging.info(email_composer_colorpicker_color);
      pickerStyle = Base.base.getDriver().findElement(By.xpath(email_composer_colorpicker_color))
          .getAttribute("style");
    } catch (WebDriverException ex) {
      Logging.error("Could not find color picker in HTML composer");
      throw ex;
    }
    Logging.info(pickerStyle);
    if (pickerStyle.contains("border-color: "+colorString)) result = true;
    Logging.info("the color" + color.toString() + " is selected in "+ colorPickerTyle +" color picker:" + result );
    return result;
  }

  /*
   * <b>Author</b>: Young Zhao<br> <b>Date</b>: Aug 04, 2015<br> <b>Description</b><br> verify
   * whether the specified text is in Bold/underline/italic format. Note that only
   * Bold/underline/italic can be accepted so far. <b>Parameters</b><br> String format - text
   * format. One of the following buttons should be provided: - Bold - Italic - Underline String
   * testStr - The specified text
   */
  public boolean verifyComposerTextFormat(String format, String testStr) {

    String email_composer_body_bold_content =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_body_bold_content");
    String email_composer_body_italic_content =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_body_italic_content");
    String email_composer_body_underline_content =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_body_underline_content");

    boolean result = false;
    String body_contents = "null";

    Logging.info("Verifying composer text format: " + format + ", expected text: " + testStr);

    try {
      Tools.setDriverDefaultValues();
      if (format.equalsIgnoreCase("bold")) {
        body_contents =
            Base.base.getDriver().findElement(By.xpath(email_composer_body_bold_content)).getText();
      } else if (format.equalsIgnoreCase("italic")) {
        body_contents = Base.base.getDriver()
            .findElement(By.xpath(email_composer_body_italic_content)).getText();
      } else if (format.equalsIgnoreCase("underline")) {
        body_contents = Base.base.getDriver()
            .findElement(By.xpath(email_composer_body_underline_content)).getText();
      }

      if (body_contents.trim().equals(testStr)) {
        result = true;
      } else {
        result = false;
      }

      Logging.info("Text found in format: " + format + ", actual text: " + body_contents
          + ". Expected text: " + testStr);

    } catch (WebDriverException ex) {
      Logging.error("Could not verify composer text format " + format);
      throw ex;
    } finally {
      // Switch to default window
      Tools.setDriverDefaultValues();
    }
    return result;
  }

	
  /**
   * verify if the text format is correct in mail body
   * 
   * @author Vivian Xie
   * @param  format -- the expected format
   * @param  testStr -- the text to verify
   * @return true if the format is correct. false if not
   */
  public boolean verifyMsgTextFormat(String format, String testStr) {

    String email_msg_body_bold_content =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_msg_body_bold_content");
    String email_msg_body_italic_content =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_msg_body_italic_content");
    String email_msg_body_underline_content =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_msg_body_underline_content");

    boolean result = false;
    String body_contents = "null";

    Logging.info("Verifying msg text format: " + format + ", expected text: " + testStr);

    try {
      Tools.setDriverDefaultValues();
      if (format.equalsIgnoreCase("bold")) {
        Logging.info(email_msg_body_bold_content);
        body_contents = Base.base.getDriver().findElement(By.xpath(email_msg_body_bold_content))
            .getAttribute("innerHTML");
      } else if (format.equalsIgnoreCase("italic")) {
        Logging.info(email_msg_body_italic_content);
        body_contents = Base.base.getDriver().findElement(By.xpath(email_msg_body_italic_content))
            .getAttribute("innerHTML");
      } else if (format.equalsIgnoreCase("underline")) {
        Logging.info(email_msg_body_underline_content);
        body_contents = Base.base.getDriver()
            .findElement(By.xpath(email_msg_body_underline_content)).getAttribute("innerHTML");
      }

      if (body_contents.trim().equals(testStr.trim())) {
        result = true;
      } else {
        result = false;
      }

      Logging.info("Text found in format: " + format + ", actual text: " + body_contents
          + ". Expected text: " + testStr);

    } catch (WebDriverException ex) {
      Logging.error("Could not verify msg text format " + format);
      throw ex;
    } finally {
      Tools.setDriverDefaultValues();
    }
    return result;
  }

	
	
	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Aug 04, 2015<br>
	 * <b>Description</b><br>
	 * click a color in color panel
	 * <b>Parameters</b><br>
	 * String color - The expected body text color. One of the following color should be provided:
	 * 			- PageObjectBase.text_color_red
	 */	
	public void selectComposerTextColor(Color color) {
		Logging.info("Select color "+color);
		String email_composer_color_in_colorpanel = PropertyHelper.coreEmailComposer
					.getPropertyValue("email_composer_color_in_colorpanel");
			
		try {
			Tools.setDriverDefaultValues();
		    email_composer_color_in_colorpanel=email_composer_color_in_colorpanel.replace("+variable+", 
		    		Tools.color2RgbString(color));
			Base.base.getDriver().findElement(By.xpath(email_composer_color_in_colorpanel)).click();		
		} catch (WebDriverException ex) {
				Logging.error("Could not click the "+color);
				throw ex;
		} 
	}
	
  /**
   * <b>Author</b>: Young Zhao<br>
   * <b>Date</b>: Aug 04, 2015<br>
   * <b>Description</b><br>
   * verify whether the email body text color is expected Note that only RED is acceptable so far.
   * <b>Parameters</b><br>
   * String color - The expected body text color. One of the following color should be provided: -
   * red String testStr - The specified text String location - composer or msg
   */
  public boolean verifyComposerTextColor(Color color, String testStr, String location) {

    Logging.info("starting to verify the text color in " + location);
    String email_text_font_content = null;
    if (location.contains("composer"))
      email_text_font_content = PropertyHelper.coreEmailComposer
          .getPropertyValue("email_composer_body_text_font_content_composer")
          .replace("+variable+", Tools.color2RgbString(color));
    else
      email_text_font_content = PropertyHelper.coreEmailComposer
          .getPropertyValue("email_composer_body_text_font_content_msgbody")
          .replace("+variable+", Tools.color2RgbString(color).replaceAll(" ", ""));

    Logging.info(email_text_font_content);
    boolean result = false;
    // String body_color = "null";

    Logging.info("Verifying composer text color: " + color.toString());

    try {
      Tools.setDriverDefaultValues();
      Logging.info(color.toString());

      String colorText =
          Base.base.getDriver().findElement(By.xpath(email_text_font_content)).getText();
      if (colorText.contains(testStr)) {
        result = true;
      }

      Logging.info("expected text: " + testStr);
      Logging.info("Composer text with color  " + color.toString() + " contains " + testStr + ":  "
          + result);

    } catch (WebDriverException ex) {
      Logging.error("Could not verify composer text color " + color.toString());
      throw ex;
    } finally {
      // Switch to default window
      Tools.setDriverDefaultValues();
    }
    return result;
  }

  /**
   * <b>Author</b>: Young Zhao<br>
   * <b>Date</b>: Aug 05, 2015<br>
   * <b>Description</b><br>
   * select an emoji from the emoji list
   */
  public void clickEmojiIconAndSelectEmojiInMailComposerToolBar(String emojiGroup, String emoji) {

    Logging.info("Clicking on Emoji icon in email composer toolbar and then select one");

    String email_composer_toolbar_emoji_icon =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_toolbar_emoji_icon");
    Logging.info(email_composer_toolbar_emoji_icon);


    String email_composer_emoji_group = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_composer_emoji_group").replace("+variable+", emojiGroup);
    Logging.info(email_composer_emoji_group);

    String email_composer_emoji_item =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_emoji_item")
            .replace("+variable1+", emojiGroup).replace("+variable2+", emoji);
    Logging.info(email_composer_emoji_item);
    try {
      Tools.setDriverDefaultValues();
      Tools.waitUntilElementDisplayedByXpath(email_composer_toolbar_emoji_icon, 5);
      Base.base.getDriver().findElement(By.xpath(email_composer_toolbar_emoji_icon)).click();
      Tools.waitUntilElementDisplayedByXpath(email_composer_emoji_group, 5);
      Base.base.getDriver().findElement(By.xpath(email_composer_emoji_group)).click();
      Tools.waitUntilElementDisplayedByXpath(email_composer_emoji_item, 5);
      Base.base.getDriver().findElement(By.xpath(email_composer_emoji_item)).click();
      Base.base.getDriver().findElement(By.xpath(email_composer_toolbar_emoji_icon)).click();
    } catch (WebDriverException ex) {
      Logging.error("Could not click on Emoji icon in email composer toolbar");
      throw ex;
    } finally {
      // Switch to default window
      Tools.setDriverDefaultValues();
    }
  }

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Aug 05, 2015<br>
	 * <b>Description</b><br>
	 * verify emoji existence in message body
	 * note that, only one emoji (sprite U-_263a) is acceptable so far
	 */
	public boolean verifyEmojiExistenceInMessageBody(String emoji) {
    	Logging.info("Verifying emoji existence in message body");
	
		String email_composer_emoji_in_body = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_emoji_in_body").replace("+variable+", emoji);
	
		boolean result = false;
    	
    	try {
			Tools.setDriverDefaultValues();
			
	    	Logging.info(email_composer_emoji_in_body);
	    	Base.base.getDriver().findElement(By.xpath(email_composer_emoji_in_body));
	    	// Emoji is found.
	    	result = true;
	    	Logging.info("Found emoji in message body");
    	} catch (WebDriverException ex) {
	    	Logging.info("Could not found emoji in message body");
		} finally {
			// Switch to default window
			Tools.setDriverDefaultValues();
		}
    	return result;
	}
	
	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Aug 05, 2015<br>
	 * <b>Description</b><br>
	 * type Keys to email composer's body
	 * 
	 * <b>Parameters</b><br>
	 * CharSequence key - Keys.ENTER is acceptable so far
	 */
	public void typeInMessageBody(CharSequence key) {
		Logging.info("Typing in email composer body: " + key);
		String email_composer_iframe_outer = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_iframe_outer");
		String email_composer_iframe_inner = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_iframe_inner");
		String email_composer_html_body = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_html_body");
		String email_composer_plain_textarea = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_plain_textarea");
		boolean isRichText;
		
		try {
	    	Tools.setDriverDefaultValues();
	    	
	    	// determine whether composer body in HTML format
	    	isRichText = Base.base.getDriver().findElement(By.xpath(email_composer_iframe_outer))
	    			.isDisplayed();
	    	Logging.info("Is email composer body in HTML format: " + isRichText);
	    	
	    	if (isRichText) {
		    	// Switch into outer iframe.
		    /*	WebElement outerFrame = Base.base.getDriver()
		    			.findElement(By.xpath(email_composer_iframe_outer));
		    	Base.base.getDriver().switchTo().frame(outerFrame);
		    	// Switch into inner iframe.
		    	WebElement innerFrame = Base.base.getDriver()
		    			.findElement(By.xpath(email_composer_iframe_inner));
		    	Base.base.getDriver().switchTo().frame(innerFrame);
		    	// Type text in body.*/
		    	Base.base.getDriver().findElement(By.xpath(email_composer_html_body)).sendKeys(key);
		    	if (Tools.isFirefox() || Tools.isIE()) {
		    	    this.fixRichEditorForFireFox();
		    	}
	    	} else {
	    		Base.base.getDriver().findElement(By.xpath(email_composer_plain_textarea)).sendKeys(key);
	    	}
		} catch (WebDriverException ex) {
			Logging.error("Could not type in email composer body");
			throw ex;
		} finally {
			// Switch to default window
			Tools.setDriverDefaultValues();
		}
	}
	
	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Aug 05, 2015<br>
	 * <b>Description</b><br>
	 * fill in the required fields in the Inline Image dialog window
	 * 
	 * <b>Parameters</b><br>
	 * String inputField: the label provided for the input field
	 * 			- Image Title
	 * 			- Image URL
	 * String inputText: the title or URL data for the field
	 */
	public void typeInInlineImageDialog(String inputField, String inputText) {
		
		Logging.info("Typing in Inline Image Dialog field: " + inputField 
					+ "with URL: " +inputText);
		String email_composer_inline_image_dialog_field = null;
		
	    if (inputField.contains("Title")){
		 email_composer_inline_image_dialog_field = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_inline_image_dialog_Title");}
	    else
	    {
	      email_composer_inline_image_dialog_field = PropertyHelper.coreEmailComposer
              .getPropertyValue("email_composer_inline_image_dialog_URL");	 
	    }
		
		try {
            Tools.setDriverDefaultValues();

             // Type text in inline image dialog field.
             Base.base.getDriver().findElement(By.xpath(email_composer_inline_image_dialog_field)).clear();
             Base.base.getDriver().findElement(By.xpath(email_composer_inline_image_dialog_field))
              .sendKeys(inputText);

			
		} catch (WebDriverException ex) {
			Logging.error("Could not type in Inline Image Text Field: " + inputField);
			throw ex;
		} finally {
			// Switch to default window
			Tools.setDriverDefaultValues();
		}
		 Tools.waitFor(1, TimeUnit.SECONDS);
	}
	
	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Aug 05, 2015<br>
	 * <b>Description</b><br>
	 * click on button in the dialog window of the Add Inline Image
	 * 
	 * <b>Parameters</b><br>
	 * String button: bottun in the dialog window of the Add Inline Image
	 * 			- OK
	 * 			- Cancel		
	 */
	public void clickButtonOnInlineImageDialog(String button) {
		
		Logging.info("Clicking the button: \"" + button + "\" in the add inline image dialog window");
		String email_add_inline_image_confirmation_button = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_add_inline_image_confirmation_button").replace("+variable+", button);
		
		try {
			Base.base.getDriver().switchTo().defaultContent();
	    	Base.base.getDriver().manage().timeouts()
	        		.implicitlyWait(Long.parseLong(Base.timeout), TimeUnit.SECONDS);
	    	
	    	
	    	Base.base.getDriver().findElement(By.xpath(email_add_inline_image_confirmation_button)).click();
	    	
	    	if (Tools.isFirefox()) {
	    		Tools.waitFor(2, TimeUnit.SECONDS);
	    	    this.fixRichEditorForFireFox();
	    	}

		} catch (WebDriverException ex) {
			Logging.error("Could not click on button " + button + " in inline image add dialog window");
			throw ex;
		} finally {
			// Switch to default window
			Tools.setDriverDefaultValues();
		}
	}
	
	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Aug 06, 2015<br>
	 * <b>Description</b><br>
	 * verify image existence in message body
	 */
	public boolean verifyImageExistenceInMessageBody(String imageURL) {
    	Logging.info("Verifying image existence in message body");
		String email_composer_image_in_body = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_image_in_body")
				.replace("+variable+", imageURL);
	
		boolean result = false;
    	
    	try {
			Tools.setDriverDefaultValues();
	    			
	    	Base.base.getDriver().findElement(By.xpath(email_composer_image_in_body));
	    	// Image is found.
	    	result = true;
	    	Logging.info("Found image in message body");
    	} catch (WebDriverException ex) {
	    	Logging.info("Could not found image in message body");
		} finally {
			// Switch to default window
			Tools.setDriverDefaultValues();
		}
    	return result;
	}
	
	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 2, 2015<br>
	 * <b>Description</b><br>
	 * Get toolbar text in composer window
	 */
	public String getToolbarText(){
		
		Logging.info("Getting toolbar text in composer window");
		String email_composer_toolbar_text = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_toolbar_text");
		String text;
		
		try {
			Tools.setDriverDefaultValues();	    	
	    	text = Base.base.getDriver().findElement(By.xpath(email_composer_toolbar_text)).getText().trim();
	    	Logging.info("toolbar text in composer window is: " + text);
	    	return text;
		} catch (WebDriverException ex) {
			Logging.error("Could not get toolbar text in composer window");
			throw ex;
		}
	}
	
	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 15, 2015<br>
	 * <b>Description</b><br>
	 * verify the email is in Auto-suggest list
	 * <b>Parameters</b><br>
	 * String email: email address who are expected to display in Auto-suggest list
	 */
	public boolean verifyAutoSuggestItemByEmail(String email) {
		Logging.info("Verifying " + email + " is in Auto-suggest list");

		String email_composer_auto_suggest_item_by_name = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_auto_suggest_item_by_name").replace("+variable+", email);
		
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			
			Base.base.getDriver().findElement(By.xpath(email_composer_auto_suggest_item_by_name));
			result = true;			
		} catch (WebDriverException ex) {
			Logging.info("Could not found " + email + " is in Auto-suggest list");
		}
		Logging.info(email + " is in Auto-suggest list: " + result);
		return result;
	}
	
	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 15, 2015<br>
	 * <b>Description</b><br>
	 * type recipient prefix into the specified field, without any validation check, to launch Auto-Suggest list.
	 * 
	 * <b>Parameters</b><br>
	 * String field - field name. One of the following buttons should be provided:
	 * 			- To
	 * 			- Cc
	 * 			- Bcc
	 * String text - input text of recipient prefix
	 */
	public void typePrefixInField(String field, String text) {
		Logging.info("Typing in email composer " + field + " field with text: " + text);
		String email_composer_recipient_input = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_recipient_input")
				.replace("+variable+", field);
		
		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(email_composer_recipient_input))
				.sendKeys(text);
		} catch (WebDriverException ex) {
			Logging.error("Could not type text in " + field + " field");
		}
	}
	
	/** It's a work round way for running rich editor case on firefox.
	 *  google editor toolbar is hidden after inserting text or emoji.
	 *  update the body's overflow style of rich editor to  make the toolbar be visible
	 * 
	 */
	public void fixRichEditorForFireFox() {
		String email_composer_iframe_outer = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_iframe_outer");
		String email_composer_format_button = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_format_button").replace("+variable+", "Bold");
		Tools.setDriverDefaultValues();

		// Switch into outer iframe.
		WebElement outerFrame = Base.base.getDriver().findElement(By.xpath(email_composer_iframe_outer));
		Base.base.getDriver().switchTo().frame(outerFrame);
		WebElement outerBody = Base.base.getDriver().findElement(By.xpath("//body"));
		String overFlowStyle = outerBody.getCssValue("overflow");
		if (overFlowStyle.contains("hidden")) {
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			js.executeScript("arguments[0].style.overflow = ''", outerBody);
			Tools.waitFor(1, TimeUnit.SECONDS);
			
		}
		Tools.scrollElementIntoViewByXpath(email_composer_format_button);
	}
	
  /**
   * <b>Author</b>: Vivian Xie<br>
   * <b>Date</b>: Dec 14, 2016<br>
   * <b>Description</b><br>
   * click color picker of text or text background
   * <b>Parameters</b><br>
   * String pickerType - One of the following buttons should be provided: - "Text color"  "Text
   * background color"
   */

  public void clickColorPicker(String pickerType) {
    Logging.info("Starting to  click " + pickerType + " color picker ");
    String email_composer_colorpicker = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_composer_colorpicker").replace("+variable+", pickerType);

    try {
      Base.base.getDriver().findElement(By.xpath(email_composer_colorpicker)).click();

    } catch (WebDriverException ex) {
      Logging.error("Could not find  " + pickerType + " color picker");
      throw ex;
    }

  }

  /**
   * <b>Author</b>: Vivian Xie<br>
   * <b>Date</b>: Dec 14, 2016<br>
   * <b>Description</b><br>
   * select color of text or text background <b>Parameters</b><br>
   * String pickerType - One of the following buttons should be provided: - Text color - Text
   * background color Color color-- the specific color
   */
  public void selectColorInColorPicker(String pickerType, Color color) {
    String colorString = Tools.color2RgbString(color);
    Logging.info("Starting to select " + pickerType + " color :" + colorString);
    String email_composer_colorpicker_color_options = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_composer_colorpicker_color_options")
        .replace("+variable1+", pickerType).replace("+variable2+", Tools.color2RgbString(color));

    try {
      Base.base.getDriver().findElement(By.xpath(email_composer_colorpicker_color_options)).click();

    } catch (WebDriverException ex) {
      Logging.error("Could not find  " + pickerType + " color " + color.toString());
      throw ex;
    }

  }

  /**
   * @Method Name: verifyFontStyleIsSelectInToolbar
   * @Author: Vivian Xie
   * @Created On: Dec/30/2016
   * @Description: This method will verify the font style toolbar selected the specific style .
   * @param: font -- value can be "Font" and "Font size".
   */
  public boolean verifyFontStyleIsSelectInToolbar(String font, String style) {
    Logging.info("Verifying if " + font + ": " + style + " selected in toolbars");
    String email_composer_font_style_button = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_composer_font_style_button").replace("+variable+", font);
    String actalStyle = null;
    boolean result = false;

    try {
      actalStyle =
          Base.base.getDriver().findElement(By.xpath(email_composer_font_style_button)).getText();
      Logging.info("Actual Style : " + actalStyle);
    } catch (WebDriverException ex) {
      Logging.error("Could not find " + font + " combine box");
      throw ex;
    }
    if (actalStyle.equals(style))
      result = true;
    Logging.info(font + style + " selected in toolbars:" + result);
    return result;
  }
  /**
   * <b>Author</b>: Vivian Xie<br>
   * <b>Date</b>: Dec 14, 2016<br>
   * <b>Description</b><br> 
   * click on the font style combine box
   * <b>parameter</b><br> String font - font style name. One of the following buttons should be provided: - Font - Font
   * size
   */
  public void clickFontStyle(String font) {
    Logging.info("Starting to  click  " + font + " combine box  ");
    String email_composer_font_style_button = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_composer_font_style_button").replace("+variable+", font);

    try {
      Base.base.getDriver().findElement(By.xpath(email_composer_font_style_button)).click();

    } catch (WebDriverException ex) {
      Logging.error("Could not click " + font);
      throw ex;
    }

  }

  /**
   * <b>Author</b>: Vivian Xie<br>
   * <b>Date</b>: Dec 14, 2016<br>
   * <b>Description</b><br>
   * click on font name of font size combine box . 
   * <b>Parameters:</b><br>
   * String font - font style name. One of the following buttons should be provided: - Font - Font
   * size 
   * String style-- the specific style
   */
  public void selectFontStyle(String font, String style) {
    Logging.info("Starting to  select"+ font + " combine box  ");

    String email_composer_font_style_options =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_font_style_options")
            .replace("+variable1+", font).replace("+variable2+", style);
    Logging.info(email_composer_font_style_options);
    try {
      Base.base.getDriver().findElement(By.xpath(email_composer_font_style_options)).click();
    } catch (WebDriverException ex) {
      Logging.error("Could not select " + font + "style: " + style);
      throw ex;
    }
  }
 
 
  /**
   * <b>Author</b>: Vivian Xie<br>
   * <b>Date</b>: Dec 15, 2016<br>
   * <b>Description</b><br>
   * Verify if the specific content has the correct font name</b><br>
   * <b>parameters:</b>String fontName -- the expected font name String String content -- the
   * content <b>Description:</b> String location-- value can be "compose" and "msg" ("compose" means
   * verifying the value in Editor, "msg" means verifying the value in the mail ) true if the font
   * name is correct ,else false
   */
  public boolean verifyFontName(String fontName, String content, String location) {
    Logging.info("Starting to verify if content: \" " + content + "\" has font name " + fontName);

    String email_composer_font_name_content = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_composer_font_style_content").replace("+variable1+", fontName)
        .replace("+variable2+", content).replace("+variable3+", location);
    Logging.info(email_composer_font_name_content);
    boolean result = false;

    try {
      ArrayList<WebElement> elem = (ArrayList<WebElement>) Base.base.getDriver()
          .findElements(By.xpath(email_composer_font_name_content));
      if (elem.size() > 0)
        result = true;
    } catch (WebDriverException ex) {
    }
    Logging.info("Content: \"" + content + "\" has font name " + fontName + ": " + result);
    return result;
  }


  /**
   * <b>Author</b>: Vivian Xie<br>
   * <b>Date</b>: Dec 15, 2016<br>
   * <b>Description</b><br>
   * Verify if the specific content has the correct font size</b><br>
   * <b>parameters</b>String fontName -- the expected font size String content -- the content String
   * location-- value can be "compose" and "msg" ("compose" means verifying the value in Editor,
   * "msg" means verifying the value in the mail ) <b>return</b>true if the font size is correct
   * ,else false
   */
  public boolean verifyFontSize(String size, String content, String location) {
    Logging.info("Starting to verify if content: \"" + content + "\" has font size " + size);
  
    String email_composer_font_size_content = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_composer_font_style_content").replace("+variable1+", size)
        .replace("+variable2+", content).replace("+variable3+", location);
    Logging.info(email_composer_font_size_content);
    boolean result = false;

    try {
      ArrayList<WebElement> elem = (ArrayList<WebElement>) Base.base.getDriver()
          .findElements(By.xpath(email_composer_font_size_content));
      if (elem.size() > 0)
        result = true;
    } catch (WebDriverException ex) {
    }
    Logging.info("Content: \"" + content + "\" has font size " + size + ": " + result);
    return result;
  }

  /**
   * <b>Author</b>: Vivian Xie<br>
   * <b>Date</b>: Dec 15, 2016<br>
   * <b>Description</b><br>
   * Verify if the specific content has the align</b><br>
   * <b>parameters:</b>String align -- the expected align ,value can be "center","right","justify"
   * String content -- the content String location-- value can be "compose" and "msg" ("compose"
   * means verifying the value in Editor, "msg" means verifying the value in the mail )
   * <b>return</b> true if the align is correct ,else false
   */
	public boolean verifyTextAlign(String align, String content, String location) {
		Logging.info("Starting to verify if content: content \"" + content + "\" has align  " + align);
		String email_composer_text_align_content = null;
		String email_composer_text_align_content2 = null;
		String email_composer_text_align_justify_content = null;
		if (location.equals("msg")) {

			email_composer_text_align_justify_content = PropertyHelper.coreEmailComposer
					.getPropertyValue("email_composer_text_align_justify_content_msg")
					.replace("+variable1+", String.valueOf(align)).replace("+variable2+", content)
					.replace("+variable3+", location);
			
			
			email_composer_text_align_content = PropertyHelper.coreEmailComposer
					.getPropertyValue("email_composer_text_align_content_msg")
					.replace("+variable1+", String.valueOf(align)).replace("+variable2+", content)
					.replace("+variable3+", location);
			
			email_composer_text_align_content2 = PropertyHelper.coreEmailComposer
					.getPropertyValue("email_composer_text_align_content_msg2")
					.replace("+variable1+", String.valueOf(align)).replace("+variable2+", content)
					.replace("+variable3+", location);
			
		} else {
			email_composer_text_align_content = PropertyHelper.coreEmailComposer
					.getPropertyValue("email_composer_text_align_content").replace("+variable1+", String.valueOf(align))
					.replace("+variable2+", content).replace("+variable3+", location);
			email_composer_text_align_justify_content = PropertyHelper.coreEmailComposer
					.getPropertyValue("email_composer_text_align_content").replace("+variable1+", String.valueOf(align))
					.replace("+variable2+", content).replace("+variable3+", location);
			email_composer_text_align_content2 = PropertyHelper.coreEmailComposer
					.getPropertyValue("email_composer_text_align_content_msg2").replace("+variable1+", String.valueOf(align))
					.replace("+variable2+", content).replace("+variable3+", location);
		}
		Logging.info(email_composer_text_align_content);
		Logging.info(email_composer_text_align_justify_content);
		Logging.info(email_composer_text_align_content2);
		boolean result = false;
		try {
			ArrayList<WebElement> elem = (ArrayList<WebElement>) Base.base.getDriver()
					.findElements(By.xpath(email_composer_text_align_content));
			
			ArrayList<WebElement> elem2 = (ArrayList<WebElement>) Base.base.getDriver()
					.findElements(By.xpath(email_composer_text_align_justify_content));
			
			ArrayList<WebElement> elem3 = (ArrayList<WebElement>) Base.base.getDriver()
					.findElements(By.xpath(email_composer_text_align_content2));
			
			if (elem.size() > 0 || elem2.size() > 0 || elem3.size() > 0)
				result = true;
		} catch (WebDriverException ex) {
		}
		Logging.info("Content: \"" + content + "\" has align " + align + ": " + result);
		return result;
	}

  /**
   * <b>Author</b>: Vivian Xie<br>
   * <b>Date</b>: Dec 15, 2016<br>
   * <b>Description</b><br>
   * Verify if the specific list has the correct type</b><br>
   * <b>parameters</b>String type -- the expected type ,value can be "bulleted","numbered" String
   * content -- the content String location-- value can be "compose" and "msg" ("compose" means
   * verifying the value in Editor, "msg" means verifying the value in the mail ) <b>return</b> true
   * if the type is correct ,else false
   */
  public boolean verifyListTyle(String type, String content, String location) {
    Logging
        .info("Starting to verify if content: content \"" + content + "\" is  " + type + " list");
    String listType = null;
    if (type.equalsIgnoreCase("bulleted"))
      listType = "ul";
    else
      listType = "ol";
    String email_composer_type_list_content = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_composer_type_list_content").replace("+variable1+", listType)
        .replace("+variable2+", content).replace("+variable3+", location);
    boolean result = false;
    try {
      Logging.info(email_composer_type_list_content);
      ArrayList<WebElement> elem = (ArrayList<WebElement>) Base.base.getDriver()
          .findElements(By.xpath(email_composer_type_list_content));
      if (elem.size() > 0)
        result = true;
    } catch (WebDriverException ex) {
    }
    Logging.info("Content: \"" + content + "\" is " + type + " list : " + result);
    return result;
  }


  /**
   * <b>Author</b>: Vivian Xie<br>
   * <b>Date</b>: Dec 15, 2016<br>
   * <b>Description</b><br>
   * <b>parameters</b>Verify if the specific text has the correct backgroun color</b><br>
   * Color color -- the expected color String content -- the content String location-- value can be
   * "compose" and "msg" ("compose" means verifying the value in Editor, "msg" means verifying the
   * value in the mail ) <b>return</b> true if the color is correct ,else false
   */
  public boolean verifyBackgroundColor(Color color, String content, String location) {
    String colorString = Tools.color2RgbString(color);
    String colorString2 = Tools.color2RgbString(color).replace(" ", "");
    Logging.info("Starting to verify if content: content \"" + content + "\" background  is  "
        + colorString);
    String email_compose_background_color_content =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_compose_background_color_content")
            .replace("+variable1+", colorString).replace("+variable2+", content)
            .replace("+variable3+", location).replace("+variable4+", colorString2);
    Logging.info(email_compose_background_color_content);
    boolean result = false;
    try {
      ArrayList<WebElement> elem = (ArrayList<WebElement>) Base.base.getDriver()
          .findElements(By.xpath(email_compose_background_color_content));
      if (elem.size() > 0)
        result = true;
    } catch (WebDriverException ex) {
    }
    Logging.info("Content: \"" + content + "\" background color is " + colorString + ":" + result);
    return result;
  }

  /**
   * <b>Author</b>: Vivian Xie<br>
   * <b>Date</b>: Dec 16, 2016<br>
   * <b>Description</b><br>
   * Click on signature comb box</b><br>
   */
  public void clickOnSignatureCombBox() {
    Logging.info("Starting to click on signature comb box");
    String email_compose_signature_comb_input =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_compose_signature_comb_input");
    Logging.info(email_compose_signature_comb_input);

    try {
      Base.base.getDriver().findElement(By.xpath(email_compose_signature_comb_input)).click();
    } catch (WebDriverException ex) {
      Logging.info("Can not click on signature comb box");
      throw ex;
    }

  }

  /**
   * <b>Author</b>: Vivian Xie<br>
   * <b>Date</b>: Dec 16, 2016<br>
   * <b>Description</b><br>
   * Select option on signature comb box</b><br>
   * <b>Parameter:</b> String signatureName -- the signature name to be selected
   */
  public void selectSignature(String signatureName) {
    Logging.info("Starting to select signature " + signatureName);
    String email_compose_signature_options = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_compose_signature_options").replace("+variable+", signatureName);
    Logging.info(email_compose_signature_options);
    try {
      Base.base.getDriver().findElement(By.xpath(email_compose_signature_options)).click();
    } catch (WebDriverException ex) {
      Logging.info("Can not click on signature:" + signatureName);
      throw ex;
    }
  }


  /**
   * <b>Author</b>: Vivian Xie<br>
   * <b>Date</b>: Dec 16, 2016<br>
   * <b>Description</b><br>
   * Verify if tooltip of toolbar button is correct</b><br>
   * <b>Parameter:</b> String buttonName -- the name of the button the value of button name can be:
   * bold,italics,underline,bullet-list,numbered-list,decrease-indent,emoji,
   * increase-indent,align-left,align-center,align-right,justify,insert-image
   * 
   * 
   * String tooltip -- the expected tooltip
   */
  public boolean verifyComposerToolbarButtonTooltip(String buttonName, String tooltip) {
    Logging.info("Verifying if button  " + buttonName + " tooltip is " + tooltip);
    String email_composer_editor_button = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_composer_editor_button").replace("+variable+", buttonName);
    String actualTooltip = null;
    boolean result = false;
    Logging.info(email_composer_editor_button);
    try {
      actualTooltip = Base.base.getDriver().findElement(By.xpath(email_composer_editor_button))
          .getAttribute("title");
      Logging.info(actualTooltip);
      if (actualTooltip.equals(tooltip))
        result = true;
    } catch (WebDriverException ex) {
      Logging.info("Can not find toolbar button " + buttonName);
      throw ex;
    }

    Logging.info("Button  " + buttonName + " tooltip is " + tooltip + ": " + result);
    return result;

  }
  
  /**
   * <b>Author</b>: Vivian Xie<br>
   * <b>Date</b>: Dec 16, 2016<br>
   * <b>Description</b><br>
   * Verify if tooltip of toolbar button is correct</b><br>
   * <b>Parameter:</b> String buttonName -- the name of the button the value of button name can be:
   * bold,italics,underline,bullet-list,numbered-list,decrease-indent,emoji,
   * increase-indent,align-left,align-center,align-right,justify,insert-image
   * 
   * 
   * String tooltip -- the expected tooltip
   */
  public boolean verifyComposerLinkButtonTooltip(String tooltip) {
    Logging.info("Verifying if Link Button   tooltip is " + tooltip);
    String email_composer_link_button =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_link_button");
    String actualTooltip = null;
    boolean result = false;
    Logging.info(email_composer_link_button);
    try {
      actualTooltip = Base.base.getDriver().findElement(By.xpath(email_composer_link_button))
          .getAttribute("title");
      Logging.info(actualTooltip);
      if (actualTooltip.equals(tooltip))
        result = true;
    } catch (WebDriverException ex) {
      Logging.info("Can not Link button ");
      throw ex;
    }

    Logging.info("Link Button  tooltip is " + tooltip + ": " + result);
    return result;

  }



  /**
   * <b>Author</b>: Vivian Xie<br>
   * <b>Date</b>: Dec 16, 2016<br>
   * <b>Description</b><br>
   * Verify if tooltip of Insert Image button</b><br>
   * <b>Parameter:</b> String imageType -- value can be "inline" and "local" String tooltip -- the
   * expected tooltip
   */
  public boolean verifyComposerInsetImageButtonTooltip(String imageType, String tooltip) {
    Logging.info("Verifying if Insert  " + imageType + "Image button tooltip is " + tooltip);
    String sImageType = null;
    if (imageType.equals("Local"))
      sImageType = "localImage";
    else
      sImageType = "picture";

    String email_compose_insert_image_button = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_compose_insert_image_button").replace("+variable+", sImageType);
    String actualTooltip = null;
    boolean result = false;
    Logging.info(email_compose_insert_image_button);
    try {
      actualTooltip = Base.base.getDriver().findElement(By.xpath(email_compose_insert_image_button))
          .getAttribute("title");
      Logging.info(actualTooltip);
      if (actualTooltip.equals(tooltip))
        result = true;
    } catch (WebDriverException ex) {
      Logging.info("Can not find Insert " + imageType + " image button");
      throw ex;
    }
    Logging.info("Insert  " + imageType + " Image button tooltip is " + tooltip + ": " + result);
    return result;
  }

  /**
   * <b>Author</b>: Vivian Xie<br>
   * <b>Date</b>: Dec 16, 2016<br>
   * <b>Description</b><br>
   * Verify if tooltip of Color Picker button</b><br>
   * <b>Parameter:</b> String colorPickerType -- value can be "Text color" and "Text background
   * color" String tooltip -- the expected tooltip
   */
  public boolean verifyComposerColorPickerButtonTooltip(String colorPickerType, String tooltip) {
    Logging.info("Verifying if " + colorPickerType + " color picker  button tooltip is " + tooltip);
    String email_compose_color_picker_button =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_compose_color_picker_button")
            .replace("+variable+", colorPickerType);
    String actualTooltip = null;
    boolean result = false;
    Logging.info(email_compose_color_picker_button);
    try {
      actualTooltip = Base.base.getDriver().findElement(By.xpath(email_compose_color_picker_button))
          .getAttribute("title");
      Logging.info(actualTooltip);
      if (actualTooltip.equals(tooltip))
        result = true;
    } catch (WebDriverException ex) {
      Logging.info("Can not find " + colorPickerType + " color picker");
      throw ex;
    }
    Logging.info(colorPickerType + " color picker  tooltip is " + tooltip + ": " + result);
    return result;
  }

  /**
   * <b>Author</b>: Vivian Xie <br>
   * <b>Date</b>: Dec 19, 2016<br>
   * <b>Description</b><br>
   * Select all the message body
   * 
   */
  public void selectAllTextByKeyBoard() {
    Logging.info("Start to click CTL+A");
    String selectAll = Keys.chord(Keys.CONTROL, "a");
    try {
      new Actions(Base.base.getDriver()).sendKeys(selectAll).build().perform();
    } catch (NoSuchElementException ex) {
      ex.printStackTrace();
      throw ex;
    }
  }

  /**
   * @Method Name: verifyEditorHasBorder
   * @Author: Vivian Xie
   * @Created On: 12/22/2016
   * @Description: verify if the editor has border
   */
  public boolean verifyEditorHasBorder() {
    String email_composer_editor_table =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_editor_table");
    boolean result = true;
    String style = null;
    try {
      style = Base.base.getDriver().findElement(By.xpath(email_composer_editor_table))
          .getAttribute("style");       
    } catch (NoSuchElementException ex) {
      ex.printStackTrace();
      throw ex;
    }
    if (style.contains("border: none"))
      result = false;
    Logging.info("The composer border displays:"+result );  
    return result;
  }
  
  /**
   * @Method Name: verifyToolBarButtonDisplay
   * @Author: Vivian Xie
   * @Created On: 12/23/2016
   * @Description: verify if the editor toolbar is display
   */
  public boolean verifyToolBarButtonDisplay() {
    Logging.info("verifying if the composer toolbar displays");
    String email_composer_editor_toolbar =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_editor_toolbar");
    boolean result = false;
    try {
      result = Base.base.getDriver()
          .findElement(By.xpath(email_composer_editor_toolbar)).isDisplayed();
    } catch (NoSuchElementException ex) {
      ex.printStackTrace();
      throw ex;
    }
    return result;
  }
  
  
  /**
   * @Method Name: uploaLocalImage
   * @Author: Vivian Xie
   * @Created On: Dec/23/2016
   * @Description: This method will upload local file to mail body being composed.
   * @parameter: String filename - file name.
   */
  public void uploaLocalImage(String filename) {
    Logging.info("Uploading file: " + filename);

    String email_composer_upload_local_file_input =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_upload_local_file_input");
    String email_composer_image_upload_success =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_image_upload_success");

    // All win7 systems have media folder here
    String fullpath = "C:\\Automation\\media\\" + filename;

    try {
      WebElement element =
          Base.base.getDriver().findElement(By.xpath(email_composer_upload_local_file_input));
      JavascriptExecutor j = (JavascriptExecutor) Base.base.getDriver();
      j.executeScript(
          "arguments[0].setAttribute('style', 'display: block; height: 1px; width: 1px; visibility: visible');",
          element);
      Tools.setDriverDefaultValues();
      Base.base.getDriver().findElement(By.xpath(email_composer_upload_local_file_input))
          .sendKeys(fullpath);

      Tools.waitUntilElementDisplayedByXpath(email_composer_image_upload_success,
          this.timeoutMedium);
      Logging.info("Image upload successfully");

    } catch (NoSuchElementException ex) {
      Logging.error("Could not upload file: " + filename);
      throw ex;
    }
  }
  /**
   * @Method Name: verifyImageUploadedByName
   * @Author: Vivian Xie
   * @Created On: Dec/26/2016
   * @Description: This method will verify if image has been upload to composer.
   * @parameter: String filename - file name.
   * * @return: true if uploaded successfully  false if failed to upload
   */
  public boolean verifyImageUploadedByName() {
    Logging.info("Verifying if image is uploaded successfully ");

    boolean result = false;
    String email_composer_image_upload_success =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_image_upload_success");
    ArrayList<WebElement> list = null;

    try {
      list = (ArrayList<WebElement>) Base.base.getDriver()
          .findElements(By.xpath(email_composer_image_upload_success));
    } catch (NoSuchElementException ex) {
      Logging.error("Webdriver error when finding upload inage");
    }
    if (list.size() == 1)
      result = true;
    return result;
  }
  /**
   * @Method Name: String
   * @Author: Vivian Xie
   * @Created On: Dec/28/2016
   * @Description: This method will verify if link has been successfully added.
   * @parameter: String linkTitle - expected link title String linkURL - expected link url String
   *             location - value can be "compose" and "msg" .
   * @return: true if exists  false if does not exist
   */
  public boolean verifyLinkExist(String linkTitle, String linkURL, String location) {
    Logging.info("Verifying if link exists.  title: " + linkTitle + ", url " + linkURL);

    boolean result = false;
    String email_composer_link_in_mail =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_link_in_mail")
            .replace("+variable1+", linkTitle).replace("+variable2+", linkURL).replace("+variable3+", location);
    Logging.info(email_composer_link_in_mail);
    ArrayList<WebElement> list = null;
    try {
      list = (ArrayList<WebElement>) Base.base.getDriver()
          .findElements(By.xpath(email_composer_link_in_mail));
    } catch (NoSuchElementException ex) {
      Logging.error("Webdriver error when finding link");
    }
    if (list.size() == 1)
      result = true;
    return result;
  }
  /**
   * @Method Name: clickOnLink
   * @Author: Vivian Xie
   * @Created On: Dec/28/2016
   * @Description: This method will click on link.
   * @parameter: String linkTitle -  link title String linkURL -  link url String
   *             location - value can be "compose" and "msg" .
   */
  public void clickOnLink(String linkTitle, String linkURL, String location) {
    Logging.info("Clicking on link.  title: " + linkTitle + ", url " + linkURL + " in " + location);
    String email_composer_link_in_mail = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_composer_link_in_mail").replace("+variable1+", linkTitle)
        .replace("+variable2+", linkURL).replace("+variable3+", location);
    Logging.info(email_composer_link_in_mail);
    try {
      Base.base.getDriver().findElement(By.xpath(email_composer_link_in_mail)).click();;
    } catch (NoSuchElementException ex) {
      Logging.error("Webdriver error when finding link");
      throw ex;
    }

  }
  
  /**
   * @Method Name: clickOnLink
   * @Author: Vivian Xie
   * @Created On: Dec/28/2016
   * @Description: This method will click on link.
   * @parameter: String buttonName - button name value can be "Go to link", "Change", "Remove"
   */
  public void clickOnLinkOperateButton(String buttonName){
    Logging.info("Clicking on link " + buttonName + " button");
    String enail_composer_operate_link_button = PropertyHelper.coreEmailComposer
        .getPropertyValue("enail_composer_operate_link_button").replace("+variable+", buttonName);
    try {
      Logging.info(enail_composer_operate_link_button);
      Tools.waitUntilElementDisplayedByXpath(enail_composer_operate_link_button, 2);
      Base.base.getDriver().findElement(By.xpath(enail_composer_operate_link_button)).click();
      Logging.info(enail_composer_operate_link_button);
    } catch (NoSuchElementException ex) {
      Logging.error("Webdriver error when finding link");
      throw ex;
    }
  }
  /**
   * @Method Name: verifyGoToLinkSuccessful
   * @Author: Vivian Xie
   * @Created On: Dec/28/2016
   * @Description: This method will verify if Go to link button can go to the specific page.
   * @parameter: String linkTitle - expected link title String linkURL - expected link url String
   * @return: true if successful  false if failed
   */
  public boolean verifyGoToLinkSuccessful(String linkURL) {
    Logging.info("Verifying if Go to link successful.   url: " + linkURL);

    boolean result = false;
    String email_composer_go_to_link_successful =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_go_to_link_successful")
            .replace("+variable1+", linkURL);
    Logging.info(email_composer_go_to_link_successful);
    ArrayList<WebElement> list = null;
    try {
      list = (ArrayList<WebElement>) Base.base.getDriver()
          .findElements(By.xpath(email_composer_go_to_link_successful));
    } catch (NoSuchElementException ex) {
      Logging.error("Webdriver error when finding Go to link button ");
    }
    if (list.size() == 1)
      result = true;
    return result;
  }
  
  /**
   * @Method Name: verifyMailBodyContentIgnoreFormat
   * @Author: Vivian Xie
   * @Created On: Dec/29/2016
   * @Description: This method will verify if mail body equals the expect string ignoring format.
   * @parameter: String expectedMailBody - expected mail body
   * @return: true if equals false if not
   */
  public boolean verifyMailBodyContentIgnoreFormat(String expectedMailBody) {
    Logging.info("Verifying if mail body is " + expectedMailBody);

    boolean result = false;
    String email_composer_body_ignore_format =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_body_ignore_format")
            .replace("+variable+", expectedMailBody);
    Logging.info(email_composer_body_ignore_format);
    ArrayList<WebElement> list = null;
    try {
      list = (ArrayList<WebElement>) Base.base.getDriver()
          .findElements(By.xpath(email_composer_body_ignore_format));
    } catch (NoSuchElementException ex) {
      Logging.error("Webdriver error when verifying mail body content ");
    }
    if (list.size() == 1)
      result = true;
    return result;
  }
  /**
   * @Method Name: verifyMailBodyContent
   * @Author: Vivian Xie
   * @Created On: Dec/29/2016
   * @Description: This method will verify if mail body equals the expect string.
   * @parameter: String expectedMailBody - expected mail body  String format - "Pain" or "Rich"
   * @return: true if equals  false if not
   */
  public boolean verifyMailBodyContent(String expectedMailBody, String format) {
    Logging.info("Verifying if mail body is " + expectedMailBody);
    String email_composer_body = null;
    String mailBodyContent = null;
    boolean result = false;
    if(format.equals("Plain")){
     email_composer_body=
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_plain_textarea");
     mailBodyContent = Base.base.getDriver().findElement(By.xpath(email_composer_body)).getAttribute("value").trim();   
    }
    else{
      email_composer_body=
          PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_html_body");
      mailBodyContent = Base.base.getDriver().findElement(By.xpath(email_composer_body)).getText();
    }
    Logging.info("mail body is "+mailBodyContent);
    if (mailBodyContent.equals(expectedMailBody.trim()))
      result = true;
    return result;
  }
  
  /**
   * getMailBodyContent
   * 
   * @Author Vivian Xie
   * @Created On April/24/2016
   * @return mail content in composer.
   */
  public String getMailBodyContent() {
    Logging.info("Starting to get mail content in composer");
    String email_composer_iframe_outer =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_iframe_outer");
    boolean isRichText;
    String email_composer_body = null;
    String mailBodyContent = null;

    try {
      // determine whether composer body in HTML format
      isRichText =
          Base.base.getDriver().findElement(By.xpath(email_composer_iframe_outer)).isDisplayed();
      Logging.info("Is email composer body in HTML format: " + isRichText);
      if (isRichText == false) {
        email_composer_body =
            PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_plain_textarea");
        mailBodyContent = Base.base.getDriver().findElement(By.xpath(email_composer_body))
            .getAttribute("value").trim();
      } else {
        email_composer_body =
            PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_html_body");
        mailBodyContent =
            Base.base.getDriver().findElement(By.xpath(email_composer_body)).getText();
      }
    } catch (WebDriverException ex) {
      Logging.info("Failed to get composer message body");

    }
    Logging.info("mail body is " + mailBodyContent);
    return mailBodyContent;
  }
  
  
  /**
   * @Method Name: verifyRichEditorScrollBarExist
   * @Author: Vivian Xie
   * @Created On: Dec/29/2016
   * @Description: This method will verify if the rich composer editor has scroll bar.
   * @return: true if has false if not
   */
  public boolean verifyRichEditorScrollBarExist() {
    Logging.info("Verifying if the rich composer editor has scroll bar");
    boolean result = false;
    Long clientHeight = null;
    Long scrollHeight = null;
    WebElement ele = null;
    String email_composer_html_body =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_html_body");
    String jsStringclientHeight =
        "var demo = arguments[0].clientHeight; return demo";
    String jsStringscrollHeight =
        "var demo = arguments[0].scrollHeight; return demo";
    try {
      ele = Base.base.getDriver().findElement(By.xpath(email_composer_html_body));
      JavascriptExecutor j = (JavascriptExecutor) Base.base.getDriver();
      clientHeight = (Long) j.executeScript(jsStringclientHeight, ele);
      scrollHeight = (Long) j.executeScript(jsStringscrollHeight, ele);
      Logging.info("clientHeight="+clientHeight);
      Logging.info("scrollHeight="+scrollHeight);
    } catch (NoSuchElementException ex) {
      Logging.error("Webdriver error when geting clientHeight and scrollHeight ");
    }
    if (clientHeight < scrollHeight) {
      result = true;
    }
    Logging.info("the rich composer editor has scroll bar:"+result);
    return result;
  }
  
  /**
   * @Method Name: clickOnParagraphInRichEidor
   * @Author: Vivian Xie
   * @Created On: Dec/29/2016
   * @Description: This method will click on the specific element in rich editor.
   * @param: elemenType -- value can be "p", "ul","ol" paragraphNumber --the specific element
   *         number.
   */
  public void clickOnElementInRichEidor(String elemenType, int number) {
    Logging.info("clicking on the  the " + number + " " + elemenType + " in rich editor");

    String elementMark = null;
    if (elemenType.equals("paragraph"))
      elementMark = "p";
    else if (elemenType.equals("list"))
      elementMark = "li";

    String email_composer_html_body_paragraph =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_html_body_paragraph")
            .replace("+variable+", String.valueOf(number)).replace("+variable2+", elementMark);
    try {
      WebDriver driver = Base.base.getDriver();
      Actions action = new Actions(driver);
      action.click(driver.findElement(By.xpath(email_composer_html_body_paragraph))).build()
          .perform();
    } catch (NoSuchElementException ex) {
      Logging.error("Webdriver error when finding paragraph in rich editor ");
    }

  }
  
  
  /**
   * @Method Name: selectTheSpecificparagraphInRichEidor
   * @Author: Vivian Xie
   * @Created On: Dec/30/2016
   * @Description: This method will select the specific paragraph in rich editor.
   * @param: paragraphNumber -- the specific paragraph number .
   */
  public void selectTheSpecificLineInRichEidor(int paragraphNumber) {
    Logging.info("Select Paragraph " + paragraphNumber + "in rich editor");
    String email_composer_html_body_paragraph =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_html_body_paragraph")
            .replace("+variable+", String.valueOf(paragraphNumber)).replace("+variable2+", "p");
    try {
        Base.base.getDriver().findElement(By.xpath(email_composer_html_body_paragraph)).click();
        this.typeInMessageBody(Keys.HOME);
        new Actions(Base.base.getDriver()).keyDown(Keys.SHIFT).build().perform();
        Base.base.getDriver().findElement(By.xpath(email_composer_html_body_paragraph)).click();
        new Actions(Base.base.getDriver()).keyUp(Keys.SHIFT).build().perform();
      } catch (NoSuchElementException ex) {
        Logging.error("Webdriver error when selecting paragraph in rich editor ");
        ex.printStackTrace();
      }
  }
  
 
  /**
   * Click on dock windows pop up button on Composer windows
   * 
   * @Author Vivian Xie
   * @Created May/9/2017
   */
  public void clickOnDockWindowPopupButton() {

    Logging.info("Starting to click on dock windows pop up button");
    String email_composer_dock_windows_pop_up_button = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_composer_dock_windows_pop_up_button");
    try {
      Base.base.getDriver().findElement(By.xpath(email_composer_dock_windows_pop_up_button))
          .click();
    } catch (NoSuchElementException ex) {
      Logging.error("Exception to find dock windows pop up button ");
      throw ex;
    }

    Tools.waitFor(5, TimeUnit.SECONDS);

  }


  /**
   * Click close button in compose dock window
   * 
   * @Author Vivian Xie
   * @Created May/12/2017
   */
  public void clickDockWindowsCloseButton() {
    Logging.info("Starting to click on close button on dock window.");
    String email_composer_dock_window_close_button = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_composer_dock_window_close_button");

    try {
      Tools.scrollElementIntoViewByXpath(email_composer_dock_window_close_button);
      Base.base.getDriver().findElement(By.xpath(email_composer_dock_window_close_button)).click();
    } catch (NoSuchElementException ex) {
      Logging.error("Exception to find dock windows close button ");
      throw ex;
    }

  }

  /**
   * Verify if "Saved at" text exists on composer
   * 
   * @Author Vivian Xie
   * @Created  May/12/2017
   */
  public boolean verifySavedAtTextExistInComposer() {
    Logging.info("Starting to verify if \"Saved at\" text exists in composer");
    String email_composer_saved_at_text =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_saved_at_text");
    boolean result = false;
    
    try {
      result =
          Base.base.getDriver().findElement(By.xpath(email_composer_saved_at_text)).isDisplayed();
    } catch (NoSuchElementException ex) {
      Logging.error("Exception to find \"Saved at\" ");
      throw ex;
    }
    Logging.info("Saved at text exists in composer:" + result);
    return result;
  }


  /**
   * Click on Options button on composer
   * 
   * @Author Vivian Xie
   * @Created May/12/2017
   */
  public void clickOnOptionsButtonInComposer() {
    Logging.info("Starting to click on Options button in Composer");

    String email_composer_options_button =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_options_button");

    try {
      Tools.scrollElementIntoViewByXpath(email_composer_options_button);
      Base.base.getDriver().findElement(By.xpath(email_composer_options_button)).click();
    } catch (NoSuchElementException ex) {
      Logging.error("Failed to click on Options button in Composer");
      throw ex;
    }
  }


  /**
   * check/uncheck Send Receipts check box
   * 
   * @Author Vivian Xie
   * @param  text of the checkbox
   * @param check -- check the checkbox if this value is true, uhcheck it of it is false
   * @Created May/12/2017
   */
  public void checkSendRecepientCheckBox(String optionText, boolean check) {
    Logging.info("Starting to " + (check == true ? "check " : "uncheck ") + optionText
        + " option in Composer");

    String email_composer_send_options_checkbox = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_composer_send_options_checkbox").replace("+variable+", optionText);
    String email_composer_send_options_checkbox_item = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_composer_send_options_checkbox_item")
        .replace("+variable+", optionText);
    WebElement checkBox = null;
    try {
      Tools.moveFocusToElementByXpath(email_composer_send_options_checkbox);
      checkBox = Base.base.getDriver().findElement(By.xpath(email_composer_send_options_checkbox));
      if (isCheckBoxChecked(checkBox) != check) {
        Base.base.getDriver().findElement(By.xpath(email_composer_send_options_checkbox_item))
            .click();
      }
    } catch (NoSuchElementException ex) {
      Logging.error("Failed to click on Options button in Composer");
      throw ex;
    }

  }


  /**
   * Verify if the given checkbox is checked
   * 
   * @Author Vivian Xie
   * @param webElement -- the checkbox that will be be checked
   * @return true if the checkbox is checked, false if not
   * @Created May/12/2017
   */
  public boolean isCheckBoxChecked(WebElement webElement) {

    String classString = null;
    boolean result = false;
    try {
      classString = webElement.getAttribute("class");
      Logging.info("The class name is " + classString);
      if (classString.contains("cb-checked")) {
        result = true;
      }
    } catch (WebDriverException ex) {

      throw ex;
    }

    return result;
  }


  /**
   * Verify if the priority combine box value has the expected value
   * 
   * @Author Vivian Xie
   * @param expectValue -- the expected value
   * @return true if the value is as expected, false if not
   * @Created May/16/2017
   */
  public boolean verifyPriorityCombineBoxValue(String expectValue) {

    String email_composer_priority_selected_option =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_priority_selected_option");
    boolean result = false;
    String actualValue = null;
    try {
      actualValue = Base.base.getDriver().findElement(By.xpath(email_composer_priority_selected_option))
          .getText();
    } catch (WebDriverException ex) {
      throw ex;
    }
    if (actualValue.trim().equals(expectValue.trim())) {
      result = true;
    }

    return result;
  }


  /**
   * Click on the priority combine box
   * 
   * @Author Vivian Xie
   * @Created May/16/2017
   */
  public void clickOnPriorityCombineBox() {
    String email_composer_priority_combine_box =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_priority_combine_box");
    try {
      Base.base.getDriver().findElement(By.xpath(email_composer_priority_combine_box)).click();
    } catch (WebDriverException ex) {
      throw ex;
    }
  }


  /**
   * Click on the specify priority option
   * 
   * @Author Vivian Xie
   * @param optionText -- the text of the option to be selected
   * @Created May/16/2017
   */
  public void clickOnPriorityOption(String optionText) {
    String email_composer_priority_option = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_composer_option_menu");
    email_composer_priority_option = String.format(email_composer_priority_option, optionText);
    
    try {
      Base.base.getDriver().findElement(By.xpath(email_composer_priority_option)).click();
    } catch (WebDriverException ex) {
      throw ex;
    }
  }


  /**
   * Click on the specify option menu
   *  
   * @Author Fiona Zhang
   * @param option -- the text of option menu, value should be "Priority" and "Send Receipts" with case insensive
   */
	public void clickButtonOnOptionMenu(String option) {
		String email_composer_option_menu = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_option_menu");
		email_composer_option_menu = String.format(email_composer_option_menu, option);
		Logging.info(email_composer_option_menu);
		try {
			Tools.moveFocusToElementByXpath(email_composer_option_menu);
			Tools.waitFor(5, TimeUnit.SECONDS);
			//Base.base.getDriver().findElement(By.xpath(email_composer_option_menu)).click();
		} catch (WebDriverException ex) {
			throw ex;
		}
	}
	
	public void clickPriorityDropdown() {
		String email_composer_priority_dropdown = PropertyHelper.coreEmailComposer
				.getPropertyValue("email_composer_priority_dropdown");
		Logging.info(email_composer_priority_dropdown);
		try {
			Base.base.getDriver().findElement(By.xpath(email_composer_priority_dropdown)).click();
		} catch (WebDriverException ex) {
			throw ex;
		}
	}
  
	
  /**
   * Close dock window
   *  
   * @Author Charlie Li
   * @Create August 29, 2017
   */
  public void closeDockWindow() {
	  Logging.info("current window: " + Base.base.getDriver().getTitle());
	  Logging.info("WindowHandles().size:" + Base.base.getDriver().getWindowHandles().size());
		try {
			Base.base.getDriver().close();
			Set<String> handles = Base.base.getDriver().getWindowHandles();
	    	for(String h : handles) {
	    		Base.base.getDriver().switchTo().window(h);
	    	}
			Logging.info("WindowHandles().size:" + Base.base.getDriver().getWindowHandles().size());
		} catch (WebDriverException ex) {
		  throw ex;
	  }
	}
  

  /**
   * <b>Author</b>: Fiona Zhang<br>
   * <b>Date</b>: August 30, 2017<br>
   * <b>Description</b><br>
   * Click the attach button in compose view
   */
  public void clickAttachFileButton() {
    Logging.info("Click the attach button in compose view");
    String loading_bar = PropertyHelper.pageObjectBaseFile.getPropertyValue("common_load_mask");
    String email_composer_attach_button =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_attach_button");
    try {
      Base.base.getDriver().findElement(By.xpath(email_composer_attach_button)).click();
      Tools.waitUntilElementNotDisplayedByXpath(loading_bar, 10);
    } catch (WebDriverException ex) {
      Logging.error("Fail to click attach button");

    }
  }

  /**
   * <b>Author</b>: Fiona Zhang<br>
   * <b>Date</b>: August 30, 2017<br>
   * <b>Description</b><br>
   * Click the attach button in compose view
   * 
   * @param location - key words should be "cloud" or "local"
   */
  public void selectAttachFileLocation(String location) {
    Logging.info("Click the attach from " + location + " button");
    String email_attachment_location = null;
    if (location.equals(CoreEmailComposer.CLOUD)) {
      email_attachment_location =
          PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_attach_from_cloud");
    } else {
      email_attachment_location =
          PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_attach_from_local");
    }

    try {
      Base.base.getDriver().findElement(By.xpath(email_attachment_location)).click();
      this.waitForLoadMaskDismissed();
    } catch (WebDriverException ex) {
      Logging.error("Fail to click attach from " + location + " button");

    }
  }

  /**
   * <b>Author</b>: Fiona Zhang<br>
   * <b>Date</b>: August 30, 2017<br>
   * <b>Description</b><br>
   * Chose to upload cloud file as link or attachment
   * 
   * @param type - key words should be "attachment" or "link"
   */
  public void clickAttachFileRadioButton(String type) {
    Logging.info("upload the attach as " + type);
    String email_attchment_type = null;
    if (type.equals(CoreEmailComposer.ATTACHMENT)) {
      email_attchment_type = PropertyHelper.coreEmailComposer
          .getPropertyValue("email_composer_attach_as_attachment_radio_button");
    } else {
      email_attchment_type = PropertyHelper.coreEmailComposer
          .getPropertyValue("email_composer_attach_as_link_radio_button");
    }

    try {
      Base.base.getDriver().findElement(By.xpath(email_attchment_type)).click();
    } catch (WebDriverException ex) {
      Logging.error("Fail to upload attach as " + type);

    }

  }

  /**
   * <b>Author</b>: Fiona Zhang<br>
   * <b>Date</b>: August 30, 2017<br>
   * <b>Description</b><br>
   * Select a file from cloud file broswer
   * 
   * @param fileName - the file name
   */
  public void selectCloudFile(String fileName) {
    Logging.info("select file : " + fileName + " in cloud file browser");
    String email_composer_cloud_file_name =
        PropertyHelper.coreEmailComposer.getPropertyValue("email_composer_cloud_file_name");
    email_composer_cloud_file_name = String.format(email_composer_cloud_file_name, fileName);
    try {
      Tools.waitUntilElementDisplayedByXpath(email_composer_cloud_file_name, 20);
      Tools.scrollElementIntoViewByXpath(email_composer_cloud_file_name);
      Base.base.getDriver().findElement(By.xpath(email_composer_cloud_file_name)).click();
    } catch (WebDriverException ex) {
      Logging.error("Fail to select attachment");

    }
  }

  /**
   * <b>Author</b>: Fiona Zhang<br>
   * <b>Date</b>: August 30, 2017<br>
   * <b>Description</b><br>
   * Click the button name of the tool bar in file browser view
   * 
   * @param buttonName - the name of button
   */
  public void clickButtonOnCloudFileToolbar(String buttonName) {
    Logging.info("Click the button " + buttonName + " in file browser tool bar");
    String toolbar_button = null;
    String loading_bar = PropertyHelper.pageObjectBaseFile.getPropertyValue("common_load_mask");

    switch (buttonName) {
      case "attach":
        toolbar_button = PropertyHelper.coreEmailComposer
            .getPropertyValue("email_composer_cloud_upload_toolbar_attach");
        break;
      case "cancel":
        toolbar_button = PropertyHelper.coreEmailComposer
            .getPropertyValue("email_composer_cloud_upload_toolbar_cancel");
        break;
      case "add folder":
        toolbar_button = PropertyHelper.coreEmailComposer
            .getPropertyValue("email_composer_cloud_upload_toolbar_add_folder");
        break;
      case "select folder":
        toolbar_button = PropertyHelper.coreEmailComposer
            .getPropertyValue("email_composer_cloud_upload_toolbar_select_folder");
        break;

    }
    try {
    	Tools.scrollElementIntoViewByXpath(toolbar_button);
      Base.base.getDriver().findElement(By.xpath(toolbar_button)).click();
      Tools.waitUntilElementNotDisplayedByXpath(loading_bar, 10);

    } catch (WebDriverException ex) {
      Logging.error("Fail select file in cloud file browser");
      throw ex;
    }
  }


  /**
   * <b>Author</b>: Fiona Zhang<br>
   * <b>Date</b>: Sep 05, 2017<br>
   * <b>Description</b><br>
   * Verify the upload progress bar displayed
   * 
   * @param fileName - the file name of attachment
   */
  public boolean verifyAttachmentUploadProgressBarDisplayed(String fileName) {
    Logging.info("Verify if the attachment : " + fileName + " loading bar displayed");
    boolean result = false;
    String email_composer_cloud_upload_progress_bar = PropertyHelper.coreEmailComposer
        .getPropertyValue("email_composer_cloud_upload_progress_bar");
    email_composer_cloud_upload_progress_bar =
        String.format(email_composer_cloud_upload_progress_bar, fileName);
    try {
      result = Base.base.getDriver().findElement(By.xpath(email_composer_cloud_upload_progress_bar))
          .isDisplayed();
      Tools.waitUntilElementNotDisplayedByXpath(email_composer_cloud_upload_progress_bar, 20);
    } catch (WebDriverException ex) {
      throw ex;

    }
    return result;

  }
}


