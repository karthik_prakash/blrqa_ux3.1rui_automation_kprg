package com.synchronoss.pageobject;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;

import com.synchronoss.core.Base;
import com.synchronoss.core.Logging;
import com.synchronoss.core.PropertyHelper;
import com.synchronoss.core.Tools;

public class CoreNavigation extends PageObjectBase {

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Jun 3, 2015<br>
	 * <b>Description</b><br>
	 * Clicks on the specified navigation tab.
	 * 
	 * @param tab
	 *            tab name
	 */
	private void clickNavigationTab(String tab) {
		String tabLocator = "";
		if (tab.toLowerCase().contains("settings")) {
			tabLocator = PropertyHelper.coreNavigationFile.getPropertyValue("navi_settings_tab");
		} else {
			tabLocator = PropertyHelper.coreNavigationFile.getPropertyValue("navi_common_tab").replace("+variable+",
					tab);
		}
		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(tabLocator);
			Base.base.getDriver().findElement(By.xpath(tabLocator)).click();
			this.waitForLoadMaskDismissed();

		} catch (WebDriverException ex) {
			Logging.error("Could not click on " + tab + " tab in navigation bar");
			throw ex;
		}
	}

	/**
	 * @Method_Name: clickMailTab
	 * @Author: Jerry Zhang
	 * @Created_Date: 03/17/2014
	 * @Description: This method simulates the user clicking on "Mail" tab on
	 *               Navigation pane.
	 */
	public void clickMailTab() {
		Logging.info("Clicking on Mail tab in navigation bar");
		this.clickNavigationTab("Mail");
	}

	/**
	 * @Method_Name: clickContactsTab
	 * @Author: Jerry Zhang
	 * @Created_Date: 03/17/2014
	 * @Description: This method simulates the user clicking on "Contacts" tab on
	 *               Navigation pane.
	 */
	public void clickContactsTab() {
		Logging.info("Clicking on Contacts tab in navigation bar");
		this.clickNavigationTab("Contacts");
	}

	/**
	 * @Method_Name: clickCalendarTab
	 * @Author: Jerry Zhang
	 * @Created_Date: 03/17/2014
	 * @Description: This method simulates the user clicking on "Calendar" tab on
	 *               Navigation pane.
	 */
	public void clickCalendarTab() {
		Logging.info("Clicking on Calendar tab in navigation bar");
		this.clickNavigationTab("Calendar");
	}

	/**
	 * @Method_Name: clickTasksTab
	 * @Author: Jerry Zhang
	 * @Created_Date: 03/17/2014
	 * @Description: This method simulates the user clicking on "Tasks" tab on
	 *               Navigation pane.
	 */
	public void clickTasksTab() {
		Logging.info("Clicking on Tasks tab in navigation bar");
		this.clickNavigationTab("Tasks");
	}

	/**
	 * @Method_Name: clickSettingsTab
	 * @Author: Jerry Zhang
	 * @Created_Date: 03/17/2014
	 * @Description: This method simulates the user clicking on "Settings" tab on
	 *               Navigation pane.
	 */
	public void clickSettingsTab() {
		Logging.info("Clicking on Settings tab in navigation bar");
		this.clickArrowdownButton();
		this.clickNavigationTab("Settings");
	}

	/**
	 * @Method_Name: clickLogoutButton
	 * @Author: Jerry Zhang
	 * @Created_Date: 03/14/2014
	 * @Description: This method simulates the user clicking on the "Logout" button
	 *               on Navigation pane.
	 */
	public void clickLogoutButton() {
		Logging.info("Clicking on 'Logout' button");
		String navi_logout_button = PropertyHelper.coreNavigationFile.getPropertyValue("navi_logout_button");

		try {
			Tools.setDriverDefaultValues();
			// Try to scroll screen to the top, as in some scenarios the logout button
			// won't be displayed after preceding scrolling-down.
			this.clickArrowdownButton();
			Tools.scrollElementIntoViewByXpath(navi_logout_button);
			Base.base.getDriver().findElement(By.xpath(navi_logout_button)).click();
			this.waitForLoadMaskDismissed();

		} catch (WebDriverException ex) {
			Logging.error("Could not click on logout button");
			throw ex;
		}
	}

	/**
	 * Click the arrow down button
	 * 
	 * @since Ux2.3
	 * 
	 * @author Fiona Zhang
	 */
	public void clickArrowdownButton() {
		String navi_down_arrow_button = PropertyHelper.coreNavigationFile.getPropertyValue("navi_down_arrow_button");
		String navi_settings_tab = PropertyHelper.coreNavigationFile.getPropertyValue("navi_settings_tab");

		try {
			Tools.scrollElementIntoViewByXpath(navi_down_arrow_button);
			Base.base.getDriver().findElement(By.xpath(navi_down_arrow_button)).click();
			Tools.waitUntilElementDisplayedByXpath(navi_settings_tab, 5);
		} catch (WebDriverException ex) {
			Logging.error("Could not click on arrow down button");
			throw ex;
		}
	}

	public String getWelcomeMessage() {
		Logging.info("Getting the welcome message in navigation bar");
		String navi_welcome_message = PropertyHelper.coreNavigationFile.getPropertyValue("navi_welcome_message");

		try {
			Tools.setDriverDefaultValues();
			String msg = Base.base.getDriver().findElement(By.xpath(navi_welcome_message)).getText();
			Logging.info("Welcome message is: " + msg);

			return msg;
		} catch (WebDriverException ex) {
			Logging.error("Could not get the welcome message");
			throw ex;
		}
	}

	public boolean verifyCollapseExpandAdvButtonDisplay(String action) {
		Logging.info("Verifying if " + action + " adv button display");

		String navi_collapse_expand_adv_button = null;

		if (action.equals("collapse")) {
			navi_collapse_expand_adv_button = PropertyHelper.coreNavigationFile
					.getPropertyValue("navi_collapse_adv_button");
		} else if (action.equals("expand")) {
			navi_collapse_expand_adv_button = PropertyHelper.coreNavigationFile
					.getPropertyValue("navi_expand_adv_button");
		}

		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(navi_collapse_expand_adv_button));
			// Subject is found.
			result = true;
			Logging.info("Found " + action + " adv button");
		} catch (WebDriverException ex) {
			Logging.info("Could not find " + action + " adv button");

		}
		return result;
	}

	public void clickCollapseExpandAdvButton(String action) {
		Logging.info("Clicking " + action + " adv button display");

		String navi_collapse_expand_adv_button = null;

		if (action.equals("collapse")) {
			navi_collapse_expand_adv_button = PropertyHelper.coreNavigationFile
					.getPropertyValue("navi_collapse_adv_button");
		} else if (action.equals("expand")) {
			navi_collapse_expand_adv_button = PropertyHelper.coreNavigationFile
					.getPropertyValue("navi_expand_adv_button");
		}

		try {

			Base.base.getDriver().findElement(By.xpath(navi_collapse_expand_adv_button)).click();

		} catch (WebDriverException ex) {
			Logging.error("Could not click on the button");
			throw ex;
		}
	}
}
