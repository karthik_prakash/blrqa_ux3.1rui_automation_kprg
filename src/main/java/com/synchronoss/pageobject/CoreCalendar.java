package com.synchronoss.pageobject;

import java.time.DateTimeException;
import java.time.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.synchronoss.core.Base;
import com.synchronoss.core.Logging;
import com.synchronoss.core.PropertyHelper;
import com.synchronoss.core.Tools;
import com.synchronoss.test.untils.EventInfo;
import org.openqa.selenium.support.FindBy;

public class CoreCalendar extends PageObjectBase {

	// Define the area of the calendar
	public static final int SIDEPANEL = 1;
	public static final int EVENTCALENDAR = 2;
    private String actualValue;

	@FindBy(name="uid")
	WebElement user99GuruName;

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Aug 20, 2015<br>
	 * <b>Description</b><br>
	 * This method will click on plus icon in calendar panel and select the
	 * specified action.
	 * 
	 * <b>Parameters</b><br>
	 * String action - the subsequent action after clicking plus icon. One of
	 * the following actions should be provided: - Add a calendar - Subscribe to
	 * a calendar
	 */
	public void clickPlusIconAddOrSubscribeCalendar(String action) {
		Logging.info("Clicking on plus icon in calendar list panel and selecting " + action);

		String calendar_add_icon = PropertyHelper.coreCalendar.getPropertyValue("calendar_add_icon");
		String calendar_add_action_item = PropertyHelper.coreCalendar.getPropertyValue("calendar_add_action_item")
				.replace("+variable+", action);

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(calendar_add_icon)).click();
			if (Tools.isIE()) {
				// On IE, make another click to ensure the menu is open.
				Base.base.getDriver().findElement(By.xpath(calendar_add_icon)).click();
			} else {
				Tools.moveFocusToElementByXpath(calendar_add_action_item);
			}
			Base.base.getDriver().findElement(By.xpath(calendar_add_action_item)).click();

		} catch (WebDriverException ex) {
			Logging.error("Could not click on plus icon in calendar list panel or selecting action");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Aug 20, 2015<br>
	 * <b>Description</b><br>
	 * This method will type text in calendar name field in add calendar window.
	 * 
	 * <b>Parameters</b><br>
	 * String name - text of the calendar name
	 */
	public void typeInCalendarNameFieldInAddCalendarWindow(String name) {
		Logging.info("Typing in calendar name field in add calendar window: " + name);

		String calendar_add_window_name_field = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_add_window_name_field");

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(calendar_add_window_name_field)).clear();
			Base.base.getDriver().findElement(By.xpath(calendar_add_window_name_field)).sendKeys(name);

		} catch (WebDriverException ex) {
			Logging.error("Could not type in calendar name field in add calendar window");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Aug 20, 2015<br>
	 * <b>Description</b><br>
	 * Delete all the calendars on the Calendar list,only left the default
	 * calendar.
	 */
	public void deleteAllCalendar() {
		Logging.info("Deleting all calendars in calendar list");

		String calendar_list_number = PropertyHelper.coreCalendar.getPropertyValue("calendar_list_number");
		String calendar_add_window_toolbar_button = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_add_window_toolbar_button").replace("+variable+", "Delete");

		try {
			Tools.setDriverDefaultValues();

			// calculate the size of the calendars on the Calendar Window
			List<WebElement> callist = Base.base.getDriver().findElements(By.xpath(calendar_list_number));

			// go through the cal lists and delete one by one
			int numOfCalList = callist.size();
			if (numOfCalList > 1) {
				for (int i = numOfCalList; i > 1; i--) {
					String calendar_list_name = Base.base.getDriver()
							.findElement(By.xpath("(" + calendar_list_number + ")[" + i + "]")).getText();
					String calendar_list_edit_button = PropertyHelper.coreCalendar
							.getPropertyValue("calendar_list_edit_button").replace("+variable+", calendar_list_name);

					Tools.moveFocusToElementByXpath("(" + calendar_list_number + ")[" + i + "]");
					Base.base.getDriver().findElement(By.xpath(calendar_list_edit_button)).click();
					Base.base.getDriver().findElement(By.xpath(calendar_add_window_toolbar_button)).click();
					this.clickButtonOnMessageBox("Yes");
				}
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not delete the calendar");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Aug 20, 2015<br>
	 * <b>Description</b><br>
	 * This method will click on tool bar button in add calendar window .
	 * 
	 * <b>Parameters</b><br>
	 * String button - button to click. One of the following button names should
	 * be supplied: - Save - Cancel - Delete
	 */
	public void clickToolBarButtonInAddCalendarWindow(String button) {
		Logging.info("Clicking tool bar button in add calendar window: " + button);

		String calendar_add_window_toolbar_button = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_add_window_toolbar_button").replace("+variable+", button);

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(calendar_add_window_toolbar_button)).click();
			this.waitForLoadMaskDismissed();

		} catch (WebDriverException ex) {
			Logging.error("Could not click tool bar button in add calendar window");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Aug 20, 2015<br>
	 * <b>Description</b><br>
	 * This method will click on the specified color in add calendar window .
	 * 
	 * <b>Parameters</b><br>
	 * int index - index of the specified color (range: 0 - 14)
	 */
	public void clickColorByIndexInAddCalendarWindow(int index) {
		Logging.info("Clicking color in add calendar window, index: " + index);

		String calendar_add_window_color_index = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_add_window_color_index").replace("+variable+", String.valueOf(index + 1));

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(calendar_add_window_color_index)).click();

		} catch (WebDriverException ex) {
			Logging.error("Could not click color in add calendar window");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Aug 20, 2015<br>
	 * <b>Description</b><br>
	 * This method will verify if a specific calendar exists by its name and
	 * color index.
	 * 
	 * <b>Parameters</b><br>
	 * String name - calendar name int index - index of the specified color
	 * (range: 0 - 14)
	 */
	public boolean verifyCalendarExistenceByNameAndColorIndex(String name, int index) {
		Logging.info("Verifying if calendar exists in calendar list panel, name: " + name + ", color index: " + index);

		String calendar_list_by_name_and_color = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_list_by_name_and_color").replace("+variable1+", name)
				.replace("+variable2+", String.valueOf(index));
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(calendar_list_by_name_and_color));
			result = true;

		} catch (WebDriverException ex) {
			Logging.error("Could not verify if calendar exists in calendar list panel: " + name);
		}
		return result;
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Oct 17, 2016<br>
	 * <b>Description</b><br>
	 * This method will verify if a event date display correctly date in month
	 * view <b>Parameters</b><br>
	 * LocalDate date- the expected date of the event, String dateformatterURI -
	 * the formatter of the date sting,
	 * 
	 */
	public boolean verifyEventDateInDateslot(LocalDate date, String dateFormatterURI) {
		Logging.info("Verifying if a event date displaying correctly on month view.");
		String calendar_event_datetime_on_month_view = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_datetime_on_month_view");
		boolean result = false;
		try {
			Tools.setDriverDefaultValues();
			String event_datetime_on_month_view = Base.base.getDriver()
					.findElement(By.xpath(calendar_event_datetime_on_month_view)).getText();
			String event_actual_date = event_datetime_on_month_view.substring(5, 11);
			char c = event_actual_date.charAt(0);
			//the date formatter would change according to the Setting of Locale date format.
			// if the first letter of event_actual_date is digital, the formatter URI is "EEE, d MMM, yyyy",  else it is "EEE, MMM d, yyyy"
			if (c >= '0' && c <= '9')
				dateFormatterURI = "EEE, d MMM, yyyy";
			else
				dateFormatterURI = "EEE, MMM d, yyyy";
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormatterURI, Locale.ENGLISH);
			Logging.info(dateFormatterURI);
			String expectDateString = date.format(formatter);
			Logging.info("event_actual_date " + event_actual_date);
			Logging.info("expect Date String " + expectDateString);
			if (expectDateString.indexOf(event_actual_date) != -1) {
				Logging.info("This event has correct date");
				result = true;
			} else {
				Logging.info("event_actual_date " + event_actual_date + " expectDateString " + expectDateString);
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not find element in verifyEventDateInDateslot");
		}
		return result;
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Oct 17, 2016<br>
	 * <b>Description</b><br>
	 * This method will verify if a event description display correct date in
	 * month view <b>Parameters</b><br>
	 * LocalDate date- the expected date of the event, String dateformatterURI -
	 * the formatter of the date sting,
	 * 
	 */
	public boolean verifyEventDescriptionInDateslot(String description) {
		Logging.info("Verifying if a repeating event description displaying correctly on month view.");
		String calendar_event_description_on_month_view = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_description_on_month_view");
		boolean result = false;
		try {
			Tools.setDriverDefaultValues();
			String event_description_on_month_view = Base.base.getDriver()
					.findElement(By.xpath(calendar_event_description_on_month_view)).getText();
			Logging.info(event_description_on_month_view);
			if (event_description_on_month_view.indexOf(description) != -1) {
				Logging.info("This event has correct description");
				result = true;
			} else {
				Logging.info("Ecpected description " + description + " actual description "
						+ event_description_on_month_view);
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not find element in verifyEventDescriptionInDateslot");
		}
		return result;
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Oct 18, 2016<br>
	 * <b>Description</b><br>
	 * This method will close event details page
	 * 
	 */
	public void closeEventDetail() {
		Logging.info("closing event details page");
		String calendar_event_details_close = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_details_close");
		try {
			Tools.setDriverDefaultValues();
			Tools.waitFor(2, TimeUnit.SECONDS);
			Base.base.getDriver().findElement(By.xpath(calendar_event_details_close)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not find element in closeEventDetail");
		}
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Oct 10, 2016<br>
	 * <b>Description</b><br>
	 * This method will verify if a repeating event display correct on month
	 * view <b>Parameters</b><br>
	 * LocalDate date- the start date of the repeating int interval - the
	 * interval of the event, int repeatingTime - the repeating time of the
	 * event String timeFormatterURI -- the formatter of the time
	 * 
	 */
	public boolean verifyDailyRepeatingEventInOneMonth(LocalDate date, int interval, int repeatingTime,
			String dateFormatterURI) {
		Logging.info("Verifying if a repeating event displaying correct on month view.");

		String calendar_evnet_list_on_month_view = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_evnet_list_on_month_view");
		String calendar_event_datetime_on_month_view = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_datetime_on_month_view");
		String calendar_event_description_on_month_view = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_description_on_month_view");
		// There is a bug, the time formatter is fixed in event details,so add
		// the time formatter here.
		DateTimeFormatter formatter;
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();

			List<WebElement> eventList = Base.base.getDriver()
					.findElements(By.xpath(calendar_evnet_list_on_month_view));
			int size = eventList.size();
			if (size < repeatingTime) {
				Logging.info("Not all of the events are on this month");
				return false;
			}
			int year = date.getYear();
			int month = date.getMonthValue();
			int dayOfMonth = date.getDayOfMonth();
			String event_expected_description = "Every " + interval + " days, " + repeatingTime + " times";
			for (int i = 0; i < size; i++) {
				eventList.get(i).click();
				String event_datetime_on_month_view = Base.base.getDriver()
						.findElement(By.xpath(calendar_event_datetime_on_month_view)).getText();
				String event_description_on_month_view = Base.base.getDriver()
						.findElement(By.xpath(calendar_event_description_on_month_view)).getText();
				Logging.info(event_datetime_on_month_view);
				String event_actual_date = event_datetime_on_month_view.substring(5, 10);

				event_actual_date = event_datetime_on_month_view.substring(5, 11);
				
				//the date formatter would change according to the Setting of Locale date format.
				// if the first letter of event_actual_date is digital, the formatter URI is "EEE, d MMM, yyyy",  else it is "EEE, MMM d, yyyy"
				char c = event_actual_date.charAt(0);
				if (c >= '0' && c <= '9')
				{
					dateFormatterURI = "EEE, d MMM, yyyy";	
				}
				else
					dateFormatterURI = "EEE, MMM d, yyyy";
				 formatter = DateTimeFormatter.ofPattern(dateFormatterURI, Locale.ENGLISH);
				String event_expect_date = LocalDate.of(year, month, dayOfMonth).format(formatter);
				Logging.info("event_actual_date=" + event_actual_date);
				Logging.info("event_expect_date=" + event_expect_date);
				if (event_expect_date.indexOf(event_actual_date) == -1) {
					Logging.info("event_actual_date=" + event_actual_date);
					Logging.info("event_expect_date=" + event_expect_date);
					return false;
				} else {
					Logging.info("Event has correct date");
				}
				if (event_description_on_month_view.indexOf(event_expected_description) == -1) {
					Logging.info("event_description_on_month_view=" + event_description_on_month_view);
					Logging.info("event_expected_description=" + event_expected_description);
					return false;
				} else {
					Logging.info("Event has correct description");
				}
				dayOfMonth = dayOfMonth + interval;

			}

			result = true;

		} catch (WebDriverException ex) {
			Logging.error("Could not find element in verifyDailingRepeatingEventInOneMonth");
		}
		return result;
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Aug 20, 2015<br>
	 * <b>Description</b><br>
	 * This method will delete a specific calendar by its name and color index.
	 * 
	 * <b>Parameters</b><br>
	 * String name - calendar name int index - index of the specified color
	 * (range: 0 - 14)
	 */
	public void deleteCalendarByNameAndColorIndex(String name, int index) {
		Logging.info("Verifying if calendar exists in calendar list panel, name: " + name + ", color index: " + index);

		String calendar_list_by_name_and_color = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_list_by_name_and_color").replace("+variable1+", name)
				.replace("+variable2+", String.valueOf(index));

		String calendar_add_window_toolbar_button = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_add_window_toolbar_button").replace("+variable+", "Delete");
		String calendar_list_edit_button = PropertyHelper.coreCalendar.getPropertyValue("calendar_list_edit_button")
				.replace("+variable+", name);

		try {
			Base.base.getDriver().switchTo().defaultContent();

			Tools.scrollElementIntoViewByXpath(calendar_list_by_name_and_color);

			Base.base.getDriver().findElement(By.xpath(calendar_list_by_name_and_color)).click();
			Base.base.getDriver().findElement(By.xpath(calendar_list_edit_button)).click();
			Base.base.getDriver().findElement(By.xpath(calendar_add_window_toolbar_button)).click();
			this.clickButtonOnMessageBox("Yes");

		} catch (WebDriverException ex) {
			Logging.error("Could not delete calendar " + name);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Aug 20, 2015<br>
	 * <b>Description</b><br>
	 * Click on calendar toolbar button.
	 * 
	 * <b>Parameters</b><br>
	 * String button: - New event - Actions - Today - Day - Week-Month - List
	 * 
	 */
	public void clickOnCalendarToolBarButton(String button) {

		Logging.info("clicking on calendar " + button + " button");
		String toolbar_button = PropertyHelper.coreCalendar.getPropertyValue("calendar_toolbar_button")
				.replace("+variable+", button);
		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(toolbar_button)).click();
			this.waitForLoadMaskDismissed();

		} catch (WebDriverException ex) {
			Logging.error("Could not click button " + button);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Aug 20, 2015<br>
	 * <b>Description</b><br>
	 * Input summary or location text in Summary or Location field of new event
	 * panel.
	 * 
	 * <b>Parameters</b><br>
	 * String field - Input field name. One of the following value is provided:
	 * Summary - Location String inputText - message input in summary or
	 * location field
	 */
	public void typeInSummaryOrLocationOnNewEventPanel(String field, String inputText) {

		Logging.info("inputting " + inputText + " to " + field + " field");

		String calendar_event_add_panel_summary_location = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_add_panel_summary_location").replace("+variable+", field);

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(calendar_event_add_panel_summary_location)).clear();
			Base.base.getDriver().findElement(By.xpath(calendar_event_add_panel_summary_location)).sendKeys(inputText);

		} catch (WebDriverException ex) {
			Logging.error("Could not input text in " + field + " field");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Aug 20, 2015<br>
	 * <b>Description</b><br>
	 * click Save/Save & Send Invitation Or Cancel Button on new event panel.
	 * 
	 * <b>Parameters</b><br>
	 * String clickButton: - Input_field name. One of the following value is
	 * provided: - Save or Save & Send Invitation - Cancel
	 */
	public void clickToolbarButtonOnNewEventPanel(String toolbar_button_on_new_event) {

		Logging.info("Clicking on " + toolbar_button_on_new_event + " button in event editor panel");

		String calendar_event_add_panel_toolbar_button = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_add_panel_toolbar_button")
				.replace("+variable+", toolbar_button_on_new_event);

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(calendar_event_add_panel_toolbar_button)).click();

			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not click " + toolbar_button_on_new_event + " button");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Aug 26, 2015<br>
	 * <b>Description</b><br>
	 * Delete all events in specific month in List view
	 * 
	 */
	public void deleteAllEventInSpecificMonthInListView() {
		Logging.info("Deleting all events in specific month in list view");

		String deleteEventPopupWindowTitle_noRecur = "Delete Event";
		String deleteEventPopupWindowTitle_Recur = "Delete Recurring Event";

		String calendar_event_list_number_inListView = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_list_number_inListView");
		String calendar_event_toolbar_button_inListView = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_toolbar_button_inListView").replace("+variable+", "Delete");

		String calendar_delete_recurring_event_series = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_delete_recurring_event_series");

		String calendar_delete_event_prompt_window_title = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_delete_event_prompt_window_title");

		String calendar_event_list_if_empty = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_list_if_empty");

		try {
			Tools.setDriverDefaultValues();
			WebElement test = Base.base.getDriver().findElement(By.xpath(calendar_event_list_if_empty));

			if (!test.getText().equals("No events")) {
				// calculate the size of the calendars on the Calendar Window
				List<WebElement> eventList = Base.base.getDriver()
						.findElements(By.xpath(calendar_event_list_number_inListView));

				// go through the calendar lists and delete one by one

				while (eventList.size() > 0) {
					Base.base.getDriver().findElement(By.xpath("(" + calendar_event_list_number_inListView + ")[1]"))
							.click();

					Base.base.getDriver().findElement(By.xpath(calendar_event_toolbar_button_inListView)).click();

					String popupWindowTitle = Base.base.getDriver()
							.findElement(By.xpath(calendar_delete_event_prompt_window_title)).getText();

					if (popupWindowTitle.equals(deleteEventPopupWindowTitle_noRecur)) {
						this.clickButtonOnMessageBox("Yes");
					} else if (popupWindowTitle.equals(deleteEventPopupWindowTitle_Recur)) {
						Base.base.getDriver().findElement(By.xpath(calendar_delete_recurring_event_series)).click();
					}

					this.waitForLoadMaskDismissed();
					eventList = Base.base.getDriver().findElements(By.xpath(calendar_event_list_number_inListView));
				}
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not delete events in List view");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Aug 20, 2015<br>
	 * <b>Description</b><br>
	 * Get datetime with integral minute.
	 * 
	 * <b>Parameters</b><br>
	 * LocalDateTime dateTime - Input LocalDateTime type datetime
	 * 
	 * <b>Return</b><br>
	 * Return LocalDateTime type datetime with integral minute
	 */
	public LocalDateTime getIntegralMinute(LocalDateTime dateTime) {
		int mod = dateTime.getMinute() % 5;		
		if (mod != 0) {
			return dateTime.plusMinutes(5 - mod);
		} else {
			return dateTime;
		}	
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Aug 20, 2015<br>
	 * <b>Description</b><br>
	 * Get the current date time in specific time zone.
	 * 
	 * <b>Parameters</b><br>
	 * String timeZone - the specific time zone
	 * 
	 * <b>Return</b><br>
	 * Return current date time with integral minute in specific time zone
	 * 
	 * @throws Exception
	 */
	public LocalDateTime getCurrentDateTimeWithTimeZone(String timeZone) {

		LocalDateTime currentDateTime = null;

		try {
			ZoneId settingTimeZone = ZoneId.of(timeZone);
			ZonedDateTime currentZonedDateTime = ZonedDateTime.now(settingTimeZone);
			currentDateTime = getIntegralMinute(currentZonedDateTime.toLocalDateTime());
		} catch (WebDriverException ex) {
			Logging.error("There is an error when getting the current datetime in sepcific timezone");
			throw ex;
		}

		return currentDateTime;
	}
	
	/**
	   * @Method Name: getCurrentTimeWithTimeZone
	   * @Author: Vivian Xie
	   * @Created On: Jan/19/2016
	   * @Description: Return current date time with integral minute in specific time zone.
	   * @param: eventStartTime -- the specific time zone
	   */
    public LocalTime getCurrentTimeWithTimeZone(String timeZone) {

        LocalTime currentTime = null;

        try {
            ZoneId settingTimeZone = ZoneId.of(timeZone);
            ZonedDateTime currentZonedDateTime = ZonedDateTime.now(settingTimeZone);
            currentTime = this.formatTimeMuniteEndingWith5Or0(currentZonedDateTime.toLocalTime());
        } catch (WebDriverException ex) {
            Logging.error("There is an error when getting the current datetime in sepcific timezone");
            throw ex;
        }

        return currentTime;
    }

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Aug 26, 2015<br>
	 * <b>Description</b><br>
	 * LocalDateTime dateTime: dateTime String format: the format of dateTime
	 * 
	 * @return String Formatted dateTime string
	 */
	public String formatDateTime(LocalDateTime dateTime, String pattern) {

		String dateTimeFormat = null;

		try {
			DateTimeFormatter format = DateTimeFormatter.ofPattern(pattern, Locale.US);
			dateTimeFormat = dateTime.format(format);
		} catch (DateTimeException ex) {
			throw ex;
		}

		return dateTimeFormat;
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Aug 26, 2015<br>
	 * <b>Description</b><br>
	 * This method will click calendar refresh button.
	 * 
	 */
	public void clickCalnedarRefreshIcon() {
		Logging.info("Clicking on calendar refresh button ");
		String calendar_refresh_icon = PropertyHelper.coreCalendar.getPropertyValue("calendar_refresh_icon");

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(calendar_refresh_icon)).click();

			// Wait for refresh icon shown again.
			Tools.waitUntilElementDisplayedByXpath(calendar_refresh_icon, this.timeoutMedium);

		} catch (WebDriverException ex) {
			Logging.error("Could not click calendar refresh icon");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Aug 26, 2015<br>
	 * <b>Description</b><br>
	 * This method will set start time in event editor panel.
	 * 
	 * <b>Parameters</b><br>
	 * LocalDateTime datetTime: dateTime of event String format: the format of
	 * time
	 */
	public void setStartTimeOnEventEditorPanel(LocalDateTime eventStartTime, String strTimeFormat) {

		String strEventStartDateTime = this.formatDateTime(eventStartTime, strTimeFormat);

		Logging.info("Setting start time: " + strEventStartDateTime + " in event editor panel");

		String calendar_event_add_panel_timefield_start = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_add_panel_timefield_start");

		String calendar_event_add_panel_timePicker_start = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_add_panel_timePicker_start")
				.replace("+variable+", strEventStartDateTime);

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(calendar_event_add_panel_timefield_start)).click();
			Tools.scrollElementIntoViewByXpath(calendar_event_add_panel_timePicker_start);
			Base.base.getDriver().findElement(By.xpath(calendar_event_add_panel_timePicker_start)).click();

		} catch (WebDriverException ex) {
			Logging.error("Could not set start time in event editor panel.");
			throw ex;
		}
	}

	/**
     * <b>Author</b>: Vivian Xie<br>
     * <b>Date</b>: Nov 18, 2016<br>
     * <b>Description</b><br>
     * This method will click on the event time picker on event editor panel.
     * 
     * <b>Parameters</b><br>
     * timePickerName: the name of the time picker ,value can be "starttime" or "endtime"
     */
  public void clickOnTimePickOnEventEditorPanel(String timePickerName) {

    String calendar_event_add_panel_timefield_start =
        PropertyHelper.coreCalendar.getPropertyValue("calendar_event_add_panel_timefield_start");
    String calendar_event_add_panel_timefield_end =
        PropertyHelper.coreCalendar.getPropertyValue("calendar_event_add_panel_timefield_end");
    try {
      if (timePickerName.equals("starttime")) {
        Base.base.getDriver().findElement(By.xpath(calendar_event_add_panel_timefield_start))
            .click();
      } else  {
        Base.base.getDriver().findElement(By.xpath(calendar_event_add_panel_timefield_end)).click();
      }
    } catch (WebDriverException ex) {
      Logging.error("Could not click "+ timePickerName +" timepicker in event editor panel.");
      throw ex;
    }

  }
  
  /**
   * <b>Author</b>: Vivian Xie<br>
   * <b>Date</b>: Nov 18, 2016<br>
   * <b>Description</b><br>
   * This method will verify if the selected option is on the top.
   * 
   * <b>Parameters</b><br>
   * timePickerName: the name of the time picker ,value can be "starttime" or "endtime"
   */
  
  public boolean verifyTimeOptionIsOnTheTop(String timePickerName, String timeString) {
    Logging.info("starting to verify time option " + timeString + "  is on the top");
    String calendar_daterange_time_options = PropertyHelper.coreCalendar
        .getPropertyValue("calendar_daterange_time_options").replace("+variable+", timePickerName);
    int index = 0;
    ArrayList<WebElement> options = null;
    boolean result = false;
    try {
      options = (ArrayList<WebElement>) Base.base.getDriver()
          .findElements(By.xpath(calendar_daterange_time_options));
    } catch (WebDriverException ex) {
      Logging.error("Failed to get timePicker option list");
      throw ex;
    }
    for (int i = 0; i < options.size(); i++) {
      String text = options.get(i).getText();
      if (timeString.contains(text)) {
        index = i;
        break;
      }
    }
    if (index == 0) {
      result = true;
    }
    return result;
  }

  /**
   * <b>Author</b>: Vivian Xie<br>
   * <b>Date</b>: Nov 18, 2016<br>
   * <b>Description</b><br>
   * This method will verify if the specific time option is selected
   * 
   * <b>Parameters</b><br>
   * timePickerName: the name of the time picker ,value can be "starttime" or "endtime"
   * LocalTime time: The specific time
   * String timeFormat: time format
   */
  public boolean verifySpecificTimeIsSelected(String timePickerName, LocalTime time, String timeFormat) {
    Logging.info("Start to verify if time "+ time.toString() + " is selected");
    String calendar_daterange_time_options_selected =
        PropertyHelper.coreCalendar.getPropertyValue("calendar_daterange_time_options_selected")
            .replace("+variable+", timePickerName);
    String selectOptionText = null;
    boolean result = false;
    try {
      selectOptionText =  Base.base.getDriver().findElement(By.xpath(calendar_daterange_time_options_selected)).getText();

    } catch (WebDriverException ex) {
      Logging.error("Could not get text of  " + timePickerName + " timepicker in event editor panel.");
      throw ex;
    }
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timeFormat, Locale.ENGLISH);
    LocalTime selectTime = LocalTime.parse(selectOptionText, formatter);
    int diff = (int) ChronoUnit.MINUTES.between(time, selectTime);
     if( diff == 0 )
     result = true;
   return result;
   
 }
	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Aug 26, 2015<br>
	 * <b>Description</b><br>
	 * This method will set end time in event editor panel.
	 * 
	 * <b>Parameters</b><br>
	 * LocalDateTime datetTime: dateTime of event String format: the format of
	 * time
	 */
	public void setEndTimeOnEventEditorPanel(LocalDateTime eventEndDateTime, String strTimeFormat) {

		String strEventEndDateTime = this.formatDateTime(eventEndDateTime, strTimeFormat);

		Logging.info("Setting end time: " + strEventEndDateTime + " in event editor panel");

		String calendar_event_add_panel_timefield_end = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_add_panel_timefield_end");

		String calendar_event_add_panel_timePicker_end = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_add_panel_timePicker_end").replace("+variable+", strEventEndDateTime);

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(calendar_event_add_panel_timefield_end)).click();
			Tools.scrollElementIntoViewByXpath(calendar_event_add_panel_timePicker_end);
			Base.base.getDriver().findElement(By.xpath(calendar_event_add_panel_timePicker_end)).click();

		} catch (WebDriverException ex) {
			Logging.error("Could not set end time in event editor panel.");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 15, 2015<br>
	 * <b>Description</b><br>
	 * This method will click action button in calendar view, and select actions
	 * <b>Parameters</b><br>
	 * String action - action for Create new calendar/Import Event/Export
	 * Event/Subscribe to other Calendar
	 */
	public void selectActionsInCalendarView(String action) {

		Logging.info("Start to select actions " + action + "in calendar view");
		String calendar_action_button = PropertyHelper.coreCalendar.getPropertyValue("calendar_actions_button");
		String calendar_action = PropertyHelper.coreCalendar.getPropertyValue("calendar_action");

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(calendar_action_button)).click();
			Base.base.getDriver().manage().timeouts().implicitlyWait(Base.timeoutShort, TimeUnit.SECONDS);
			Base.base.getDriver().findElement(By.xpath(calendar_action.replace("+variable+", action))).click();
		} catch (WebDriverException ex) {
			Logging.error("The Element of action is not found");
			throw ex;
		}
		Logging.info("finish to select actions " + action + "in calendar view");
	}

	/**
	 * <b>Author</b>:Vivian Xie<br>
	 * <b>Date</b>: Oct 16, 2016<br>
	 * <b>Description</b><br>
	 * This method selects the specific month on month view</b><br>
	 * LocalDate date - the date that is to be selected.
	 * 
	 */
	public void selectMonthOnMonthView(LocalDate date) {

		Logging.info("Start to select month on month view ");
		String calendar_nexnt_month_icon = PropertyHelper.coreCalendar.getPropertyValue("calendar_nexnt_month_icon");
		String calendar_previous_month_icon = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_previous_month_icon");
		LocalDate currentDay = LocalDate.now();
		int diff = (int) ChronoUnit.MONTHS.between(currentDay, date);
		try {
			Tools.setDriverDefaultValues();
			if (diff > 0) {
				for (int i = 0; i < diff; i++) {
					Base.base.getDriver().findElement(By.xpath(calendar_nexnt_month_icon)).click();
				}
			} else {
				for (int i = 0; i < -diff; i++) {
					Base.base.getDriver().findElement(By.xpath(calendar_previous_month_icon)).click();
				}
			}

		} catch (WebDriverException ex) {
			Logging.error("Failed to select month on month view");
			throw ex;
		}
		Logging.info("finish to selct month on month view date:" + date.toString());
	}

	/**
	 * <b>Author</b>:Vivian Xie<br>
	 * <b>Date</b>: Oct 16, 2016<br>
	 * <b>Description</b><br>
	 * This method selects the specific month on month view</b><br>
	 * LocalDate date - the date that is to be selected.
	 * 
	 */
	public void selectNextMonthOnMonthView(int diff) {

		Logging.info("Start to select next " + diff + " month");
		String calendar_nexnt_month_icon = PropertyHelper.coreCalendar.getPropertyValue("calendar_nexnt_month_icon");
		String calendar_previous_month_icon = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_previous_month_icon");
		try {
			Tools.setDriverDefaultValues();
			if (diff > 0) {
				for (int i = 0; i < diff; i++) {
					Base.base.getDriver().findElement(By.xpath(calendar_nexnt_month_icon)).click();
				}
			} else {
				for (int i = 0; i < -diff; i++) {
					Base.base.getDriver().findElement(By.xpath(calendar_previous_month_icon)).click();
				}
			}

		} catch (WebDriverException ex) {
			Logging.error("Failed to select next " + diff + " month");
			throw ex;
		}
		Logging.info("finish to select next " + diff + " month");
	}

	/**
	 * <b>Author</b>:Vivian Xie<br>
	 * <b>Date</b>: Oct 16, 2016<br>
	 * <b>Description</b><br>
	 * This method selects the specific day on month view</b><br>
	 * LocalDate date - the date that is to be seleted.
	 * 
	 */
	public void selectDayOnMonthView(LocalDate date) {

		Logging.info("Start to select day on month view ");
		int day = date.getDayOfMonth();
		String calendar_dayslot = PropertyHelper.coreCalendar.getPropertyValue("calendar_dayslot").replace("+variable+",
				String.valueOf(day));
		try {
			Tools.setDriverDefaultValues();
			System.out.println("calendar_dayslot is: "+calendar_dayslot);
			Base.base.getDriver().findElement(By.xpath(calendar_dayslot)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not select dayslot on month view");
			throw ex;
		}
		Logging.info("finish to select dayslot on month view date:" + date.toString());
	}

	/**
	 * <b>Author</b>:Vivian Xie<br>
	 * <b>Date</b>: Oct 17, 2016<br>
	 * <b>Description</b><br>
	 * This method click the input of event start time or end time</b><br>
	 * String timeType- value can be "start" or "end".
	 * 
	 */
	public void clickNewEventTime(String timeType) {

		Logging.info("Clicking event " + timeType + " time");

		String calendar_event_time_input = PropertyHelper.coreCalendar.getPropertyValue("calendar_event_time_input")
				.replace("+variable+", String.valueOf(timeType));

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(calendar_event_time_input)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click event " + timeType + " time");
			throw ex;
		}
		Logging.info("finish to click event " + timeType + " time");
	}

	/**
	 * <b>Author</b>:Vivian Xie<br>
	 * <b>Date</b>: Oct 17, 2016<br>
	 * <b>Description</b><br>
	 * This method enter event start time or event end time</b><br>
	 * String timeType- value can be "start" or "end". String timeString - value
	 * of time
	 * 
	 */
	public void inputNewEventTime(String timeType, String timeString) {

		Logging.info("Inputing event " + timeType + " time");

		String calendar_event_time_input_option = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_time_input_option").replace("+variable+", timeString);
		try {
			Base.base.getDriver().findElement(By.xpath(calendar_event_time_input_option)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not input event " + timeType + " time");
			throw ex;
		}
		Logging.info("finish to input event " + timeType + " time");
	}

	/**
	 * <b>Author</b>:Vivian Xie<br>
	 * <b>Date</b>: Oct 16, 2016<br>
	 * <b>Description</b><br>
	 * This method selects the specific day on month view</b><br>
	 * LocalDate startdate - the start date LocalDate enddate - the enddate .
	 * 
	 */
	public void dragAndDropASlot(LocalDate startdate, LocalDate endDate) {

		Logging.info("Start to drag and drop dateslot month view ");
		int startDay = startdate.getDayOfMonth();
		int endDay = endDate.getDayOfMonth();
		String calendar_startdayslot = PropertyHelper.coreCalendar.getPropertyValue("calendar_dayslot")
				.replace("+variable+", String.valueOf(startDay));
		String calendar_enddayslot = PropertyHelper.coreCalendar.getPropertyValue("calendar_dayslot")
				.replace("+variable+", String.valueOf(endDay));
		try {
			Tools.setDriverDefaultValues();
			WebElement startdateSlot = Base.base.getDriver().findElement(By.xpath(calendar_startdayslot));
			WebElement endateSlot = Base.base.getDriver().findElement(By.xpath(calendar_enddayslot));
			Actions action = new Actions(Base.base.getDriver());
			action.dragAndDrop(startdateSlot, endateSlot).perform();

		} catch (WebDriverException ex) {
			Logging.error("Could not drag and drop dayslot on month view");
			throw ex;
		}
		Logging.info("finish to drap and drop dayslot on month view date. StartDate:" + startdate.toString()
				+ " EndDate:" + endDate.toString());
	}

	/**
	 * <b>Author</b>:Vivian Xie<br>
	 * <b>Date</b>: Oct 16, 2016<br>
	 * <b>Description</b>Drag and drop a event to a specific day in month
	 * view</b><br>
	 * <b>parameter</b>: LocalDate targetDate -- the target date when the event
	 * will move to
	 * 
	 * 
	 */
	public void dragAndDropAEventInMonthView(LocalDate targetDate) {

		Logging.info("Start to drag and drop the event in month view  to " + targetDate.toString());
		int tagetDay = targetDate.getDayOfMonth();
		String calendar_evnet_list_on_month_view = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_evnet_list_on_month_view");
		String calendar_enddayslot = PropertyHelper.coreCalendar.getPropertyValue("calendar_dayslot")
				.replace("+variable+", String.valueOf(String.valueOf(tagetDay)));
		try {
			Tools.setDriverDefaultValues();
			List<WebElement> eventList = Base.base.getDriver()
					.findElements(By.xpath(calendar_evnet_list_on_month_view));
			if (eventList.size() != 0) {
				WebElement endateSlot = Base.base.getDriver().findElement(By.xpath(calendar_enddayslot));
				Actions action = new Actions(Base.base.getDriver());
				action.dragAndDrop(eventList.get(0), endateSlot).perform();
			} else {
				Logging.info("There is no event in the month view ");
			}

		} catch (WebDriverException ex) {
			Logging.error("Could not drag and drop dayslot on month view");
			throw ex;
		}
		Logging.info("finish to drap and drop dayslot on month view date. targetDate:" + targetDate.toString());
	}

	/**
	 * <b>Author</b>:Vivian Xie<br>
	 * <b>Date</b>: Oct 16, 2016<br>
	 * <b>Description</b>:This method selects verify if the current page is new
	 * event page<br>
	 */
	public boolean verifyNewEventPage() {

		Logging.info("Start to Verifying if the page in New Event Page ");
		String calendar_new_evnet_string = PropertyHelper.coreCalendar.getPropertyValue("calendar_new_evnet_string");
		boolean result = false;
		try {
			Tools.setDriverDefaultValues();
			List<WebElement> list = Base.base.getDriver().findElements(By.xpath(calendar_new_evnet_string));
			if (list.size() == 0)
				result = false;
			else
				result = true;
		} catch (WebDriverException ex) {
			Logging.error("Could not find element in verifyNewEventPage");
			throw ex;
		}
		Logging.info("finish to verify New Event Page");
		return result;
	}

	
	
	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 16, 2015<br>
	 * <b>Description</b><br>
	 * This method will import event file from local <b>Parameters</b><br>
	 * String fileName - the .ics file
	 * 
	 */
	public void importEventInCalendarView(String fileName, String filePath) {
		Logging.info("Starts to import event to the calendar");

		String eventInput = PropertyHelper.coreCalendar.getPropertyValue("calendar_action_attach_event");

		String importButton = PropertyHelper.coreCalendar.getPropertyValue("calendar_action_attach_import_button");
		try {
			Tools.setDriverDefaultValues();

			// find input html tag, and attach the .ics file
			Base.base.getDriver().findElement(By.xpath(eventInput)).sendKeys(filePath + fileName);

			Base.base.getDriver().manage().timeouts().implicitlyWait((Base.timeoutShort), TimeUnit.SECONDS);

			// click the import button
			Base.base.getDriver().findElement(By.xpath(importButton)).click();
		} catch (WebDriverException ex) {
			Logging.error("The Element of action is not found");
			throw ex;
		}
		Tools.waitFor(timeoutShort, TimeUnit.SECONDS);
		Logging.info("Finish to import event to the calendar");
	}

	// /**
	// * <b>Author</b>: Fiona Zhang<br>
	// * <b>Date</b>: Sep 16, 2015<br>
	// * <b>Description</b><br>
	// * This method will select specific date and click the day
	// * <b>Parameters</b><br>
	// * LocalDateTime date - the date you want to select in side bar
	// * int area - the area of calendar, now , its including 2 parts, one is on
	// side panel, the other is in event view
	// */
	// public void selectCalendarDate(LocalDateTime date, int area) {
	// Logging.info("Starts to select calendar date");
	//
	// int currentYear = date.getYear();
	// String month = date.getMonth().toString();
	// int currentDay = date.getDayOfMonth();
	// List<WebElement> allDays = null;
	// String calendar_area = null;
	// switch (area) {
	// case 1:
	// calendar_area = PropertyHelper.coreCalendar
	// .getPropertyValue("calendar_in_side_panel");
	// break;
	// case 2:
	// calendar_area = PropertyHelper.coreCalendar
	// .getPropertyValue("calendar_in_event_start_end");
	// break;
	// }
	// String down_Arrow_Button = PropertyHelper.coreCalendar
	// .getPropertyValue("calendar_action_down_arrow");
	// String all_Years_Element = PropertyHelper.coreCalendar
	// .getPropertyValue("calendar_action_collection_years");
	// String year_Select = all_Years_Element
	// + PropertyHelper.coreCalendar
	// .getPropertyValue("calendar_action_year");
	// String all_Month_Element = PropertyHelper.coreCalendar
	// .getPropertyValue("calendar_action_collection_months");
	// String month_Select = all_Month_Element
	// + PropertyHelper.coreCalendar
	// .getPropertyValue("calendar_action_month");
	// String ok_Button_in_calendar = PropertyHelper.coreCalendar
	// .getPropertyValue("calendar_action_ok_button");
	// String all_Days_Element = PropertyHelper.coreCalendar
	// .getPropertyValue("calendar_action_collection_days");
	// String tipElementChose = PropertyHelper.coreCalendar
	// .getPropertyValue("calendar_tip_chose_month");
	//
	// String calendar_action_navi_prev = PropertyHelper.coreCalendar
	// .getPropertyValue("calendar_action_navi_prev");
	// String calendar_action_navi_next = PropertyHelper.coreCalendar
	// .getPropertyValue("calendar_action_navi_next");
	// String calendar_action_year_picker_first_item =
	// PropertyHelper.coreCalendar
	// .getPropertyValue("calendar_action_year_picker_first_item");
	//
	//
	// try {
	// Tools.setDriverDefaultValues();
	//
	// Tools.waitUntilElementDisplayedByXpath(calendar_area
	// + down_Arrow_Button, 15);
	//
	// // click down arrow
	// Logging.info("Starts to click on down arrow, and pick up a date");
	// Base.base.getDriver()
	// .findElement(By.xpath(calendar_area + down_Arrow_Button))
	// .click();
	//
	// // determine if year-page navigation needed or not
	// int firstYear = Integer.valueOf(Base.base.getDriver()
	// .findElement(By.xpath("(" + calendar_area +
	// calendar_action_year_picker_first_item + ")[1]"))
	// .getText());
	// int count = 0;
	// String navi_button_to_click = null;
	//
	// if ((firstYear - currentYear) > 0) {
	// // go to previous pages
	// count = ((firstYear - currentYear - 1) / 10) + 1;
	// navi_button_to_click = calendar_area + calendar_action_navi_prev;
	// Logging.info("Year to select: " + currentYear + ", first year displayed:
	// "
	// + firstYear + ", prev-clicking count: " + count);
	// } else {
	// // go to next pages
	// count = (currentYear - firstYear) / 10;
	// navi_button_to_click = calendar_area + calendar_action_navi_next;
	// Logging.info("Year to select: " + currentYear + ", first year displayed:
	// "
	// + firstYear + ", next-clicking count: " + count);
	// }
	//
	// for (int i = 0; i < count; i++) {
	// Base.base.getDriver().findElement(By.xpath(navi_button_to_click)).click();
	// try {
	// Thread.sleep(500);
	// } catch (InterruptedException e) {
	// e.printStackTrace();
	// }
	// }
	//
	// // start to find year
	// Logging.info("Starts to find the year");
	// Tools.waitUntilElementNotDisplayedByXpath(tipElementChose,
	// Base.timeoutMedium);
	// Base.base
	// .getDriver()
	// .findElement(
	// By.xpath(calendar_area
	// + year_Select.replace("+variable+",
	// String.valueOf(currentYear))))
	// .click();
	// Tools.waitFor(timeoutShort, TimeUnit.MILLISECONDS);
	//
	// // start to find month
	// Logging.info("Starts to find the month");
	// String currentMonth = "";
	// currentMonth = month.substring(0, 1)
	// + month.substring(1, 3).toLowerCase();
	// Base.base
	// .getDriver()
	// .findElement(
	// By.xpath(calendar_area
	// + month_Select.replace("+variable+",
	// currentMonth))).click();
	//
	// Tools.waitUntilElementDisplayedByXpath(calendar_area
	// + ok_Button_in_calendar, 15);
	//
	// // click ok button
	// Base.base
	// .getDriver()
	// .findElement(
	// By.xpath(calendar_area + ok_Button_in_calendar))
	// .click();
	//
	// // find a day and click
	// Logging.info("Starts to find the day");
	// Tools.waitFor(Base.timeoutLong, TimeUnit.MILLISECONDS);
	// allDays = Base.base.getDriver().findElements(
	// By.xpath(calendar_area + all_Days_Element));
	// allDays.get(currentDay - 1).click();
	//
	// Base.base.getDriver().manage().timeouts()
	// .implicitlyWait((Base.timeoutLong), TimeUnit.SECONDS);
	// this.waitForLoadMaskDismissed();
	//
	// } catch (WebDriverException ex) {
	// Logging.error("The Element in selectCalendarDate is not found");
	// throw ex;
	// }finally{
	// Tools.setDriverDefaultValues();
	// }
	// Logging.info("Finished to select calendar date");
	// }

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 16, 2015<br>
	 * <b>Description</b><br>
	 * This method will select specific date and click the day
	 * <b>Parameters</b><br>
	 * LocalDateTime date - the date you want to select in side bar int area -
	 * the area of calendar, now , its including 2 parts, one is on side panel,
	 * the other is in event view
	 */
	public void selectCalendarDate(LocalDateTime date, int area) {
		Logging.info("Starts to select calendar date");

		try {
			switch (area) {
			case 1:
				String calendar_side_panel_datepicker = PropertyHelper.coreCalendar
						.getPropertyValue("calendar_side_panel_datepicker");
				this.selectInDatePicker(date.toLocalDate(), calendar_side_panel_datepicker);
				break;
			case 2:
				this.selectDateInFloatingDatePicker(date.toLocalDate());
				break;
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not select calendar date: " + date);
			throw ex;
		}

	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 19, 2015<br>
	 * <b>Description</b><br>
	 * Verify whether the specific toolbar button is clicked.
	 * 
	 * <b>Parameters</b><br>
	 * String button: - Day - Week -Month - List
	 * 
	 */
	public boolean verifyToolBarButtonPressed(String button) {
		Logging.info("Verifying " + button + " button is pressed");

		boolean result = false;
		String calendar_toolbar_button = PropertyHelper.coreCalendar.getPropertyValue("calendar_toolbar_button")
				.replace("+variable+", button);
		try {
			Tools.setDriverDefaultValues();
			result = Base.base.getDriver().findElement(By.xpath(calendar_toolbar_button)).getAttribute("class")
					.contains("x-btn-pressed");
			if (result) {
				Logging.info(button + " button is actually pressed");
			} else {
				Logging.info(button + " button is NOT pressed");
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not verify " + button + "button is pressed");
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 21, 2015<br>
	 * <b>Description</b><br>
	 * Verify whether the specific time point is in daytime in Day view.
	 * 
	 * <b>Parameters</b><br>
	 * String time: the whole point of time One of the following item should be
	 * provided: - 6:00 AM - 7:00 AM - ... - 6:00 PM - 7:00 PM - ...
	 * 
	 */
	public boolean verifyTimePointInDaytimeInDayView(String time) {
		Logging.info("Verifying specific time " + time + " is in daytime");

		boolean result = false;
		String calendar_time_background_row_inDayView = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_time_background_row_inDayView").replace("+variable+", time);
		try {
			Tools.setDriverDefaultValues();
			String attributes = Base.base.getDriver().findElement(By.xpath(calendar_time_background_row_inDayView))
					.getAttribute("class");
			if (!(attributes.contains("cal-hour-prev")) && !(attributes.contains("cal-hour-next"))) {
				result = true;
				Logging.info(time + " is actually in daytime");
			} else {
				Logging.info(time + " is NOT in daytime");
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not verify " + time + " is in daytime");
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 21, 2015<br>
	 * <b>Description</b><br>
	 * Verify whether the specific week date is the start of week in month view.
	 * 
	 * <b>Parameters</b><br>
	 * String time: the date of week One of the following item should be
	 * provided: - MONDAY - TUESDAY - WEDNESDAY - THURDAY - FRIDAY - SATURDAT -
	 * SUNDAY
	 * 
	 */
	public boolean verifyWeekDateIsStartOfWeekInMonthView(String weekdate) {
		Logging.info("Verifying " + weekdate + " is the start of week");

		boolean result = false;
		String calendar_head_day_inMonthView = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_head_day_inMonthView").replace("+variable+", weekdate);
		try {
			Tools.setDriverDefaultValues();
			result = Base.base.getDriver().findElement(By.xpath(calendar_head_day_inMonthView)).getAttribute("class")
					.contains("rui-cal-day-first");
			if (result) {
				Logging.info(weekdate + " is the start of week");
			} else {
				Logging.info(weekdate + " is NOT the start of week");
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not verify " + weekdate + " is the start of week");
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 22, 2015<br>
	 * <b>Description</b><br>
	 * Get start time in event editor panel.
	 * 
	 * <b>Return</b><br>
	 * Return the start date time
	 */
	public String getStartTimeOnEventEditorPanel() {
		Logging.info("Getting start time in event editor panel");

		String calendar_event_add_panel_timefield_start = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_add_panel_timefield_start");
		String calendar_event_add_panel_datefield_start = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_add_panel_datefield_start");
		String time = null, date = null, datetime = null;

		try {
			Tools.setDriverDefaultValues();

			time = Base.base.getDriver().findElement(By.xpath(calendar_event_add_panel_timefield_start))
					.getAttribute("value");
			date = Base.base.getDriver().findElement(By.xpath(calendar_event_add_panel_datefield_start))
					.getAttribute("value");
			datetime = date + " " + time;

		} catch (WebDriverException ex) {
			Logging.error("Could not get start time in event editor panel");
			throw ex;
		}
		Logging.info("Start datetime in event editor panel is: " + datetime);
		return datetime;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 22, 2015<br>
	 * <b>Description</b><br>
	 * Get end time in event editor panel.
	 * 
	 * <b>Return</b><br>
	 * Return the end date time
	 */
	public String getEndTimeOnEventEditorPanel() {
		Logging.info("Getting end time in event editor panel");

		String calendar_event_add_panel_timefield_end = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_add_panel_timefield_end");
		String calendar_event_add_panel_datefield_end = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_add_panel_datefield_end");
		String time = null, date = null, datetime = null;

		try {
			Tools.setDriverDefaultValues();

			time = Base.base.getDriver().findElement(By.xpath(calendar_event_add_panel_timefield_end))
					.getAttribute("value");
			date = Base.base.getDriver().findElement(By.xpath(calendar_event_add_panel_datefield_end))
					.getAttribute("value");
			datetime = date + " " + time;

		} catch (WebDriverException ex) {
			Logging.error("Could not get end time in event editor panel");
			throw ex;
		}
		Logging.info("End datetime in event editor panel is: " + datetime);
		return datetime;
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 21, 2015<br>
	 * <b>Description</b><br>
	 * This method will set start time in event editor panel.
	 * 
	 * <b>Parameters</b><br>
	 * LocalDateTime datetTime: dateTime of event
	 */

	public void setStartDateOnEventEditorPanel(LocalDateTime eventStartDateTime) {
		Logging.info("Setting start time: " + eventStartDateTime + " in event editor panel");

		String calendar_event_start_date = PropertyHelper.coreCalendar.getPropertyValue("calendar_event_start_date");

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(calendar_event_start_date)).click();
			Base.base.getDriver().manage().timeouts().implicitlyWait((Base.timeoutShort), TimeUnit.SECONDS);

			this.selectCalendarDate(eventStartDateTime, EVENTCALENDAR);

		} catch (WebDriverException ex) {
			Logging.error("Could not set start time in event editor panel.");
			throw ex;
		} finally {
			Tools.setDriverDefaultValues();
		}
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 21, 2015<br>
	 * <b>Description</b><br>
	 * This method will set end time in event editor panel.
	 * 
	 * <b>Parameters</b><br>
	 * LocalDateTime datetTime: dateTime of event
	 */
	public void setEndDateOnEventEditorPanel(LocalDateTime eventEndTimeFormat) {

		Logging.info("Setting end time: " + eventEndTimeFormat + " in event editor panel");
		String calendar_event_end_date = PropertyHelper.coreCalendar.getPropertyValue("calendar_event_end_date");

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(calendar_event_end_date)).click();
			Base.base.getDriver().manage().timeouts().implicitlyWait((Base.timeoutShort), TimeUnit.SECONDS);

			this.selectCalendarDate(eventEndTimeFormat, EVENTCALENDAR);

		} catch (WebDriverException ex) {
			Logging.error("Could not set end time in event editor panel.");
			throw ex;
		} finally {
			Tools.setDriverDefaultValues();
		}
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Oct 13, 2015<br>
	 * <b>Description</b><br>
	 * This method enable all day event
	 * 
	 */
	public void EnableAllDayEvent() {
		Logging.info("Enable all day event");

		String calendar_event_allday_option = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_allday_option");
		try {
			Tools.setDriverDefaultValues();

			WebElement allDay = Base.base.getDriver().findElement(By.xpath(calendar_event_allday_option));
			if (!allDay.isSelected()) {
				allDay.click();
			}
		} catch (WebDriverException ex) {
			Logging.error("Coundn't enable all day event");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 23, 2015<br>
	 * <b>Description</b><br>
	 * This method will set repeat frequency of the event.
	 * 
	 * <b>Parameters</b><br>
	 * String repeatableTime : repeat time of the event
	 * 
	 */
	public void setEventRepeatbleTime(String repeatableTime) {
		Logging.info("Start to select repeatable time");

		String calendar_event_repeatable = PropertyHelper.coreCalendar.getPropertyValue("calendar_event_repeatable");
		String calendar_event_repeatable_selected = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_repeatable_selected");

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(calendar_event_repeatable)).click();
			Base.base.getDriver()
					.findElement(By.xpath(calendar_event_repeatable_selected.replace("+variable+", repeatableTime)))
					.click();
		} catch (WebDriverException ex) {
			Logging.error("Element is not found in setEventRepeatbleTime");
			throw ex;
		}
		Logging.info("Finished to set event repeatable time");

	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Sept 18, 2016<br>
	 * <b>Description</b><br>
	 * This method will set interval value of the recurrence event.
	 * 
	 * <b>Parameters</b><br>
	 * int interval : the interval value
	 * 
	 */
	public void setRecurrenceEventInterval(int interval) {
		Logging.info("Start to input interval time");

		String calendar_event_interval_input = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_interval_input");

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(calendar_event_interval_input)).clear();
			Base.base.getDriver().findElement(By.xpath(calendar_event_interval_input))
					.sendKeys(String.valueOf(interval));
		} catch (WebDriverException ex) {
			Logging.error("Element is not found in setRecurrenceEventInterval");
			throw ex;
		}
		Logging.info("Finished to set event repeat interval");

	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Oct 17, 2016<br>
	 * <b>Description</b><br>
	 * This method will click the event on the specific date in month view
	 * <b>Parameters</b><br>
	 * LocalDate endDate : the specific date
	 * 
	 */
	public void clickEventInSpecificDateslot(LocalDate date) {
		Logging.info("Start to click event on date " + date.toString());
		int day = date.getDayOfMonth();
		// Sunday -- the 1st day of the week and the dayOfWeek of Sunday should
		// be 1;
		// Monday -- the 2nd of the week the dayOfWeek of Monday should be 2;
		int dayOfWeek = date.getDayOfWeek().getValue() + 1;
		if (dayOfWeek == 8)
			dayOfWeek = 1;

		// replace +dayOfWeek+ with value of dayOfWeek , and get the xpath of
		// the event of the specific day
		String calendar_event_on_specific_dateslot = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_on_specific_dateslot").replace("+variable+", String.valueOf(day))
				.replace("+dayOfWeek+", String.valueOf(dayOfWeek));
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(calendar_event_on_specific_dateslot)).click();
		} catch (WebDriverException ex) {
			Logging.error("Element is not found in clickEventInSpecificDateslot");
			throw ex;
		}
		Logging.info("Finished to click the event on date:" + date.toString());
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Oct 17, 2016<br>
	 * <b>Description</b><br>
	 * This method will verify if the event on the specific date exists in month
	 * view <b>Parameters</b><br>
	 * LocalDate endDate : the specific date
	 * 
	 */
	public boolean verifyEventInSpecificDateslotExist(LocalDate date) {
		Logging.info("Start to verify if event on date exist " + date.toString());
		int day = date.getDayOfMonth();
		int dayOfWeek = date.getDayOfWeek().getValue() + 1;
		if (dayOfWeek == 8)
			dayOfWeek = 1;
		Logging.info(String.valueOf(dayOfWeek));
		boolean result = false;
		String calendar_event_on_specific_dateslot = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_on_specific_dateslot").replace("+variable+", String.valueOf(day))
				.replace("+dayOfWeek+", String.valueOf(dayOfWeek));
	
		try {
			Tools.setDriverDefaultValues();
			List<WebElement> list = Base.base.getDriver().findElements(By.xpath(calendar_event_on_specific_dateslot));
			if (list.size() != 0)
				result = true;
		} catch (WebDriverException ex) {
			Logging.error("Element is not found in verifyEventInSpecificDateslotExist");
			throw ex;
		}
		Logging.info("Finished to verify if  the event exist on date:" + date.toString());
		return result;
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Sept 18, 2016<br>
	 * <b>Description</b><br>
	 * This method will click the specific end date input box.
	 * <b>Parameters</b><br>
	 * LocalDate endDate : the specific end date
	 * 
	 */
	public void clickRecurrenceEventEndsOnASpecificDateInputBox() {
		Logging.info("Start to click the specfic end date input box");

		String calendar_endDate_input = PropertyHelper.coreCalendar.getPropertyValue("calendar_endDate_input");

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(calendar_endDate_input)).click();
		} catch (WebDriverException ex) {
			Logging.error("Element is not found in clickRecurrenceEventEndsOnASpecificDateInputBox");
			throw ex;
		}
		Logging.info("Finished to click the specfic end date input box");
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Oct 18, 2016<br>
	 * <b>Description</b><br>
	 * This method will check the week day of the weekly Repeating Event.
	 * <b>Parameters</b><br>
	 * String weekDay : SUN,MON,TUE,WED,THu,FRI,SAT
	 * 
	 */
	public void checkWeekDayOnNewEventPage(String weekDay, boolean selected) {

		String calendar_new_event_week_day_checkbox = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_new_event_week_day_checkbox").replace("+variable+", weekDay);
		String calendar_checkbox_subItem = PropertyHelper.coreCalendar.getPropertyValue("calendar_checkbox_subItem");

		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(calendar_new_event_week_day_checkbox);

			boolean isChecked = Base.base.getDriver().findElement(By.xpath(calendar_new_event_week_day_checkbox))
					.getAttribute("class").contains("x-form-cb-checked");

			if (selected) {

				if (!isChecked) {
					Base.base.getDriver().findElement(By.xpath(calendar_new_event_week_day_checkbox))
							.findElement(By.xpath(calendar_checkbox_subItem)).click();
				}
				Logging.info("Checked on checkbox: " + weekDay);
			} else {

				if (isChecked) {

					Base.base.getDriver().findElement(By.xpath(calendar_new_event_week_day_checkbox))
							.findElement(By.xpath(calendar_checkbox_subItem)).click();
				}
				Logging.info(weekDay);
			}

		} catch (WebDriverException ex) {
			Logging.error("Could not change Mail General Settings - " + weekDay);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Sept 18, 2016<br>
	 * <b>Description</b><br>
	 * This method will set Repeating Event End Type value of the recurrence
	 * event. <b>Parameters</b><br>
	 * String type : value can be "Never ends","Ends after a number of
	 * times","Ends on a specific date"
	 * 
	 */

	public void selectRepeatingEventEndType(String type) {
		Logging.info("Start to set recurrence event ending method:" + type);

		String calendar_event_ends_select = PropertyHelper.coreCalendar.getPropertyValue("calendar_event_ends_select");
		String calendar_event_ends_option = PropertyHelper.coreCalendar.getPropertyValue("calendar_event_ends_option")
				.replace("+variable+", type);
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(calendar_event_ends_select)).click();
			Base.base.getDriver().findElement(By.xpath(calendar_event_ends_option)).click();
		} catch (WebDriverException ex) {
			Logging.error("Element is not found in selectRepeatingEventEndType");
			throw ex;
		}
		Logging.info("Finished to set recurrence event ending type:" + type);
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Sept 18, 2016<br>
	 * <b>Description</b><br>
	 * This method will set the event repeating times value..
	 * 
	 * <b>Parameters</b><br>
	 * int times -- the event repeating times value.
	 * 
	 */
	public void setRecurrenceEventEndsAfterANumberOfTime(int times) {
		Logging.info("Start to set recurrence event endsAfterANumberOfTime:" + times);

		String calendar_event_ends_afterANumberOfTime = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_ends_afterANumberOfTime");
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(calendar_event_ends_afterANumberOfTime)).clear();
			Base.base.getDriver().findElement(By.xpath(calendar_event_ends_afterANumberOfTime))
					.sendKeys(String.valueOf(times));
		} catch (WebDriverException ex) {
			Logging.error("Element is not found in setRecurrenceEventEndsAfterANumberOfTime");
			throw ex;
		}
		Logging.info("Finished to set recurrence event ends after a mumber of time:" + times);

	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Jan 27, 2016<br>
	 * <b>Description</b><br>
	 * This method will set reminder value of the event.
	 * 
	 * <b>Parameters</b><br>
	 * String reminderTime : reminder of the event
	 * 
	 */
	public void setEventReminder(String reminderTime) {
		Logging.info("Start to set reminder value");
		String calendar_event_reminder_day = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_reminder_day");

		String calendar_event_reminderPicker = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_reminderPicker").replace("+variable+", reminderTime);

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(calendar_event_reminder_day)).click();
			Tools.scrollElementIntoViewByXpath(calendar_event_reminderPicker);
			Base.base.getDriver().findElement(By.xpath(calendar_event_reminderPicker)).click();

		} catch (WebDriverException ex) {
			Logging.error("Element is not found");
			throw ex;
		}
		Logging.info("Finished to set reminder time");
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 23, 2015<br>
	 * <b>Description</b><br>
	 * This method will set reminder value of the event.
	 * 
	 * <b>Parameters</b><br>
	 * long reminderTime : reminder of the event
	 * 
	 */
	public void setEventReminderValue(long reminderTime) {
		Logging.info("Start to set reminder value");
		String calendar_event_reminder_day = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_reminder_day");
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(calendar_event_reminder_day)).clear();
			Base.base.getDriver().findElement(By.xpath(calendar_event_reminder_day))
					.sendKeys(String.valueOf(reminderTime));
		} catch (WebDriverException ex) {
			Logging.error("Element is not found in setEventReminderValue");
			throw ex;
		}
		Logging.info("Finished to set event repeatable time");
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 23, 2015<br>
	 * <b>Description</b><br>
	 * This method will set reminder frequency of the event.
	 * 
	 * <b>Parameters</b><br>
	 * String reminderFreq : reminder frequency of the event
	 * 
	 */
	public void setEventReminderFrequency(String reminderFreq) {
		Logging.info("Start to set reminder time frequency");
		String calendar_event_reminder_freq_time = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_reminder_freq_time");
		String calendar_event_reminder_freq_time_selected = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_reminder_freq_time_selected");
		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(calendar_event_reminder_freq_time)).click();
			Base.base.getDriver()
					.findElement(
							By.xpath(calendar_event_reminder_freq_time_selected.replace("+variable+", reminderFreq)))
					.click();
		} catch (WebDriverException ex) {
			Logging.error("Element is not found in setReminderFrequency");
			throw ex;
		}
		Logging.info("Finished to set event reminder frequency");
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 23, 2015<br>
	 * <b>Description</b><br>
	 * This method will set calendar name of the event.
	 * 
	 * <b>Parameters</b><br>
	 * String calendarName : calendar name of the event
	 * 
	 */
	public void setEventCalendarName(String calendarName) {
		Logging.info("Start to set calendar name");
		String calendar_event_calendar_name = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_calendar_name");
		String calendar_event_calendar_name_selected = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_calendar_name_selected");

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(calendar_event_calendar_name)).click();
			Base.base.getDriver()
					.findElement(By.xpath(calendar_event_calendar_name_selected.replace("+variable+", calendarName)))
					.click();
		} catch (WebDriverException ex) {
			Logging.error("Element is not found in setEventCalendarName");
			throw ex;
		}
		Logging.info("Finished to set event reminder frequency");
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 23, 2015<br>
	 * <b>Description</b><br>
	 * This method will set calendar category of the event.
	 * 
	 * <b>Parameters</b><br>
	 * String category : calendar category of the event
	 * 
	 */
	public void setEventCategory(String category) {
		Logging.info("Start to select category");
		String calendar_event_category = PropertyHelper.coreCalendar.getPropertyValue("calendar_event_category");
		String calendar_event_category_selected = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_category_selected");

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(calendar_event_category)).click();
			Base.base.getDriver()
					.findElement(By.xpath(calendar_event_category_selected.replace("+variable+", category))).click();
		} catch (WebDriverException ex) {
			Logging.error("Element is not found in setEventCategory");
			throw ex;
		}
		Logging.info("Finished to set event category ");

	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 23, 2015<br>
	 * <b>Description</b><br>
	 * This method will set calendar description of the event.
	 * 
	 * <b>Parameters</b><br>
	 * String description : calendar description of the event
	 * 
	 */
	public void setEventDescription(String description) {
		Logging.info("Start to write description");
		String calendar_event_description = PropertyHelper.coreCalendar.getPropertyValue("calendar_event_description");
		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(calendar_event_description)).clear();
			Base.base.getDriver().findElement(By.xpath(calendar_event_description)).sendKeys(description);
		} catch (WebDriverException ex) {
			Logging.error("Element is not found in setEventLocation");
			throw ex;
		}
		Logging.info("Finished to set event location ");

	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 22, 2015<br>
	 * <b>Description</b><br>
	 * This method will searchEvent event.
	 * 
	 * <b>Parameters</b><br>
	 * String eventName: the event name you're looking for
	 */
	public void searchEvent(String eventName) {
		Logging.info("Start to search event: " + eventName);
		String calendar_search_input = PropertyHelper.coreCalendar.getPropertyValue("calendar_search_input");
		String calendar_search_button = PropertyHelper.coreCalendar.getPropertyValue("calendar_search_button");
		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(calendar_search_input)).clear();
			Base.base.getDriver().findElement(By.xpath(calendar_search_input)).sendKeys(eventName);
			Base.base.getDriver().findElement(By.xpath(calendar_search_button)).click();
			Tools.waitFor(this.timeoutLong, TimeUnit.MILLISECONDS);

		} catch (WebDriverException ex) {
			Logging.error("The element is not found in searchEvent");
			throw ex;
		}
		Logging.info("Finish to search event: " + eventName);

	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 22, 2015<br>
	 * <b>Description</b><br>
	 * This method will verify the search result
	 * 
	 * <b>Parameters</b><br>
	 * String postiveEventName: expected event name int eventDayCountExpect:
	 * expected number of the event boolean empty: if the search result should
	 * be empty, will pass true, else, pass false
	 */
	public boolean verifyCalendarEventSearchResult(String postiveEventName, int eventDayCountExpect, boolean empty) {
		Logging.info("Start to verify calendar, expect event name " + postiveEventName
				+ " and expect event day count is " + eventDayCountExpect);
		String calendar_search_display_count = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_search_display_count");
		String calendar_search_result_collection = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_search_result_collection");
		String calendar_search_back_button = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_search_back_button");
		boolean result = false;
		String expectSearchTitle = null;
		String verifyEmptyKeyWord = "Display";

		if (empty) {
			expectSearchTitle = "No data to display";
			verifyEmptyKeyWord = expectSearchTitle;
		} else
			expectSearchTitle = "Displaying 1 - " + eventDayCountExpect + " of " + eventDayCountExpect;

		try {

			Tools.setDriverDefaultValues();

			int eventCount = 0;
			String displayCount = Base.base.getDriver()
					.findElement(By.xpath(calendar_search_display_count.replace("+variable+", verifyEmptyKeyWord)))
					.getText();
			List<WebElement> events = Base.base.getDriver().findElements(By.xpath(calendar_search_result_collection));

			Logging.info("expected search event title is " + postiveEventName);
			Logging.info("actual search event title is " + displayCount);

			// check search title part
			if (displayCount.equals(expectSearchTitle) && events.size() == eventDayCountExpect) {
				// check search event name
				while (!result) {
					result = events.get(eventCount).getText().contains(postiveEventName);
					eventCount++;
				}
				Logging.info("eventCount is " + eventCount);
			} else
				result = false;
			Base.base.getDriver().findElement(By.xpath(calendar_search_back_button)).click();
			Tools.waitFor(Base.timeoutLong, TimeUnit.MILLISECONDS);

		} catch (WebDriverException ex) {
			Logging.error("The element is not found in verifyCalendarEventSearchResult");
		}
		return result;
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Sep 23, 2015<br>
	 * <b>Description</b><br>
	 * This method will click edit or delete button on event summary window.
	 * String button - button to click. One of the following button names should
	 * be supplied: - Edit - Delete
	 */
	public void clickToolBarButtonInEventSummaryWindow(String summary, LocalDateTime eventStartDateTime,
			LocalDateTime eventEndDateTime, String button) {
		Logging.info("Clicking on  " + button + " button in event summary window ");

		String calendar_event_summary_window_toolbar_button = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_summary_window_toolbar_button").replace("+variable+", button);

		String calendar_event_inDiffView = "";

		if (this.verifyToolBarButtonPressed("Month")) {
			calendar_event_inDiffView = PropertyHelper.coreCalendar.getPropertyValue("calendar_event_inMonthView")
					.replace("+variable+", summary);
		} else if (this.verifyToolBarButtonPressed("Week")) {
			calendar_event_inDiffView = PropertyHelper.coreCalendar.getPropertyValue("calendar_event_inWeekView")
					.replace("+variable+", summary);
		} else if (this.verifyToolBarButtonPressed("Day")) {
			calendar_event_inDiffView = PropertyHelper.coreCalendar.getPropertyValue("calendar_event_inDayView")
					.replace("+variable+", summary);
		}

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(calendar_event_inDiffView)).click();
			Base.base.getDriver().findElement(By.xpath(calendar_event_summary_window_toolbar_button)).click();
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on " + button + " button on event summary window.");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Sep 23, 2015<br>
	 * <b>Description</b><br>
	 * This method will verify if a specific event exists in month/week/day view
	 * by expected date and summary.
	 * 
	 * <b>Parameters</b><br>
	 * String summary - the summary of the event LocalDateTime
	 * eventStartDateTime - event start time LocalDateTime eventEndDateTime -
	 * event end time String settingDateFormat - the format of date in setting
	 * String strTimeFormat - the format of time in setting
	 * 
	 * <b>Return</b><br>
	 * Return boolean - true: event found false: event not found
	 */
	public boolean verifyEventExistBySummaryAndDate(String summary, LocalDateTime eventStartDateTime,
			LocalDateTime eventEndDateTime, String settingDateFormat, String settingTimeFormat) {

		Logging.info("Verifying event " + summary + " in month view ");

		boolean result = false;
		WebElement event = null;
		String strTimeFormat = "";
		String strDateFormatOneDay = "";
		String strDateFormatMoreDay_1 = "MMMM d, ";
		String strDateFormatMoreDay_2 = "";

		if (settingTimeFormat.equals("24")) {
			strTimeFormat = "H:mm";
		} else if (settingTimeFormat.equals("12")) {
			strTimeFormat = "h:mm a";
		}

		// get date format of one day event in event summary window by the date
		// format setting
		if (settingDateFormat.equals("YYYY/MM/DD") || settingDateFormat.equals("MM/DD/YYYY")) {
			strDateFormatOneDay = "E, MMM d, ";
		} else {
			strDateFormatOneDay = "E, d MMM, ";
		}

		// get date format of more days event in event summary window by the
		// date format setting
		if (settingDateFormat.equals("DD/MM/YY")) {
			strDateFormatMoreDay_2 = "d MMM, yy, ";
		} else if (settingDateFormat.equals("YYYY/MM/DD")) {
			strDateFormatMoreDay_2 = "yyyy MMM d, ";
		} else if (settingDateFormat.equals("DD/MM/YYYY") || settingDateFormat.equals("DD-MM-YYYY")
				|| settingDateFormat.equals("DD.MM.YYYY")) {
			strDateFormatMoreDay_2 = "d MMM, yyyy, ";
		} else if (settingDateFormat.equals("MM/DD/YYYY")) {
			strDateFormatMoreDay_2 = "MMM d, yyyy, ";
		}

		String calendar_event_summary_window_time_period = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_summary_window_time_period");
		String calendar_event_summmary_window_close_button = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_summmary_window_close_button");

		long spanDays = ChronoUnit.DAYS.between(eventStartDateTime, eventEndDateTime);
		// if the event begins at one day and ends in another day ,
		// the spandays should be more 1.
		if (!eventStartDateTime.toLocalDate().isEqual(eventEndDateTime.toLocalDate())) {
			spanDays += 1;
		}
		String calendar_event_inDiffView = "";

		if (this.verifyToolBarButtonPressed("Month")) {
			calendar_event_inDiffView = PropertyHelper.coreCalendar.getPropertyValue("calendar_event_inMonthView")
					.replace("+variable+", summary);
		} else if (this.verifyToolBarButtonPressed("Week")) {
			calendar_event_inDiffView = PropertyHelper.coreCalendar.getPropertyValue("calendar_event_inWeekView")
					.replace("+variable+", summary);
		} else if (this.verifyToolBarButtonPressed("Day")) {
			calendar_event_inDiffView = PropertyHelper.coreCalendar.getPropertyValue("calendar_event_inDayView")
					.replace("+variable+", summary);
		}

		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(calendar_event_inDiffView);
			// Tools.scrollElementIntoViewByXpath("//div[contains(@class,'cal-day-time-inner')
			// and text()='23:00']");
			event = Base.base.getDriver().findElement(By.xpath(calendar_event_inDiffView));

			// check event name
			if (!event.getText().contains(summary)) {
				Logging.error("Event not found: " + summary);
				throw new WebDriverException();
			}

			event.click();
			String actualSummaryDate = Base.base.getDriver()
					.findElement(By.xpath(calendar_event_summary_window_time_period)).getText();

			Logging.info("actualSummaryDate is " + actualSummaryDate);
			// compared with event name
			if (spanDays == 0) {
				String expectSummaryDate = this.formatDateTime(eventStartDateTime, strDateFormatOneDay + strTimeFormat)
						+ " - " + this.formatDateTime(eventEndDateTime, strTimeFormat);

				Logging.info("expectSummaryDate is " + expectSummaryDate);
				if (actualSummaryDate.equals(expectSummaryDate)) {
					result = true;
				}
			} else {
				String expectSummaryDate1 = this.formatDateTime(eventStartDateTime,
						strDateFormatMoreDay_1 + strTimeFormat) + " - "
						+ this.formatDateTime(eventEndDateTime, strDateFormatMoreDay_1 + strTimeFormat) + ", "
						+ spanDays + " days";

				String expectSummaryDate2 = this.formatDateTime(eventStartDateTime,
						strDateFormatMoreDay_2 + strTimeFormat) + " - "
						+ this.formatDateTime(eventEndDateTime, strDateFormatMoreDay_2 + strTimeFormat) + ", "
						+ spanDays + " days";

				if (actualSummaryDate.equals(expectSummaryDate1) || actualSummaryDate.equals(expectSummaryDate2)) {
					result = true;
				}
				Logging.info("expectSummaryDate1 is " + expectSummaryDate1);
				Logging.info("expectSummaryDate2 is " + expectSummaryDate2);

			}
		} catch (WebDriverException ex) {
			Logging.error("The Element in verifyEventExistBySummaryAndDate is not found");
			throw ex;
		} finally {
			Base.base.getDriver().findElement(By.xpath(calendar_event_summmary_window_close_button)).click();
			return result;
		}
		//Logging.info("Event  " + summary + " exist");
		//return result;
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Sept 16, 2016<br>
	 * <b>Description</b><br>
	 * assert if the recurrence event has correct interval and repeating time
	 * 
	 * <b>Return</b><br>
	 * Return boolean
	 */
	public boolean assertDailyRecurrenceEvent(int interval, int times) {
		Logging.info("Verify daily recurrence event with interval:" + interval + ", times:" + times);
		String calendar_event_list_number_inListView = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_list_number_inListView");
		String details = "Every " + interval + " days, " + times + " times";
		String calerdar_event_detail_repeat = PropertyHelper.coreCalendar
				.getPropertyValue("calerdar_event_detail_repeat").replace("+variable+", details);
		boolean result = false;
		try {
			Tools.setDriverDefaultValues();
			List<WebElement> eventList = Base.base.getDriver()
					.findElements(By.xpath(calendar_event_list_number_inListView));
			if (eventList.size() != times)
				return false;
			Base.base.getDriver().findElements(By.xpath(calerdar_event_detail_repeat));
			result = true;
		} catch (WebDriverException ex) {
			Logging.error("Could not find element in assertDailyRecurrenceEvent ");

		}
		return result;
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 25, 2015<br>
	 * <b>Description</b><br>
	 * This method will verify the time sequency and event info in list view. So
	 * please make sure you added the event in array list in correct sequent.
	 * <b>Parameters</b><br>
	 * ArrayList<EventInfo> : event in array list.
	 * 
	 */
	public boolean VerifyAllEventInSpecificMonthInListView(ArrayList<EventInfo> event, String dataFormat,
			String timeFormat) {
		Logging.info("Verify events in specific month in list view");

		boolean result = false;

		String calendar_event_list_number_inListView = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_list_number_inListView");
		String calendar_event_detail_title = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_detail_title");
		String calendar_event_detail_start_time = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_detail_start_time");
		String calendar_event_detail_end_time = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_detail_end_time");
		String calendar_event_detail_category = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_detail_category");

		try {
			Tools.setDriverDefaultValues();

			// calculate the size of the calendars on the Calendar Window
			List<WebElement> eventList = Base.base.getDriver()
					.findElements(By.xpath(calendar_event_list_number_inListView));

			// go through the calendar lists and delete one by one
			if (event.size() == eventList.size()) {
				for (int i = 0; i < event.size(); i++) {

					String expStartDate = formatDateTime(event.get(i).getStartDateTime(), dataFormat) + " "
							+ formatDateTime(event.get(i).getStartDateTime(), timeFormat);
					String expEndDate = formatDateTime(event.get(i).getEndDateTime(), dataFormat) + " "
							+ formatDateTime(event.get(i).getEndDateTime(), timeFormat);

					// click on specific list
					Base.base.getDriver()
							.findElement(By.xpath("(" + calendar_event_list_number_inListView + ")[" + (i + 1) + "]"))
							.click();

					// get the title of the detail view
					String eventSummary = Base.base.getDriver().findElement(By.xpath(calendar_event_detail_title))
							.getText();
					String eventStartTime = Base.base.getDriver()
							.findElement(By.xpath(calendar_event_detail_start_time)).getText();
					String eventEndTime = Base.base.getDriver().findElement(By.xpath(calendar_event_detail_end_time))
							.getText();
					String eventCategory = Base.base.getDriver().findElement(By.xpath(calendar_event_detail_category))
							.getText();

					if (event.get(i).getSummary().equals(eventSummary) && eventStartTime.contains(expStartDate)
							&& eventEndTime.contains(expEndDate) && event.get(i).getCategory().equals(eventCategory)) {
						result = true;
					} else {
						result = false;
						break;
					}
					this.waitForLoadMaskDismissed();
				}
			} else {
				result = false;
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not delete events in List view");
		}
		return result;
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Sep 24, 2015<br>
	 * <b>Description</b><br>
	 * Input attendees in Add attendee field.
	 * 
	 * <b>Parameters</b><br>
	 * String field - Input event attendees
	 *
	 */
	public void typeInAddAttendee(String strAttendees) {

		Logging.info("Inputting " + strAttendees + " in Add attendee field");

		String calendar_event_add_panel_attendee_input = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_add_panel_attendee_input");

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(calendar_event_add_panel_attendee_input)).clear();
			Base.base.getDriver().findElement(By.xpath(calendar_event_add_panel_attendee_input)).sendKeys(strAttendees);
			// Sends an extra Tab key to avoid element focusing issue of the
			// attendee plus button.
			Base.base.getDriver().findElement(By.xpath(calendar_event_add_panel_attendee_input)).sendKeys(Keys.TAB);
		} catch (WebDriverException ex) {
			Logging.error("Could not input " + strAttendees + " in Add attendee field");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Sep 24, 2015<br>
	 * <b>Description</b><br>
	 * This method will click attendee plus button.
	 * 
	 */
	public void clickAttendeePlusButton() {
		Logging.info("Clicking attendee plus button");

		String calendar_event_add_panel_attendee_plus_button = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_add_panel_attendee_plus_button");
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(calendar_event_add_panel_attendee_plus_button)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could click attendee plus button");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Sep 25, 2015<br>
	 * <b>Description</b><br>
	 * This method will verify if an attendee exists in month/week/day view by
	 * expected summary.
	 * 
	 * <b>Parameters</b><br>
	 * String summary - the summary of the event
	 * 
	 * <b>Return</b><br>
	 * Return boolean - true: event found false: event not found
	 */
	public boolean verifyAttendeesOnEventSummaryWindow(List<String> expectAttendeeList) {

		Logging.info("Verifying attendee exist on event summary window.");

		List<String> attendeesList = new ArrayList<String>();
		boolean result = false;

		String calendar_event_summary_window_attendee = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_summary_window_attendee");

		try {
			Tools.setDriverDefaultValues();

			List<WebElement> attendeesListXpath = Base.base.getDriver()
					.findElements(By.xpath(calendar_event_summary_window_attendee));

			int numOfAttendees = attendeesListXpath.size();

			if (numOfAttendees > 0) {
				for (int i = 1; i <= numOfAttendees; i++) {

					Tools.scrollElementIntoViewByXpath("(" + calendar_event_summary_window_attendee + ")[" + i + "]");
					String attendeeAddress = Base.base.getDriver()
							.findElement(By.xpath("(" + calendar_event_summary_window_attendee + ")[" + i + "]"))
							.getText();
					attendeesList.add(attendeeAddress);
				}
			}
			if (expectAttendeeList.size() != attendeesList.size()) {
				result = false;
			}
			Collections.sort(expectAttendeeList);
			Collections.sort(attendeesList);
			if (attendeesList.equals(expectAttendeeList)) {
				result = true;
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not find attendee on event summary window.");
		}
		return result;
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Sep 25, 2015<br>
	 * <b>Description</b><br>
	 * This method will click event in month/week/day view by summary.
	 * 
	 * <b>Parameters</b><br>
	 * String summary - the summary of the event
	 * 
	 */
	public void clickEventBySummary(String summary) {

		Logging.info("Verifying event " + summary + " in month view ");
		WebElement event = null;

		String calendar_event_inDiffView = "";

		if (this.verifyToolBarButtonPressed("Month")) {
			calendar_event_inDiffView = PropertyHelper.coreCalendar.getPropertyValue("calendar_event_inMonthView")
					.replace("+variable+", summary);
		} else if (this.verifyToolBarButtonPressed("Week")) {
			calendar_event_inDiffView = PropertyHelper.coreCalendar.getPropertyValue("calendar_event_inWeekView")
					.replace("+variable+", summary);
		} else if (this.verifyToolBarButtonPressed("Day")) {
			calendar_event_inDiffView = PropertyHelper.coreCalendar.getPropertyValue("calendar_event_inDayView")
					.replace("+variable+", summary);
		}

		try {
			Tools.setDriverDefaultValues();

			event = Base.base.getDriver().findElement(By.xpath(calendar_event_inDiffView));
			// check event name
			if (!event.getText().contains(summary)) {
				Logging.error("Not a find the event: " + summary);
				throw new WebDriverException();
			}
			event.click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click event");
		}
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Sep 25, 2015<br>
	 * <b>Description</b><br>
	 * This method will close event summary window.
	 * 
	 */
	public void clickCloseEventSummaryWindow() {
		Logging.info("Clicking close on event summary window");

		String calendar_event_summmary_window_close_button = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_summmary_window_close_button");
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(calendar_event_summmary_window_close_button)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click close on event summary window");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Sep 28, 2015<br>
	 * <b>Description</b><br>
	 * This method will verify if the event reminder exists on event summary
	 * window
	 * 
	 * <b>Parameters</b><br>
	 * long reminder - reminder time
	 * 
	 * <b>Return</b><br>
	 * Return boolean - true: reminder found false: reminder not found
	 */
	public boolean verifyReminderOnEventSummaryWindow(String reminder) {
		boolean result = false;

		Logging.info("Verifying reminder" + " exist on event summary window.");

		String calendar_event_summary_window_reminder = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_event_summary_window_reminder");

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElements(By.xpath(calendar_event_summary_window_reminder));
			result = true;
			Logging.info(reminder + " is found on event summary window.");
		} catch (WebDriverException ex) {
			Logging.error("Could not find " + reminder + " on event summary window.");
		}
		return result;
	}

	/**
	 * <b>Author</b>: Lynn Li<br>
	 * <b>Date</b>: Oct 09, 2015<br>
	 * <b>Description</b><br>
	 * This method will click share calendar icon.
	 * 
	 * <b>Parameters</b><br>
	 * String calendatName - calendar's name
	 */
	public void clickShareCalendarIcon(String calendarName) {
		Logging.info("Clicking share calendar icon of " + calendarName);

		String calendar_share_calendar_icon = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_share_calendar_icon").replace("+variable+", calendarName);

		String calendar_list_by_name = PropertyHelper.coreCalendar.getPropertyValue("calendar_list_by_name")
				.replace("+variable+", calendarName);
		try {
			Tools.setDriverDefaultValues();

			Tools.moveFocusToElementByXpath(calendar_list_by_name);
			Base.base.getDriver().findElement(By.xpath(calendar_share_calendar_icon)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click share calendar icon of " + calendarName);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Lynn Li <br>
	 * <b>Date</b>: Oct 09, 2015 <br>
	 * <b>Description</b><br>
	 * Type the share cal email filed in add share calendar window
	 * 
	 * <b>Parameters</b><br>
	 * String name - the email input
	 */
	public void typeInEmailFieldOnShareCalendarWindow(String name) {
		Logging.info("Typing the share calendar email: " + name + "in share calendar window");

		String calendar_share_addEmail_filed = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_share_addEmail_filed");

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(calendar_share_addEmail_filed)).clear();
			Base.base.getDriver().findElement(By.xpath(calendar_share_addEmail_filed)).sendKeys(name);

		} catch (WebDriverException ex) {
			Logging.error("Could not type " + name + " in add email filed in share calendar window");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Lynn Li <br>
	 * <b>Date</b>: Oct 09, 2015 <br>
	 * <b>Description</b><br>
	 * Select the calendar access type
	 * 
	 * <b>Parameters</b><br>
	 * String acessType - the calendar access type -View Only -View and Edit
	 */
	public void selectCalendarAcessTypeOnShareCalWindow(String acessType) {
		Logging.info("select the share calendar type " + acessType);

		String calendar_share_access_arrow = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_share_access_arrow");
		String calendar_share_access_menu = PropertyHelper.coreCalendar.getPropertyValue("calendar_share_access_menu")
				.replace("+variable+", acessType);

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(calendar_share_access_arrow)).click();
			Base.base.getDriver().findElement(By.xpath(calendar_share_access_menu)).click();

		} catch (WebDriverException ex) {
			Logging.error("Could not select the share cal type:  " + acessType);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Lynn Li <br>
	 * <b>Date</b>: Oct 09, 2015 <br>
	 * <b>Description</b><br>
	 * click the toolbar button on the share calendar windows
	 * 
	 * <b>Parameters</b><br>
	 * String button - the button on share calendar window -Save -Cancel
	 */
	public void clickToolBarButtonOnShareCalendarWindow(String button) {
		Logging.info("Clicking " + button + " button on share calendar window");

		String calendar_share_toolbar_button = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_share_toolbar_button").replace("+variable+", button);

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(calendar_share_toolbar_button)).click();
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not click " + button + " on share calendar window");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Lynn Li <br>
	 * <b>Date</b>: Oct 09, 2015 <br>
	 * <b>Description</b><br>
	 * click the "+" button on the Share calendar window
	 * 
	 */
	public void clickPlusButtonOnShareCalendarWindow() {
		Logging.info("Clicking plus button in share calendar window");

		String calendar_share_plus_button = PropertyHelper.coreCalendar.getPropertyValue("calendar_share_plus_button");

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(calendar_share_plus_button)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click " + "/+" + " in share calendar window");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Lynn Li <br>
	 * <b>Date</b>: Oct 09, 2015 <br>
	 * <b>Description</b><br>
	 * click the "Link" button of Share calendar
	 * 
	 */
	public void clickLinkButtonInShareCalendarEmail() {
		Logging.info("Clicking Link button in share calendar notification email");

		String calendar_share_link = PropertyHelper.coreCalendar.getPropertyValue("calendar_share_link");

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(calendar_share_link)).click();
			this.waitForLoadMaskDismissed();
			Tools.waitFor(2, TimeUnit.SECONDS);
		} catch (WebDriverException ex) {
			Logging.error("Could not click Link button");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Lynn Li <br>
	 * <b>Date</b>: Oct 09, 2015 <br>
	 * <b>Description</b><br>
	 * verify the share cal name and its owner name followed by 'Subscribed
	 * Calendar';
	 * 
	 * <b>Parameters</b><br>
	 * String calName - shared calendar name
	 * 
	 * <b>Return</b><br>
	 * Return true if found, false if not
	 */
	public boolean verifyShareCalInfo(String calName) {
		Logging.info("Verifying the shared calendar name: " + calName);
		boolean result = false;
		String calendar_share_name = PropertyHelper.coreCalendar.getPropertyValue("calendar_share_name")
				.replace("+variable+", calName);

		try {
			Tools.setDriverDefaultValues();

			result = Base.base.getDriver().findElement(By.xpath(calendar_share_name)).getText().equals(calName);
		} catch (WebDriverException ex) {
			Logging.error("Couldn't find the shared calendar: " + calName);
		}
		return result;
	}

	/**
	 * <b>Author</b>: Lynn Li <br>
	 * <b>Date</b>: Oct 09, 2015 <br>
	 * <b>Description</b><br>
	 * Verify the shared calendar access type
	 * 
	 * <b>Parameters</b><br>
	 * String calName -share calendar name String accessType - the access type
	 * of shared calendar draft - View and Edit readonly - View Only
	 * 
	 * <b>Return</b><br>
	 * Return true if access type is correct, false if not correct
	 */
	public boolean verifyShareCalendarAccessTypeInCalendarWindow(String calName, String accessType) {

		Logging.info("Verifing the share calendar " + calName + "access type is " + accessType);
		String calendar_share_access_type_icon = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_share_access_type_icon").replace("+variable+", calName);
		boolean result = false;
		try {
			Tools.setDriverDefaultValues();

			String attribute = Base.base.getDriver().findElement(By.xpath(calendar_share_access_type_icon))
					.getAttribute("class");
			result = attribute.contains(accessType);
		} catch (WebDriverException ex) {
			Logging.error("The share calendar " + calName + "is not" + accessType);
		}
		return result;
	}

	/**
	 * <b>Author</b>: Lynn Li <br>
	 * <b>Date</b>: Oct 10, 2015 <br>
	 * <b>Description</b><br>
	 * click edit button by shared calendar name
	 * 
	 * <b>Parameters</b><br>
	 * String calendarName - calendar name
	 */
	public void clickEditButtonOnSharedCalendar(String calendarName) {
		Logging.info("Clicking edit button on shared calendar " + calendarName);

		String calendar_share_edit_button = PropertyHelper.coreCalendar.getPropertyValue("calendar_share_edit_button")
				.replace("+variable+", calendarName);
		String calendar_share_name = PropertyHelper.coreCalendar.getPropertyValue("calendar_share_name")
				.replace("+variable+", calendarName);

		try {
			Tools.setDriverDefaultValues();

			Tools.moveFocusToElementByXpath(calendar_share_name);
			Base.base.getDriver().findElement(By.xpath(calendar_share_name)).click();
			Base.base.getDriver().findElement(By.xpath(calendar_share_edit_button)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click edit button on shared calendar " + calendarName);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Lynn Li <br>
	 * <b>Date</b>: Oct 10, 2015 <br>
	 * <b>Description</b><br>
	 * click minus icon on share calendar window by email address
	 * 
	 * <b>Parameters</b><br>
	 * String emailAddress - the email address
	 */
	public void clickMinusIconOnShareCalendarWindow(String emailAddress) {
		Logging.info("Clicking minus icon of " + emailAddress);

		String calendar_share_minus_icon = PropertyHelper.coreCalendar.getPropertyValue("calendar_share_minus_icon")
				.replace("+variable+", emailAddress);

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(calendar_share_minus_icon)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click minus icon of " + emailAddress);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Lynn Li <br>
	 * <b>Date</b>: Oct 10, 2015 <br>
	 * <b>Description</b><br>
	 * click toolbar button on edit calendar window
	 * 
	 * <b>Parameters</b><br>
	 * String button - button of clicking. One of the following button names
	 * should be supplied: Delete Save Cancel
	 * 
	 */
	public void clickToolbarOnEditCalendarWindow(String button) {
		Logging.info("Clicking " + button + " button on edit calendar window");

		String calendar_add_window_toolbar_button = PropertyHelper.coreCalendar
				.getPropertyValue("calendar_add_window_toolbar_button").replace("+variable+", button);

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(calendar_add_window_toolbar_button)).click();
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not click " + button + " button on edit calendar window");
			throw ex;
		}
	}


  

  /**
   * <b>Author</b>: Vivian Xie <br>
   * <b>Date</b>: Nov 21, 2016 <br>
   * <b>Description</b><br>
   * disable the srollbar of starttime or endtime dropdown list <b>Parameters</b><br>
   * String timePickerName - value can be starttime, endtime
   */
  public void disableTimeDropdownListScroll(String timePickerName) {
    Logging.info("Starting to disable "+timePickerName+" scroll bar");
    String calendar_event_time_picker_list = PropertyHelper.coreCalendar
        .getPropertyValue("calendar_event_time_picker_list").replace("+variable+", timePickerName);
    String timePickerId = null;
    try {
      timePickerId = Base.base.getDriver().findElement(By.xpath(calendar_event_time_picker_list))
          .getAttribute("id");

    } catch (WebDriverException ex) {
      Logging.error("Could not find " + timePickerName + " timepicker list in event editor panel.");
      throw ex;
    }
    Logging.info(timePickerId);

    JavascriptExecutor jsExecutor = (JavascriptExecutor) Base.base.getDriver();
    String jsCode = " document.getElementById('" + timePickerId + "').style.overflow='hidden'";
    Logging.info(jsCode);
    try {
      jsExecutor.executeScript(jsCode);
    } catch (Exception e) {
      e.printStackTrace();
      Logging.info("Failed to run java script:" + jsCode);
    }

  }

  /**
   * <b>Author</b>: Vivian Xie <br>
   * <b>Date</b>: Nov 21, 2016 <br>
   * <b>Description</b><br>
   * Format the time minute to end with 0 or 5 <b>Parameters</b><br>
   * int hour--hour of the time, int minute--minute of the time
   */
  public LocalTime formatTimeMuniteEndingWith5Or0(int hour, int minute) {
    LocalTime time = LocalTime.of(hour, minute);
    Logging.info("starting to format time " + time.toString());
    int i = minute % 5;
    if (i == 0)// when the time ends with 0 or 5, iNeedToAdd should be 0. 
      i = 5;
    int iNeedToAdd = 5 - i;
    LocalTime dest = time.plusMinutes(iNeedToAdd);
    Logging.info("after formatting, the time is " + dest.toString());
    return dest;
  }
  /**
   * <b>Author</b>: Vivian Xie <br>
   * <b>Date</b>: Nov 21, 2016 <br>
   * <b>Description</b><br>
   * Format the time minute to end with 0 or 5
   *  <b>Parameters</b><br> 
   * LocalTime time  -- the given time to format
   */
  public LocalTime formatTimeMuniteEndingWith5Or0(LocalTime time) {
    int minute = time.getMinute();
    Logging.info("starting to format time " + time.toString());
    int i = minute % 5;
    if (i == 0)// when the time ends with 0 or 5, iNeedToAdd should be 0. 
      i = 5;
    int iNeedToAdd = 5 - i;
    LocalTime dest = time.plusMinutes(iNeedToAdd);
    Logging.info("after formatting, the time is " + dest.toString());
    return dest;
  }

  /**
   * @Method Name: inputEventAttendee
   * @Author: Vivian Xie
   * @Created On: Jan/16/2016
   * @Description: This method will input attendee on new event panel .
   */
  public void inputEventAttendee(String attendee){
    Logging.info("Starting to input "+ attendee+" in new event panel");
    String calendar_event_attendee_input = PropertyHelper.coreCalendar
        .getPropertyValue("calendar_event_attendee_input");
    try {
     Base.base.getDriver().findElement(By.xpath(calendar_event_attendee_input)).click();
     Base.base.getDriver().findElement(By.xpath(calendar_event_attendee_input)).sendKeys(attendee);    
    } catch (WebDriverException ex) {
      Logging.error("Could not find attendee input box.");
      throw ex;
    }
  }
  
  
  /**
   * @Method Name: inputEventAttendee
   * @Author: Vivian Xie
   * @Created On: Jan/16/2016
   * @Description: This method will input attendee on new event panel .
   */
  public void clickAddAttendeeButtonOnNenEventPanel(){
    Logging.info("Starting to click add attendee button on new event panel");
    String calendar_event_attendee_add = PropertyHelper.coreCalendar
        .getPropertyValue("calendar_event_attendee_add");
    String calendar_event_attendee_input = PropertyHelper.coreCalendar
        .getPropertyValue("calendar_event_attendee_input");
    Logging.info(calendar_event_attendee_add);
    try {    
      String attendee = Base.base.getDriver().findElement(By.xpath(calendar_event_attendee_input)).getAttribute("value");
     while (attendee != null && !attendee.trim().equals("")){
     Base.base.getDriver().findElement(By.xpath(calendar_event_attendee_add)).click();
     attendee = Base.base.getDriver().findElement(By.xpath(calendar_event_attendee_input)).getAttribute("value");
     }
    } catch (WebDriverException ex) {
      Logging.error("Could not find attendee add button.");
      throw ex;
    }
  }
  
  
  /**
   * @Method Name: checkAllDayOnNewEventPanel
   * @Author: Vivian Xie
   * @Created On: Jan/16/2016
   * @Description: This method will click on All day checkbox
   * @param - checked  true means that the select the checkbox , false means
   * uncheck the checkbox
   */
  public void checkAllDayOnNewEventPanel(boolean checked){
    Logging.info("Starting to click add attendee button on new event panel");
    String calendar_event_all_day_table = PropertyHelper.coreCalendar
        .getPropertyValue("calendar_event_all_day_table");
    String calendar_event_all_day_checkbox = PropertyHelper.coreCalendar
        .getPropertyValue("calendar_event_all_day_checkbox");
    Logging.info(calendar_event_all_day_table);
    try {    
      String classStr = Base.base.getDriver().findElement(By.xpath(calendar_event_all_day_table)).getAttribute("class");
      Logging.info(classStr);
     while  ( classStr.contains("x-form-cb-checked") !=  checked ){
       Logging.info("click");
       Base.base.getDriver().findElement(By.xpath(calendar_event_all_day_checkbox)).click();
       Tools.waitFor(1, TimeUnit.SECONDS);
       classStr = Base.base.getDriver().findElement(By.xpath(calendar_event_all_day_table)).getAttribute("class"); 
       Logging.info(classStr);
     }
    } catch (WebDriverException ex) {
      Logging.error("Could not find All day checkbox button.");
      throw ex;
    }
    Tools.waitFor(5, TimeUnit.SECONDS);
  }
  
  /**
   * @Method Name: verifyInviteEventAbsolutePosition
   * @Author: Vivian Xie
   * @Created On: Jan/16/2016
   * @Description: This method will verify if invite event display correctly in time shaft in
   *               inviting mail.
   * @param positionAttributeName - left , height, top, width
   * @param value - the expected value of the attributeName .
   * @return -- true if the attribute has the specific value.
   */
  public boolean verifyInviteEventAbsolutePosition(String positionAttributeName, int value) {
    Logging.info("Starting to verfiy invite event " + positionAttributeName + ", expected value is "
        + value + "px");
    String calendar_invite_event_in_time_shaft =
        PropertyHelper.coreCalendar.getPropertyValue("calendar_invite_event_in_time_shaft");
    String actualStyle = null;
    String actualValue = null;
    boolean result = false;
    try {
      actualStyle = Base.base.getDriver().findElement(By.xpath(calendar_invite_event_in_time_shaft))
          .getAttribute("style");
      Logging.info(actualStyle);

    } catch (WebDriverException ex) {
      Logging.error("Could not find invite event");
      throw ex;
    }
    if (actualStyle != null && !actualStyle.trim().equals("")) {
      String[] list = actualStyle.split(";");
      for (int i = 0; i < list.length; i++) {
        String attributeMap = list[i];
        if (list[i].contains(positionAttributeName + ":"))
        {
          actualValue = attributeMap.split(":")[1];
          break;
        }
      }
    }
    if (actualValue != null && !actualValue.equals("")) {
      int len = actualValue.length();
      int actualIntegalValue = Integer.parseInt(actualValue.substring(0, len - 2).trim());
      if (actualIntegalValue == value)
        result = true;
    }
    return result;

  }
  
  /**
   * @Method Name: verifyCommonEventAbsolutePosition
   * @Author: Vivian Xie
   * @Created On: Jan/16/2016
   * @Description: This method will verify if common event has the correct left, top, height, width
   *               attribute value in time shaft in invitation mail.
   * @param positionAttributeName - left , height, top, width
   * @param value - the expected value of the attributeName .
   * @return -- true if the attribute has the specific value.
   */
  public boolean verifyCommonEventAbsolutePosition(String positionAttributeName, int value,
      String eventSummary) {
    Logging.info("Starting to verfiy common event " + positionAttributeName + ", expected value is "
        + value);
    String calendar_common_event_in_time_shaft =
        PropertyHelper.coreCalendar.getPropertyValue("calendar_common_event_in_time_shaft")
            .replace("+variable+", eventSummary);

    String actualStyle = null;
    actualValue = null;
    boolean result = false;
    try {
      Tools.waitFor(10, TimeUnit.SECONDS);
      Tools.scrollElementIntoViewByXpath(calendar_common_event_in_time_shaft);

      actualStyle = Base.base.getDriver().findElement(By.xpath(calendar_common_event_in_time_shaft))
          .getAttribute("style");
      Logging.info(actualStyle);

    } catch (WebDriverException ex) {
      Logging.error("Could not find  event: " + eventSummary + " in time shaft");
      throw ex;
    }
    if (actualStyle != null && !actualStyle.trim().equals("")) {
      String[] list = actualStyle.split(";");
      for (int i = 0; i < list.length; i++) {
        String attributeMap = list[i];
        if (list[i].contains(positionAttributeName + ":")) {
          actualValue = attributeMap.split(":")[1];
          break;
        }
      }
    }
    if (actualValue != null && !actualValue.equals("")) {
      int len = actualValue.length();
      int actualIntegalValue = Integer.parseInt(actualValue.substring(0, len - 2).trim());
      if (actualIntegalValue == value)
        result = true;
    }
    return result;

  }

  /**
   * @Method Name: clickOnEventStartOrEndDateInput
   * @Author: Vivian Xie
   * @Created On: Jan/17/2016
   * @Description: This method will click on event start time or end time input.
   * @param: startOrEnd -- value can be startdate or enddate
   */
  public void clickOnEventStartOrEndDateInput(String startOrEnd) {
    Logging.info("Starting to click on event " + startOrEnd + "input");
    String calendar_event_date_input = PropertyHelper.coreCalendar
        .getPropertyValue("calendar_event_date_input").replace("+variable+", startOrEnd);
    Logging.info(calendar_event_date_input);
    try {
      Base.base.getDriver().findElement(By.xpath(calendar_event_date_input)).click();
    } catch (WebDriverException ex) {
      Logging.error("Could not find event "+ startOrEnd +" input box");
      throw ex;
    }
  }

  /**
   * @Method Name: deleteEventInListView
   * @Author: Vivian Xie
   * @Created On: Jan/19/2016
   * @Description: This method will delete specific event in list view.
   * @param: eventTitle -- the summary of the event
   */
  public void deleteEventInListView(String eventTitle) {
    Logging.info("Deleting event " + eventTitle + " in list view");

    String deleteEventPopupWindowTitle_noRecur = "Delete Event";
    String deleteEventPopupWindowTitle_Recur = "Delete Recurring Event";

    String calendar_event_in_list = PropertyHelper.coreCalendar
        .getPropertyValue("calendar_event_in_list").replace("+variable+", eventTitle);
    String calendar_event_toolbar_button_inListView =
        PropertyHelper.coreCalendar.getPropertyValue("calendar_event_toolbar_button_inListView")
            .replace("+variable+", "Delete");

    String calendar_delete_recurring_event_series =
        PropertyHelper.coreCalendar.getPropertyValue("calendar_delete_recurring_event_series");

    String calendar_delete_event_prompt_window_title =
        PropertyHelper.coreCalendar.getPropertyValue("calendar_delete_event_prompt_window_title");

    try {
      Tools.setDriverDefaultValues();
      Base.base.getDriver().findElement(By.xpath(calendar_event_in_list)).click();
      Base.base.getDriver().findElement(By.xpath(calendar_event_toolbar_button_inListView)).click();
      String popupWindowTitle = Base.base.getDriver()
          .findElement(By.xpath(calendar_delete_event_prompt_window_title)).getText();
      if (popupWindowTitle.equals(deleteEventPopupWindowTitle_noRecur)) {
        this.clickButtonOnMessageBox("Yes");
      } else if (popupWindowTitle.equals(deleteEventPopupWindowTitle_Recur)) {
        Base.base.getDriver().findElement(By.xpath(calendar_delete_recurring_event_series)).click();
      }

    } catch (WebDriverException ex) {
      Logging.error("Could not delete event " + eventTitle);
      throw ex;
    }
  }

  /**
   * @Method Name: clickEventInMonthView
   * @Author: Vivian Xie
   * @Created On: Jan/19/2016
   * @Description: This method will click event "Edit" or "Delete" button in month view.
   * @param: buttName -- Edit or Delete
   */
  public void clickEventEditOrDeleteButtonInMonthView(String buttName) {
    Logging.info("Starting to click on event " + buttName + " button  in month view");
    String calendar_event_editOrDelete_button_in_month_view = PropertyHelper.coreCalendar
        .getPropertyValue("calendar_event_editOrDelete_button_in_month_view")
        .replace("+variable+", buttName);
    Logging.info(calendar_event_editOrDelete_button_in_month_view);
    try {
      Base.base.getDriver().findElement(By.xpath(calendar_event_editOrDelete_button_in_month_view))
          .click();
      this.waitForLoadMaskDismissed();
    } catch (WebDriverException ex) {
      Logging.error("Could not click event " + buttName + " in month view.");
      throw ex;
    }
  }


  /**
   * @Method Name: getInvitingEventTopPositionInTimeShaft
   * @Author: Vivian Xie
   * @Created On: Jan/19/2016
   * @Description: This method will calculate the inviting event's expected top .
   * @param: eventStartTime -- The start time of the local event
   */
  public int getInvitingEventTopPositionInTimeShaft(LocalDateTime eventStartTime) {
    Logging.info("Starting to caculate the expected top of inviting event starttime: "
        + eventStartTime.toString());
    int top = 0;
    if (eventStartTime.getHour() > 2) {
      top = eventStartTime.getMinute() * 2 / 3 + 100;
    } else {
      top = eventStartTime.getHour() * 40 + eventStartTime.getMinute() * 2 / 3 + 20;
    }

    return top;
  }


  /**
   * @Method Name: getInviteEventHeightInTimeShaft
   * @Author: Vivian Xie
   * @Created On: Jan/19/2016
   * @Description: This method will calculate the invite event's expected height.
   * @param: eventStartTime -- The start time of the inviting event
   * @param: eventEndTime -- the end time of the inviting event
   */
  public int getInviteEventHeightInTimeShaft(LocalDateTime eventStartTime,
      LocalDateTime eventEndTime) {
    Logging.info("Starting to caculate the expected height of inviting event starttime: "
        + eventStartTime.toString() + ", endtime:" + eventEndTime.toString());
    int height = 0;
    int diff = (int) ChronoUnit.MINUTES.between(eventStartTime, eventEndTime);
    height = diff * 2 / 3;
    return height;
  }

  /**
   * @Method Name: getCommonEventHeightInTimeShaft
   * @Author: Vivian Xie
   * @Created On: Jan/19/2016
   * @Description: This method will calculate the invite event's expected height.
   * @param: eventStartTime -- The start time of the inviting event
   * @param: eventEndTime -- the end time of the inviting event
   */
  public int getCommonEventHeightInTimeShaft(LocalDateTime eventStartTime,
      LocalDateTime eventEndTime, LocalDateTime inviteEventStartTime,
      LocalDateTime inviteEventEndTime) {
    Logging.info("Starting to caculate the expected height of common event starttime: "
        + eventStartTime.toLocalTime().toString() + ", endtime:"
        + eventEndTime.toLocalTime().toString());
    LocalTime startTime = eventStartTime.toLocalTime();
    LocalTime endTime = eventEndTime.toLocalTime();
    LocalTime startTimeDisplay = null;
    LocalTime endTimeDisplay = null;
    Logging.info(inviteEventStartTime.toString());
    Logging.info(inviteEventEndTime.toString());

    int inviteStartHour = inviteEventStartTime.getHour();
    int commonStartHour = eventStartTime.getHour();
    int inviteEndHour = inviteEventEndTime.getHour();
    int commonEndHour = eventEndTime.getHour();

    // all the part of common event is in the shaft
    if (commonStartHour >= inviteStartHour - 2)
      startTimeDisplay = startTime;
    else
      startTimeDisplay = LocalTime.of(inviteStartHour - 2, 0);

    if (commonEndHour < inviteEndHour + 2)
      endTimeDisplay = endTime;
    else
      endTimeDisplay = LocalTime.of(inviteEndHour + 2, 0);

    int height = 0;
    int diff = (int) ChronoUnit.MINUTES.between(startTimeDisplay, endTimeDisplay);
    height = diff * 2 / 3;
    return height;

  }


  /**
   * @Method Name: getInvitingEventTopPositionInTimeShaft
   * @Author: Vivian Xie
   * @Created On: Jan/19/2016
   * @Description: This method will calculate the common event's expected top .
   * @param: eventStartTime -- The start time of the event
   */
  public int getCommonEventTopPositionInTimeShaft(LocalDateTime eventStartTime,
      LocalDateTime invitingEventStartTime) {
    Logging.info("Starting to caculate the expected top of common event starttime: "
        + eventStartTime.toLocalTime().toString());
    int top = 0;
    if (invitingEventStartTime.getHour() > 2) {
      if (eventStartTime.getHour() - invitingEventStartTime.getHour() + 2 >= 0)
        top = eventStartTime.getMinute() * 2 / 3
            + (eventStartTime.getHour() - invitingEventStartTime.getHour() + 2) * 40 + 20;
      else
        top = 20;

    } else {
      top = eventStartTime.getHour() * 40 + eventStartTime.getMinute() * 2 / 3 + 20;
    }

    return top;
  }

  /**
   * @Method Name: clickAcceptOrNotButton
   * @Author: Vivian Xie
   * @Created On: Jan/20/2016
   * @Description: This method will click on given button in inviting mail or event detail page .
   * @param: buttonText -- "Yes", "Maybe" or "No"
   * @param: location -- button location, value can be "inviting mail" and "event detail"
   */
  public void clickAcceptOrNotButton(String buttonText, String location) {
    Logging.info("Starting to click " + buttonText + "button " + location);
    String calendar_accept_or_not_button = null;
    if (location.equals("inviting mail"))
      calendar_accept_or_not_button =
          PropertyHelper.coreCalendar.getPropertyValue("calendar_invite_email_accept_or_not_button")
              .replace("+variable+", buttonText);

    else
      calendar_accept_or_not_button =
          PropertyHelper.coreCalendar.getPropertyValue("calendar_event_detail_accept_or_not_button")
              .replace("+variable+", buttonText.toLowerCase());
    try {
      Base.base.getDriver().findElement(By.xpath(calendar_accept_or_not_button)).click();
    } catch (WebDriverException e) {
      Logging.info("Could not find " + buttonText + " button in inviting email");
      throw e;
    }

  }

  /**
   * @Method Name: verifyAcceptOrNotButtonSelectedInEventDetailsPage
   * @Author: Vivian Xie
   * @Created On: Jan/22/2016
   * @Description: This method will verify the given button on event detail page is selected.
   * @param: buttonText -- "Yes", "Maybe" or "No"
   * @return: true if selected, else false
   */
  public boolean verifyAcceptOrNotButtonSelectedInEventDetailsPage(String buttonText) {
    Logging.info("Starting to verify if " + buttonText + "button  is selected");
    boolean result = false;
    String calendar_accept_or_not_button =
        PropertyHelper.coreCalendar.getPropertyValue("calendar_event_detail_accept_or_not_button")
            .replace("+variable+", buttonText.toLowerCase());
    String classStr = null;
    try {
      classStr = Base.base.getDriver().findElement(By.xpath(calendar_accept_or_not_button))
          .getAttribute("class");
    } catch (WebDriverException e) {
      Logging.info("Could not find " + buttonText + " button in inviting email");
      throw e;
    }

    if (classStr.contains("selected"))
      result = true;
    return result;

  }


  /**
   * @Method Name: getCommonEventNumberInTimeShaft
   * @Author: Vivian Xie
   * @Created On: Jan/22/2016
   * @Description: This method will return the number of common event in time shaft.
   * @return: the number of common event in time shaft
   */
  public int getCommonEventNumberInTimeShaft() {
    Logging.info("Starting to get the number of common event in time shaft");
    String calendar_common_event_in_time_shaft = PropertyHelper.coreCalendar
        .getPropertyValue("calendar_common_event_in_time_shaft").replace("+variable+", "");
    ArrayList<WebElement> commonEvents = null;
    try {
      commonEvents = (ArrayList<WebElement>) Base.base.getDriver()
          .findElements(By.xpath(calendar_common_event_in_time_shaft));
    } catch (WebDriverException e) {
      Logging.info("Error when looking for common event in time shaft");
      throw e;
    }
    return commonEvents.size();

  }

  /**
   * @Method Name: verifyTimeShaftStartTime
   * @Author: Vivian Xie
   * @Created On: Jan/22/2016
   * @Description: This method will verify the start time in time shaft.
   * @param: timeString -- The expected start time string in time shaft
   * @return: true if start time is correct, else false
   */
  public boolean verifyStartTimeInTimeShaft(String timeString) {
    Logging.info("Starting to verify the start time in time shaft");
    boolean result = false;
    String calendar_start_time_in_time_shaft =
        PropertyHelper.coreCalendar.getPropertyValue("calendar_start_time_in_time_shaft");
    String actulStartTimeString = null;

    try {
      actulStartTimeString =
          Base.base.getDriver().findElement(By.xpath(calendar_start_time_in_time_shaft)).getText();
      Logging.info("Actual start time is " + actulStartTimeString);
    } catch (WebDriverException e) {
      Logging.info("Error when looking for common event in time shaft");
      throw e;
    }
    if (actulStartTimeString.trim().equals(timeString.trim())) {
      result = true;
    }

    Logging.info("Time shaft starts with " + timeString + ": " + result);
    return result;
  }

  /**
   * @Method Name: verifyDateofTimeShaft
   * @Author: Vivian Xie
   * @Created On: Jan/22/2016
   * @Description: This method will verify the date of time shaft.
   * @param: dateString -- The expected date string
   * @return: true if time shaft date is correct, else false
   */
  public boolean verifyDateofTimeShaft(String dateString) {
    Logging.info("Starting to verify the date in time shaft");
    boolean result = false;
    String calendar_date_of_time_shaft =
        PropertyHelper.coreCalendar.getPropertyValue("calendar_date_of_time_shaft");
    String actulDateString = null;

    try {
      actulDateString =
          Base.base.getDriver().findElement(By.xpath(calendar_date_of_time_shaft)).getText();
      Logging.info("Actual dateis: " + actulDateString);
    } catch (WebDriverException e) {
      Logging.info("Error when looking for common event in time shaft");
      throw e;
    }
		if (actulDateString.contains(dateString)
				|| (actulDateString.split(" ")[0] + " 0" + actulDateString.split(" ")[1]).contains(dateString)) {
			result = true;
		}
    Logging.info("Time shaft date is " + dateString + ": " + result);
    return result;
  }
  /**
   * @Method Name: getIntegralTimeStrIn12Or24HourTimeFormat
   * @Author: Vivian Xie
   * @Created On: Feb/8/2016
   * @Description: This method will return the integral time string in 12 or 24 Time Format
   * @param: datetime -- the date time to be formated
   * @param: timeFormat -- "12" or "24"
   * @return: return the integral time string
   */
  public String getIntegralTimeStrIn12Or24HourTimeFormat(LocalDateTime datetime,
      String timeFormat) {
    String result = null;
    if (timeFormat.equals("12")) {
      DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("K a").withLocale(Locale.ENGLISH);
      String timeStr2 = datetime.format(formatter2);
      String dateString2[] = timeStr2.split(" ");
      result = dateString2[0] + ":00 " + dateString2[1];
      if (result.equals("0:00 AM"))
        result = "12:00 AM";
    } else {
      DateTimeFormatter formatter3 = DateTimeFormatter.ofPattern("H").withLocale(Locale.ENGLISH);
      String timeStr3 = datetime.minusHours(2).format(formatter3);
      result = timeStr3 + ":00 ";
    }
    
    return result;

  }

}
