package com.synchronoss.pageobject;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.synchronoss.core.Base;
import com.synchronoss.core.Logging;
import com.synchronoss.core.PropertyHelper;
import com.synchronoss.core.Tools;
import com.synchronoss.test.untils.SizeComparator;

public class CoreEmail extends PageObjectBase {

	private static final String ATTCHMENTTITLE = "attachmenttitle";
	private static final String ATTCHMENTSUBJECT = "subject";
	private static final String ATTCHMENTSENDER = "sender";
	private static final String ATTCHMENTTIME = "time";
	private static final String ATTCHMENTSIZE = "size";

	private static final String FIRSTPAGE = "firstPage";
	private static final String LASTPAGE = "lastPage";
	private static final String NEXTPAGE = "nextPage";
	private static final String PREPAGE = "previousPage";

	private static final String IMAGES = "Images";
	private static final String DOCUMENT = "Documents";
	private static final String AUDIOS = "Audios";
	private static final String VIDEOS = "Videos";
	private static final String ARCHIVES = "Archives";
	public static final String DOCKWINDOWTITLE = "WebMail";
	public static final String DEFAULTWINDOWTITLE = "DefaultMailAccount";

	public CoreEmailComposer emailComposer = new CoreEmailComposer();

	// ####################################################################
	// ----------------------- UTILITY METHODS ---------------------------
	// ####################################################################

	/**
	 * @Method Name: getComposer
	 * @Author: Jerry Zhang
	 * @Created On: 07/28/2014
	 * @Description: This method will return an instance of CoreEmailComposer.
	 * @Return: CoreEmailComposer - an instance of CoreEmailComposer.
	 */
	public CoreEmailComposer getComposer() {
		return this.emailComposer;
	}

	// ####################################################################
	// ------------------------- ACTION METHODS ---------------------------
	// ####################################################################

	/**
	 * @Method Name: clickRefresh
	 * @Author: Jerry Zhang
	 * @Created On: 07/25/2014
	 * @Description: This method will click on refresh button of mail page and then
	 *               wait for load mask disappeared.
	 */
	public void clickRefresh() {
		// Logging.info("Clicking on refresh button of mail page");

		String email_refresh_button = PropertyHelper.coreEmailFile.getPropertyValue("email_refresh_button");

		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(email_refresh_button);
			Base.base.getDriver().findElement(By.xpath(email_refresh_button)).click();
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on refresh button of mail page");
			throw ex;
		}
	}

	/**
	 * @Method Name: clickEmailToolBarButton
	 * @Author: Jerry Zhang
	 * @Created On: 07/25/2014
	 * @Description: This method will click on the specified email toolbar button.
	 * @Parameter1: String button - button name One of the following item should be
	 *              provided: - Compose - Reply - Reply All - Forward (SBM only) -
	 *              Forward Inline - Forward Attachment - Delete - Mark as spam -
	 *              Mark as not spam - Mark as unread - Mark as read
	 */
	public void clickEmailToolBarButton(String button) {
		Logging.info("Clicking on email toolbar button: " + button);

		button = button.trim();
		String email_toolbar_button = PropertyHelper.coreEmailFile.getPropertyValue("email_toolbar_button")
				.replace("+variable+", button);
		String email_toolbar_button_forward = PropertyHelper.coreEmailFile
				.getPropertyValue("email_toolbar_button_forward_button");
		String email_toolbar_button_forward_option = PropertyHelper.coreEmailFile
				.getPropertyValue("email_toolbar_button_forward_option");

		if (button.toLowerCase().contains("inline")) {
			email_toolbar_button_forward_option = email_toolbar_button_forward_option.replace("+variable+", "Inline");
		} else if (button.toLowerCase().contains("attachment")) {
			email_toolbar_button_forward_option = email_toolbar_button_forward_option.replace("+variable+",
					"Attachment");
		}

		String email_toolbar_more_button = PropertyHelper.coreEmailFile.getPropertyValue("email_toolbar_button")
				.replace("+variable+", "More");
		String email_toolbar_hidden_button = PropertyHelper.coreEmailFile
				.getPropertyValue("email_toolbar_hidden_button").replace("+variable+", button);
		String email_tooltip_context_mask = PropertyHelper.coreEmailFile.getPropertyValue("email_tooltip_context_mask");

		try {
			Tools.setDriverDefaultValues();
			// Scroll toolbar More button into view.
			Tools.scrollElementIntoViewByXpath(email_toolbar_more_button);

			// Handle Forward options.
			if (button.toLowerCase().contains("inline") || button.toLowerCase().contains("attachment")) {
				// :after could not be located. When clicking on the forwardButton (the xpath is
				// email_toolbar_button_forward_button in CoreEmail.properties ),
				// it pops up composer page,
				// When clicking the position between forwardButton and its wrapper, it pops up
				// forward sub-menu
				// Here in the code, first find out WebElement forwardButton, and move by offset
				// (60,5) to locate the position between forwardButton and its wrapper,
				// then click to pop up forward sub-menu.
				WebElement forwardButton = Base.base.getDriver().findElement(By.xpath(email_toolbar_button_forward));
				new Actions(Base.base.getDriver()).moveToElement(forwardButton).moveByOffset(60, 5).click().build()
						.perform();

				Tools.waitUntilElementDisplayedByXpath(email_toolbar_button_forward_option, this.timeoutMedium);

				// click on Forward options button
				Base.base.getDriver().findElement(By.xpath(email_toolbar_button_forward_option)).click();
			} else {
				boolean buttonDisplayed;
				try {
					// To speed up hidden button searching.
					Base.base.getDriver().manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
					buttonDisplayed = Base.base.getDriver().findElement(By.xpath(email_toolbar_button)).isDisplayed();
				} catch (NoSuchElementException ex) {
					buttonDisplayed = false;
				} finally {
					// Set driver timeout to default.
					Tools.setDriverDefaultValues();
				}

				this.waitForLoadMaskDismissed();
				Logging.info(email_toolbar_button);
				if (buttonDisplayed) {
					Base.base.getDriver().findElement(By.xpath(email_toolbar_button)).click();

				} else {
					// Button was not found, searching within hidden buttons.
					Base.base.getDriver().findElement(By.xpath(email_toolbar_more_button)).click();
					Tools.scrollElementIntoViewByXpath(email_toolbar_hidden_button);
					// wait for the context tool tip to go away before clicking
					Tools.waitUntilElementNotDisplayedByXpath(email_tooltip_context_mask, this.timeoutMedium);
					Base.base.getDriver().findElement(By.xpath(email_toolbar_hidden_button)).click();
				}
			}
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on email toolbar button: " + button);
			throw ex;
		}
	}

	/**
	 * @Method Name: verifyEmailToolBarButtonEnable
	 * @Author: Vivian Xie
	 * @Created On: 11/25/2016
	 * @Description: This method will verify if email tool bar button is enable.
	 * @Parameter1: String button - button name One of the following item should be
	 *              provided: - Compose - Reply - Reply All - Forward - Forward
	 *              Inline - Forward Attachment - Delete - Mark as spam - Mark as
	 *              not spam - Mark as unread - Mark as read
	 */
	public boolean verifyEmailToolBarButtonEnable(String button) {
		Logging.info("verifying if  email toolbar button: " + button + " is enable");
		String buttonClassString = null;
		boolean result = true;
		button = button.trim();
		String email_toolbar_button = PropertyHelper.coreEmailFile.getPropertyValue("email_toolbar_button")
				.replace("+variable+", button);
		String email_toolbar_button_forward = PropertyHelper.coreEmailFile
				.getPropertyValue("email_toolbar_button_forward_button");
		String email_toolbar_button_forward_option = PropertyHelper.coreEmailFile
				.getPropertyValue("email_toolbar_button_forward_option");

		if (button.toLowerCase().contains("inline")) {
			email_toolbar_button_forward_option = email_toolbar_button_forward_option.replace("+variable+", "Inline");
		} else if (button.toLowerCase().contains("attachment")) {
			email_toolbar_button_forward_option = email_toolbar_button_forward_option.replace("+variable+",
					"Attachment");
		}

		String email_toolbar_more_button = PropertyHelper.coreEmailFile.getPropertyValue("email_toolbar_button")
				.replace("+variable+", "More");
		String email_toolbar_hidden_button = PropertyHelper.coreEmailFile
				.getPropertyValue("email_toolbar_hidden_button").replace("+variable+", button);
		String email_tooltip_context_mask = PropertyHelper.coreEmailFile.getPropertyValue("email_tooltip_context_mask");

		try {
			Tools.setDriverDefaultValues();
			// Scroll toolbar More button into view.
			Tools.scrollElementIntoViewByXpath(email_toolbar_more_button);

			// Handle Forward options.
			if (button.toLowerCase().contains("inline") || button.toLowerCase().contains("attachment")) {
				// :after could not be located. When clicking on the forwardButton (the xpath is
				// email_toolbar_button_forward_button in CoreEmail.properties ),
				// it pops up composer page,
				// When clicking the position between forwardButton and its wrapper, it pops up
				// forward sub-menu
				// Here in the code, first find out WebElement forwardButton, and move by offset
				// (60,5) to locate the position between forwardButton and its wrapper,
				// then click to pop up forward sub-menu.
				WebElement forwardButton = Base.base.getDriver().findElement(By.xpath(email_toolbar_button_forward));
				new Actions(Base.base.getDriver()).moveToElement(forwardButton).moveByOffset(60, 5).click().build()
						.perform();
				Tools.waitUntilElementDisplayedByXpath(email_toolbar_button_forward_option, this.timeoutMedium);
				// get the class attribute on Forward options button
				buttonClassString = Base.base.getDriver().findElement(By.xpath(email_toolbar_button_forward_option))
						.getAttribute("class");
				new Actions(Base.base.getDriver()).moveToElement(forwardButton).moveByOffset(60, 5).click().build()
						.perform();
			} else {
				boolean buttonDisplayed;
				try {
					// To speed up hidden button searching.
					Base.base.getDriver().manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
					buttonDisplayed = Base.base.getDriver().findElement(By.xpath(email_toolbar_button)).isDisplayed();
				} catch (NoSuchElementException ex) {
					buttonDisplayed = false;
				} finally {
					// Set driver timeout to default.
					Tools.setDriverDefaultValues();
				}

				this.waitForLoadMaskDismissed();
				if (buttonDisplayed) {
					buttonClassString = Base.base.getDriver().findElement(By.xpath(email_toolbar_button))
							.getAttribute("class");
				} else {
					// Button was not found, searching within hidden buttons.
					Base.base.getDriver().findElement(By.xpath(email_toolbar_more_button)).click();
					Tools.scrollElementIntoViewByXpath(email_toolbar_hidden_button);
					// wait for the context tool tip to go away before clicking
					Tools.waitUntilElementNotDisplayedByXpath(email_tooltip_context_mask, this.timeoutMedium);
					buttonClassString = Base.base.getDriver().findElement(By.xpath(email_toolbar_hidden_button))
							.getAttribute("class");
				}
			}
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not find  email toolbar button: " + button);
			throw ex;
		}

		Logging.info(buttonClassString);
		if (buttonClassString.contains("disabled")) {
			result = false;
		}

		return result;
	}

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Jun 10, 2015<br>
	 * <b>Description</b><br>
	 * Clicks on mail layout button and ensures the menu will be opened.
	 * 
	 * @return void
	 */
	private void openMailLayoutMenu() {
		String email_layout_button = PropertyHelper.coreEmailFile.getPropertyValue("email_layout_button");
		String email_layout_menu = PropertyHelper.coreEmailFile.getPropertyValue("email_layout_menu");
		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(email_layout_button);

			this.waitForQuicktipsDismissed();

			// Need to click layout button for the first time as layout menu is
			// not available just after
			// user login.
			Base.base.getDriver().findElement(By.xpath(email_layout_button)).click();
			boolean menuInvisible = Base.base.getDriver().findElement(By.xpath(email_layout_menu)).getAttribute("style")
					.contains("hidden");
			if (menuInvisible) {
				this.waitForLoadMaskDismissed();
				Base.base.getDriver().findElement(By.xpath(email_layout_button)).click();
			}
			Tools.waitUntilElementDisplayedByXpath(email_layout_menu, this.timeoutMedium);
		} catch (WebDriverException ex) {
			Logging.error("Could not open mail layout menu");
			throw ex;
		}
	}

	/**
	 * @Method Name: selectLayout
	 * @Author: Jerry Zhang
	 * @Created On: 07/30/2014
	 * @Description: This method will select the specified layout in email view.
	 * @Parameter1: String layout - layout name One of the following item should be
	 *              provided: - Preview right - Preview below - No preview
	 */
	public void selectLayout(String layout) {
		Logging.info("Selecting email preview layout: " + layout);
		String layoutOption = layout.trim().toLowerCase();
		String email_layout_menu_item = null;

		if (layoutOption.contains("right")) {
			layoutOption = "right";
		} else if (layoutOption.contains("below")) {
			layoutOption = "below";
		} else if (layoutOption.contains("attachment")) {
			layoutOption = "attachment";
		} else {
			layoutOption = "none";
		}

		email_layout_menu_item = PropertyHelper.coreEmailFile.getPropertyValue("email_layout_menu_item")
				.replace("+variable+", layoutOption);

		try {
			this.openMailLayoutMenu();
			Base.base.getDriver().findElement(By.xpath(email_layout_menu_item)).click();
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not select email preview layout: " + layout);
			throw ex;
		}
	}

	/**
	 * @Method Name: clickOnMessageBySubject
	 * @Author: Jerry Zhang
	 * @Created On: 07/28/2014
	 * @Description: This method will click on email message in list based on
	 *               subject.
	 * @Parameter1: String subject - email message subject
	 */
	public void clickOnMessageBySubject(String subject) {
		Logging.info("Clicking on email message: " + subject);
		subject = subject.trim();
		String email_list_message_clicking_area = PropertyHelper.coreEmailFile
				.getPropertyValue("email_list_message_clicking_area").replace("+variable+", subject);

		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(email_list_message_clicking_area);

			WebElement element = Base.base.getDriver().findElement(By.xpath(email_list_message_clicking_area));
			// in FF, the Actions click don't work, so need js-click action
			if (Tools.isFirefox()) {
				new Actions(Base.base.getDriver()).moveToElement(element);
				JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
				js.executeScript("arguments[0].click()", element);
			} else {
				new Actions(Base.base.getDriver()).moveToElement(element).click().build().perform();
			}
			this.waitForLoadMaskDismissed();
			Logging.info("Clicked on email message: " + subject);
		} catch (WebDriverException ex) {
			Logging.error("Could not click on email message: " + subject);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Jun 10, 2015<br>
	 * <b>Description</b><br>
	 * Clicks on an email message based on subject in preview below layout.
	 * 
	 * @param subject
	 *            email subject
	 * @return void
	 */
	public void clickOnMessageBySubject_PB(String subject) {
		Logging.info("Clicking on email message in preview below layout: " + subject);
		subject = subject.trim();
		String email_list_message_clicking_area_pb = PropertyHelper.coreEmailFile
				.getPropertyValue("email_list_message_clicking_area_pb").replace("+variable+", subject);
		String email_detail_subject = PropertyHelper.coreEmailFile.getPropertyValue("email_detail_subject")
				.replace("+variable+", subject);

		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(email_list_message_clicking_area_pb);
			Base.base.getDriver().findElement(By.xpath(email_list_message_clicking_area_pb)).click();
			this.waitForLoadMaskDismissed();
			Tools.waitUntilElementDisplayedByXpath(email_detail_subject, this.timeoutMedium);
		} catch (WebDriverException ex) {
			Logging.error("Could not click on email message: " + subject);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Charlie<br>
	 * <b>Date</b>: August 23, 2017<br>
	 * <b>Description</b><br>
	 * Double clicks on an email message based on subject in preview below layout.
	 * 
	 * @param subject
	 *            email subject
	 * @return void
	 */
	public void doubleClickOnMessageBySubject(String subject) {
		Logging.info("Double clicking on email message: " + subject);
		subject = subject.trim();
		String email_list_message_clicking_area = PropertyHelper.coreEmailFile
				.getPropertyValue("email_list_message_clicking_area").replace("+variable+", subject);

		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(email_list_message_clicking_area);

			WebElement element = Base.base.getDriver().findElement(By.xpath(email_list_message_clicking_area));
			// in FF, the Actions click don't work, so need js-click action
			if (Tools.isFirefox()) {
				new Actions(Base.base.getDriver()).doubleClick(element).build().perform();
			} else {
				new Actions(Base.base.getDriver()).doubleClick(element).build().perform();
			}
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not double click on email message: " + subject);
			throw ex;
		}
	}

	/**
	 * @Method Name :clickAFolder
	 * @Author: Young Zhao
	 * @Created On: 06/15/2015
	 * @Description : This method will click on the specified mail folder.
	 * 
	 * @Parameter1: String folder - the specified mail folder to click on
	 */
	public void clickFolder(String folder) {
		Logging.info("Clicking on " + folder + " folder");

		try {
			// expand my Folders
			if (!folder.equals("Inbox") && !folder.equals("Drafts") && !folder.equals("Sent") && !folder.equals("Spam")
					&& !folder.equals("Trash")) {
				String my_folder = PropertyHelper.coreEmailFile.getPropertyValue("email_folder_row")
						.replace("+variable+", "My Folders");
				boolean expanderEnabled = Base.base.getDriver().findElement(By.xpath(my_folder)).getAttribute("class")
						.contains("expanded");
				if (!expanderEnabled) {
					String email_my_folders_expander = PropertyHelper.coreEmailFile
							.getPropertyValue("email_my_folders_expander");
					Base.base.getDriver().findElement(By.xpath(email_my_folders_expander)).click();
				}
			}

			String email_folder_row;
			if (folder.contains("'") || folder.contains("\"")) {
				folder = Tools.generateConcatForXPath(folder);
				email_folder_row = PropertyHelper.coreEmailFile.getPropertyValue("email_folder_row_concat")
						.replace("+variable+", folder);
			} else {
				email_folder_row = PropertyHelper.coreEmailFile.getPropertyValue("email_folder_row")
						.replace("+variable+", folder);
			}
			Tools.scrollElementIntoViewByXpath(email_folder_row);
			Base.base.getDriver().findElement(By.xpath(email_folder_row)).click();
			this.waitForLoadMaskDismissed();

		} catch (WebDriverException ex) {
			Logging.info("The folder " + folder + " can not be found");
			throw ex;
		}
	}

	/**
	 * @Method Name :clickSystemFolder(Inbox, Spam, Sent, Drafts)
	 * @Author: Supreeth Kumar
	 * @Created On: 08/19/2019
	 * @Description : This method will click on the system folders.
	  	This method was written to avoid the conflict between Spam folders which exists even under My folders section.
	 * @Parameter1: String folder - inbox, spam(junk), sent, trash & drafts.
	 */
	public void clickSystemFolder(String folder) {
		try {
			String my_folder = PropertyHelper.coreEmailFile.getPropertyValue("email_system_folder")
					.replace("+variable+", folder);
			Base.base.getDriver().findElement(By.xpath(my_folder)).click();
		} catch (WebDriverException ex) {
			Logging.info("The folder " + folder + " can not be found");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jun 17, 2015<br>
	 * <b>Description</b><br>
	 * This method will select an email message by clicking on its check box.
	 * 
	 * @param String
	 *            subject email message subject
	 * @return void
	 */
	public void selectMessageBySubject(String subject) {
		Logging.info("Selecting email message: " + subject);

		subject = subject.trim();
		String email_list_message_cell_by_subject = PropertyHelper.coreEmailFile
				.getPropertyValue("email_list_message_cell_by_subject").replace("+variable+", subject);
		String email_list_message_checkbox = PropertyHelper.coreEmailFile
				.getPropertyValue("email_list_message_checkbox");

		try {
			Tools.setDriverDefaultValues();

			Tools.scrollElementIntoViewByXpath(email_list_message_cell_by_subject);

			boolean isChecked = false;
			String checkbox = email_list_message_cell_by_subject + email_list_message_checkbox;

			// Following code implemented a workaround to fix intermittent
			// failures of clicking
			// on message checkbox.
			int i = 0;
			int maxTry = 5;

			while (i < maxTry) {
				isChecked = Base.base.getDriver().findElement(By.xpath(email_list_message_cell_by_subject))
						.getAttribute("class").contains("row-selected");

				if (!isChecked) {
					Logging.info("Checking on message checkbox, try: " + i);
					Logging.info("The checkbox is not checked initially.");
					Logging.info("Checkbox : " + checkbox);
					//Tools.moveFocusToElementByXpath(checkbox);
					Tools.clickOnElementByXpath(checkbox);
					//Base.base.getDriver().findElement(By.xpath(checkbox)).click();
					Tools.waitFor(1, TimeUnit.SECONDS);
					Logging.info("Checkbox is checked successfully.");
					isChecked=true;
				} else {
					break;
				}

				i++;
			}

			if (!isChecked && (i == maxTry)) {
				Logging.warn("Unable to check on message checkbox after " + maxTry + " tries");
			}

		} catch (WebDriverException ex) {
			Logging.error("Could not select email message: " + subject);
			throw ex;
		}
	}

	public void clickMailBySubject(String subject){
		Logging.info("Clicking email message: " + subject);

		subject = subject.trim();
		String subject_in_mail_list = PropertyHelper.coreEmailFile
				.getPropertyValue("subject_in_mail_list").replace("variable", subject);
		Logging.info("XPATH of the subject being clicked is = "+subject_in_mail_list);
		Base.base.getDriver().findElement(By.xpath(subject_in_mail_list)).click();
	}

	public boolean findMailBySubject(String subject) {
		Logging.info("Finding email message: " + subject);
		boolean result = false;
		subject = subject.trim();
		String subject_in_mail_list = PropertyHelper.coreEmailFile
				.getPropertyValue("subject_in_mail_list").replace("variable", subject);
		Logging.info("XPATH of the subject being clicked is = " + subject_in_mail_list);
		try {
			result = Base.base.getDriver().findElement(By.xpath(subject_in_mail_list)).isDisplayed();
		} catch (Exception e){
			Logging.info("Mail with subject \""+ subject+"\" not found");
		} return result;
	}

	/**
	 * @Method Name: verifyMessageExistenceBySubjectInRightLayout
	 * @Author: Jerry Zhang
	 * @Created On: 07/28/2014
	 * @Description: This method will verify email message existence in message list
	 *               based on subject in preview right layout.
	 * @Parameter1: String subject - email message subject
	 * @Return: boolean - true/false
	 */
	public boolean verifyMessageExistenceBySubject_PR(String subject) {
		Logging.info("Verifying message existence in list in preview right layout: " + subject);
		subject = subject.trim();
		// String email_list_message_subject =
		// PropertyHelper.coreEmailFile.getPropertyValue("email_list_message_subject")
		// .replace("+variable+", subject);

		String email_list_message_subject = PropertyHelper.coreEmailFile
				.getPropertyValue("email_list_message_clicking_area").replace("+variable+", subject);
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(email_list_message_subject));
			// Subject is found.
			result = true;
			Logging.info("Found message with subject: " + subject);
		} catch (WebDriverException ex) {
			Logging.info("Could not found message with subject: " + subject);

		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jun 17, 2015<br>
	 * <b>Description</b><br>
	 * This method will verify the selected conversation number that displayed at
	 * the mail details view.
	 * 
	 * @param int
	 *            num - selected messages number
	 * @return boolean - true/false
	 */
	public boolean verifySelectedConversationNumber(int num) {
		Logging.info("Verifying whether selected conversation number is: " + num);

		String email_checkedmask_text = PropertyHelper.coreEmailFile.getPropertyValue("email_checkedmask_text");
		boolean result = false;

		String number = String.valueOf(num);
		String text = number + " conversation(s) selected";

		try {
			Tools.setDriverDefaultValues();
			String actText = Base.base.getDriver().findElement(By.xpath(email_checkedmask_text)).getText();
			Logging.info("Actual text got from mail details view: " + actText);

			if (actText.equalsIgnoreCase(text)) {
				result = true;
			}

			Logging.info("Select conversation number is as expected: " + result);
		} catch (WebDriverException ex) {
			Logging.error("Could not verify number of selected conversation");
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>:Oct 14, 2016<br>
	 * <b>Description</b><br>
	 * This method will verify the selected mail number that displayed at the mail
	 * details view.
	 * 
	 * @param
	 *            num - selected messages number
	 * @return boolean - true/false
	 */
	public boolean verifySelectedMailNumber(int num) {
		Logging.info("Verifying whether selected mail number is: " + num);

		String email_checkedmask_text = PropertyHelper.coreEmailFile.getPropertyValue("email_checkedmask_text");
		boolean result = false;

		String number = String.valueOf(num);
		String text = number + " message(s) selected";

		try {
			Tools.setDriverDefaultValues();
			String actText = Base.base.getDriver().findElement(By.xpath(email_checkedmask_text)).getText();
			Logging.info("Actual text got from mail details view: " + actText);

			if (actText.equalsIgnoreCase(text)) {
				result = true;
			}

			Logging.info("Select conversation number is as expected: " + result);
		} catch (WebDriverException ex) {
			Logging.error("Could not verify number of selected mail");
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Aug 25, 2015<br>
	 * <b>Description</b><br>
	 * verify the displayed name in greeting field
	 * 
	 * <b>Parameters</b><br>
	 * String name - The expected displayed name in greeting field
	 */
	public boolean verifyDisplayedNameInGreeting(String name) {
		Logging.info("Verifying the displayed name in greeting field is: " + name);

		String email_greeting = PropertyHelper.coreEmailFile.getPropertyValue("email_greeting");
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			String actText = Base.base.getDriver().findElement(By.xpath(email_greeting)).getText();
			System.out.println("Comparing " + actText + " with "+ name);
			if (actText.contains(name)) {
				result = true;
			}

			Logging.info("Displayed name in greeting is as expected: " + result);
		} catch (WebDriverException ex) {
			Logging.error("Could not verify the displayed name in greeting field");
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Aug 26, 2015<br>
	 * <b>Description</b><br>
	 * verify the displayed name in mail list field, discriminated by subject
	 * 
	 * <b>Parameters</b><br>
	 * String name - The expected displayed name in mail list field, discriminated
	 * by subject
	 */
	public boolean verifyDisplyedEmailNameInMailListBySubject(String name, String subject) {
		Logging.info("Verifying the displayed name in mail list field is: " + name);

		subject = subject.trim();
		name = name.trim();
		String email_headlistfrom_maillist_by_subject = PropertyHelper.coreEmailFile
				.getPropertyValue("email_headlistfrom_maillist_by_subject").replace("+variable+", subject);
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			String actText = Base.base.getDriver().findElement(By.xpath(email_headlistfrom_maillist_by_subject))
					.getText();
			if (actText.contains(name)) {
				result = true;
			}

			Logging.info("Displayed name in mail list is as expected: " + result);
		} catch (WebDriverException ex) {
			Logging.error("Could not verify the displayed name in mail list field");
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Aug 26, 2015<br>
	 * <b>Description</b><br>
	 * verify the displayed name in mail preview window
	 * 
	 * <b>Parameters</b><br>
	 * String email - The expected displayed email address in mail preview window
	 */
	public boolean verifyDisplyedEmailNameInPreview(String name) {
		Logging.info("Verifying the displayed email name in mail preview window is: " + name);

		name = name.trim();
		String email_msgheaderfrom_preview = PropertyHelper.coreEmailFile
				.getPropertyValue("email_msgheaderfrom_preview");
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			String actText = Base.base.getDriver().findElement(By.xpath(email_msgheaderfrom_preview)).getText();
			Logging.info("Actual name found in mail preview: " + actText);

			if (actText.contains(name)) {
				result = true;
			}
			Logging.info("Displayed email name in mail preview window is as expected: " + result);
		} catch (WebDriverException ex) {
			Logging.error("Could not verify the displayed email name in mail preview window");
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Aug 31, 2015<br>
	 * <b>Description</b><br>
	 * get the received date for the message in message preview window in right
	 * layout
	 * 
	 */
	public String getMessageDateInPreview_PR() {
		Logging.info("Getting the received message date");
		String email_preview_receivedate_pr = PropertyHelper.coreEmailFile
				.getPropertyValue("email_preview_receivedate_pr");

		try {
			Tools.setDriverDefaultValues();
			String date = Base.base.getDriver().findElement(By.xpath(email_preview_receivedate_pr)).getText();
			date = date.substring(0, date.indexOf(" ")).trim();
			Logging.info("Actual message date found: " + date);

			return date;
		} catch (WebDriverException ex) {
			Logging.error("Could not get the received date for email");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Aug 31, 2015<br>
	 * <b>Description</b><br>
	 * get the received time for the message in message preview window in right
	 * layout
	 * 
	 */
	public String getMessageTimeInPreview_PR() {
		Logging.info("Get the received message time");
		String email_preview_receivedate_pr = PropertyHelper.coreEmailFile
				.getPropertyValue("email_preview_receivedate_pr");

		try {
			Tools.setDriverDefaultValues();
			String date = Base.base.getDriver().findElement(By.xpath(email_preview_receivedate_pr)).getText();
			int length = date.length();
			date = date.substring(date.indexOf(" "), length).trim();
			Logging.info("Actual message time found: " + date);

			return date;
		} catch (WebDriverException ex) {
			Logging.error("Could not get the received time for email");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 1, 2015<br>
	 * <b>Description</b><br>
	 * verify the message is displayed in message preview window in right layout
	 * 
	 */
	public boolean verifyMessageDisplayedBySubject_PR(String subject) {
		Logging.info("Verify the message " + subject + " is displayed in preview for layout right");
		String email_msgheadersubject_preview_pr = PropertyHelper.coreEmailFile
				.getPropertyValue("email_msgheadersubject_preview_pr");
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			String actSubject = Base.base.getDriver().findElement(By.xpath(email_msgheadersubject_preview_pr))
					.getText();
			Logging.info("Actual message subject found: " + actSubject);

			if (actSubject.equals(subject)) {
				result = true;
			}
			Logging.info("Message subject is as expected: " + result);

		} catch (WebDriverException ex) {
			Logging.info("Could not verify the message is displayed");
		}
		return result;
	}

	// ####################################################################
	// CONVERSATION VIEW
	// ####################################################################

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Jun 08, 2015<br>
	 * <b>Description</b><br>
	 * Changes email preview options conversation view to be enabled or disabled.
	 * 
	 * @param enable
	 *            enable or disable conversation view
	 * @return void
	 */
	public void changeConversationView(boolean enable) {
		Logging.info("Changing conversation view to be: " + (enable ? "enabled" : "diabled"));
		String email_conver_view_menuitem = PropertyHelper.coreEmailFile.getPropertyValue("email_conver_view_menuitem");
		String email_layout_button = PropertyHelper.coreEmailFile.getPropertyValue("email_layout_button");

		try {
			Tools.setDriverDefaultValues();
			this.openMailLayoutMenu();
			boolean viewEnabled = Base.base.getDriver().findElement(By.xpath(email_conver_view_menuitem))
					.getAttribute("class").contains("active");

			if ((enable && !viewEnabled) || (!enable && viewEnabled)) {
				Base.base.getDriver().findElement(By.xpath(email_conver_view_menuitem)).click();
				this.waitForLoadMaskDismissed();
			} else {
				// Expected option already selected, clicking on layout button
				// again to close
				// layout menu so that subsequent actions won't be affected.
				Base.base.getDriver().findElement(By.xpath(email_layout_button)).click();
			}

		} catch (WebDriverException ex) {
			Logging.error("Could not change conversation view to be: " + (enable ? "enabled" : "diabled"));
			throw ex;
		}
	}

	/*	*//**
			 * <b>Author</b>: Jerry Zhang<br>
			 * <b>Date</b>: Jun 08, 2015<br>
			 * <b>Description</b><br>
			 * Clicks button on conversation float toolbar by message index of the
			 * conversation.
			 * 
			 * @param index
			 *            conversation message index
			 * @param button
			 *            button name
			 * @return void
			 *//*
				 * public void clickButtonOnConversationFloatToolbar(int index, String button) {
				 * Logging.info("Clicking on button " + button +
				 * " of conversation message index " + index); String
				 * email_conver_float_toolbar_by_index_pr = PropertyHelper.coreEmailFile
				 * .getPropertyValue("email_conver_float_toolbar_by_index_pr")
				 * .replace("+variable+", String.valueOf(index)); String
				 * email_conver_msg_header_by_index_pr = PropertyHelper.coreEmailFile
				 * .getPropertyValue("email_conver_msg_header_by_index_pr").replace(
				 * "+variable+", String.valueOf(index));
				 * 
				 * try { Tools.setDriverDefaultValues(); // Workaround code to move focus on
				 * message folder first and then // move to float toolbar.
				 * Tools.moveFocusToElementByXpath(email_conver_msg_header_by_index_pr);
				 * Tools.moveFocusToElementByXpath(email_conver_float_toolbar_by_index_pr); if
				 * ((button.contains("Reply")) || (button.contains("Reply All")) ||
				 * (button.contains("Forward"))) {
				 * 
				 * String buttonToClick = PropertyHelper.coreEmailFile
				 * .getPropertyValue("email_conver_float_toolbar_by_index_button_pr")
				 * .replace("+variable1+", String.valueOf(index)).replace("+variable2+",
				 * button); Base.base.getDriver().findElement(By.xpath(buttonToClick)).click();
				 * } else { String menuTrigger = PropertyHelper.coreEmailFile
				 * .getPropertyValue("email_conver_float_toolbar_by_index_menu_trigger_pr")
				 * .replace("+variable+", String.valueOf(index)); String menuItem =
				 * PropertyHelper.coreEmailFile.getPropertyValue("email_menu_item_common")
				 * .replace("+variable+", button);
				 * Base.base.getDriver().findElement(By.xpath(menuTrigger)).click();
				 * Base.base.getDriver().findElement(By.xpath(menuItem)).click(); }
				 * this.waitForLoadMaskDismissed(); } catch (WebDriverException ex) {
				 * Logging.error("Could not click on button " + button +
				 * " of conversation message index " + index); throw ex; } }
				 */

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Aug 10, 2017<br>
	 * <b>Description</b><br>
	 * Clicks button on conversation float toolbar by message index of the
	 * conversation.
	 * 
	 * @param index
	 *            conversation message index
	 * @param button
	 *            button name
	 * @return void
	 */
	public void clickMenuOptionInConversationDetail(int index, String button) {
		Logging.info("Clicking on button " + button + " of conversation message index " + index);
		String email_conver_float_toolbar_by_index_pr = PropertyHelper.coreEmailFile
				.getPropertyValue("email_conver_float_toolbar_by_index_pr")
				.replace("+variable+", String.valueOf(index));
		String email_conver_msg_header_by_index_pr = PropertyHelper.coreEmailFile
				.getPropertyValue("email_conver_msg_header_by_index_pr").replace("+variable+", String.valueOf(index));
		String email_conver_float_toolbar_arrow_down_option = PropertyHelper.coreEmailFile
				.getPropertyValue("email_conver_float_toolbar_arrow_down_option");
		String email_conver_float_toolbar_reply_all = PropertyHelper.coreEmailFile
				.getPropertyValue("email_conver_float_toolbar_reply_all");

		try {
			Tools.setDriverDefaultValues();
			// Workaround code to move focus on message folder first and then
			// move to float toolbar.
			Tools.moveFocusToElementByXpath(email_conver_msg_header_by_index_pr);
			Tools.moveFocusToElementByXpath(email_conver_float_toolbar_by_index_pr);

			if (button.equals("Reply All")) {
				email_conver_float_toolbar_reply_all = String.format(email_conver_float_toolbar_reply_all,
						String.valueOf(index));
				Base.base.getDriver().findElement(By.xpath(email_conver_float_toolbar_reply_all)).click();

			} else {
				System.out.println("Here am i");
				email_conver_float_toolbar_arrow_down_option = String
						.format(email_conver_float_toolbar_arrow_down_option, String.valueOf(index));
				Base.base.getDriver().findElement(By.xpath(email_conver_float_toolbar_arrow_down_option)).click();
				Tools.waitFor(10, TimeUnit.SECONDS);
				this.moveToMenuItem(button, 2);
				this.clickMailDetailMenuItem(button);
			}

			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on button " + button + " of conversation message index " + index);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Aug 10, 2017<br>
	 * <b>Description</b><br>
	 * Clicks button on conversation float toolbar by message index of the
	 * conversation.
	 * 
	 * @param index
	 *            conversation message index
	 * @param button
	 *            button name
	 * @return void
	 */
	public void clickMenuOptionInSingleDetail(String button) {
		Logging.info("Clicking on button " + button);
		String email_detail_header = PropertyHelper.coreEmailFile.getPropertyValue("email_detail_header");
		String email_single_detail_arrow_down_option = PropertyHelper.coreEmailFile
				.getPropertyValue("email_single_detail_arrow_down_option");
		String email_single_detail_arrow_reply_all = PropertyHelper.coreEmailFile
				.getPropertyValue("email_single_detail_arrow_reply_all");
		String email_single_detail_float_tool_bar = PropertyHelper.coreEmailFile
				.getPropertyValue("email_single_detail_float_tool_bar");
		try {
			Tools.setDriverDefaultValues();
			// Workaround code to move focus on message folder first and then
			// move to float toolbar.
			Tools.moveFocusToElementByXpath(email_detail_header);
			Tools.moveFocusToElementByXpath(email_single_detail_float_tool_bar);

			if (button.equals("Reply All")) {
				Base.base.getDriver().findElement(By.xpath(email_single_detail_arrow_reply_all)).click();

			} else {
				Base.base.getDriver().findElement(By.xpath(email_single_detail_arrow_down_option)).click();
				Tools.waitFor(10, TimeUnit.SECONDS);
				this.moveToMenuItem(button, 2);
				this.clickMailDetailMenuItem(button);
			}

			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on button " + button);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Aug 18, 2017<br>
	 * <b>Description</b>: Move focus on a menu item<br>
	 * <b>String menuItem</b>: the text of the menu item
	 */
	public void clickMailDetailMenuItem(String menuItem) {
		Logging.info("clicking menu item in conversation detail" + menuItem);
		String buttonXpath = null;
		switch (menuItem.replace(" ", "").toLowerCase()) {
		case "reply":
			buttonXpath = PropertyHelper.coreEmailFile.getPropertyValue("email_detail_menu_button_reply");
			break;
		case "forward":
			buttonXpath = PropertyHelper.coreEmailFile.getPropertyValue("email_detail_menu_button_forward");
			break;
		case "delete":
			buttonXpath = PropertyHelper.coreEmailFile.getPropertyValue("email_detail_menu_button_delete");
			break;
		case "blocksender":
			buttonXpath = PropertyHelper.coreEmailFile.getPropertyValue("email_detail_menu_button_blocksender");
			break;
		}
		try {
			Base.base.getDriver().findElement(By.xpath(buttonXpath)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click menu item " + menuItem);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jun 16, 2015<br>
	 * <b>Description</b><br>
	 * verify whether the specified mail is expanded in a conversation.
	 * 
	 * @param index
	 *            conversation message index
	 * @return boolean
	 */
	public boolean verifyMessageExpandedInConversation_PR(int index) {
		Logging.info("Verifying the message status");
		String email_conver_header_by_index_pr = PropertyHelper.coreEmailFile
				.getPropertyValue("email_conver_header_by_index_pr").replace("+variable+", String.valueOf(index));
		boolean result = false;

		try {
			boolean msgExpanded = Base.base.getDriver().findElement(By.xpath(email_conver_header_by_index_pr))
					.getAttribute("style").contains("display: none");
			Logging.info("Message " + index + " is " + (msgExpanded ? "expanded" : "collapsed"));
		} catch (WebDriverException ex) {
			Logging.info("Could not verify the message status");
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Jun 11, 2015<br>
	 * <b>Description</b><br>
	 * Expands the specified message in a conversation in preview right layout.
	 * 
	 * @param index
	 *            conversation message index
	 * @return void
	 */
	public void expandMessageInConversation_PR(int index) {
		Logging.info("Expanding message " + index + " in conversation in preview right layout");
		String email_conver_header_by_index_pr = PropertyHelper.coreEmailFile
				.getPropertyValue("email_conver_header_by_index_pr").replace("+variable+", String.valueOf(index));
		String email_conver_item_by_index_pr = PropertyHelper.coreEmailFile
				.getPropertyValue("email_conver_item_by_index_pr").replace("+variable+", String.valueOf(index));

		try {
			Tools.setDriverDefaultValues();
			boolean msgExpanded = Base.base.getDriver().findElement(By.xpath(email_conver_header_by_index_pr))
					.getAttribute("style").contains("display: none");
			if (!msgExpanded) {
				Tools.scrollElementIntoViewByXpath(email_conver_item_by_index_pr);
				Base.base.getDriver().findElement(By.xpath(email_conver_item_by_index_pr)).click();
			}
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not expand message " + index + " in conversation in preview right layout");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Jun 11, 2015<br>
	 * <b>Description</b><br>
	 * Collapses the specified message in a conversation in preview right layout.
	 * 
	 * @param index
	 *            conversation message index
	 * @return void
	 */
	public void collapseMessageInConversation_PR(int index) {
		Logging.info("Collapsing message " + index + " in conversation in preview right layout");
		String email_conver_header_by_index_pr = PropertyHelper.coreEmailFile
				.getPropertyValue("email_conver_header_by_index_pr").replace("+variable+", String.valueOf(index));
		String email_conver_msg_header_by_index_pr = PropertyHelper.coreEmailFile
				.getPropertyValue("email_conver_msg_header_by_index_pr").replace("+variable+", String.valueOf(index));

		try {
			Tools.setDriverDefaultValues();
			boolean msgExpanded = Base.base.getDriver().findElement(By.xpath(email_conver_header_by_index_pr))
					.getAttribute("style").contains("display: none");
			if (msgExpanded) {
				Tools.scrollElementIntoViewByXpath(email_conver_msg_header_by_index_pr);
				Base.base.getDriver().findElement(By.xpath(email_conver_msg_header_by_index_pr)).click();
			}
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not collapse message " + index + " in conversation in preview right layout");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Jun 09, 2015<br>
	 * <b>Description</b><br>
	 * Verifies if the specified conversation has correct number in email list in
	 * preview right layout.
	 * 
	 * @param subject
	 *            conversation subject
	 * @param number
	 *            message number in conversation
	 * @return number is correct or not
	 */
	public boolean verifyConversationNumberBySubject_PR(String subject, int number) {
		Logging.info("Verifying number of conversation " + subject + " is " + number
				+ " in email list in preview right layout");
		String email_conver_number_by_subject_pr = PropertyHelper.coreEmailFile
				.getPropertyValue("email_conver_number_by_subject_pr").replace("+variable+", subject.trim());
		boolean result = false;
		String numberString = "";
		if (number < 10) {
			numberString = "0" + number;
		} else {
			numberString = String.valueOf(number);
		}

		try {
			Tools.setDriverDefaultValues();
			String actualNumber = Base.base.getDriver().findElement(By.xpath(email_conver_number_by_subject_pr))
					.getText();

			if (actualNumber.equals(numberString)) {
				result = true;
			}
			Logging.info("Conversation number is as expected: " + result + ", actual number: " + actualNumber);
		} catch (WebDriverException ex) {
			Logging.error("Could not verify number of conversation " + subject);
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Jun 09, 2015<br>
	 * <b>Description</b><br>
	 * Verifies if the specified conversation has correct number in email list in
	 * preview below layout.
	 * 
	 * @param subject
	 *            conversation subject
	 * @param number
	 *            message number in conversation
	 * @return number is correct or not
	 */
	public boolean verifyConversationNumberBySubject_PB(String subject, int number) {
		Logging.info("Verifying number of conversation " + subject + " is " + number
				+ " in email list in preview below layout");
		String email_conver_number_by_subject_pb = PropertyHelper.coreEmailFile
				.getPropertyValue("email_conver_number_by_subject_pb").replace("+variable+", subject.trim());
		boolean result = false;
		String numberString = "";
		if (number < 10) {
			numberString = "0" + number;
		} else {
			numberString = String.valueOf(number);
		}
		try {
			Tools.setDriverDefaultValues();
			String actualNumber = Base.base.getDriver().findElement(By.xpath(email_conver_number_by_subject_pb))
					.getText();

			if (actualNumber.equals(numberString)) {
				result = true;
			}
			Logging.info("Conversation number is as expected: " + result + ", actual number: " + actualNumber);
		} catch (WebDriverException ex) {
			Logging.error("Could not verify number of conversation " + subject);
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jun 10, 2015<br>
	 * <b>Description</b><br>
	 * Verifies if the specified conversation has correct number in email list in
	 * preview none layout.
	 * 
	 * @param subject
	 *            conversation subject
	 * @param number
	 *            message number in conversation
	 * @return number is correct or not
	 */
	public boolean verifyConversationNumberBySubject_PN(String subject, int number) {
		Logging.info("Verifying number of conversation " + subject + " is " + number + " in email list");
		String email_conver_number_by_subject_pn = PropertyHelper.coreEmailFile
				.getPropertyValue("email_conver_number_by_subject_pn").replace("+variable+", subject.trim());
		boolean result = false;
		String numberString = "";
		if (number < 10) {
			numberString = "0" + number;
		} else {
			numberString = String.valueOf(number);
		}
		try {
			Tools.setDriverDefaultValues();
			String actualNumber = Base.base.getDriver().findElement(By.xpath(email_conver_number_by_subject_pn))
					.getText();

			if (actualNumber.equals(numberString)) {
				result = true;
			}
			Logging.info("Conversation number is as expected: " + result + ", actual number: " + actualNumber);
		} catch (WebDriverException ex) {
			Logging.error("Could not verify number of conversation " + subject);
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Jun 09, 2015<br>
	 * <b>Description</b><br>
	 * Verifies if conversation messages displayed in mail preview window and
	 * conversation number is correct in preview right layout.
	 * 
	 * @param number
	 *            message number in conversation
	 * @return conversation is displayed in preview window and number is correct
	 */
	public boolean verifyConversationDisplayedInPreviewWindow_PR(int number) {
		Logging.info("Verifying conversation displayed in preview window in preview right layout and "
				+ "message number is " + number);
		String email_conver_item_pr = PropertyHelper.coreEmailFile.getPropertyValue("email_conver_item_pr");
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			int actualNumber = Base.base.getDriver().findElements(By.xpath(email_conver_item_pr)).size();

			if (actualNumber > 0) {
				Logging.info("Conversation messages displayed in preview window...verifying number");
			} else {
				Logging.info("Conversation messages not displayed in preview window");
			}

			if (actualNumber == number) {
				result = true;
			}
			Logging.info("Conversation number is as expected: " + result + ", actual number: " + actualNumber);
		} catch (WebDriverException ex) {
			Logging.error("Could not verify conversation in preview window in preview right layout");
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Jun 09, 2015<br>
	 * <b>Description</b><br>
	 * Verifies if conversation messages displayed in mail preview window and
	 * conversation number is correct in preview below layout.
	 * 
	 * @param number
	 *            message number in conversation
	 * @return conversation is displayed in preview window and number is correct
	 */
	public boolean verifyConversationDisplayedInPreviewWindow_PB(int number) {
		Logging.info("Verifying conversation displayed in preview window in preview below layout and "
				+ "message number is " + number);
		String email_conver_item_pb = PropertyHelper.coreEmailFile.getPropertyValue("email_conver_item_pb");
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			int actualNumber = Base.base.getDriver().findElements(By.xpath(email_conver_item_pb)).size();

			if (actualNumber > 0) {
				Logging.info("Conversation messages displayed in preview window...verifying number");
			} else {
				Logging.info("Conversation messages not displayed in preview window");
			}

			if (actualNumber == number) {
				result = true;
			}
			Logging.info("Conversation number is as expected: " + result + ", actual number: " + actualNumber);
		} catch (WebDriverException ex) {
			Logging.error("Could not verify conversation in preview window in preview below layout");
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Jun 11, 2015<br>
	 * <b>Description</b><br>
	 * Verifies if conversation message body contains specified text.
	 * 
	 * @param index
	 *            conversation message index
	 * @param text
	 *            expected text
	 * @return true if message body contains expected text.
	 */
	public boolean verifyMessageBodyInConversation_PR(int index, String text) {
		Logging.info(
				"Verifying in preview right layout conversation message " + index + " contains body text: " + text);
		String email_conver_msg_body_by_index_pr = PropertyHelper.coreEmailFile
				.getPropertyValue("email_conver_msg_body_by_index_pr").replace("+variable+", String.valueOf(index));
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			String actualText = Base.base.getDriver().findElement(By.xpath(email_conver_msg_body_by_index_pr))
					.getText();
			Logging.info("Actual body: \n" + actualText);
			result = actualText.contains(text);
			Logging.info("Conversation message " + index + " contains expected body text: " + result);
		} catch (WebDriverException ex) {
			Logging.error("Could not verify conversation message " + index + " contains body text: " + text);
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Jun 11, 2015<br>
	 * <b>Description</b><br>
	 * Verifies if conversation message subject is identical to specified text.
	 * 
	 * @param index
	 *            conversation message index
	 * @param subject
	 *            expected subject
	 * @return true if message subject equals to expected text.
	 */
	public boolean verifyMessageSubjectInConversation_PR(int index, String subject) {
		Logging.info("Verifying in preview right layout conversation message " + index + " has subject: " + subject);
		String email_conver_msg_header_subject_by_index_pr = PropertyHelper.coreEmailFile
				.getPropertyValue("email_conver_msg_header_subject_by_index_pr")
				.replace("+variable+", String.valueOf(index));
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			String actualText = Base.base.getDriver().findElement(By.xpath(email_conver_msg_header_subject_by_index_pr))
					.getText();
			Logging.info("Actual subject: " + actualText);
			result = actualText.contains(subject);
			Logging.info("Conversation message " + index + " has expected subject: " + result);
		} catch (WebDriverException ex) {
			Logging.error("Could not verify conversation message " + index + " has subject: " + subject);
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Jun 17, 2015<br>
	 * <b>Description</b><br>
	 * Verifies if time stamp displayed on a collapsed conversation message in
	 * preview right layout.
	 * 
	 * @param index
	 *            conversation message index
	 * @return true if time stamp is displayed.
	 */
	public boolean verifyMessageTimeStampInConversation_PR(int index) {
		Logging.info("Verifying if time stamp displayed on conversation message " + index);
		String email_conver_header_time_stamp_by_index_pr = PropertyHelper.coreEmailFile
				.getPropertyValue("email_conver_header_time_stamp_by_index_pr")
				.replace("+variable+", String.valueOf(index));
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(email_conver_header_time_stamp_by_index_pr));
			result = true;
			Logging.info("Conversation message " + index + " time stamp found: " + result);
		} catch (WebDriverException ex) {
			Logging.error("Could not verify time stamp on conversation message " + index);
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 2, 2015<br>
	 * <b>Description</b><br>
	 * Verifies if message body contains specified text
	 */
	public boolean verifyMessageBody_PR(String text) {
		Logging.info("Verifying in preview right layout message contains body text: " + text);
		String email_conver_msg_body_pr = PropertyHelper.coreEmailFile.getPropertyValue("email_conver_msg_body_pr");
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			String actualText = Base.base.getDriver().findElement(By.xpath(email_conver_msg_body_pr)).getText();
			Logging.info("Actual body: \n" + actualText);
			result = actualText.contains(text);
			Logging.info("Message contains expected body text: " + result);
		} catch (WebDriverException ex) {
			Logging.error("Could not verify message contains body text: " + text);
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 9, 2015<br>
	 * <b>Description</b><br>
	 * Verifies if text1's position is before text2 in message body
	 */
	public boolean verifyPositionInMessageBody_PR(String text1, String text2) {
		Logging.info("Verifying if text1's position is before text2 in message body");
		String email_conver_msg_body_pr = PropertyHelper.coreEmailFile.getPropertyValue("email_conver_msg_body_pr");
		int position1, position2;
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			String actualText = Base.base.getDriver().findElement(By.xpath(email_conver_msg_body_pr)).getText();
			Logging.info("Actual body: \n" + actualText);
			position1 = actualText.indexOf(text1);
			position2 = actualText.indexOf(text2);
			result = (position1 < position2);
			Logging.info("text1's position is before text2 in message body: " + result);
		} catch (WebDriverException ex) {
			Logging.error("if text1's position is before text2 in message body");
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 14, 2015<br>
	 * <b>Description</b><br>
	 * verify attachment existence by name in message preview in right layout.
	 * <b>Parameters</b><br>
	 * String name: attachment name
	 */
	public boolean verifyAttachmentExistenceByName_PR(String name) {
		Logging.info("Verifying message attachment existence by name in message preview in right layout: " + name);
		name = name.trim();
		String email_attachment_preview_pr = PropertyHelper.coreEmailFile
				.getPropertyValue("email_attachment_preview_pr").replace("+variable+", name);
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(email_attachment_preview_pr));

			result = true;
			Logging.info("Found attachment by name: " + name);
		} catch (WebDriverException ex) {
			Logging.error("Could not found attachment by name: " + name);
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 16, 2015<br>
	 * <b>Description</b><br>
	 * verify image displayed by URL in message preview in right layout.
	 * <b>Parameters</b><br>
	 * String URL: original URL of image
	 */
	public boolean verifyImageDisplayedByURL_PR(String URL) {
		Logging.info("Verifying image displayed by URL in message preview in right layout: " + URL);
		boolean result = false;
		URL = URL.trim();
		String email_image_pr = PropertyHelper.coreEmailFile.getPropertyValue("email_image_pr");

		try {
			Tools.setDriverDefaultValues();
			WebElement image = Base.base.getDriver().findElement(By.xpath(email_image_pr));
			String actualURL = image.getAttribute("src");
			Logging.info("Actual URL = " + actualURL + " comapred witn = " + URL);
			if (actualURL.equals(URL))  result = true;
		} catch (WebDriverException ex) {
			Logging.error("Could not find image displayed by URL: " + URL);
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Sep 16, 2015<br>
	 * <b>Description</b><br>
	 * Verifies "Show image" button existence in message preview in right layout
	 */
	public boolean verifyShowImageButtonExistence_PR() {
		Logging.info("Verifying \"Show image\" button existence in message preview in right layout");
		String email_show_image_btn_pr = PropertyHelper.coreEmailFile.getPropertyValue("email_show_image_btn_pr");
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(email_show_image_btn_pr));

			result = true;
			Logging.info("Found \"Show image\" button");
		} catch (WebDriverException ex) {
			Logging.error("Could not found \"Show image\" button in message preview in right layout");
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Oct 19, 2015<br>
	 * <b>Description</b><br>
	 * Click on "+" to create a new folder in right layout
	 */
	public void clickOnNewFolderFromMyFolders() {
		Logging.info("Clicking on \"+\" button to create a new folder in right layout");

		String email_newfolder_bnt_pr = PropertyHelper.coreEmailFile.getPropertyValue("email_newfolder_bnt_pr");

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(email_newfolder_bnt_pr)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on \"+\" button to create a new folder in right layout");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Oct 19, 2015<br>
	 * <b>Description</b><br>
	 * type new folder name in add new folder window <b>Parameters</b><br>
	 * String name: new folder name
	 */
	public void typeFolderNameInAddNewFolderWindow(String name) {
		Logging.info("type folder name in add new folder window: " + name);

		String email_folder_name_input_in_newfolder_window = PropertyHelper.coreEmailFile
				.getPropertyValue("email_folder_name_input_in_newfolder_window");

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(email_folder_name_input_in_newfolder_window)).clear();
			Base.base.getDriver().findElement(By.xpath(email_folder_name_input_in_newfolder_window)).sendKeys(name);

		} catch (WebDriverException ex) {
			Logging.error("Could not type folder name in add new folder window");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Oct 19, 2015<br>
	 * <b>Description</b><br>
	 * Click on toolbar button in new folder window <b>Parameters</b><br>
	 * String button: Save or Cancle
	 */
	public void clickOnToolbarButtonInAddNewFolderWindow(String button) {
		Logging.info("Clicking on toolbar button in new folder window");

		String email_newfolder_toolbar_bnt_in_newfolder_window = PropertyHelper.coreEmailFile
				.getPropertyValue("email_newfolder_toolbar_bnt_in_newfolder_window").replace("+variable+", button);

		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(email_newfolder_toolbar_bnt_in_newfolder_window);

			Base.base.getDriver().findElement(By.xpath(email_newfolder_toolbar_bnt_in_newfolder_window)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on toolbar button in new folder window");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Oct 19, 2015<br>
	 * <b>Description</b><br>
	 * verify folder is expanded by folder name in right layout.
	 * <b>Parameters</b><br>
	 * String name: folder name
	 */
	public boolean verifyFolderExpandedByName_PR(String name) {
		Logging.info("Verifying folder is expanded by folder name in right layout: " + name);

		String email_folder_treeview_in_list;
		if (name.contains("'") || name.contains("\"")) {
			name = Tools.generateConcatForXPath(name);
			email_folder_treeview_in_list = PropertyHelper.coreEmailFile
					.getPropertyValue("email_folder_treeview_in_list_concat").replace("+variable+", name);
		} else {
			email_folder_treeview_in_list = PropertyHelper.coreEmailFile
					.getPropertyValue("email_folder_treeview_in_list").replace("+variable+", name);
		}

		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			WebElement expander = Base.base.getDriver().findElement(By.xpath(email_folder_treeview_in_list));
			result = expander.getAttribute("class").contains("node-expanded");
			Logging.info("The folder is expanded : " + name);
		} catch (WebDriverException ex) {
			Logging.info("Could not verify folder is expanded by folder name: " + name);
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Oct 19, 2015<br>
	 * <b>Description</b><br>
	 * Expand folder by folder name or NOT * <b>Parameters</b><br>
	 * String name: folder name boolean expand: expand or not
	 */
	public void expandFolder(String name, boolean expand) {
		Logging.info("Expanding folder \"" + name + "\" in folder list: " + expand);

		boolean isExpanded = this.verifyFolderExpandedByName_PR(name);

		String email_folder_expander_in_list;
		if (name.contains("'") || name.contains("\"")) {
			name = Tools.generateConcatForXPath(name);
			email_folder_expander_in_list = PropertyHelper.coreEmailFile
					.getPropertyValue("email_folder_expander_in_list_concat").replace("+variable+", name);
		} else {
			email_folder_expander_in_list = PropertyHelper.coreEmailFile
					.getPropertyValue("email_folder_expander_in_list").replace("+variable+", name);
		}

		try {
			Tools.setDriverDefaultValues();

			if ((expand && !isExpanded) || (!expand && isExpanded)) {
				Base.base.getDriver().findElement(By.xpath(email_folder_expander_in_list)).click();
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not expand folder \"" + name + "\" in folder list");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Oct 20, 2015<br>
	 * <b>Description</b><br>
	 * Expand folder by folder name or NOT * <b>Parameters</b><br>
	 * String name: folder name boolean expand: expand or not
	 */
	public void expandFolder(WebElement folder, boolean expand) {
		Logging.info("Expanding folder in folder list: " + expand);

		String name = folder.getText();
		String email_folder_expander_in_list;

		if (name.contains("'") || name.contains("\"")) {
			name = Tools.generateConcatForXPath(name);
			email_folder_expander_in_list = PropertyHelper.coreEmailFile
					.getPropertyValue("email_folder_expander_in_list_concat").replace("+variable+", name);
		} else {
			email_folder_expander_in_list = PropertyHelper.coreEmailFile
					.getPropertyValue("email_folder_expander_in_list").replace("+variable+", name);
		}
		boolean isExpanded = false;

		try {
			Tools.setDriverDefaultValues();
			isExpanded = this.verifyFolderExpandedByName_PR(name);
			if ((expand && !isExpanded) || (!expand && isExpanded)) {
				Base.base.getDriver().findElement(By.xpath(email_folder_expander_in_list)).click();
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not expand folder \"" + name + "\" in folder list");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Oct 19, 2015<br>
	 * <b>Description</b><br>
	 * verify folder existence by name in right layout. <b>Parameters</b><br>
	 * String name: folder name
	 */
	public boolean verifyFolderExistenceByName_PR(String name) {
		Logging.info("Verifying folder existence by name in right layout: " + name);

		String email_folder_row;
		if (name.contains("'") || name.contains("\"")) {
			name = Tools.generateConcatForXPath(name);
			email_folder_row = PropertyHelper.coreEmailFile.getPropertyValue("email_folder_row_concat")
					.replace("+variable+", name);
		} else {
			email_folder_row = PropertyHelper.coreEmailFile.getPropertyValue("email_folder_row").replace("+variable+",
					name);
		}
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(email_folder_row));

			result = true;
			Logging.info("Found folder by name: " + name);
		} catch (WebDriverException ex) {
			Logging.error("Could not found folder by name: " + name);
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Oct 19, 2015<br>
	 * <b>Description</b><br>
	 * verify invalid icon is displayed in add new folder window.
	 */
	public boolean verifyInvalidIconDisplayInAddNewFolderWindow() {
		Logging.info("Verifying invalid icon is displayed in new folder window");

		String email_folder_invalid_icon_in_newfolder_window = PropertyHelper.coreEmailFile
				.getPropertyValue("email_folder_invalid_icon_in_newfolder_window");
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(email_folder_invalid_icon_in_newfolder_window))
					.getAttribute("class").contains("tree-node-expanded");
			result = true;
			Logging.info("Found invalid icon");
		} catch (WebDriverException ex) {
			Logging.error("Could not verify invalid icon is displayed in new folder window");
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Oct 19, 2015<br>
	 * <b>Description</b><br>
	 * Click on hover panel button in folder list by folder name
	 * <b>Parameters</b><br>
	 * String name: folder name String button: hover panel button One of the
	 * following item should be provided: - edit - delete - add - empty
	 */
	public void clickOnHoverPanelButtonInFolderListByFolderName(String name, String button) {
		Logging.info("Clicking on hover panel button \"" + button + "\" in folder list by folder name: " + name);

		String email_folder_row;
		if (name.contains("'") || name.contains("\"")) {
			name = Tools.generateConcatForXPath(name);
			email_folder_row = PropertyHelper.coreEmailFile.getPropertyValue("email_folder_row_concat")
					.replace("+variable+", name);
		} else {
			email_folder_row = PropertyHelper.coreEmailFile.getPropertyValue("email_folder_row").replace("+variable+",
					name);
		}
		String email_folder_hoverpanel_bnt = PropertyHelper.coreEmailFile
				.getPropertyValue("email_folder_hoverpanel_bnt").replace("+variable+", button);

		try {
			Tools.setDriverDefaultValues();

			Tools.moveFocusToElementByXpath(email_folder_row);

			Base.base.getDriver().findElement(By.xpath(email_folder_hoverpanel_bnt)).click();

		} catch (WebDriverException ex) {
			Logging.error("Could not click on hover panel button in folder list by folder name");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Oct 19, 2015<br>
	 * <b>Description</b><br>
	 * Click on "Rename folder" hover panel button to rename folder in folder list
	 * by original folder name <b>Parameters</b><br>
	 * String origName: original folder name String newName: new folder name
	 */
	public void renameFolderInFolderListByFolderName(String origName, String newName) {
		Logging.info("Rename the folder: " + origName + " to new name: " + newName);

		origName = origName.trim();
		newName = newName.trim();
		String email_folder_edit_field_in_list = PropertyHelper.coreEmailFile
				.getPropertyValue("email_folder_edit_field_in_list");

		try {
			Tools.setDriverDefaultValues();
			this.clickOnHoverPanelButtonInFolderListByFolderName(origName, "edit");
			//Base.base.getDriver().findElement(By.xpath(email_folder_edit_field_in_list)).clear();
			Base.base.getDriver().findElement(By.xpath(email_folder_edit_field_in_list)).sendKeys(Keys.CONTROL + "a");
			Base.base.getDriver().findElement(By.xpath(email_folder_edit_field_in_list)).sendKeys(Keys.DELETE);
			Tools.waitFor(1000, TimeUnit.MILLISECONDS);
			Logging.info("One second wait after clearing the existing folder name.");
			Base.base.getDriver().findElement(By.xpath(email_folder_edit_field_in_list)).sendKeys(newName);
			Tools.waitFor(1000, TimeUnit.MILLISECONDS);
			Logging.info("One second wait after changing the folder name.");
			Base.base.getDriver().findElement(By.xpath(email_folder_edit_field_in_list)).sendKeys(Keys.ENTER);
		} catch (WebDriverException ex) {
			Logging.error("Could not rename the folder: " + origName + " to new name: " + newName);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Nov 09, 2015<br>
	 * <b>Description</b><br>
	 * Click on "Rename folder" context click menu button to rename folder in folder
	 * list by original folder name <b>Parameters</b><br>
	 * String origName: original folder name String newName: new folder name
	 */
	public void renameFolderByContextClickByFolderName(String origName, String newName) {
		Logging.info("Rename the folder: " + origName + " to new name: " + newName + " by context click menu button");

		origName = origName.trim();
		newName = newName.trim();
		String email_folder_edit_field_in_list = PropertyHelper.coreEmailFile
				.getPropertyValue("email_folder_edit_field_in_list");

		try {
			Tools.setDriverDefaultValues();
			this.clickOnContextClickMenuButtonByFolderName(origName, "Rename Folder");
			Tools.waitFor(1000, TimeUnit.MILLISECONDS);
			//Base.base.getDriver().findElement(By.xpath(email_folder_edit_field_in_list)).clear();
			Base.base.getDriver().findElement(By.xpath(email_folder_edit_field_in_list)).sendKeys(Keys.CONTROL + "a");
			Base.base.getDriver().findElement(By.xpath(email_folder_edit_field_in_list)).sendKeys(Keys.DELETE);
			Tools.waitFor(1000, TimeUnit.MILLISECONDS);
			Logging.info("One second wait after clearing the existing folder name.");
			Base.base.getDriver().findElement(By.xpath(email_folder_edit_field_in_list)).sendKeys(newName);
			Tools.waitFor(1000, TimeUnit.MILLISECONDS);
			Logging.info("One second wait after changing the folder name.");
			Base.base.getDriver().findElement(By.xpath(email_folder_edit_field_in_list)).sendKeys(Keys.ENTER);
		} catch (WebDriverException ex) {
			Logging.error("Could not rename the folder: " + origName + " to new name: " + newName
					+ " by context click menu button");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Oct 19, 2015<br>
	 * <b>Description</b><br>
	 * Click on "Delete folder" hover panel button to delete folder in folder list
	 * by folder name <b>Parameters</b><br>
	 * String name: folder name
	 */
	public void deleteFolderInFolderListByFolderName(String name) {
		Logging.info("Deleting the folder: " + name);

		try {
			Tools.setDriverDefaultValues();
			this.clickOnHoverPanelButtonInFolderListByFolderName(name, "delete");
			this.clickButtonOnMessageBox("Yes");
			Logging.info("The folder is deleted: " + name);
		} catch (WebDriverException ex) {
			Logging.error("Could not delete the folder: " + name);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Nov 09, 2015<br>
	 * <b>Description</b><br>
	 * Click on "Delete folder" context click menu button to delete folder in folder
	 * list by folder name <b>Parameters</b><br>
	 * String name: folder name
	 */
	public void deleteFolderByContextClickByFolderName(String name) {
		Logging.info("Deleting the folder: " + name + " by context click menu button");

		try {
			Tools.setDriverDefaultValues();
			this.clickOnContextClickMenuButtonByFolderName(name, "Delete Folder");
			Tools.waitFor(100, TimeUnit.MILLISECONDS);
			this.clickButtonOnMessageBox("Yes");
			Logging.info("The folder is deleted: " + name);
		} catch (WebDriverException ex) {
			Logging.error("Could not delete the folder: " + name + " by context click menu button");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Nov 06, 2015<br>
	 * <b>Description</b><br>
	 * verify folder contains any sub-folder by name in right layout.
	 * <b>Parameters</b><br>
	 * String name: folder name
	 */
	public boolean verifyFolderContainsSubFolderByName_PR(String name) {
		Logging.info("Verifying folder contains any sub-folder by folder name in right layout: " + name);

		boolean exist = this.verifyFolderExistenceByName_PR(name);
		String email_folder_expander_in_list;
		if (name.contains("'") || name.contains("\"")) {
			name = Tools.generateConcatForXPath(name);
			email_folder_expander_in_list = PropertyHelper.coreEmailFile
					.getPropertyValue("email_folder_expander_in_list_concat").replace("+variable+", name);
		} else {
			email_folder_expander_in_list = PropertyHelper.coreEmailFile
					.getPropertyValue("email_folder_expander_in_list").replace("+variable+", name);
		}

		boolean result = false;

		try {
			Tools.setDriverDefaultValues();

			if (exist) {
				Base.base.getDriver().findElement(By.xpath(email_folder_expander_in_list));

				result = true;
				Logging.info("This folder contains sub-folder");
			} else {
				Logging.info("This folder doesn't exist " + name);
			}
		} catch (WebDriverException ex) {
			Logging.info("This folder does NOT contain any sub-folder by name: " + name);
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Oct 20, 2015<br>
	 * <b>Description</b><br>
	 * cleanup all private folders in folder list
	 * 
	 */
	public void cleanupAllPrivateFolders() {
		Logging.info("Cleanup all private folders in folder list");
		String email_folder_folderlist = PropertyHelper.coreEmailFile.getPropertyValue("email_folder_folderlist");
		String folderName;

		try {
			Tools.setDriverDefaultValues();

			if (this.verifyFolderContainsSubFolderByName_PR("My Folders")) {
				// expand "My Folders" folder
				this.expandFolder("My Folders", true);

				List<WebElement> folders = Base.base.getDriver().findElements(By.xpath(email_folder_folderlist));

				for (int i = folders.size() - 1; i >= 0; i--) {
					List<WebElement> foldersAfter = Base.base.getDriver()
							.findElements(By.xpath(email_folder_folderlist));
					if (foldersAfter.size() > 0) {
						folderName = foldersAfter.get(0).getAttribute("innerHTML");
						Logging.info("folderNames " + folderName);
						Logging.info("deleting " + folderName);
						this.deleteFolderInFolderListByFolderName(folderName);
						Logging.info("Deleted " + folderName);
					}

				}
			}

		} catch (WebDriverException ex) {
			Logging.error("Could not cleanup the private folders");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Nov 09, 2015<br>
	 * <b>Description</b><br>
	 * Empty system folders
	 * 
	 */
	public void emptySystemFolder(String folder) {
		Logging.info("Emptying system folder: " + folder);
		String common_msgbox = PropertyHelper.pageObjectBaseFile.getPropertyValue("common_msgbox");

		try {
			Tools.setDriverDefaultValues();

			this.clickOnHoverPanelButtonInFolderListByFolderName(folder, "empty");

			boolean boxDisplayed = false;
			try {
				Base.base.getDriver().manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
				boxDisplayed = !Base.base.getDriver().findElement(By.xpath(common_msgbox)).getAttribute("class")
						.contains("hide-offsets");
			} catch (WebDriverException ex) {
				Logging.info("There is no message box pops up");
			} finally {
				Tools.setDriverDefaultValues();
			}

			if (boxDisplayed) {
				this.clickButtonOnMessageBox("Yes");
			}

			Logging.info("The system folder is emptied");
		} catch (WebDriverException ex) {
			Logging.error("Could not empty system folder");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Nov 09, 2015<br>
	 * <b>Description</b><br>
	 * Click on context click menu button by folder name <b>Parameters</b><br>
	 * String name: folder name String button: context click menu button One of the
	 * following item should be provided: - Add New Folder - Rename Folder - Delete
	 * Folder
	 */
	public void clickOnContextClickMenuButtonByFolderName(String name, String button) {
		Logging.info("Clicking on context click menu button \"" + button + "\" by folder name: " + name);

		String email_folder_row;
		if (name.contains("'") || name.contains("\"")) {
			name = Tools.generateConcatForXPath(name);
			email_folder_row = PropertyHelper.coreEmailFile.getPropertyValue("email_folder_row_concat")
					.replace("+variable+", name);
		} else {
			email_folder_row = PropertyHelper.coreEmailFile.getPropertyValue("email_folder_row").replace("+variable+",
					name);
		}
		String email_folder_contextclick_menu = PropertyHelper.coreEmailFile
				.getPropertyValue("email_folder_contextclick_menu").replace("+variable+", button);

		try {
			Tools.setDriverDefaultValues();
			Tools.moveFocusToElementByXpath(email_folder_row);
			Tools.contextClickOnElementByXpath(email_folder_row);
			Base.base.getDriver().findElement(By.xpath(email_folder_contextclick_menu)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on context click menu button by folder name");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Nov 10, 2015<br>
	 * <b>Description</b><br>
	 * Drag and drop source folder to target folder by folder name
	 * <b>Parameters</b><br>
	 * String sourceName: source folder name String targetName: target folder name
	 */
	public void dragAndDropSrouceFolderToTargetFolderByName(String sourceName, String targetName) {
		Logging.info("Dragging and dropping folder \"" + sourceName + "\" to folder \"" + targetName + "\"");

		String email_folder_row_source;
		String email_folder_row_target;
		if (sourceName.contains("'") || sourceName.contains("\"")) {
			sourceName = Tools.generateConcatForXPath(sourceName);
			email_folder_row_source = PropertyHelper.coreEmailFile.getPropertyValue("email_folder_row_concat")
					.replace("+variable+", sourceName);
		} else {
			email_folder_row_source = PropertyHelper.coreEmailFile.getPropertyValue("email_folder_row")
					.replace("+variable+", sourceName);
		}
		if (targetName.contains("'") || targetName.contains("\"")) {
			targetName = Tools.generateConcatForXPath(targetName);
			email_folder_row_target = PropertyHelper.coreEmailFile.getPropertyValue("email_folder_row_concat")
					.replace("+variable+", targetName);
		} else {
			email_folder_row_target = PropertyHelper.coreEmailFile.getPropertyValue("email_folder_row")
					.replace("+variable+", targetName);
		}

		try {
			Tools.setDriverDefaultValues();
			Tools.moveFocusToElementByXpath(email_folder_row_source);
			Tools.dragAndDropSourceToTargetByXpath(email_folder_row_source, email_folder_row_target);
		} catch (WebDriverException ex) {
			Logging.error("Could not Drag and drop folder \"" + sourceName + "\" to folder \"" + targetName + "\"");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Nov 10, 2015<br>
	 * <b>Description</b><br>
	 * Drag and drop source message to target folder by folder name
	 * <b>Parameters</b><br>
	 * String sourceName: source message subject name String targetName: target
	 * folder name
	 */
	public void dragAndDropSrouceMsgToTargetFolderByName(String sourceName, String targetName) {
		Logging.info("Dragging and dropping message \"" + sourceName + "\" to folder \"" + targetName + "\"");

		String email_message_subject_source;
		String email_folder_row_target;
		email_message_subject_source = PropertyHelper.coreEmailFile.getPropertyValue("email_list_whole_message_subject")
				.replace("+variable+", sourceName);

		if (targetName.contains("'") || targetName.contains("\"")) {
			targetName = Tools.generateConcatForXPath(targetName);
			email_folder_row_target = PropertyHelper.coreEmailFile.getPropertyValue("email_folder_row_concat")
					.replace("+variable+", targetName);
		} else {
			email_folder_row_target = PropertyHelper.coreEmailFile.getPropertyValue("email_folder_row")
					.replace("+variable+", targetName);
		}

		try {
			Tools.setDriverDefaultValues();
			Tools.moveFocusToElementByXpath(email_message_subject_source);
			Tools.dragAndDropSourceToTargetByXpath(email_message_subject_source, email_folder_row_target);
		} catch (WebDriverException ex) {
			Logging.error("Could not Drag and drop message \"" + sourceName + "\" to folder \"" + targetName + "\"");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Nov 10, 2015<br>
	 * <b>Description</b><br>
	 * Click on hover panel button in folder list by folder name
	 * <b>Parameters</b><br>
	 * String subject: email subject String button: float icon button One of the
	 * following item should be provided: - compose - reply - flag - delete
	 */
	public void clickEmailFloatIconButtonBySubject(String subject, String button) {
		Logging.info("Clicking on float icon button \"" + button + "\" on message by message subject: " + subject);

		String email_list_message_clicking_area = PropertyHelper.coreEmailFile
				.getPropertyValue("email_list_message_clicking_area").replace("+variable+", subject);
		String email_float_icon_button = PropertyHelper.coreEmailFile.getPropertyValue("email_float_icon_button")
				.replace("+variable+", button);

		try {
			Tools.setDriverDefaultValues();
			Tools.moveFocusToElementByXpath(email_list_message_clicking_area);
			Base.base.getDriver().findElement(By.xpath(email_float_icon_button)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on float icon button \"" + button + "\" on message by message subject: "
					+ subject);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Nov 10, 2015<br>
	 * <b>Description</b><br>
	 * verify message is unread by subject in right layout. <b>Parameters</b><br>
	 * String subject: message subject
	 */
	public boolean verifyMsgIsUnreadBySubject_PR(String subject) {
		Logging.info("Verifying message is unread by subject in right layout: " + subject);

		String email_unseen_icon_by_subject = PropertyHelper.coreEmailFile
				.getPropertyValue("email_unseen_icon_by_subject").replace("+variable+", subject);

		boolean result = false;

		try {
			Tools.setDriverDefaultValues();

			// To speed up unread icon searching.
			Base.base.getDriver().manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

			String className = Base.base.getDriver().findElement(By.xpath(email_unseen_icon_by_subject))
					.getAttribute("class");
			if (className.contains("unread"))
				result = true;
			Logging.info("Found unread icon. The message \"" + subject + "\" is unread");
		} catch (WebDriverException ex) {
			Logging.info("Could not find unread icon on message: " + subject);
		} finally {
			// restore default values
			Tools.setDriverDefaultValues();
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Nov 10, 2015<br>
	 * <b>Description</b><br>
	 * verify folder contains unread message by folder name in right layout.
	 * <b>Parameters</b><br>
	 * String foldername: folder name
	 */
	public boolean verifyFolderContainsUnreadMsgByName_PR(String foldername) {
		Logging.info("Verifying folder contains unread message by folder name in right layout: " + foldername);

		String email_folder_unreadcount_by_name = PropertyHelper.coreEmailFile
				.getPropertyValue("email_folder_unreadcount_by_name").replace("+variable+", foldername);

		boolean result = false;

		try {
			Tools.setDriverDefaultValues();

			// To speed up unread icon searching.
			Base.base.getDriver().manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

			Base.base.getDriver().findElement(By.xpath(email_folder_unreadcount_by_name));

			result = true;
			Logging.info("Found unread accounter. The folder \"" + foldername + "\" contains unread message");
		} catch (WebDriverException ex) {
			Logging.error("Could not verify folder contains unread message by folder name: " + foldername);
		} finally {
			// restore default values
			Tools.setDriverDefaultValues();
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Nov 10, 2015<br>
	 * <b>Description</b><br>
	 * retrieve unread counter number by folder name in right layout.
	 * <b>Parameters</b><br>
	 * String foldername: folder name
	 */
	public int retrieveUnreadCounterNumberByFolderName_PR(String foldername) {
		Logging.info("Retrieve unread counter number by folder name in right layout: " + foldername);

		String email_folder_unreadcount_by_name = PropertyHelper.coreEmailFile
				.getPropertyValue("email_folder_unreadcount_by_name").replace("+variable+", foldername);

		int result = 0;

		try {
			Tools.setDriverDefaultValues();

			if (this.verifyFolderContainsUnreadMsgByName_PR(foldername)) {
				result = Integer.parseInt(
						Base.base.getDriver().findElement(By.xpath(email_folder_unreadcount_by_name)).getText());
			}
			Logging.info("The folder \"" + foldername + "\" contains \"" + result + "\" unread message");
		} catch (WebDriverException ex) {
			Logging.error("Could not retrieve unread counter number by folder name: " + foldername);
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Nov 12, 2015<br>
	 * <b>Description</b><br>
	 * Mark message as Unread by message subject <b>Parameters</b><br>
	 * String subject: email subject boolean mark: mark message as unread or not
	 */
	public void markMessageAsUnreadBySubject(String subject, boolean mark) {
		Logging.info("Marking message as unread by message subject: " + subject);

		String email_list_message_clicking_area = PropertyHelper.coreEmailFile
				.getPropertyValue("email_list_message_clicking_area").replace("+variable+", subject);
		boolean isUnread = this.verifyMsgIsUnreadBySubject_PR(subject);

		try {
			Tools.setDriverDefaultValues();
			Tools.moveFocusToElementByXpath(email_list_message_clicking_area);

			if ((mark && !isUnread) || (!mark && isUnread)) {
				this.clickEmailFloatIconButtonBySubject(subject, "compose");
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not mark message as unread by subject: " + subject);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Oct 13, 2016<br>
	 * <b>Description</b><br>
	 * Move focus on a menu item </b><br>
	 * String menuItem: the text of the menu item
	 */
	public void moveFocusOnMenu(String menuItem) {
		Logging.info("clicking menu item " + menuItem);

		String email_more_menu_item = PropertyHelper.coreEmailFile.getPropertyValue("email_menu_item")
				.replace("+variable+", menuItem);

		try {
			Tools.moveFocusToElementByXpath(email_more_menu_item);
		} catch (WebDriverException ex) {
			Logging.error("Could not move to menu item " + menuItem);
			throw ex;
		}
	}

	/**
	 * /** <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Oct 13, 2016<br>
	 * <b>Description</b>: Move focus on a menu item<br>
	 * <b>String menuItem</b>: the text of the menu item
	 */
	public void clickMenuItem(String menuItem) {
		Logging.info("clicking menu item " + menuItem);
		String email_menu_item = PropertyHelper.coreEmailFile.getPropertyValue("email_menu_item").replace("+variable+",
				menuItem);
		Logging.info(email_menu_item);
		try {
			Base.base.getDriver().findElement(By.xpath(email_menu_item)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click menu item " + menuItem);
			throw ex;
		}
	}

	public void clickFlagFollowUpMenuItem(String menuItem) {
		Logging.info("clicking flag follow up menu item " + menuItem);
		String email_menu_item = PropertyHelper.coreEmailFile.getPropertyValue("email_menu_item").replace("+variable+",
				menuItem);
		Logging.info(email_menu_item);
		try {
			WebElement element = Base.base.getDriver().findElement(By.xpath(email_menu_item));
			Actions moveTo = new Actions(Base.base.getDriver()).moveToElement(element).click(element);
			moveTo.perform();
			//Base.base.getDriver().findElement(By.xpath(email_menu_item)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click menu item " + menuItem);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Oct 13, 2016<br>
	 * <b>Description</b><br>
	 * verify if the menu item is disabled </b><br>
	 * String menuItem: the text of the menu item
	 */
	public boolean verifyMenuDisabled(String menuItem) {
		Logging.info("Verifying if " + menuItem + " is disabled");

		String email_menu_item_disabled = PropertyHelper.coreEmailFile.getPropertyValue("email_menu_item_disabled")
				.replace("+variable+", menuItem);
		Logging.info(email_menu_item_disabled);

		try {
			List<WebElement> list = Base.base.getDriver().findElements(By.xpath(email_menu_item_disabled));
			if (list.size() != 0)
				return true;
			else
				return false;

		} catch (WebDriverException ex) {
			ex.printStackTrace();
			return false;
		}
	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Oct 13, 2016<br>
	 * <b>Description</b><br>
	 * Verify if the mail is flagged or not </b><br>
	 * String mailSubject: the text of the mail subject
	 * 
	 * @return true - the mail is flagged, false - the mail is not flagged
	 */
	public boolean verifyIfMailFlagged(String mailSubject) {
		Logging.info("Verifying if mail " + mailSubject + " is flagged");

		String email_flag = PropertyHelper.coreEmailFile.getPropertyValue("email_flag").replace("+variable+",
				mailSubject);
		List<WebElement> list = null;
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			Tools.waitFor(1, TimeUnit.SECONDS);
			list = Base.base.getDriver().findElements(By.xpath(email_flag));

		} catch (WebDriverException ex) {
			Logging.info("Could not find element in verifyMailFlageed " + mailSubject);
		}
		if (list.size() != 0)
			result = true;
		return result;

	}

	/**
	 * <b>Author</b>: Vivian Xie<br>
	 * <b>Date</b>: Oct 13, 2016<br>
	 * <b>Description</b><br>
	 * Move focus on a menu item </b><br>
	 * String menuItem: the text of the menu item int time--the driver may be failed
	 * to locate the sub-menu of menuItem, so give the time to moveto the menu.
	 */
	public void moveToMenuItem(String menuItem, int time) {
		Logging.info("Moving to menu item " + menuItem);
		String email_menu_item = PropertyHelper.coreEmailFile.getPropertyValue("email_menu_item").replace("+variable+",
				menuItem);
		try {
			Tools.setDriverDefaultValues();
			for (int i = 0; i < time; i++) {
				Tools.moveFocusToElementByXpath(email_menu_item);
				Tools.waitFor(1, TimeUnit.SECONDS);
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not select menu item " + menuItem);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Nov 13, 2015<br>
	 * <b>Description</b><br>
	 * Click on email search trigger button to launch advanced search window
	 */
	public void clickOnEmailSearchTrigger() {
		Logging.info("Clicking on email search trigger button to launch advanced search window");

		String email_search_trigger = PropertyHelper.coreEmailFile.getPropertyValue("email_search_trigger");

		try {
			Tools.setDriverDefaultValues();
			Tools.moveFocusToElementByXpath(email_search_trigger);
			Base.base.getDriver().findElement(By.xpath(email_search_trigger)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on email search trigger button");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Oct 19, 2015<br>
	 * <b>Description</b><br>
	 * type text in search field in search panel by field name <b>Parameters</b><br>
	 * String fieldName: field name for typing One of the following item should be
	 * provided: - Recipient - Sender - Subject - Body
	 */
	public void typeInSearchFieldInSearchPanelByFieldName(String fieldName, String inputText) {
		Logging.info("type text in search field in search panel by field name: " + fieldName);

		String email_searchpanel_input = PropertyHelper.coreEmailFile.getPropertyValue("email_searchpanel_input")
				.replace("+variable+", fieldName);

		try {
			Tools.setDriverDefaultValues();

			Base.base.getDriver().findElement(By.xpath(email_searchpanel_input)).clear();
			Base.base.getDriver().findElement(By.xpath(email_searchpanel_input)).sendKeys(inputText);
		} catch (WebDriverException ex) {
			Logging.error("Could not type text in search field in search panel by field name");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Nov 13, 2015<br>
	 * <b>Description</b><br>
	 * Click on folder trigger button in search panel
	 */
	public void clickOnFolderTriggerInSearchPanel() {
		Logging.info("Clicking on folder trigger button in search panel");

		String email_searchpanel_folder_trigger = PropertyHelper.coreEmailFile
				.getPropertyValue("email_searchpanel_folder_trigger");

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(email_searchpanel_folder_trigger)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on folder trigger button in search panel");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Nov 13, 2015<br>
	 * <b>Description</b><br>
	 * Select folder in search panel by folder name <b>Parameters</b><br>
	 * String folderName: folder name One of the following item should be provided:
	 * - All Folders - Inbox - Drafts - Sent - Spam - Trash - My Folders
	 */
	public void selectFolderInSearchPanelByFolderName(String folderName) {
		Logging.info("Selecting folder in search panel by folder name: " + folderName);

		String email_searchpanel_folder_menuitem = PropertyHelper.coreEmailFile
				.getPropertyValue("email_searchpanel_folder_menuitem").replace("+variable+", folderName);

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(email_searchpanel_folder_menuitem)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not select folder in search panel by folder name: " + folderName);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Nov 13, 2015<br>
	 * <b>Description</b><br>
	 * Click on search button in search panel
	 */
	public void clickOnSearchButtonInSearchPanel() {
		Logging.info("Clicking on search button in search panel");

		String email_searchpanel_search_button = PropertyHelper.coreEmailFile
				.getPropertyValue("email_searchpanel_search_button");

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(email_searchpanel_search_button)).click();
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on search button in search panel");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Nov 17, 2015<br>
	 * <b>Description</b><br>
	 * verify the clean-button icon displayed in search panel by field name
	 * <b>Parameters</b><br>
	 * String fieldName: field name for typing One of the following item should be
	 * provided: - Recipient - Sender - Subject - Body
	 */
	public boolean verifyCleanButtonExistenceInSearchPanelByFieldName(String fieldName) {
		Logging.info("verifying clean-button displayed in search field in search panel by field name: " + fieldName);

		boolean result = false;
		String email_searchpanel_clearbutton = PropertyHelper.coreEmailFile
				.getPropertyValue("email_searchpanel_clearbutton").replace("+variable+", fieldName);
		String email_searchpanel_input = PropertyHelper.coreEmailFile.getPropertyValue("email_searchpanel_input")
				.replace("+variable+", fieldName);

		try {
			Tools.setDriverDefaultValues();
			Tools.moveFocusToElementByXpath(email_searchpanel_input);
			result = Base.base.getDriver().findElement(By.xpath(email_searchpanel_clearbutton)).getAttribute("class")
					.contains("icon-clearbutton-on");
			Logging.info("found clean-button in this field, it means there is text in it");
		} catch (WebDriverException ex) {
			Logging.info("Could not found clean-button in search field in search panel by field name");
		}
		return result;
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Nov 17, 2015<br>
	 * <b>Description</b><br>
	 * click clean-button to clean text in search field in search panel by field
	 * name <b>Parameters</b><br>
	 * String fieldName: field name for typing One of the following item should be
	 * provided: - Recipient - Sender - Subject - Body
	 */
	public void cleanSearchFieldInSearchPanelByFieldName(String fieldName) {
		Logging.info("clicking clean-button to clean text in search field in search panel by field name: " + fieldName);

		String email_searchpanel_clearbutton = PropertyHelper.coreEmailFile
				.getPropertyValue("email_searchpanel_clearbutton").replace("+variable+", fieldName);
		String email_searchpanel_input = PropertyHelper.coreEmailFile.getPropertyValue("email_searchpanel_input")
				.replace("+variable+", fieldName);
		boolean isFilled = this.verifyCleanButtonExistenceInSearchPanelByFieldName(fieldName);

		try {
			Tools.setDriverDefaultValues();
			if (isFilled) {
				Tools.moveFocusToElementByXpath(email_searchpanel_input);
				Base.base.getDriver().findElement(By.xpath(email_searchpanel_clearbutton)).click();
			}
		} catch (WebDriverException ex) {
			Logging.error("Could not clean text in search field in search panel by field name");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Nov 17, 2015<br>
	 * <b>Description</b><br>
	 * click clean-button to clean text in search fields in search panel by field
	 * name
	 */
	public void cleanAllSearchFieldInSearchPanel() {
		Logging.info("cleaning text in all search fields in search panel");
		this.cleanSearchFieldInSearchPanelByFieldName("Recipient");
		this.cleanSearchFieldInSearchPanelByFieldName("Sender");
		this.cleanSearchFieldInSearchPanelByFieldName("Subject");
		this.cleanSearchFieldInSearchPanelByFieldName("Body");
	}

	/**
	 * @Method Name: click_email_recipients_count
	 * @Author: Vivian Xie
	 * @Created On: 09/14/2016
	 * @Description: click recipient count in the open mail
	 * @parameter1:
	 */
	public void clickEmailRecipientsCount() {

		Logging.info("clicking on email recipients count");
		String email_searchpanel_clearbutton = PropertyHelper.coreEmailFile.getPropertyValue("email_contact_count");
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(email_searchpanel_clearbutton)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on email recipients count");
			throw ex;
		}
	}

	/**
	 * @Method Name: click_email_recipients_click
	 * @Author: Vivian Xie
	 * @Created On: 09/14/2016
	 * @Description: click a recipient in the open mail
	 * @parameter1: String recipientAddress - recipient email address
	 */
	public void clickEmailRecipients(String group, String recipientAddress) {

		Logging.info("clicking on email recipients " + recipientAddress);

		String email_to_recipient = PropertyHelper.coreEmailFile.getPropertyValue("email_to_recipient")
				.replace("+variable+", recipientAddress).replace("+groupName+", group);
		try {
			if (group.equals("To:")) {
				email_to_recipient = "(" + email_to_recipient + ")[2]";
			}
			Tools.waitUntilElementDisplayedByXpath(email_to_recipient, 2000);
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(email_to_recipient)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on email recipients ");
			throw ex;
		}

	}

	/**
	 * @Author: Vivian Xie
	 * @Created On: 09/14/2016
	 * @Description: click Add to existing to contact menu
	 * @parameter1:
	 */
	public void clickMailRecipientMenu(String menuText) {
		Logging.info("clicking on " + menuText + " menu ");

		String email_recipient_menu = PropertyHelper.coreEmailFile.getPropertyValue("email_add_contact_menu")
				.replace("+variable+", menuText);
		String email_recipient_menu_click = PropertyHelper.coreEmailFile
				.getPropertyValue("email_add_contact_menu_click").replace("+variable+", menuText);
		try {
			Tools.setDriverDefaultValues();
			Tools.waitUntilElementDisplayedByXpath(email_recipient_menu, 2000);
			Base.base.getDriver().findElement(By.xpath(email_recipient_menu_click)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click " + menuText + " menu ");
			throw ex;
		}

	}

	/**
	 * @Author: Vivian Xie
	 * @Created On: 09/14/2016
	 * @Description: select an existing contact
	 * @parameter1: String contactName- the Fist Name of the the contact
	 */
	public void selectExistingContact(String contactName) {
		Logging.info("selecting an exixting contact");

		String email_select_exsiting_contact = PropertyHelper.coreEmailFile
				.getPropertyValue("email_select_exsiting_contact").replace("+variable+", contactName);
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(email_select_exsiting_contact)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not select existing contact " + contactName);
			throw ex;
		}

	}

	/**
	 * @Author: Vivian Xie
	 * @Created On: 09/14/2016
	 * @Description: click select button on "Select contact" Dialog
	 * @parameter1: String contactName- the Fist Name of the the contact
	 */

	public void clickAddToExistingContactSelect() {
		Logging.info("clicking add to existing contact select");

		String email_add_to_exsisting_contact_select_button = PropertyHelper.coreEmailFile
				.getPropertyValue("email_add_to_exsisting_contact_select_button");
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(email_add_to_exsisting_contact_select_button)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click add to existing contact select button ");
			throw ex;
		}

	}

	/**
	 * @Author: Vivian Xie
	 * @Created On: 11/7/2016
	 * @Description: click "Show image" button on email detail page
	 */
	public void clickShowImageButton() {
		Logging.info("clicking Show image button");

		String email_content_show_image_button = PropertyHelper.coreEmailFile
				.getPropertyValue("email_content_show_image_button");
		try {
			Tools.setDriverDefaultValues();
			Logging.info(email_content_show_image_button);
			Base.base.getDriver().findElement(By.xpath(email_content_show_image_button)).click();

		} catch (WebDriverException ex) {
			Logging.error("Could not click Show image button ");
			throw ex;
		}
	}

	/**
	 * @Author: Vivian Xie
	 * @Created On: 11/6/2016
	 * @Description: verify if the attached image in the selected email is not block
	 * @param type:
	 *            attachment or line or local image
	 * @return true if the image is not blocked else return false.
	 */
	public boolean verifyImageNotBlocked(String type) {
		Logging.info("Verifying if Image is block in mail content ");
		boolean result = false;
		String email_content_image = null;
		if (type.equalsIgnoreCase("attachment"))
			email_content_image = PropertyHelper.coreEmailFile.getPropertyValue("email_content_attachment_image");
		else if (type.equalsIgnoreCase("local"))
			email_content_image = PropertyHelper.coreEmailFile.getPropertyValue("email_content_image_local");
		String src = "";
		String[] list = null;
		String sBlock = "";
		try {
			Tools.setDriverDefaultValues();
			Logging.info(email_content_image);
			src = Base.base.getDriver().findElements(By.xpath(email_content_image)).get(0).getAttribute("src");
			list = src.split("/");
			int length = list.length;
			sBlock = list[length - 1];
		} catch (WebDriverException ex) {
			Logging.error("Could not find Image in mail content");
			throw ex;
		}
		Logging.info(sBlock);
		if (sBlock.equals("blocked")) {
			result = false;
			Logging.info("Image is blocked");
		} else {
			result = true;
			Logging.info("Image is not blocked");
		}
		return result;
	}

	/**
	 * @Author: Vivian Xie
	 * @Created On: 11/6/2016
	 * @Description: Click on private folder
	 * @parameters: String folderName- the name of the private folder
	 */
	public void clickOnPrivateFolder(String privateFolderName) {
		Logging.info("Clicking on private folder: " + privateFolderName);
		String email_private_folder = PropertyHelper.coreEmailFile.getPropertyValue("email_private_folder")
				.replace("+variable+", privateFolderName);
		try {
			Tools.setDriverDefaultValues();
			Logging.info(email_private_folder);
			Base.base.getDriver().findElement(By.xpath(email_private_folder)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on private folder: " + privateFolderName);
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 7, 2016<br>
	 * <b>Description</b><br>
	 * Click the attachment in attachment view by name
	 * 
	 */
	public void clickAttachmentByName(String emailSubject, String attachmentName) {
		Logging.info("click attachment by name");

		String attachment_view_name = PropertyHelper.coreEmailFile.getPropertyValue("attachment_view_name");
		attachment_view_name = String.format(attachment_view_name, attachmentName);
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(attachment_view_name)).click();
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 7, 2016<br>
	 * <b>Description</b><br>
	 * Verify the attachment view tooltip details by get "title" of the element
	 * 
	 * @param attachmentInfo
	 *            - the attachment info, like attachment title, sender, attachment
	 *            time, email subject etc.
	 * @param emailSubjectWithAttachment
	 *            - the email subject that contains the attachment
	 * 
	 */
	public String verifyAttachmentDetails(String attachmentInfo, String emailSubjectWithAttachment) {
		String email_attachment_name = PropertyHelper.coreEmailFile.getPropertyValue("attachment_view_name");
		String attachment_view_subject = PropertyHelper.coreEmailFile.getPropertyValue("attachment_view_sender");
		String attachment_view_date = PropertyHelper.coreEmailFile.getPropertyValue("attachment_view_date");
		String attachment_view_sender = PropertyHelper.coreEmailFile.getPropertyValue("attachment_view_sender");

		String elementXath = null;
		String attachmentTitle = null;
		switch (attachmentInfo) {
		case ATTCHMENTTITLE:
			elementXath = email_attachment_name;
			break;
		case ATTCHMENTSENDER:
			elementXath = attachment_view_sender;
			break;
		case ATTCHMENTTIME:
			elementXath = attachment_view_date;
			break;
		case ATTCHMENTSUBJECT:
			elementXath = attachment_view_subject;
			break;

		}
		elementXath = String.format(elementXath, emailSubjectWithAttachment);

		try {
			if (attachmentInfo.equals(ATTCHMENTTITLE)) {
				Tools.setDriverDefaultValues();
				attachmentTitle = Base.base.getDriver().findElement(By.xpath(elementXath)).getText();
				Logging.info("attachment info " + attachmentInfo + " tool tip displayed as " + attachmentTitle);
			} else {
				Tools.setDriverDefaultValues();
				attachmentTitle = Base.base.getDriver().findElement(By.xpath(elementXath)).getAttribute("title");
				Logging.info("attachment info " + attachmentInfo + " tool tip displayed as " + attachmentTitle);
			}
		} catch (WebDriverException ex) {
			throw ex;
		}

		if (attachmentInfo.equals(ATTCHMENTSENDER)) {
			attachmentTitle = attachmentTitle.replaceAll("\r|\n", " ").split(" ")[1];
		} else if (attachmentInfo.equals(ATTCHMENTSUBJECT)) {
			attachmentTitle = attachmentTitle.replaceAll("\r|\n", " ").split(" ")[0];
		}
		return attachmentTitle;
	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 7, 2016<br>
	 * <b>Description</b><br>
	 * Click the thumbnail of the attachment in attachment view
	 * 
	 * @param emailSubjectWithAttachment
	 *            - the email subject of the attachment
	 * 
	 */
	public void clickAttachmentByThumbnail(String emailSubjectWithAttachment) {
		String attachment_view_thumbnail = PropertyHelper.coreEmailFile.getPropertyValue("attachment_view_thumbnail");
		attachment_view_thumbnail = String.format(attachment_view_thumbnail, emailSubjectWithAttachment);

		try {
			Tools.setDriverDefaultValues();
			Tools.moveFocusToElementByXpath(attachment_view_thumbnail);
			Base.base.getDriver().findElement(By.xpath(attachment_view_thumbnail)).click();

		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 7, 2016<br>
	 * <b>Description</b><br>
	 * Click close button in attachment preview view
	 * 
	 */
	public void clickAttachmentPreviewCloseButton() {
		String attachment_preview_close_button = PropertyHelper.coreEmailFile
				.getPropertyValue("attachment_preview_close_button");
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(attachment_preview_close_button)).click();

		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 7, 2016<br>
	 * <b>Description</b><br>
	 * Verify the attachment view preview header info by compared with sender and
	 * email subject
	 * 
	 * @param sender
	 *            - the email sender
	 * @param emailSubject
	 *            - the subject of the email
	 */
	public boolean verifyAttachmentHeaderPreviewInfo(String sender, String emailSubject) {
		String attachment_preview_header_sender = PropertyHelper.coreEmailFile
				.getPropertyValue("attachment_preview_header_sender");
		String attachment_preview_header_subject = PropertyHelper.coreEmailFile
				.getPropertyValue("attachment_preview_header_subject");

		boolean result = false;
		String attachmentHeaderText_sender = null;
		String attachmentHeaderText_subject = null;

		try {
			Tools.setDriverDefaultValues();
			attachmentHeaderText_sender = Base.base.getDriver().findElement(By.xpath(attachment_preview_header_sender))
					.getText();
			attachmentHeaderText_subject = Base.base.getDriver()
					.findElement(By.xpath(attachment_preview_header_subject)).getText();
		} catch (WebDriverException ex) {
			throw ex;
		}
		if (attachmentHeaderText_sender.contains(sender) && attachmentHeaderText_subject.contains(emailSubject))
			result = true;

		Logging.info("The attachment preview view info is as: " + attachmentHeaderText_sender + " "
				+ attachmentHeaderText_subject);
		return result;
	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 9, 2016<br>
	 * <b>Description</b><br>
	 * Verify if display is correct for the "preview view" of attachment view with
	 * different type
	 * 
	 * @param ifSupport
	 *            - if the attachment support the type , true support, false not
	 *            support
	 * @param warningText
	 *            - the warinings for the unsupported attachment file
	 * 
	 */
	public boolean verifyAttachmentPreviewSupportOrNot(boolean ifSupport, String warningText) {
		String attachment_preview_xath = null;
		boolean result = false;
		List<WebElement> headInfoElement;
		if (ifSupport)
			attachment_preview_xath = PropertyHelper.coreEmailFile.getPropertyValue("attachment_preview_support_type");
		else
			attachment_preview_xath = PropertyHelper.coreEmailFile
					.getPropertyValue("attachment_preview_not_support_type_text");
		try {
			Tools.setDriverDefaultValues();
			headInfoElement = Base.base.getDriver().findElements(By.xpath(attachment_preview_xath));
			if (headInfoElement.size() != 0)
				if (!ifSupport)
					result = headInfoElement.get(0).getText().equals(warningText);
				else
					result = true;
		} catch (WebDriverException ex) {
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 9, 2016<br>
	 * <b>Description</b><br>
	 * Verify different type of thumbnail icon
	 * 
	 * @param emailSubject
	 *            - attachment email subject
	 * @param expectedType
	 *            - the expected attachment type
	 */
	public boolean verifyAttachmentViewThumbnailType(String emailSubject, String expectedType) {
		String attachment_view_thumbnail_img = PropertyHelper.coreEmailFile
				.getPropertyValue("attachment_view_thumbnail_img");
		attachment_view_thumbnail_img = String.format(attachment_view_thumbnail_img, emailSubject);

		boolean result = false;
		String imgSrcLink = null;
		String srcUndecoded = null;
		try {
			Tools.setDriverDefaultValues();
			WebElement thumbnailImg = Base.base.getDriver().findElement(By.xpath(attachment_view_thumbnail_img));
			imgSrcLink = thumbnailImg.getAttribute("src");

			srcUndecoded = java.net.URLDecoder.decode(imgSrcLink, "UTF-8");

		} catch (WebDriverException | UnsupportedEncodingException ex) {
			ex.printStackTrace();
		}
		srcUndecoded = srcUndecoded.substring(imgSrcLink.lastIndexOf("/") + 1);
		Logging.info("Got thumbail of image of mail" + emailSubject + " and result is " + srcUndecoded);
		Logging.info("Expected image type is " + expectedType);
		result = srcUndecoded.contains(expectedType);
		return result;
	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 14, 2016<br>
	 * <b>Description</b><br>
	 * Verify if the photo displayed correct
	 * 
	 * @return true - the photo displayed with user defined photo, false - the photo
	 *         displayed by default pic
	 */
	public boolean verifyPhotoDisplayInMailDetail() {
		boolean result = false;
		String email_photo_displayed = PropertyHelper.coreEmailFile.getPropertyValue("email_photo_displayed");
		try {
			Tools.setDriverDefaultValues();
			String elementSrc = Base.base.getDriver().findElement(By.xpath(email_photo_displayed)).getAttribute("src");
			if (elementSrc.startsWith("http") && elementSrc.contains("%"))
				result = true;
			else if (elementSrc.contains("resources") && elementSrc.contains("icon_blank_contact"))
				result = false;
		} catch (WebDriverException ex) {
			throw ex;
		}
		return result;

	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 15, 2016<br>
	 * <b>Description</b><br>
	 * Verify if the attachment download URL sucessfully
	 * 
	 * @param folder
	 *            - the folder name of the attachment belongs to
	 * @param attachmentPart
	 *            - the attachment part, part 1, part 2 indicates the number of the
	 *            attachment
	 * 
	 * @return true - the attachment contains the url of the folder and attachment
	 *         part and verify pass, false - not contain and verify failed
	 */
	public boolean verifyAttachmentDownloadURL(String folder, String attachmentPart) {
		String attachment_view_get_download_url_js = PropertyHelper.coreEmailFile
				.getPropertyValue("attachment_view_get_download_url_js");
		boolean result = false;
		try {
			JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
			String tmpResult = (String) js.executeScript(attachment_view_get_download_url_js);
			if (tmpResult.contains(folder) && tmpResult.contains(attachmentPart))
				result = true;
		} catch (WebDriverException ex) {
			throw ex;
		}
		Logging.info("Attachment download url is sucessfull? " + result);
		return result;
	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 18, 2016<br>
	 * <b>Description</b><br>
	 * Get attachment count in attachment view
	 * 
	 */
	public int getAttachmentCountInAttachmentView() {
		int result = 0;
		String attachment_view_attachment_items = PropertyHelper.coreEmailFile
				.getPropertyValue("attachment_view_attachment_items");

		try {
			Tools.setDriverDefaultValues();
			List<WebElement> attachmentCount = Base.base.getDriver()
					.findElements(By.xpath(attachment_view_attachment_items));
			result = attachmentCount.size();
		} catch (WebDriverException ex) {
			throw ex;
		}
		Logging.info("Get attachment count in attachment view " + result);
		return result;
	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 18, 2016<br>
	 * <b>Description</b><br>
	 * verify attachment page button is dsiabled or enabled
	 * 
	 * @param pageButton
	 *            - the page button
	 * 
	 * @return true - the button is enabled, false - the button is disabled
	 */
	public boolean verifyAttachmentPaging(String pageButton) {
		String pagingButton = null;
		String buttonDisabledOrNot = null;
		boolean result = true;

		switch (pageButton) {
		case FIRSTPAGE:
			pagingButton = PropertyHelper.coreEmailFile.getPropertyValue("attachment_view_paging_first_page");
			break;
		case LASTPAGE:
			pagingButton = PropertyHelper.coreEmailFile.getPropertyValue("attachment_view_paging_last_page");
			break;
		case NEXTPAGE:
			pagingButton = PropertyHelper.coreEmailFile.getPropertyValue("attachment_view_paging_next_page");
			break;
		case PREPAGE:
			pagingButton = PropertyHelper.coreEmailFile.getPropertyValue("attachment_view_paging_previous_page");
			break;

		}

		try {
			Tools.setDriverDefaultValues();
			WebElement pagingButtonElement = Base.base.getDriver().findElement(By.xpath(pagingButton));
			buttonDisabledOrNot = pagingButtonElement.getAttribute("class");

			if (buttonDisabledOrNot.contains("disabled"))
				result = false;
		} catch (WebDriverException ex) {
			throw ex;
		}
		Logging.info("The page element " + pageButton + " is enable or not ? " + result);
		return result;

	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 18, 2016<br>
	 * <b>Description</b><br>
	 * Click the page button in attachment view
	 * 
	 * @param pageButton
	 *            - the page button, value could be firstpage, lastpage,
	 *            previouspage,nextpage
	 */
	public void clickAttachmentPagingButton(String pageButton) {
		String pagingButton = null;
		String pagingButton_xpath = null;
		boolean pageNumber = false;
		switch (pageButton) {
		case FIRSTPAGE:
			pagingButton = "attachment_view_paging_first_page";
			break;
		case LASTPAGE:
			pagingButton = "attachment_view_paging_last_page";
			break;
		case NEXTPAGE:
			pagingButton = "attachment_view_paging_next_page";
			break;
		case PREPAGE:
			pagingButton = "attachment_view_paging_previous_page";
			break;
		default: {
			pagingButton = "attachment_view_paging_with_number";
			pageNumber = true;
			break;
		}
		}
		pagingButton_xpath = PropertyHelper.coreEmailFile.getPropertyValue(pagingButton);
		String attachment_view_paging_link = PropertyHelper.coreEmailFile
				.getPropertyValue("attachment_view_paging_link");
		if (pageNumber)
			pagingButton_xpath = String.format(pagingButton_xpath, pageButton);
		pagingButton_xpath = pagingButton_xpath + attachment_view_paging_link;
		try {
			Tools.setDriverDefaultValues();
			Tools.clickOnElementByXpath(pagingButton_xpath);
			Tools.waitFor(5, TimeUnit.SECONDS);

		} catch (WebDriverException ex) {
			throw ex;
		}
		Logging.info("Click the page button " + pageButton);
	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 21, 2016<br>
	 * <b>Description</b><br>
	 * Select multiple attachment by press shift on keyboard
	 * 
	 * @param startNumber
	 *            - The number for start with, if start number is bigger than whole
	 *            size, it will start from 0
	 * @param endNumber
	 *            - The number for end with, if the end number is bigger than whole
	 *            size, it will end with the last attachment
	 */
	public void selectMultipleAttachmentByShift(int startNumber, int endNumber) {
		String attachment_view_attachment_size_items = PropertyHelper.coreEmailFile
				.getPropertyValue("attachment_view_attachment_size_items");
		try {
			Tools.setDriverDefaultValues();
			List<WebElement> attachmentCount = Base.base.getDriver()
					.findElements(By.xpath(attachment_view_attachment_size_items));
			// if the start number is larger than total size, will select from 0
			if (startNumber > attachmentCount.size())
				attachmentCount.get(0).click();
			else
				attachmentCount.get(startNumber - 1).click();

			// if the end number is larger than total size, will select from the last one
			if (endNumber > attachmentCount.size())
				new Actions(Base.base.getDriver()).keyDown(Keys.SHIFT)
						.click(attachmentCount.get(attachmentCount.size() - 1)).keyUp(Keys.SHIFT).build().perform();
			else
				new Actions(Base.base.getDriver()).keyDown(Keys.SHIFT).click(attachmentCount.get(endNumber - 1))
						.keyUp(Keys.SHIFT).build().perform();

			Tools.waitFor(10, TimeUnit.SECONDS);
			Logging.info("Select attachment from " + startNumber + " to " + endNumber);
		} catch (WebDriverException ex) {
			throw ex;
		}

	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 21, 2016<br>
	 * <b>Description</b><br>
	 * To verify the number of selected attachments
	 * 
	 * @param expectedNumber
	 *            - the numbers for expected attachment that is selected
	 */
	public boolean verifyAttachmentIsSelected(int expectedNumber) {
		int numbersForSelectedAttachment = 0;
		boolean result = false;
		String attachment_view_attachment_items = PropertyHelper.coreEmailFile
				.getPropertyValue("attachment_view_attachment_items");
		try {
			List<WebElement> attachmentCount = Base.base.getDriver()
					.findElements(By.xpath(attachment_view_attachment_items));

			for (WebElement el : attachmentCount)

				if (el.getAttribute("class").contains("selected")) {
					numbersForSelectedAttachment++;
				}
		} catch (WebDriverException ex) {
			throw ex;
		}
		Logging.info("Numbers for attachment is selected is : " + numbersForSelectedAttachment);
		if (expectedNumber == numbersForSelectedAttachment)
			result = true;
		Logging.info("The verify for attachment is selected result is " + result);
		return result;

	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 21, 2016<br>
	 * <b>Description</b><br>
	 * Select multiple attachment by press ctrl on keyboard
	 * 
	 * @param selectAttachment
	 *            - the array for which attachment will be selected
	 * 
	 */
	public void selectMultipleAttachmentByCtrl(int[] selectAttachment) {
		String attachment_view_attachment_size_items = PropertyHelper.coreEmailFile
				.getPropertyValue("attachment_view_attachment_size_items");
		try {
			Tools.setDriverDefaultValues();
			List<WebElement> attachmentCount = Base.base.getDriver()
					.findElements(By.xpath(attachment_view_attachment_size_items));
			Arrays.sort(selectAttachment);
			if (selectAttachment.length > attachmentCount.size()
					|| selectAttachment[selectAttachment.length - 1] > attachmentCount.size())
				throw new ArrayIndexOutOfBoundsException();

			attachmentCount.get(selectAttachment[0] - 1).click();
			new Actions(Base.base.getDriver()).keyDown(Keys.CONTROL).build().perform();

			for (int i = 1; i < selectAttachment.length; i++) {
				attachmentCount.get(selectAttachment[i] - 1).click();
				Logging.info("Select the attachment of " + selectAttachment[i]);
			}
			new Actions(Base.base.getDriver()).keyUp(Keys.CONTROL).build().perform();
			Tools.waitFor(10, TimeUnit.SECONDS);
		} catch (WebDriverException ex) {
			throw ex;
		}

	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 22, 2016<br>
	 * <b>Description</b><br>
	 * click the attachment view sort by button
	 * 
	 * @param sortAccordance
	 *            - the sort method
	 * 
	 */
	public void clickAttachmentViewSortBy(String sortAccordance) {
		String attachment_view_sort_by_button = PropertyHelper.coreEmailFile
				.getPropertyValue("attachment_view_sort_by_button");

		String attachment_view_sort_accordance = PropertyHelper.coreEmailFile
				.getPropertyValue("attachment_view_sort_accordance");

		attachment_view_sort_accordance = String.format(attachment_view_sort_accordance, sortAccordance);
		Logging.info(attachment_view_sort_accordance);
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(attachment_view_sort_by_button)).click();
			Tools.waitUntilElementDisplayedByXpath(attachment_view_sort_accordance, 10);
			Tools.moveFocusToElementByXpath(attachment_view_sort_accordance);
			Tools.waitFor(5, TimeUnit.SECONDS);
			Tools.waitUntilElementDisplayedByXpath(attachment_view_sort_accordance, 10);
			Tools.clickOnElementByXpath(attachment_view_sort_accordance);

		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 23, 2016<br>
	 * <b>Description</b><br>
	 * Get the current attachment info list
	 * 
	 * @param attachmentInfoSection
	 *            - the attachment info section, like size, subject, received
	 *            datetime
	 * @return return the attachment info list collection according to the parameter
	 */
	public ArrayList<String> getCurrentAttachmentInfoList(String attachmentInfoSection) {
		String elementxpath = null;
		switch (attachmentInfoSection) {
		case ATTCHMENTSUBJECT:
			elementxpath = "attachment_view_attachment_list_by_name";
			break;
		case ATTCHMENTTIME:
			elementxpath = "attachment_view_date";
			break;
		case ATTCHMENTSIZE:
			elementxpath = "attachment_view_attachment_list_by_size";
			break;
		}
		elementxpath = PropertyHelper.coreEmailFile.getPropertyValue(elementxpath);
		ArrayList<String> result = new ArrayList<String>();
		try {
			Tools.setDriverDefaultValues();
			this.waitForLoadMaskDismissed();
			if (attachmentInfoSection.equals(ATTCHMENTTIME)) {

				List<WebElement> elements = Base.base.getDriver().findElements(By.xpath(elementxpath));
				ArrayList<String> getDateTxt = new ArrayList<String>();
				for (WebElement el : elements) {

					getDateTxt.add(el.getText().replaceAll("\r|\n", " ").split(" ")[2] + " "
							+ el.getText().replaceAll("\r|\n", " ").split(" ")[3]);

				}
				result.addAll(getDateTxt);

			} else if (attachmentInfoSection.equals(ATTCHMENTSIZE)) {
				List<WebElement> elements = Base.base.getDriver().findElements(By.xpath(elementxpath));
				ArrayList<String> getDateTxt = new ArrayList<String>();
				for (WebElement el : elements) {
					getDateTxt.add(el.getText().replaceAll(" ", ""));

				}
				result.addAll(getDateTxt);
			}

			else {
				List<WebElement> elements = Base.base.getDriver().findElements(By.xpath(elementxpath));
				for (WebElement el : elements) {
					result.add(el.getText());
				}
			}

		} catch (WebDriverException ex) {
			throw ex;
		}
		Logging.info("get current attachment list " + attachmentInfoSection + " : " + result.toString());

		return result;
	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 24, 2016<br>
	 * <b>Description</b><br>
	 * Verify if the attachment is sorted by alpha beta sort
	 * 
	 * @param timeSortAscc
	 *            - the result of name list
	 * @param ifDesc
	 *            - if the order is desc
	 */
	public boolean verifyAttachmentSortByAlpha(ArrayList<String> tmpResult, boolean ifDesc) {
		boolean result = false;

		// revert array list to lower case
		tmpResult = (ArrayList<String>) tmpResult.stream().map(String::toLowerCase).collect(Collectors.toList());

		ArrayList<String> calculateResult = new ArrayList<String>();
		calculateResult.addAll(tmpResult);

		// array sort by case insensitive order
		Collections.sort(calculateResult, String.CASE_INSENSITIVE_ORDER);

		if (ifDesc) {
			// verify if the two result equals or not
			Logging.info("sort result" + calculateResult.toString());
			result = tmpResult.equals(calculateResult);
		} else {
			// if sort by desc, reverse the sort result, then compare with result
			Collections.reverse(calculateResult);
			Logging.info(tmpResult.toString());
			result = tmpResult.equals(calculateResult);
		}

		Logging.info("verify attachment sort by alpha result is : " + result);
		return result;

	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 25, 2016<br>
	 * <b>Description</b><br>
	 * Verify if the attachment is sorted by time, convert the string to
	 * Localdatetime
	 * 
	 * @param timeSortAscc
	 *            - the result of time list
	 * @param dateFormatPattern
	 *            - the date format pattern
	 * @param ifDesc
	 *            - if the order is desc
	 */
	public boolean verifyAttachmentSortByTime(ArrayList<String> timeSortAscc, String dateFormatPattern,
			boolean ifDesc) {
		boolean result = false;
		ArrayList<LocalDateTime> attachmentLocalDateTime = new ArrayList<LocalDateTime>();

		// date format all Strings to date time
		DateTimeFormatter dfm = DateTimeFormatter.ofPattern(dateFormatPattern, Locale.ENGLISH);

		for (int i = 0; i < timeSortAscc.size(); i++) {
			attachmentLocalDateTime.add(LocalDateTime.parse(timeSortAscc.get(i), dfm));
		}
		ArrayList<LocalDateTime> attachmentLocalDateTimeTmp = new ArrayList<LocalDateTime>();
		attachmentLocalDateTimeTmp.addAll(attachmentLocalDateTime);

		// sort date time
		Collections.sort(attachmentLocalDateTime);

		if (ifDesc) {
			Logging.info(attachmentLocalDateTime.toString());
			result = attachmentLocalDateTimeTmp.equals(attachmentLocalDateTime);
		} else {
			Collections.reverse(attachmentLocalDateTime);
			Logging.info(attachmentLocalDateTime.toString());
			Logging.info(attachmentLocalDateTimeTmp.toString());
			result = attachmentLocalDateTimeTmp.equals(attachmentLocalDateTime);

		}
		return result;

	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 25, 2016<br>
	 * <b>Description</b><br>
	 * Verify if the attachment is sorted by size, using the SizeComparable class
	 * 
	 * @param sizeSortAscc
	 *            - the result of size list
	 * @param ifDesc
	 *            - if the order is desc
	 */
	public boolean verifyAttachmentSortBySize(ArrayList<String> sizeSortAscc, boolean ifDesc) {
		boolean result = false;

		ArrayList<String> attachmentSizeSortListTmp = new ArrayList<String>();
		attachmentSizeSortListTmp.addAll(sizeSortAscc);

		// sort date time
		Collections.sort(sizeSortAscc, new SizeComparator());

		if (ifDesc) {
			Logging.info(sizeSortAscc.toString());
			result = attachmentSizeSortListTmp.equals(sizeSortAscc);
		} else {
			Collections.reverse(sizeSortAscc);
			Logging.info(sizeSortAscc.toString());
			Logging.info(attachmentSizeSortListTmp.toString());
			result = attachmentSizeSortListTmp.equals(sizeSortAscc);

		}

		return result;

	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 22, 2016<br>
	 * <b>Description</b><br>
	 * click the attachment view filter by button
	 * 
	 * @param filterAccordance
	 *            - the filter method
	 * 
	 */
	public void clickAttachmentViewFilterBy(String filterAccordance) {
		String attachment_view_filter_by_button = PropertyHelper.coreEmailFile
				.getPropertyValue("attachment_view_filter_by_button");

		String attachment_view_filter_accordance = PropertyHelper.coreEmailFile
				.getPropertyValue("attachment_view_filter_accordance");

		attachment_view_filter_accordance = String.format(attachment_view_filter_accordance, filterAccordance);
		Logging.info(attachment_view_filter_accordance);
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(attachment_view_filter_by_button)).click();
			Tools.waitUntilElementDisplayedByXpath(attachment_view_filter_accordance, 10);
			Tools.moveFocusToElementByXpath(attachment_view_filter_accordance);
			Tools.waitFor(5, TimeUnit.SECONDS);
			Tools.waitUntilElementDisplayedByXpath(attachment_view_filter_accordance, 10);
			Tools.clickOnElementByXpath(attachment_view_filter_accordance);

		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 25, 2016<br>
	 * <b>Description</b><br>
	 * Verify if the attachment filters correctly
	 * 
	 * @param calculateAttachmentNames
	 *            - the calculated attachment name
	 * @param currentAttachmentList
	 *            - if the order is desc
	 */
	public boolean verifyAttachmentFilter(ArrayList<String> calculateAttachmentNames,
			ArrayList<String> currentAttachmentList) {
		boolean result = false;

		Collections.sort(calculateAttachmentNames);
		Collections.sort(currentAttachmentList);
		Logging.info(calculateAttachmentNames.toString());
		Logging.info(currentAttachmentList.toString());
		result = currentAttachmentList.equals(calculateAttachmentNames);

		return result;

	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 25, 2016<br>
	 * <b>Description</b><br>
	 * Get collection of a classified attachment type
	 * 
	 * @param allAttachmentNames
	 *            all attachment name list
	 * 
	 * @return the collection of hashmap, the keyword is set by filter option regex
	 *         is from: http://www
	 *         .mkyong.com/regular-expressions/how-to-validate-image-file-extension-with-regular
	 *         -expression/
	 */
	public HashMap<String, ArrayList<String>> filterAttachmentsAndGetResult(ArrayList<String> allAttachmentNames) {
		// the regrex for matches the postfix of the attachment list
		String imageRegex = "([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)";
		String documentRegex = "([^\\s]+(\\.(?i)(doc|docx|xlsx|xls|ppt|pptx))$)";
		String audioRegex = "([^\\s]+(\\.(?i)(mp3))$)";
		String videoRegex = "([^\\s]+(\\.(?i)(mp4))$)";
		String archiveRegex = "([^\\s]+(\\.(?i)(zip))$)";

		ArrayList<String> imageResult = new ArrayList<String>();
		ArrayList<String> archiveResult = new ArrayList<String>();
		ArrayList<String> docResult = new ArrayList<String>();
		ArrayList<String> videoResult = new ArrayList<String>();
		ArrayList<String> audioResult = new ArrayList<String>();
		HashMap<String, ArrayList<String>> result = new HashMap<String, ArrayList<String>>();

		for (String s : allAttachmentNames) {
			Logging.info("The string is " + s);
			// need to split out the white space, some attachment name may have
			if (s.replaceAll(" ", "").matches(imageRegex))
				imageResult.add(s);
			else if (s.replaceAll(" ", "").matches(archiveRegex))
				archiveResult.add(s);
			else if (s.replaceAll(" ", "").matches(documentRegex))
				docResult.add(s);
			else if (s.replaceAll(" ", "").matches(videoRegex))
				videoResult.add(s);
			else if (s.replaceAll(" ", "").matches(audioRegex))
				audioResult.add(s);
		}

		// put the result into the hash map
		result.put(IMAGES, imageResult);
		result.put(DOCUMENT, docResult);
		result.put(AUDIOS, audioResult);
		result.put(VIDEOS, videoResult);
		result.put(ARCHIVES, archiveResult);

		return result;

	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 28, 2016<br>
	 * <b>Description</b><br>
	 * Zoom in / Zoom out in attachment view
	 * 
	 * <b>Parameters</b><br>
	 * String zoomSize - zoom in / zoom out
	 */
	public void moveAttachmentViewZoomActionButton(String zoomSize) {
		Logging.info(zoomSize + " the attachment. ");
		String attachment_view_zoom_button = PropertyHelper.coreEmailFile
				.getPropertyValue("attachment_view_zoom_button");
		try {
			Tools.setDriverDefaultValues();
			if (zoomSize.equalsIgnoreCase("zoomout"))
				Tools.dragAndDropSourceToTargetByXpath(attachment_view_zoom_button, 2000, 0);
			else if (zoomSize.equalsIgnoreCase("zoomin"))
				Tools.dragAndDropSourceToTargetByXpath(attachment_view_zoom_button, -2000, 0);
		} catch (WebDriverException ex) {
			throw ex;
		}

	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Nov 28, 2016<br>
	 * <b>Description</b><br>
	 * Verify the Zoom in / Zoom out attachment view size and controller
	 * 
	 * <b>Parameters</b><br>
	 * String zoomSize - zoom in / zoom out
	 */
	public boolean verifyTheAttachmentViewChangesSize(String zoomSize) {
		boolean result = false;
		String attachment_view_zoom_track = PropertyHelper.coreEmailFile.getPropertyValue("attachment_view_zoom_track");
		String attachment_view_zoom_controller = PropertyHelper.coreEmailFile
				.getPropertyValue("attachment_view_zoom_controller");

		try {
			Tools.setDriverDefaultValues();
			String sizeChanges = Base.base.getDriver().findElement(By.xpath(attachment_view_zoom_track))
					.getAttribute("style");
			String controllerPosition = Base.base.getDriver().findElement(By.xpath(attachment_view_zoom_controller))
					.getAttribute("style");

			if (zoomSize.equalsIgnoreCase("zoomout"))
				result = sizeChanges.contains("width: 102.564%") && controllerPosition.contains("left: 102.564%");
			else if (zoomSize.equalsIgnoreCase("zoomin"))
				result = sizeChanges.contains("width: 0%") && controllerPosition.contains("left: 0%");
		} catch (NoSuchElementException ex) {
			ex.printStackTrace();
			throw ex;
		}
		Logging.info("Verifying if the attachment view zoom in zoom out to " + zoomSize + " sucessfully " + result);
		return result;
	}

	/**
	 * <b>Author</b>: Vivian Xie <br>
	 * <b>Date</b>: Nov 24, 2016<br>
	 * <b>Description</b><br>
	 * verify the mail subject displayed as expected on preview view
	 * 
	 * @param expectMailSubject
	 *            - the expected mail subject
	 * 
	 */
	public boolean verifyMailSubjectInPreviewView(String expectMailSubject) {
		Logging.info("Starting to verifing preview mail subject");
		String mail_preview_subject = PropertyHelper.coreEmailFile.getPropertyValue("mail_preview_subject");
		String mailSubject = null;
		boolean result = false;
		try {
			Tools.waitUntilElementDisplayedByXpath(mail_preview_subject, 30);
			mailSubject = Base.base.getDriver().findElement(By.xpath(mail_preview_subject)).getText();
			Logging.info(mailSubject);
		} catch (WebDriverException ex) {
			Logging.info("Could not find mail subject in preview");
			throw ex;
		}
		if (expectMailSubject.trim().equals(mailSubject.trim())) {
			result = true;
		}
		return result;

	}

	/**
	 * <b>Author</b>: Vivian Xie <br>
	 * <b>Date</b>: Nov 24, 2016<br>
	 * <b>Description</b><br>
	 * verify the mail body displayed as expected on preview view
	 * 
	 * @param expectMailBody
	 *            - the expected mail body
	 * 
	 */
	public boolean verifyMailBodyInPreviewView(String expectMailBody) {
		Logging.info("Starting to verify mail body in preview view");
		String mail_preview_msgbody = PropertyHelper.coreEmailFile.getPropertyValue("mail_preview_msgbody");
		String msgbody = null;
		boolean result = false;
		try {
			msgbody = Base.base.getDriver().findElement(By.xpath(mail_preview_msgbody)).getAttribute("innerHTML");
		} catch (WebDriverException ex) {
			Logging.info("Could not find mail body in preview");
			throw ex;
		}
		if (msgbody.trim().contains(expectMailBody.trim())) {
			result = true;
		}
		return result;

	}

	/**
	 * <b>Author</b>: Vivian Xie <br>
	 * <b>Date</b>: Nov 24, 2016<br>
	 * <b>Description</b><br>
	 * verify the Edit button displayed on mail preview view
	 * 
	 * @return true--Edit button is displayed false--Edit button is not displayed
	 * 
	 */
	public boolean verifyEditButtonDisplayOnDraftPreviewView() {
		Logging.info("Starting to verify if Edit button exists on draft preview view");
		String draft_edit_button = PropertyHelper.coreEmailFile.getPropertyValue("draft_edit_button");
		boolean result = false;
		try {
			Tools.waitUntilElementDisplayedByXpath(draft_edit_button, 3);
			result = true;
		} catch (WebDriverException ex) {
			Logging.info("Could not find Edit button in preview view ");
			throw ex;
		}
		return result;
	}

	/**
	 * <b>Author</b>: Vivian Xie <br>
	 * <b>Date</b>: Nov 24, 2016<br>
	 * <b>Description</b><br>
	 * click the Edit button on mail preview view
	 * 
	 */
	public void clickEditButtonOnDraftPreviewView() {
		Logging.info("Starting to click Edit button on draft preview view");
		String draft_edit_button = PropertyHelper.coreEmailFile.getPropertyValue("draft_edit_button");
		try {
			Tools.waitUntilElementDisplayedByXpath(draft_edit_button, 3);
			Base.base.getDriver().findElement(By.xpath(draft_edit_button)).click();
		} catch (WebDriverException ex) {
			Logging.info("Could not find Edit button in preview view ");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Vivian Xie <br>
	 * <b>Date</b>: Nov 24, 2016<br>
	 * <b>Description</b><br>
	 * click external account name link on the left panel of mail page
	 * <b>parameter:</b>name -- the name of the external account
	 */
	public void clickExternalAccout(String name) {

		Logging.info("Starting to click external account link:" + name);
		String email_external_account_link = PropertyHelper.coreEmailFile
				.getPropertyValue("email_external_account_link").replace("+variable+", name);
		try {
			Tools.waitUntilElementDisplayedByXpath(email_external_account_link, 3);
			Base.base.getDriver().findElement(By.xpath(email_external_account_link)).click();
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.info("Could not click external account link:" + name);
			throw ex;
		}

	}

	/**
	 * @Method Name : waitForExternalAccountFolderListToBeLoad
	 * @author : Vivian Xie
	 * @Created on : 12/1/2016
	 * @Description : This method is used to make the automation wait until the
	 *              external account folder list has been completely loaded
	 * @Parameter 1: int timeoutInSeconds - value specified in seconds to wait
	 */
	public void waitForExternalAccountFolderListToBeLoad(String externalAccountName, int timeoutInSeconds) {
		Logging.info("Waiting for external account folder list to load, timeout seconds: " + timeoutInSeconds);
		// Verify login page loaded then change language.
		String[] folders = { "inbox", "draft", "sent", "junk", "trash" };
		String[] folderXpath = new String[5];
		for (int i = 0; i < folders.length; i++) {
			folderXpath[i] = PropertyHelper.coreEmailFile.getPropertyValue("email_external_account_folder")
					.replace("+variable1+", externalAccountName).replace("+variable2+", folders[i]);
			Logging.info(folderXpath[i]);
		}
		try {
			for (int i = 0; i < folderXpath.length; i++) {
				Tools.waitUntilElementDisplayedByXpath(folderXpath[i], timeoutInSeconds);
			}
		} catch (WebDriverException ex) {
			Logging.info("Could not load folder list.");
			throw ex;
		}

	}

	/**
	 * @Method Name : verifyExternalAccountIsExpand
	 * @author : Vivian Xie
	 * @Created on : 12/1/2016
	 * @Description : verify if the specific external account is expanded.
	 * @Parameter : String externalAccountName -- name
	 */
	public boolean verifyExternalAccountIsExpand(String externalAccountName) {
		Logging.info("Verifying if external account " + externalAccountName + "is expand ");
		String email_external_account_head_panel = PropertyHelper.coreEmailFile
				.getPropertyValue("email_external_account_head_panel").replace("+variable+", externalAccountName);
		String headPanelClass = null;
		boolean result = true;
		try {
			headPanelClass = Base.base.getDriver().findElement(By.xpath(email_external_account_head_panel))
					.getAttribute("class");
			Logging.info(headPanelClass);
		} catch (WebDriverException ex) {
			Logging.info("Could not click external account link:" + externalAccountName);
			throw ex;
		}
		if (headPanelClass.contains("collapsed"))
			result = false;

		return result;
	}

	/**
	 * @Method Name : clickOnExternalAccountSystenFolder
	 * @author : Vivian Xie
	 * @Created on : 12/1/2016
	 * @Description : Click on the system folder of external account .
	 * @Parameter : externalAccountName -- account name folderName--folder name
	 */
	public void clickOnExternalAccountSystenFolder(String externalAccountName, String folderName) {
		Logging.info("Clicking external account folder " + folderName);
		String folder = PropertyHelper.coreEmailFile.getPropertyValue("email_external_account_folder")
				.replace("+variable1+", externalAccountName).replace("+variable2+", folderName);
		try {
			Base.base.getDriver().findElement(By.xpath(folder)).click();
		} catch (WebDriverException ex) {
			Logging.info("Could not click external account fold" + folderName);
			throw ex;
		}

	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Dec 2, 2016<br>
	 * <b>Description</b><br>
	 * Select attachment by name
	 * 
	 * @param attachmentName
	 * 
	 */
	public void selectAttachmentByName(String[] attachmentName) {
		String xpath = null;
		Logging.info("length is : " + attachmentName.length);
		String attachment_view_attachment_single_select_by_size = PropertyHelper.coreEmailFile
				.getPropertyValue("attachment_select_byname");
		xpath = String.format(attachment_view_attachment_single_select_by_size, attachmentName[0]);

		try {

			if (attachmentName.length > 1) {
				Logging.info("Start to select multiple attachment");
				new Actions(Base.base.getDriver()).keyDown(Keys.CONTROL).build().perform();
				for (int i = 0; i <= attachmentName.length - 1; i++) {
					xpath = String.format(attachment_view_attachment_single_select_by_size, attachmentName[i]);
					Logging.info("xpath value is " + xpath);
					Tools.setDriverDefaultValues();
					Base.base.getDriver().findElement(By.xpath(xpath)).click();
				}
				new Actions(Base.base.getDriver()).keyUp(Keys.CONTROL).build().perform();
			} else {
				Tools.setDriverDefaultValues();
				Base.base.getDriver().findElement(By.xpath(xpath)).click();
			}

		} catch (NoSuchElementException ex) {
			ex.printStackTrace();
			throw ex;
		}

	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Dec 2, 2016<br>
	 * <b>Description</b><br>
	 * Verify the "No Attachment Found" is displayed or not
	 * 
	 * @return true - the list is empty, false -the list is not empty
	 * 
	 */
	public boolean verifyIfAttachmentViewEmpty() {
		boolean result = false;
		String attachment_view_no_item = PropertyHelper.coreEmailFile.getPropertyValue("attachment_view_no_item");

		try {
			Tools.setDriverDefaultValues();
			List<WebElement> attachmentCount = Base.base.getDriver().findElements(By.xpath(attachment_view_no_item));
			if (attachmentCount.size() == 1)
				result = true;
		} catch (WebDriverException ex) {
			throw ex;
		}
		Logging.info("The attachment view is empty " + result);

		return result;
	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Dec 2, 2016<br>
	 * <b>Description</b><br>
	 * Verify if the current attachemnt list is equals to the expected attachment
	 * list without sort
	 * 
	 * @param currentAttachmentList
	 * @param expectedAttachmentList
	 * 
	 * @return true - the 2 lists are equals, false -the 2 lists are not equals
	 * 
	 */
	public boolean verifyIfAttachmentNameListWithoutSortEquals(ArrayList<String> currentAttachmentList,
			String[] expectedAttachmentList) {
		boolean result = false;
		List<String> compareAttachmentList = new ArrayList<String>();
		compareAttachmentList = Arrays.asList(expectedAttachmentList);
		Collections.sort(compareAttachmentList);
		Collections.sort(currentAttachmentList);

		Logging.info("expected result: " + compareAttachmentList);
		Logging.info("current result: " + currentAttachmentList);

		if (compareAttachmentList.equals(currentAttachmentList))
			result = true;

		return result;

	}

	/**
	 * <b>Author</b>: Fiona Zhang <br>
	 * <b>Date</b>: Dec 5, 2016<br>
	 * <b>Description</b><br>
	 * Verify if the tool bar button is disabled or not
	 * 
	 * @param buttonName
	 *            the button name
	 * 
	 * @return true - the button is enabled, false, the button is disabled
	 * 
	 */
	public boolean verifyIfEmailToolBarButtonIsEnabled(String buttonName) {
		boolean result = false;

		String xpathOfFlag = null;
		switch (buttonName) {
		case "Flag for follow-up":
			xpathOfFlag = "attachment_view_tool_bar_flag_button";
			break;
		case "Clear flag":
			xpathOfFlag = "attachment_view_tool_bar_unflag_button";
			break;
		case "Mark as unread":
			xpathOfFlag = "attachment_view_tool_bar_mark_as_read_button";
			break;
		case "Mark as read":
			xpathOfFlag = "attachment_view_tool_bar_mark_as_unread_button";
			break;
		}
		xpathOfFlag = PropertyHelper.coreEmailFile.getPropertyValue(xpathOfFlag);
		try {
			WebElement flagElement = Base.base.getDriver().findElement(By.xpath(xpathOfFlag));
			if (!flagElement.getAttribute("class").contains("disabled"))
				result = true;
		} catch (WebDriverException ex) {
			throw ex;
		}
		Logging.info("verify if the email tool bar flag button : " + buttonName + " is enabled result is " + result);
		return result;
	}

	/**
	 * @Method Name: verifyLocalImageExistsOnPreviewView
	 * @Author: Vivian Xie
	 * @Created On: Dec/30/2016
	 * @Description: This method will verify if local image display correct on
	 *               preview view.
	 */
	public boolean verifyLocalImageExistsOnPreviewView() {
		Logging.info("Verifying if local image exist on preview view");
		boolean result = false;
		String mail_preview_local_image = PropertyHelper.coreEmailFile.getPropertyValue("mail_preview_local_image");
		ArrayList<WebElement> list = null;
		try {
			list = (ArrayList<WebElement>) Base.base.getDriver().findElements(By.xpath(mail_preview_local_image));
		} catch (NoSuchElementException ex) {
			Logging.error("Webdriver error when finding local image");
		}
		if (list.size() > 0)
			result = true;
		return result;
	}

	/**
	 * Remove all the emails in the current folder
	 * 
	 * @Author: Vivian Xie
	 * @Created On: Feb/28/2017
	 */
	public void removeAllTheEmails() {

		Logging.info("Deleting all the emails");
		String email_list_message_checkbox = PropertyHelper.coreEmailFile
				.getPropertyValue("email_list_message_checkbox");
		ArrayList<WebElement> list = null;

		try {
			list = (ArrayList<WebElement>) Base.base.getDriver().findElements(By.xpath(email_list_message_checkbox));
			while (list.size() > 0) {
				checkSelectAllEmailBox(true);
				clickEmailToolBarButton("Delete");
				this.waitForLoadMaskDismissed();
				this.clickButtonOnMessageBoxIfDisplayed("Yes");
				this.waitForLoadMaskDismissed();
				list = (ArrayList<WebElement>) Base.base.getDriver()
						.findElements(By.xpath(email_list_message_checkbox));
				Tools.waitFor(2, TimeUnit.SECONDS);
			}

		} catch (NoSuchElementException ex) {
			Logging.error("Webdriver error when deleting all the emails.");
		}
	}

	/**
	 * check the select all email checkbox
	 * 
	 * @Author Vivian Xie
	 * @param checked
	 *            -- true means that the action will check the checkbox; else
	 *            uncheck the checkbox
	 * @Created Mar/01/2017
	 */
	public void checkSelectAllEmailBox(boolean checked) {

		Logging.info("selecting all the emails");
		boolean isChecked = false;
		String email_all_email_checkBox_table = PropertyHelper.coreEmailFile
				.getPropertyValue("email_all_email_checkBox_table");
		String email_all_email_checkBox = PropertyHelper.coreEmailFile.getPropertyValue("email_all_email_checkBox");

		// in case the selenium is failed to check the checkbox, there is a loop (5
		// times, any value can be OK)
		for (int i = 0; i < 5; i++) {
			try {
				isChecked = Base.base.getDriver().findElement(By.xpath(email_all_email_checkBox_table))
						.getAttribute("class").contains("checked");

			} catch (WebDriverException ex) {
				Logging.error("Failed to get checkbox status");
			}
			if (isChecked != checked) {
				try {
					Base.base.getDriver().findElement(By.xpath(email_all_email_checkBox)).click();
				} catch (WebDriverException ex) {
					Logging.error("Failed to click on Select all email checkbox");
				}
			} else
				break;
		}
	}

	/**
	 * Click float toolbar button in email detail view
	 * 
	 * @Author Vivian Xie
	 * @Created 2017/04/25
	 * @parameter1 ButtonName -- the name of the button to be clicked, value can be
	 *             "Reply", "Reply all","Forward","Delete", "More"
	 */
	public void clickFloatToolbarButtonInEmailDetailView(String buttonName) {
		Logging.info("Starting to click on float toolbar button in Email Detail view");
		String buttonNameNew = buttonName.toLowerCase().replaceAll(" ", "");
		String email_detail_from = PropertyHelper.coreEmailFile.getPropertyValue("email_detail_from");
		String email_detail_float_toolbar = PropertyHelper.coreEmailFile.getPropertyValue("email_detail_float_toolbar")
				.replace("+variable+", buttonNameNew);

		try {
			Tools.moveFocusToElementByXpath(email_detail_from);
			Base.base.getDriver().findElement(By.xpath(email_detail_float_toolbar)).click();
		} catch (WebDriverException ex) {
			Logging.info("Could not click on Float toolbar button in email detail view");
			throw ex;
		}

	}

	/**
	 * get the specific line content in email preview view
	 * 
	 * @Author Vivian Xie
	 * @Created 2017/5/8
	 * @parameter1 lineNumber -- the line number of the line
	 */
	public String getEmailBodyLineContent(int lineNumber) {
		Logging.info("Starting to get email body line " + String.valueOf(lineNumber) + " in priview view");
		String result = null;
		String mail_preview_msgbody = PropertyHelper.coreEmailFile.getPropertyValue("mail_preview_msgbody");
		try {
			String emailConent = Base.base.getDriver().findElement(By.xpath(mail_preview_msgbody))
					.getAttribute("innerHTML");
			result = emailConent.split("<br>")[lineNumber];
			Logging.info("Line " + lineNumber + " in the mail is " + result);
		} catch (WebDriverException ex) {
			Logging.info("Could not get  email body line content in email preview view");
			throw ex;
		}
		return result;
	}

	/**
	 * Verify if the specific mail has correct priority
	 * 
	 * @Author Vivian Xie
	 * @param mailSubject
	 *            -- the mail subject of the mail to be check
	 * @param priority
	 *            -- the expected priority value can be "Normal", "high", "Low"
	 * @Created 2017/5/16
	 */
	public boolean verifyMailPriorityInList(String mailSubject, String priority) {
		Logging.info("Starting to verify mail " + mailSubject + "has priortity " + priority);
		boolean result = false;
		String classString = null;
		String email_list_message_priority = PropertyHelper.coreEmailFile
				.getPropertyValue("email_list_message_priority").replace("+variable+", mailSubject);
		if (priority.equalsIgnoreCase("normal")) {
			result = true;
			try {
				classString = Base.base.getDriver().findElement(By.xpath(email_list_message_priority))
						.getAttribute("class");
				result = false;
			} catch (WebDriverException ex) {
			}
		} else {
			try {
				classString = Base.base.getDriver().findElement(By.xpath(email_list_message_priority))
						.getAttribute("class");
			} catch (WebDriverException ex) {
				Logging.info("Could not get  email body line content in email preview view");
				throw ex;
			}

			if (classString.contains("priority" + priority))
				result = true;
		}
		return result;
	}

	/**
	 * Verify if the menu list has correct menu item
	 * 
	 * @Author Charlie Li
	 * @param menuItem
	 *            -- The text of menu item
	 * @Created On: August/28/2017
	 */
	public boolean verifyMenuItemExists(String menuItem) {
		Logging.info("Start to verify mail menu has" + menuItem);
		boolean result = false;
		menuItem = menuItem.trim();
		Logging.info("Found to menu item " + menuItem);
		String email_menu_item = PropertyHelper.coreEmailFile.getPropertyValue("email_menu_item").replace("+variable+",
				menuItem);
		try {
			Base.base.getDriver().findElement(By.xpath(email_menu_item));
			result = true;
		} catch (WebDriverException ex) {
			Logging.error("Could not found menu item " + menuItem);
		}
		return result;
	}

	/**
	 * Click more button on dock window
	 * 
	 * @Author Charlie Li
	 * @Created On: August/28/2017
	 */
	public void clickMoreMenuOptionInSingleDetail() {

		Logging.info("Clicking on button: More ");
		String email_single_detail_arrow_down_option = PropertyHelper.coreEmailFile
				.getPropertyValue("email_single_detail_arrow_down_option");
		String email_detail_header = PropertyHelper.coreEmailFile.getPropertyValue("email_detail_header");
		String email_single_detail_float_tool_bar = PropertyHelper.coreEmailFile
				.getPropertyValue("email_single_detail_float_tool_bar");

		try {
			Tools.setDriverDefaultValues();
			Tools.moveFocusToElementByXpath(email_detail_header);
			Tools.moveFocusToElementByXpath(email_single_detail_float_tool_bar);
			Base.base.getDriver().findElement(By.xpath(email_single_detail_arrow_down_option)).click();
		} catch (WebDriverException ex) {
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: August 30, 2017<br>
	 * <b>Description</b><br>
	 * Get the message body content
	 * 
	 */
	public String getMessageBodyContent() {
		Logging.info("Get mail body content");
		StringBuilder messageContent = new StringBuilder();
		String mail_body_content = PropertyHelper.coreEmailFile.getPropertyValue("mail_body_content");
		String loading_bar = PropertyHelper.pageObjectBaseFile.getPropertyValue("common_load_mask");

		try {
			Tools.waitUntilElementNotDisplayedByXpath(loading_bar, 5);
			List<WebElement> msgBody = Base.base.getDriver().findElements(By.xpath(mail_body_content));
			for (WebElement el : msgBody)
				messageContent.append((String) ((JavascriptExecutor) Base.base.getDriver())
						.executeScript("return arguments[0].innerText;", el));

		} catch (WebDriverException ex) {
			Logging.error("Fail to get message body content");

		}

		return messageContent.toString().replaceAll("\r|\n", " ").trim();
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: August 30, 2017<br>
	 * <b>Description</b><br>
	 * Verify the link in mail body exists
	 * 
	 * @param fileName
	 *            - the file name of the link
	 * 
	 */
	public boolean verifyCloudLinkInMailBody(String fileName) {
		Logging.info("Start to verify the link of cloud file in mail body");
		boolean result = false;
		String mail_body_content_link = PropertyHelper.coreEmailFile.getPropertyValue("mail_body_content_link");
		mail_body_content_link = String.format(mail_body_content_link, fileName);
		try {
			String cloudLink = Base.base.getDriver().findElement(By.xpath(mail_body_content_link)).getAttribute("href");
			Logging.info("Link of the cloud : " + cloudLink);
			if (cloudLink.contains("https://"))
				result = true;
		} catch (WebDriverException ex) {
			Logging.error("Fail to verify cloud link in mail body");

		}
		return result;
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 1, 2017<br>
	 * <b>Description</b><br>
	 * Click save to cloud button of the attachment in mail body view
	 * 
	 * @param fileName
	 *            - the attachment file name
	 * 
	 */
	public void clickSaveToCloudButton(String fileName) {
		String mail_body_save_file_to_cloud_button = PropertyHelper.coreEmailFile
				.getPropertyValue("mail_body_save_file_to_cloud_button");
		String mail_body_attachment_dropdown = PropertyHelper.coreEmailFile
				.getPropertyValue("mail_body_attachment_dropdown");
		
		mail_body_save_file_to_cloud_button = String.format(mail_body_save_file_to_cloud_button, fileName);
		try {
			Tools.scrollElementIntoViewByXpath(mail_body_attachment_dropdown);
			Base.base.getDriver().findElement(By.xpath(mail_body_attachment_dropdown)).click();
			Base.base.getDriver().findElement(By.xpath(mail_body_save_file_to_cloud_button)).click();
		} catch (WebDriverException ex) {
			Logging.error("Failed to click cloud file folder");
		}
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 1, 2017<br>
	 * <b>Description</b><br>
	 * Select file folder in cloud file browser view
	 * 
	 * @param file
	 *            - the file location like "AAA/BBB/CCC"
	 * 
	 */
	public void clickCloudFileFolder(String file) {
		Logging.info("Start to click folder in cloud file browser");
		String loading_bar = PropertyHelper.pageObjectBaseFile.getPropertyValue("common_load_mask");
		String[] filePath = file.split("/");
		for (String f : filePath) {
			String cloud_file_name = PropertyHelper.coreEmailFile.getPropertyValue("cloud_file_name");
			cloud_file_name = String.format(cloud_file_name, f);
			try {
				//Tools.scrollElementIntoViewByXpath(cloud_file_name);
				Base.base.getDriver().findElement(By.xpath(cloud_file_name)).click();
				Tools.waitUntilElementNotDisplayedByXpath(loading_bar, 10);
			} catch (WebDriverException ex) {
				Logging.error("Failed to click cloud file folder");

			}

		}
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 1, 2017<br>
	 * <b>Description</b><br>
	 * Get the file list in current cloud folder
	 */
	public ArrayList<String> getCloudFileListUnderCurrentFolder() {
		ArrayList<String> fileList = new ArrayList<String>();
		String cloud_file_list = PropertyHelper.coreEmailFile.getPropertyValue("cloud_file_list");
		try {
			//Tools.scrollElementIntoViewByXpath(cloud_file_list);
			List<WebElement> fileListElement = Base.base.getDriver().findElements(By.xpath(cloud_file_list));
			if (fileListElement.size() == 0)
				fileList.add("");
			else {
				for (WebElement el : fileListElement) {
					fileList.add(el.getText());
				}
			}
		} catch (WebDriverException ex) {
			Logging.error("Failed to click cloud file folder");
		}
		return fileList;
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 4, 2017<br>
	 * <b>Description</b><br>
	 * Click save all to cloud button
	 */
	public void clickSaveAllToCloudButton() {
		Logging.info("Start to click save all to cloud button");
		String mail_body_save_all_to_cloud_button = PropertyHelper.coreEmailFile
				.getPropertyValue("mail_body_save_all_to_cloud_button");
		try {
			Tools.scrollElementIntoViewByXpath(mail_body_save_all_to_cloud_button);
			Base.base.getDriver().findElement(By.xpath(mail_body_save_all_to_cloud_button)).click();
		} catch (WebDriverException ex) {
			Logging.error("Failed to click cloud file folder");

		}
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 4, 2017<br>
	 * <b>Description</b><br>
	 * Input the folder name to cloud file browser
	 * 
	 * @param folderName
	 *            - the name of the folder
	 */
	public void typeFolderNameInCloudFileBrowser(String folderName) {
		Logging.info("type folder name in cloud add new folder window: " + folderName);
		String cloud_file_name_input = PropertyHelper.coreEmailFile.getPropertyValue("cloud_file_name_input");
		String loading_bar = PropertyHelper.pageObjectBaseFile.getPropertyValue("common_load_mask");

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(cloud_file_name_input)).clear();
			Base.base.getDriver().findElement(By.xpath(cloud_file_name_input)).sendKeys(folderName);
			Base.base.getDriver().findElement(By.xpath(cloud_file_name_input)).sendKeys(Keys.ENTER);
			Tools.waitFor(10, TimeUnit.SECONDS);
		} catch (WebDriverException ex) {
			Logging.error("Could not type folder name in add new folder window");
			throw ex;
		}
	}

	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 4, 2017<br>
	 * <b>Description</b><br>
	 * Get current folder name in cloud file browser view
	 * 
	 */
	public String getCurrentCloudFolderName() {
		Logging.info("Get current cloud folder name : ");
		String result = null;
		String cloud_current_folder_name = PropertyHelper.coreEmailFile.getPropertyValue("cloud_current_folder_name");
		try {
			Tools.setDriverDefaultValues();
			result = Base.base.getDriver().findElement(By.xpath(cloud_current_folder_name)).getText();
		} catch (WebDriverException ex) {
			Logging.error("Get cloud folder name fail");
			throw ex;
		}
		return result;
	}

	/**
	 * @Author: Chalrie
	 * @Created On: Sep/12/2017
	 * @Description: This method will click on email message in list based on
	 *               subject in preview none layout.
	 * @Parameter1: String subject - email message subject
	 */
	public void clickOnMessageBySubject_PN(String subject) {
		Logging.info("Clicking on email message: " + subject);
		subject = subject.trim();
		String email_list_message_clicking_area = PropertyHelper.coreEmailFile
				.getPropertyValue("email_list_message_clicking_area_pn").replace("+variable+", subject);

		try {
			Tools.setDriverDefaultValues();
			Tools.scrollElementIntoViewByXpath(email_list_message_clicking_area);

			WebElement element = Base.base.getDriver().findElement(By.xpath(email_list_message_clicking_area));
			// in FF, the Actions click don't work, so need js-click action
			if (Tools.isFirefox()) {
				new Actions(Base.base.getDriver()).moveToElement(element);
				JavascriptExecutor js = (JavascriptExecutor) Base.base.getDriver();
				js.executeScript("arguments[0].click()", element);
			} else {
				//new Actions(Base.base.getDriver()).moveToElement(element).click().build().perform();
				new Actions(Base.base.getDriver()).moveToElement(element).click(element).perform();
			}
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on email message: " + subject);
			throw ex;
		}
	}

	/**
	 * @Author: Charlie li
	 * @Created On: Sep/13/2017
	 * @Description: This method will verify email message existence in message list
	 *               based on subject in preview none layout.
	 * @Parameter1: String subject - email message subject
	 * @Return: boolean - true/false
	 */
	public boolean verifyMessageExistenceBySubject_PN(String subject) {
		Logging.info("Verifying message existence in list in preview right layout: " + subject);
		subject = subject.trim();

		String email_list_message_subject = PropertyHelper.coreEmailFile
				.getPropertyValue("email_list_message_clicking_area_pn").replace("+variable+", subject);
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(email_list_message_subject));
			// Subject is found.
			result = true;
			Logging.info("Found message with subject: " + subject);
		} catch (WebDriverException ex) {
			Logging.info("Could not found message with subject: " + subject);

		}
		return result;
	}

	/**
	 * @Author: Charlie li
	 * @Created On: Sep/15/2017
	 * @Description: This method will verify attachment existence in attachment list
	 *               based on subject in preview attachment layout.
	 * @Parameter1: String fileName - the name attachment
	 * @Return: boolean - true/false
	 */
	public boolean verifyAttachmentExistenceByFileName(String fileName) {
		Logging.info("Verifying attachment existence in list in attachment layout: " + fileName);
		boolean result = false;

		String attachment_view_attachment_by_name = PropertyHelper.coreEmailFile
				.getPropertyValue("attachment_view_attachment_by_name").replace("+variable+", fileName);

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(attachment_view_attachment_by_name));
			// Attachment is found.
			result = true;
			Logging.info("Found attachment with file name: " + fileName);
		} catch (WebDriverException ex) {
			Logging.info("Could not found attachment with subject: " + fileName);

		}

		return result;
	}

	/**
	 * @Author: Charlie li
	 * @Created On: Sep/15/2017
	 * @Description: This method will verify message body preview in massage list
	 *               based on message body in right layout.
	 * @Parameter1: String body - the message body
	 * @Return: boolean - true/false
	 */
	public boolean verifyEmailBodyPreview(String body) {
		Logging.info("Verifying message body preview in message list in right layout: " + body);
		boolean result = false;

		if (body.length() > 79) {
			body = body.substring(0, 79);
		}
		String email_list_message_body = PropertyHelper.coreEmailFile.getPropertyValue("email_list_message_body")
				.replace("+variable+", body);

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(email_list_message_body));
			result = true;
			Logging.info("Found message body with body: " + body);
		} catch (WebDriverException ex) {
			Logging.info("Could not found message body with body: " + body);

		}

		return result;

	}

	/**
	 * @Author: Charlie li
	 * @Created On: Sep/15/2017
	 * @Description: This method will verify message body preview in massage list
	 *               based on message body in none layout.
	 * @Parameter1: String body - the message body
	 * @Return: boolean - true/false
	 */
	public boolean verifyEmailBodyPreview_PN(String body) {
		Logging.info("Verifying message body preview in message list in none layout: " + body);
		boolean result = false;

		if (body.length() > 79) {
			body = body.substring(0, 79);
		}
		String email_list_message_body = PropertyHelper.coreEmailFile.getPropertyValue("email_list_message_body_pn")
				.replace("+variable+", body);

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(email_list_message_body));
			result = true;
			Logging.info("Found message body with body: " + body);
		} catch (WebDriverException ex) {
			Logging.info("Could not found message body with body: " + body);

		}

		return result;

	}

	/**
	 * @Author: Charlie li
	 * @Created On: Sep/15/2017
	 * @Description: This method will verify message body preview in massage list
	 *               based on message body in below layout.
	 * @Parameter1: String body - the message body
	 * @Return: boolean - true/false
	 */
	public boolean verifyEmailBodyPreview_PB(String body) {
		Logging.info("Verifying message body preview in message list in below layout: " + body);
		boolean result = false;

		if (body.length() > 79) {
			body = body.substring(0, 79);
		}
		String email_list_message_body = PropertyHelper.coreEmailFile.getPropertyValue("email_list_message_body_pb")
				.replace("+variable+", body);

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(email_list_message_body));
			result = true;
			Logging.info("Found message body with body: " + body);
		} catch (WebDriverException ex) {
			Logging.info("Could not found message body with body: " + body);

		}

		return result;

	}

	/**
	 * @Author: Charlie li
	 * @Created On: Sep/20/2017
	 * @Description: This method will verify message body preview in attachment list
	 *               in attachment view.
	 * @Parameter: String fileName - the file name
	 * @Parameter: String body - the message body
	 * @Return: boolean - true/false
	 */
	public boolean verifyBodyPreviewInAttachmentView(String fileName, String body) {
		Logging.info("Verifying message body preview in message list in attachment view: " + body);
		boolean result = false;

		String attachment_view_attachment_item_info = PropertyHelper.coreEmailFile
				.getPropertyValue("attachment_view_attachment_item_info").replace("+variable+", fileName);

		try {
			Tools.setDriverDefaultValues();
			String attachmentInfo = Base.base.getDriver().findElement(By.xpath(attachment_view_attachment_item_info))
					.getText();
			if (attachmentInfo.contains(body)) {
				result = true;
			}
			Logging.info("Found message body with file: " + fileName);
		} catch (WebDriverException ex) {
			Logging.info("Could not found message body with file: " + fileName);

		}

		return result;
	}

	/**
	 * @Author: Charlie li
	 * @Created On: Sep/20/2017
	 * @Description: This method will verify virtual folder list in attachment view.
	 * @Parameter: String folderName - the folder name
	 */
	public boolean verifyCategoryFolder(String folderName) {
		Logging.info("Verifying virtual folder " + folderName + " in mail list ");
		boolean result = false;

		String mail_category_folder = PropertyHelper.coreEmailFile.getPropertyValue("mail_category_folder")
				.replace("+variable+", folderName);
		Logging.info(mail_category_folder);
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(mail_category_folder));
			result = true;

			Logging.info("Found virtual folder: " + folderName);
		} catch (WebDriverException ex) {
			Logging.info("Could not found message body with file: " + folderName);

		}

		return result;
	}

	/**
	 * @Author: Charlie li
	 * @Created On: Otc/13/2017
	 * @Description: This method will verify suspect email message existence in
	 *               message list based on subject in preview right layout.
	 * @Parameter: String subject - email message subject
	 */
	public boolean verifyCategorySuspectEmailBySubject(String subject) {
		Logging.info("Verifying virtual folder " + subject + " in mail list ");
		boolean result = false;

		String mail_category_suspect_email = PropertyHelper.coreEmailFile
				.getPropertyValue("mail_category_suspect_email").replace("+variable+", subject);
		Logging.info(mail_category_suspect_email);
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(mail_category_suspect_email));
			result = true;

			Logging.info("Found virtual folder: " + subject);
		} catch (WebDriverException ex) {
			Logging.info("Could not found message body with file: " + subject);

		}

		return result;
	}

	/**
	 * @Author: Charlie li
	 * @Created On: Otc/13/2017
	 * @Description: This method will select the virtual folder by folder name
	 * @Parameter: String folderName - the folder name
	 */
	public boolean selectCategoryFolder(String folderName) {
		Logging.info("Select virtual folder: " + folderName);
		boolean result = false;

		String mail_category_folder = PropertyHelper.coreEmailFile.getPropertyValue("mail_category_folder")
				.replace("+variable+", folderName);
		Logging.info(mail_category_folder);
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(mail_category_folder)).click();
			;

			Logging.info("Seleted " + mail_category_folder + " folder");
		} catch (WebDriverException ex) {
			Logging.info("Could not select " + mail_category_folder + " folder");

		}

		return result;
	}

	/**
	 * <b>Author</b>: Joey Fang <br>
	 * <b>Date</b>: Oct 26, 2017<br>
	 * <b>Description</b><br>
	 * click on double confirm window
	 * 
	 * @param filterAccordance
	 *            - the filter method
	 * 
	 */
	public void clickOnDoubleConfirmWin(String action) {
		String double_confrim_yes = PropertyHelper.coreEmailFile.getPropertyValue("double_confrim_yes");

		String double_confrim_no = PropertyHelper.coreEmailFile.getPropertyValue("double_confrim_no");

		Logging.info("Clicking " + action + "on double confirm window");
		try {
			Tools.setDriverDefaultValues();
			if (action.equals("Yes")) {
				Base.base.getDriver().findElement(By.xpath(double_confrim_yes)).click();
			} else {
				Base.base.getDriver().findElement(By.xpath(double_confrim_no)).click();
			}

		} catch (WebDriverException ex) {
			throw ex;
		}
	}

}
