package com.synchronoss.pageobject;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.synchronoss.core.Base;
import com.synchronoss.core.Logging;
import com.synchronoss.core.PropertyHelper;
import com.synchronoss.core.Tools;
import com.synchronoss.test.untils.TaskItem;

public class CoreTasks extends PageObjectBase {
	
	//#########################################################################
	//							  Static Constants
	//#########################################################################
	public static final String DEFAULT_GROUP_NAME = "New Group";
	public static final String ERR_MSG_EMPTY_GROUP_NAME = "Task group name cannot be empty.";
	public static final int TASK_DETAIL_TIME_INCREMENT = 15;
	
	public static final String DETAIL_FIELD_TITLE = "Title";
	public static final String DETAIL_FIELD_LIST = "List";
	public static final String DETAIL_FIELD_PRIORITY = "Priority";
	public static final String DETAIL_FIELD_STATUS = "Status";
	public static final String DETAIL_FIELD_DUE_DATE = "Due Date";
	public static final String DETAIL_FIELD_DUE_TIME = "Time";
	public static final String DETAIL_FIELD_REMINDER = "Reminder";
	public static final String DETAIL_FIELD_URL = "URL";
	public static final String DETAIL_FIELD_DESCRIPTION = "Description";
	
	public static final String LIST_UNGROUPED = "Ungrouped";
	public static final String LIST_NONE = "";
	public static final String LIST_OTHERGROUP = "otherGroup";
	
	public static final String PRIORITY_HIGH = "High";
	public static final String PRIORITY_NORMAL = "Normal";
	public static final String PRIORITY_LOW = "Low";
	
	public static final String STATUS_INCOMPLETE = "Incomplete";
	public static final String STATUS_COMPLETE = "Complete";
	public static final String STATUS_NEEDS_ACTION = "Needs action";
	
	public static final String REMINDER_NONE = "None";
	public static final String REMINDER_AT_TIME_OF_EVENT = "At time of event";
	public static final String REMINDER_5_MIN_BEFORE = "5 minutes before";
	public static final String REMINDER_1_HOUR_BEFORE = "1 hour before";
	
	public static final String ORDER_ASCENDING = "Ascending";
	public static final String ORDER_DESCENDING = "Descending";
	
	// The buffer to be added to a waiting time so that possible network latency
	// time is tolerated. E.g. waiting a reminder to be received. 
	// This buffer time should be set within a reasonable range.
	public static final long BUFFER_IN_SECONDS = 120;
	
	
	//#########################################################################
	//							  Page Objects APIs
	//#########################################################################
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 03, 2016<br>
	 * <b>Description: </b><br>
	 * Clicks on add group button (plus icon in tasks group list).
	 */
	public void clickOnGroupAddButton() {
		Logging.info("Clicking on task group add button");
		String tasks_group_add_button = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_group_add_button");
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(tasks_group_add_button)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on task group add button");
			throw ex;
		}
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 10, 2016<br>
	 * <b>Description: </b><br>
	 * Clicks on add task button (plus icon in task detail view).
	 */
	public void clickOnTaskAddButton() {
		Logging.info("Clicking on task add button");
		String tasks_add_button = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_add_button");
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(tasks_add_button)).click();
			Tools.waitFor(10, TimeUnit.SECONDS);
		} catch (WebDriverException ex) {
			Logging.error("Could not click on task add button");
			throw ex;
		}
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 06, 2016<br>
	 * <b>Description: </b><br>
	 * Clears and then types text into group name input.
	 */
	public void typeInGroupNameInput(String groupName) {
		String tasks_group_name_input = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_group_name_input");
		try {
			Tools.setDriverDefaultValues();
			
			Logging.info("Clearing group name input");
			Base.base.getDriver().findElement(By.xpath(tasks_group_name_input)).clear();
			
			Logging.info("typing in group name input: " + groupName );
			Base.base.getDriver().findElement(By.xpath(tasks_group_name_input)).sendKeys(groupName);
			Base.base.getDriver().findElement(By.xpath(tasks_group_name_input)).sendKeys(Keys.ENTER);

		} catch (WebDriverException ex) {
			Logging.error("Could not clear and type in group name input");
			throw ex;
		}
	}
	
	
	
	/**
     * <b>Author: </b>Vivian Xie<br>
     * <b>Date: </b>Dec 13, 2016<br>
     * <b>Description: </b><br>
     * Clears and then types text into group rename input.
     */
    public void typeInGroupReNameInput(String groupName) {
        String tasks_group_rename_input = PropertyHelper.CoreTasks
                .getPropertyValue("tasks_group_rename_input");
        try {
            Tools.setDriverDefaultValues();
            
            Logging.info("Clearing group rename input");
            Base.base.getDriver().findElement(By.xpath(tasks_group_rename_input)).clear();
            
            Logging.info("typing in group name input: " + groupName );
            Base.base.getDriver().findElement(By.xpath(tasks_group_rename_input)).sendKeys(groupName);
            Base.base.getDriver().findElement(By.xpath(tasks_group_rename_input)).sendKeys(Keys.ENTER);

        } catch (WebDriverException ex) {
            Logging.error("Could not clear and type in group rename input");
            throw ex;
        }
    }
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 03, 2016<br>
	 * <b>Description: </b><br>
	 * Adds new task group to list with specified group name.
	 */
	public void addNewGroup(String groupName) {
		Logging.info("Adding new task group: " + groupName );
		try {
			this.clickOnGroupAddButton();
			this.typeInGroupNameInput(groupName);
			this.clickButtonWhenAddingTaskGroup("Save");
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not add new task group");
			throw ex;
		}
	}
	
	/**
     * <b>Author: </b>Vivian Xie<br>
     * <b>Date: </b>Dec 13, 2016<br>
     * <b>Description: </b><br>
     * Click "Save" or "Cancel" burtton on Add task group dialog
     * <b>Parameter: </b>buttonName -- "Save" or "Cancel"
     */
    public void clickButtonWhenAddingTaskGroup(String buttonName) {
        Logging.info("Clicking button  " + buttonName );
        String task_group_newgroupwin_button = PropertyHelper.CoreTasks
        .getPropertyValue("task_group_newgroupwin_button").replace("+variable+",buttonName);
        try {     
         Base.base.getDriver()
                  .findElement(By.xpath(task_group_newgroupwin_button)).click();        
      } catch (WebDriverException ex) {
          Logging.error("Could not click "+buttonName+ "button ");
          throw ex;
      }
    }
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 03, 2016<br>
	 * <b>Description: </b><br>
	 * Adds new task group to list with default name
	 */
	public void addNewGroupWithDefaultName() {
		Logging.info("Adding new task group with default name" );
		String tasks_group_name_input = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_group_name_input");
		try {
			Tools.setDriverDefaultValues();
			this.clickOnGroupAddButton();
			 Base.base.getDriver()
					.findElement(By.xpath(tasks_group_name_input)).sendKeys(CoreTasks.DEFAULT_GROUP_NAME);
			Logging.info("System generated default group name: " + CoreTasks.DEFAULT_GROUP_NAME);	
			this.clickButtonWhenAddingTaskGroup("Save");
		} catch (WebDriverException ex) {
			Logging.error("Could not add new task group");
			throw ex;
		}
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 04, 2016<br>
	 * <b>Description: </b><br>
	 * Clicks the specified button on task group hover panel. Destination group name 
	 * should be Specified as well.
	 * 
	 * @param button - possible options:
	 * 				- rename
	 * 				- delete
	 */
	public void clickButtonOnHoverPanelByGroupName(String groupName, String button) {
		Logging.info("Clicking on hover panel button: " + button + ", group name: " + groupName);
		
		String btn = button.trim().toLowerCase();
		if (btn.equals("rename")) {
			btn = "compose";
		}
		
		String tasks_group_row_by_name = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_group_row_by_name")
				.replace("+variable+", groupName);
		String tasks_group_hover_panel_button = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_group_hover_panel_button")
				.replace("+variable+", btn);
		
		try {
			Tools.setDriverDefaultValues();
			Tools.moveFocusToElementByXpath(tasks_group_row_by_name);
		} catch (WebDriverException ex) {
			Logging.error("Could not move focus on group: " + groupName);
			throw ex;
		}
		
		try {
			Base.base.getDriver().findElement(By.xpath(tasks_group_hover_panel_button)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on hover panel button: " + button);
			throw ex;
		}
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 04, 2016<br>
	 * <b>Description: </b><br>
	 * Clicks the specified button on task group hover panel. Destination group index 
	 * should be Specified as well.
	 * 
	 * @param index - starting from 1
	 * @param button - possible options:
	 * 				- rename
	 * 				- delete
	 */
	public void clickButtonOnHoverPanelByGroupIndex(int index, String button) {
		Logging.info("Clicking on hover panel button: " + button + ", group index: " + index);
		
		String btn = button.trim().toLowerCase();
		if (btn.equals("rename")) {
			btn = "compose";
		}
		
		String tasks_group_row_by_index = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_group_row_by_index")
				.replace("+variable+", String.valueOf(index));
		String tasks_group_hover_panel_button = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_group_hover_panel_button")
				.replace("+variable+", btn);
		Logging.info("tasks_group_row_by_index="+tasks_group_row_by_index);
		Logging.info("tasks_group_hover_panel_button="+tasks_group_hover_panel_button);
		Boolean hoverIconDisplay = false;
		ArrayList <WebElement> hoverIcon;
		int i = 0;
		
		try {
			Tools.setDriverDefaultValues();		
			while(!hoverIconDisplay && (i<10)){
			Tools.moveFocusToElementByXpath(tasks_group_row_by_index);
		    hoverIcon = (ArrayList<WebElement>) Base.base.getDriver().findElements(By.xpath(tasks_group_hover_panel_button));
			if(hoverIcon.size()>0) hoverIconDisplay = true;	
			i++;
			}		
		} catch (WebDriverException ex) {
			Logging.error("Could not move focus on group with index: " + index);
			throw ex;
		}
		
		try {
			Base.base.getDriver().findElement(By.xpath(tasks_group_hover_panel_button)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on hover panel button: " + button);
			throw ex;
		}
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 04, 2016<br>
	 * <b>Description: </b><br>
	 * Deletes all task groups sequentially from list.
	 */
	public void deleteAllGroups() {
		Logging.info("Deleting all task groups from list");
		String tasks_group_row_all = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_group_row_all");
		
		try {
			Tools.setDriverDefaultValues();
			int count = Base.base.getDriver().findElements(By.xpath(tasks_group_row_all)).size();
			Logging.info("Group count: " + count);
			
			if (count == 0) {
				return;
			}
			
			for (int i = 1; i <= count; i++) {
				Logging.info("Deleting group, original index: " + i);
				// Always deletes from the top - index 1
				this.clickButtonOnHoverPanelByGroupIndex(1, "delete");
				this.clickButtonOnMessageBoxIfDisplayed("Yes");
				this.waitForLoadMaskDismissed();
			}
			Logging.info("Deleted all task groups");
			
		} catch (WebDriverException ex) {
			Logging.error("Could not perform group deletion");
			throw ex;
		}
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 05, 2016<br>
	 * <b>Description: </b><br>
	 * Clicks on system group 'All Tasks'.
	 */
	public void clickOnAllTasks() {
		Logging.info("Clicking on system group All Tasks");
		
		String tasks_group_row_alltasks = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_group_row_alltasks");
		try {
			Tools.setDriverDefaultValues();
			WebElement allTasks = Base.base.getDriver().findElement(By.xpath(tasks_group_row_alltasks));
			Actions actions = new Actions(Base.base.getDriver());
			actions.moveToElement(allTasks).click().perform();
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on system group All Tasks");
			throw ex;
		}
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 26, 2016<br>
	 * <b>Description: </b><br>
	 * Clicks on a task group with the group name specified.
	 */
	public void clickOnGroupByName(String groupName) {
		Logging.info("Clicking on group: " + groupName);
		
		String tasks_group_row_by_name = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_group_row_by_name")
				.replace("+variable+", groupName);
		
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(tasks_group_row_by_name)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on group: " + groupName);
			throw ex;
		}
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 05, 2016<br>
	 * <b>Description: </b><br>
	 * Clicks on a task item in list by specified index
	 * 
	 * @param index - starting from 1
	 */
	public void clickOnTaskByIndex(int index) {
		Logging.info("Clicking on task item, index: " + index);
		
		String tasks_item_by_index = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_item_by_index")
				.replace("+variable+", String.valueOf(index));
		
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(tasks_item_by_index)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on task item, index: " + index);
			throw ex;
		}
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 21, 2016<br>
	 * <b>Description: </b><br>
	 * Clicks on a task item in list by specified title.
	 */
	public void clickOnTaskByTitle(String title) {
		Logging.info("Clicking on task item: " + title);
		
    	String tasks_item_title = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_item_title")
				.replace("+variable+", title);
		
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(tasks_item_title)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on task item: " + title);
			throw ex;
		}
	}
	
	public void clickOnEditTaskButton() {
		Logging.info("Clicking on edit task button");
		String tasks_item_edit = PropertyHelper.CoreTasks.getPropertyValue("tasks_item_edit");
		try {
			Tools.setDriverDefaultValues();
			System.out.println("jfang tasks_item_edit is: "+tasks_item_edit);
			Base.base.getDriver().findElement(By.xpath(tasks_item_edit)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on edit task button");
			throw ex;
		}
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 05, 2016<br>
	 * <b>Description: </b><br>
	 * Clicks on the specified toolbar button in task detail view.
	 * 
	 * @param button - possible options:
	 * 				- Save
	 * 				- Cancel
	 * 				- Delete
	 */
	public void clickToolbarButtonInDetail(String button) {
		Logging.info("Clicking on toolbar button in task details view: " + button);
		
		String buttonToClickOn;
		if (button.trim().toLowerCase().equals("delete")) {
			buttonToClickOn = PropertyHelper.CoreTasks
					.getPropertyValue("tasks_detail_view_toolbar_button_delete");
		} else {
			buttonToClickOn = PropertyHelper.CoreTasks.getPropertyValue("tasks_detail_view_toolbar_button")
					.replace("+variable+", button);
		}
		
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(buttonToClickOn)).click();
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on toolbar button in task details view: " + button);
			throw ex;
		}
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 11, 2016<br>
	 * <b>Description: </b><br>
	 * Clears and then types text into the specified detail field.
	 * 
	 * @param field - possible options:
	 * 				- Title
	 * 				- URL
	 * 				- Description
	 */
	public void typeInDetailTextField(String field, String text) {
		
		Logging.info("Type in detail view text field: " + field + ", text: " + text);
		String tasks_detail_textfield = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_detail_textfield")
				.replace("+variable+", field);
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(tasks_detail_textfield)).clear();
			Base.base.getDriver().findElement(By.xpath(tasks_detail_textfield)).sendKeys(text);
			Base.base.getDriver().findElement(By.xpath(tasks_detail_textfield)).sendKeys(Keys.TAB);
		} catch (WebDriverException ex) {
			Logging.error("Could not type in detail view text field: " + field);
			throw ex;
		}
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 11, 2016<br>
	 * <b>Description: </b><br>
	 * Selects an option from the a detail combo box field.
	 * 
	 * @param field - possible options:
	 * 				- List
	 * 				- Priority
	 * 				- Status
	 * 				- Time
	 * 				- Reminder
	 */
	public void selectOptionInDetailComboBox(String field, String option) {
		Logging.info("Selecting in detail view combo box field: " + field + ", option: " + option);
		
		String tasks_detail_combobox_input = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_detail_combobox_input")
				.replace("+variable+", field);
		String tasks_boundlist_item_by_text = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_boundlist_item_by_text")
				.replace("+variable+", option);
		
		if (field.equals(DETAIL_FIELD_DUE_TIME)) {
			tasks_detail_combobox_input = PropertyHelper.CoreTasks
					.getPropertyValue("tasks_detail_combobox_time_input")
					.replace("+variable+", field);
		} else if (field.equals(DETAIL_FIELD_REMINDER)) {
			tasks_detail_combobox_input = PropertyHelper.CoreTasks
					.getPropertyValue("tasks_detail_combobox_reminder_input")
					.replace("+variable+", field);
		}
		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(tasks_detail_combobox_input)).click();
			
			Tools.scrollElementIntoViewByXpath(tasks_boundlist_item_by_text);
			Base.base.getDriver().findElement(By.xpath(tasks_boundlist_item_by_text)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not select in detail view combo box field: " + field);
			throw ex;
		}
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 13, 2016<br>
	 * <b>Description: </b><br>
	 * Clicks due date combo box to trigger date picker in detail.
	 */
	private void clickDueDateComboboxInDetail() {
		Logging.info("Clicking on due date combobox to trigger date picker");
		
		String tasks_detail_combobox_input = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_detail_combobox_input")
				.replace("+variable+", "Due Date");

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(tasks_detail_combobox_input)).click();
		} catch (WebDriverException ex) {
			Logging.error("Could not click on due date combobox to trigger date picker");
			throw ex;
		}
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 12, 2016<br>
	 * <b>Description: </b><br>
	 * Checks or unchecks the due date checkbox in task detail.
	 * 
	 * @param check - True: to check; Fasle: to uncheck
	 */
	public void checkOrUncheckDueDateInDetail(boolean check ) {
		Logging.info("Handling Due Date checkbox in detail: " + (check ? "check" : "uncheck"));
		
		String tasks_detail_checkbox = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_detail_checkbox")
				.replace("+variable+", "Due Date");
		String tasks_detail_checkbox_flag = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_detail_checkbox_flag")
				.replace("+variable+", "Due Date");
		
		try {
			Tools.setDriverDefaultValues();
	    	
	    	boolean isChecked = Base.base.getDriver()
	    			.findElement(By.xpath(tasks_detail_checkbox_flag))
	    			.getAttribute("class").contains("cb-checked");		
	    	
	    	if ((check&&!isChecked) || (!check&&isChecked)) {
	    		Base.base.getDriver().findElement(By.xpath(tasks_detail_checkbox)).click();
	    	}
		} catch (WebDriverException ex) {
			Logging.error("Could not handling Due Date checkbox in detail");
			throw ex;
		}
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 05, 2016<br>
	 * <b>Description: </b><br>
	 * Deletes all tasks sequentially from list.
	 */
	public void deleteAllTasks() {
		Logging.info("Deleting all tasks from list");
		String tasks_item_row_all = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_item_row_all");
		
		try {
			Tools.setDriverDefaultValues();
			this.waitForLoadMaskDismissed();
			int count = Base.base.getDriver().findElements(By.xpath(tasks_item_row_all)).size();
			Logging.info("Task count: " + count);
			
			if (count == 0) {
				return;
			}
			int i = 1;
			
			while (count >0) {
				Logging.info("Deleting task, original index: " + i);
				// Always deletes from the top - index 1
				this.clickOnTaskByIndex(1);
				this.clickToolbarButtonInDetail(DELETE);
				this.clickButtonOnMessageBoxIfDisplayed(YES);
				this.waitForLoadMaskDismissed();
				count = Base.base.getDriver().findElements(By.xpath(tasks_item_row_all)).size();
				i++;
			}
			Logging.info("Deleted all tasks from list");
			
		} catch (WebDriverException ex) {
			Logging.error("Could not perform task deletion");
			throw ex;
		}
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 05, 2016<br>
	 * <b>Description: </b><br>
	 * Clears all tasks and task groups respectively
	 */
	public void clearEverything() {
		Logging.info("Clearing all tasks and task groups");
		this.deleteAllGroups();
		this.clickOnAllTasks();
		this.deleteAllTasks();
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 13, 2016<br>
	 * <b>Description: </b><br>
	 * Selects due date from date picker in task detail.
	 */
	public void selectDueDateInDetail(LocalDate date) {
		Logging.info("Selecting due date in task detail: " + date);
    	try {
    		this.clickDueDateComboboxInDetail();
    		Logging.info(this.getBrowserDateTime().toString());
    		this.selectDateInFloatingDatePicker(date);
		} catch (WebDriverException ex) {
			Logging.error("Could not select due date in task detail: " + date);
			throw ex;
		}
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 15, 2016<br>
	 * <b>Description: </b><br>
	 * Gets the next integral minute based on the increment value. The current 
	 * increment value is 15 minutes which can be acquired from task detail UI.
	 */
	public LocalTime getNextIntegralMinute(LocalTime baseTime) {
		int mod = baseTime.getMinute() % TASK_DETAIL_TIME_INCREMENT;
		LocalTime nextIntegralTime = baseTime.plusMinutes(TASK_DETAIL_TIME_INCREMENT - mod);
		Logging.info("Next integral time: " + nextIntegralTime);
		return nextIntegralTime;
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 15, 2016<br>
	 * <b>Description: </b><br>
	 * Selects due time in task detail.
	 */
	public void selectDueTimeInDetail(LocalTime time, String timeFormat) {
		String timeToSelect = this.getFormattedLocalTimeString(time, timeFormat);
		Logging.info(this.getBrowserDateTime().toString());
		this.selectOptionInDetailComboBox("Time", timeToSelect);
	}
	
	
  /**
   * @author Jerry Zhang
   * @create Jun 20, 2016
   * @decription Composes a reminder line that is shown in email list as subject.
   */
  public String composeReminderHeadline(TaskItem task, String timeZoneCity, String dateFormat,
      String timeformat) {
    Logging.info("Starting to format task title. dateFormat is " + dateFormat + " and timeformat is"
        + timeformat);
    String patternDate = null;
    String patternTime = null;
    if (dateFormat.equals("MM/DD/YYYY")) {
      patternDate = "ccc MMM dd, yyyy ";
    } else {
      patternDate = "ccc " + dateFormat;
      patternDate = patternDate.replace("YYYY", "yyyy").replaceAll("MM", "MMM")
          .replaceAll("DD", "dd").replace("YY", "yyyy").replaceAll("\\.", " ").replaceAll("/", " ")
          .replaceAll("-", " ");
      Logging.info(patternDate);
    }
    if (timeformat.equals("12"))
      patternTime = " h:mma";
    else
      //patternTime = " HH:mm";
    	patternTime = " h:mma";

    Logging.info(patternDate + patternTime);

    // DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
    DateTimeFormatter formatter =
        DateTimeFormatter.ofPattern(patternDate + patternTime, Locale.ENGLISH);
    String dueString = task.due.format(formatter).replace("AM", "am").replace("PM", "pm");
    String line = "Reminder: " + task.title + " due " + dueString + " " + timeZoneCity;

    Logging.info("Composed reminder headline: " + line);
    return line;
  }

	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 30, 2016<br>
	 * <b>Description: </b><br>
	 * Selects an option from sort menu drop-down list.
	 * 
	 * @param option - the argument should be constrained to CoreTasks static constants:
	 * 				- DETAIL_FIELD_DUE_DATE
	 * 				- DETAIL_FIELD_PRIORITY
	 * 				- DETAIL_FIELD_TITLE
	 * 				- ORDER_ASCENDING
	 * 				- ORDER_DESCENDING
	 */
	public void selectOptionInSortMenu(String option) {
		Logging.info("Selecting Sort Menu option: " + option );
		
		String tasks_sort_menu_dropdown_button = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_sort_menu_dropdown_button");
		// Applying a workaround to replace "Date" with "date" as the lower letter "d" is 
		// used in sort menu UI.
		String tasks_sort_menu_option = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_sort_menu_option")
				.replace("+variable+", option.replace("Date", "date"));

		try {
			Tools.setDriverDefaultValues();
			Base.base.getDriver().findElement(By.xpath(tasks_sort_menu_dropdown_button)).click();
			Base.base.getDriver().findElement(By.xpath(tasks_sort_menu_option)).click();
			this.waitForLoadMaskDismissed();
		} catch (WebDriverException ex) {
			Logging.error("Could not select Sort Menu option: " + option);
			throw ex;
		}
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 06, 2016<br>
	 * <b>Description: </b><br>
	 * Verifies if the expected task group exists in list.
	 */
	public boolean verifyGroupExistenceByName(String name) {
		Logging.info("Verifying existence of group: " + name);
		
    	String tasks_group_row_by_name = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_group_row_by_name")
				.replace("+variable+", name);
    	boolean result = false;
    	
    	try {
    		Tools.setDriverDefaultValues();
	    	Base.base.getDriver().findElement(By.xpath(tasks_group_row_by_name));
	    	result = true;
		} catch (WebDriverException ex) {
			// Do nothing.
		}
    	Logging.info("Found group in list: " + result);
		return result;
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 12, 2016<br>
	 * <b>Description: </b><br>
	 * Verifies if the expected task item exists in list.
	 */
	public boolean verifyTaskExistenceByTitle(String title) {
		Logging.info("Verifying existence of task: " + title);

		String tasks_item_title = PropertyHelper.CoreTasks.getPropertyValue("tasks_item_title").replace("+variable+",
				title);
		boolean result = false;

		try {
			Tools.setDriverDefaultValues();
			this.clickOnTaskByTitle(title);
			Base.base.getDriver().findElement(By.xpath(tasks_item_title));
			result = true;
		} catch (WebDriverException ex) {
			// Do nothing.
		}
		Logging.info("Found task in list: " + result);
		return result;
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 16, 2016<br>
	 * <b>Description: </b><br>
	 * Verifies in task list if the due date and time of a task is as expected.
	 */
	public boolean verifyTaskDueInList(String title, LocalDateTime due, 
			String dateFormat, String timeFormat) {
				
		String expected = this.getFormattedLocalDateString(due.toLocalDate(), dateFormat)
				+ " "
				+ this.getFormattedLocalTimeString(due.toLocalTime(), timeFormat);
		
		Logging.info("Verifying task due date, title: " + title + ", due: " + expected);
		
    	String tasks_item_due_by_title = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_item_due_by_title")
				.replace("+variable+", title);
    	boolean result = false;
    	
    	try {
    		Tools.setDriverDefaultValues();
	    	String actual = Base.base.getDriver()
	    			.findElement(By.xpath(tasks_item_due_by_title)).getText();
	    	Logging.info("Actual due: " + actual);
	    	if (actual.equals(expected)) {
		    	result = true;
	    	}
		} catch (WebDriverException ex) {
			Logging.error("Could not verify task due date");
		}
		return result;
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 22, 2016<br>
	 * <b>Description: </b><br>
	 * Verifies in task list if priority of a task is as expected.
	 * 
	 * @param priority - the argument should be constrained to CoreTasks static constants 
	 * initialized with "PRIORITY"
	 */
	public boolean verifyTaskPriorityInList(String title, String priority) {
		Logging.info("Verifying task priority, title: " + title + ", priority: " + priority);
		
    	String tasks_item_priority_by_title = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_item_priority_by_title")
				.replace("+variable+", title);
    	boolean result = false;
    	
    	try {
    		Tools.setDriverDefaultValues();
	    	String attrClass = Base.base.getDriver()
	    			.findElement(By.xpath(tasks_item_priority_by_title)).getAttribute("class");
	    	
	    	String actual = this.determinePriority(attrClass);
	    	
	    	Logging.info("Actual priority: " + actual);
	    	if (actual.equals(priority)) {
		    	result = true;
	    	}
		} catch (WebDriverException ex) {
			Logging.error("Could not verify task priority");
		}
		return result;
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 22, 2016<br>
	 * <b>Description: </b><br>
	 * Verifies in task list if status of a task is as expected.
	 * 
	 * @param status - the argument should be constrained to CoreTasks static constants 
	 * initialized with "STATUS"
	 */
	public boolean verifyTaskStatusInList(String title, String status) {
		Logging.info("Verifying task status, title: " + title + ", priority: " + status);
		
    	String tasks_item_status_by_title = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_item_status_by_title")
				.replace("+variable+", title);
    	boolean result = false;
    	
    	try {
    		Tools.setDriverDefaultValues();
	    	String attrClass = Base.base.getDriver()
	    			.findElement(By.xpath(tasks_item_status_by_title)).getAttribute("class");
	    	
	    	String actual = null;
	    	if (attrClass.contains("inProcess")) {
	    		actual = STATUS_INCOMPLETE;
	    	} else if (attrClass.contains("completed")) {
	    		actual = STATUS_COMPLETE;
	    	} else if (attrClass.contains("action")) {
	    		actual = STATUS_NEEDS_ACTION;
	    	}
	    	
	    	Logging.info("Actual status: " + actual);
	    	if (actual.equals(status)) {
		    	result = true;
	    	}
		} catch (WebDriverException ex) {
			Logging.error("Could not verify task status");
		}
		return result;
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 22, 2016<br>
	 * <b>Description: </b><br>
	 * Verifies in task list if description of a task is as expected.
	 */
	public boolean verifyTaskDescriptionInList(String title, String description) {
		Logging.info("Verifying task description, title: " + title + ", description: " + description);
		
    	String tasks_item_description_by_title = PropertyHelper.CoreTasks
				.getPropertyValue("tasks_item_description_by_title")
				.replace("+variable+", title);
    	boolean result = false;
    	
    	try {
    		Tools.setDriverDefaultValues();
	    	String actual = Base.base.getDriver()
	    			.findElement(By.xpath(tasks_item_description_by_title)).getText().trim();
	    	
	    	Logging.info("Actual text: " + actual);
	    	if (actual.equals(description)) {
		    	result = true;
	    	}
		} catch (WebDriverException ex) {
			Logging.error("Could not verify task description");
		}
		return result;
	}
	
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 21, 2016<br>
	 * <b>Description: </b><br>
	 * Verifies if the value from a detail field is as expected.
	 * 
	 * @param field - argument should be constrained to CoreTasks static constants initialized 
	 * with "DETAIL_FIELD"
	 */
	public boolean verifyDetailFieldValue(String field, String value) {
		Logging.info("Verifying detail field value, field: " + field + ", value: " + value);

		String detail_field = null;
		if (field.equals(DETAIL_FIELD_URL) || field.equals(DETAIL_FIELD_DESCRIPTION)) {

			detail_field = PropertyHelper.CoreTasks.getPropertyValue("tasks_detail_textfield");
		} else if (field.equals(DETAIL_FIELD_DUE_TIME)) {
			detail_field = PropertyHelper.CoreTasks.getPropertyValue("tasks_detail_combobox_input1");
		} else if (field.equals(DETAIL_FIELD_REMINDER)) {
			detail_field = PropertyHelper.CoreTasks.getPropertyValue("tasks_detail_combobox_input1");
		} else if (field.equals(DETAIL_FIELD_TITLE)) {
			detail_field = PropertyHelper.CoreTasks.getPropertyValue("tasks_detail_title");
		} else {
			detail_field = PropertyHelper.CoreTasks.getPropertyValue("tasks_detail_combobox_input1");
		}

		boolean result = false;
		String actualValue = null;
		try {
			Tools.setDriverDefaultValues();
			if (field.equals(CoreTasks.DETAIL_FIELD_LIST)) {
				detail_field = PropertyHelper.CoreTasks.getPropertyValue("tasks_detail_combobox_input2");
				actualValue = Base.base.getDriver().findElement(By.xpath(detail_field)).getText();

				Logging.info("Actual value: " + actualValue);

			} else if (field.equals(CoreTasks.DETAIL_FIELD_DESCRIPTION)) {
				detail_field = PropertyHelper.CoreTasks.getPropertyValue("tasks_detail_combobox_Des");
				actualValue = Base.base.getDriver().findElement(By.xpath(detail_field)).getText();

				Logging.info("Actual value: " + actualValue);

			} else {
				actualValue = Base.base.getDriver().findElement(By.xpath(detail_field.replace("+variable+", field)))
						.getText();
				Logging.info("Actual value: " + actualValue);
			}

			if ((actualValue.trim()).equals((value).trim())) {
				result = true;
			}
			Logging.info("Actual value is as expected: " + result);

		} catch (WebDriverException ex) {
			Logging.error("Could not verify value for detail field: " + field);
		}
		return result;
	}
	
	/**
	 * <b>Author: </b>Jerry Zhang<br>
	 * <b>Date: </b>Jun 29, 2016<br>
	 * <b>Description: </b><br>
	 * Verifies if all task items in list are sorted as expected.
	 * 
	 * @param sortBy - the argument should be constrained to CoreTasks static constants:
	 * 				- DETAIL_FIELD_DUE_DATE
	 * 				- DETAIL_FIELD_PRIORITY
	 * 				- DETAIL_FIELD_TITLE
	 * 
	 * @param order - the argument should be constrained to CoreTasks static constants 
	 * initialized with "ORDER"
	 */
	public boolean verifyTaskListSortByAndOrder(String sortBy, String order, 
			String dateFormat, String timeFormat) {
		
		Logging.info("Verifying if task list is sorted by " + sortBy + " in order " + order 
				+ ". Date format: " + dateFormat + ", time format" + timeFormat);
    	
    	String tasks_item_title_all = PropertyHelper.CoreTasks.getPropertyValue("tasks_item_title_all");
    	
    	String allFields = null; 
    	if (sortBy.equals(DETAIL_FIELD_DUE_DATE)) {
    		allFields = PropertyHelper.CoreTasks.getPropertyValue("tasks_item_due_all");
    	} else if (sortBy.equals(DETAIL_FIELD_PRIORITY)) {
    		allFields = PropertyHelper.CoreTasks.getPropertyValue("tasks_item_priority_all");
    	} else if (sortBy.equals(DETAIL_FIELD_TITLE)) {
    		allFields = tasks_item_title_all;
    	} else {
    		throw new IllegalArgumentException("Invalid sort criteria");
    	}
    	
    	try {
    		Tools.setDriverDefaultValues();
    		
    		// Get task items count.
    		int count = Base.base.getDriver().findElements(By.xpath(tasks_item_title_all)).size();
    		Logging.info("Task list item count: " + count);
    		if (count <= 1) {
    			// Zero or one task only, just return false.
    			Logging.info("Cannot verify task order, zero or only one task in list");
    			return false;
    		}
			
    		String previous = null;
    		String current = null;
    		
    		// Iterate the list and start comparing.
    		for (int i = 1; i <= count; i++) {
    			String elemByIndex = "(" + allFields + ")[" + String.valueOf(i) + "]";
    			
    			if (sortBy.equals(DETAIL_FIELD_PRIORITY)) {
    				String cssClass = Base.base.getDriver().findElement(By.xpath(elemByIndex)).getAttribute("class");
    				current = this.determinePriority(cssClass);
    			} else {
    				current = Base.base.getDriver().findElement(By.xpath(elemByIndex)).getText();
    			}
    			Logging.info("Task item " + String.valueOf(i) + ", " + sortBy + ": " + current);
    			
    			if (i == 1) {
    				// Save the first value and skip the loop.
    				previous = current;
    				continue;
    			}
    			
    			// Compare current value with the previous one.
    			int compared;
    			if (sortBy.equals(DETAIL_FIELD_TITLE)) {
    				compared = previous.compareTo(current);
    			} else if (sortBy.equals(DETAIL_FIELD_PRIORITY)) {
    				compared = this.comparePriorityString(previous, current);
    			} else {
    				compared = this.compareDateTimeString(previous, current, dateFormat, timeFormat);
    			}
    			
    			// Compare to verify correct order, otherwise return false.
    			if ((order.equals(ORDER_ASCENDING) && compared > 0) 
    					|| (order.equals(ORDER_DESCENDING) && compared < 0)) {
    				Logging.info("Unordered item found, index: " + i 
    						+ ". previous value: " + previous + ", current value: " + current 
    						+ ", order: " + order);
    				return false;
    			}
    			
    			// Set current value to previous.
    			previous = current;
    		}
    		
		} catch (WebDriverException ex) {
			Logging.error("Could not verify task list sort by and order");
		}
    	
    	// All items are ordered as expected.
		return true;
	}
	
	
	//#########################################################################
	//							  Utility Methods
	//#########################################################################
	
	private int comparePriorityString(String priority1, String priority2) {
		if (priority1.equals(priority2)) {
			return 0;
		} else if (priority1.equals(PRIORITY_HIGH)) {
			return 1;
		} else if (priority2.equals(PRIORITY_HIGH)) {
			return -1;
		} else if (priority1.equals(PRIORITY_NORMAL)) {
			return 1;
		} else {
			return -1;
		}
	}
	
	private String determinePriority(String cssClass) {
		if (cssClass.contains("High")) {
			return PRIORITY_HIGH;
		} else if (cssClass.contains("Low")) {
			return PRIORITY_LOW;
		} else {
			return PRIORITY_NORMAL;
		}
	}
	
	private int compareDateTimeString(String dateTimeStr1, String dateTimeStr2, 
			String dateFormat, String timeFormat) {
		
		if (dateTimeStr1.equals("") && dateTimeStr2.equals("")) {
			return 0;
		} else if (dateTimeStr1.equals("")) {
			return -1;
		} else if (dateTimeStr2.equals("")) {
			return 1;
		}
		
		String pattern = formatPatternMappings.get(dateFormat) + " " 
				+ formatPatternMappings.get(timeFormat);
		LocalDateTime dt1 = LocalDateTime.parse(dateTimeStr1, DateTimeFormatter.ofPattern(pattern));
		LocalDateTime dt2 = LocalDateTime.parse(dateTimeStr2, DateTimeFormatter.ofPattern(pattern));
		
		return dt1.compareTo(dt2);
	}
	
	/**
     * <b>Author: </b>Vivian Xie<br>
     * <b>Date: </b>Dec 14, 2016<br>
     * <b>Description: </b><br>
     * Verifies if the error message is as expected in new task group windows.
     */
    public boolean verifyErrorMessageInNewTaskGroupWin(String body) {
        Logging.info("Verifying if message box body is: " + body);
        

        String task_new_group_error_Message = PropertyHelper.CoreTasks.getPropertyValue("task_new_group_error_Message");
        boolean result = false;

        try {
            Tools.setDriverDefaultValues();
            Tools.waitFor(1, TimeUnit.SECONDS);
            String actText = Base.base.getDriver().findElement(By.xpath(task_new_group_error_Message)).getAttribute("innerHTML");
            Logging.info("Actual text of message box body: " + actText);

            if (actText.contains(body)) {
                result = true;
            }

            Logging.info("Message box body is as expected: " + result);
        } catch (WebDriverException ex) {
            Logging.error("Could not verify message box body");
            throw ex;
        }
        return result;
    }
	
}
