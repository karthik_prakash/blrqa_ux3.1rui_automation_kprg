package com.synchronoss.core;

import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * Customized version of Remote Webdriver. Only use this version to track CSS class values.
 * A parameter from the build environment (Ant) should determine whether to track CSS class values.
 * 
 * @author Jerry Zhang
 */
public class CustomizedRemoteWebDriver extends RemoteWebDriver{
	
	public CustomizedRemoteWebDriver(URL remoteAddress, Capabilities desiredCapabilities) {
		super(remoteAddress, desiredCapabilities);
	}

	@Override
	public WebElement findElement(By by) {
		String xpath = by.toString().replaceFirst("By.xpath: ", "");
		String cssClass;
		try {
			// Get css class value
			cssClass = super.findElement(by).getAttribute("class");
		} catch (NoSuchElementException e) {
			cssClass = CssClassTracker.NULL_SIGN;
		}
		
		// Add this record to CSS tracker
		//Base.cssTracker.add(xpath, cssClass);
		
		// Do the real job
		return super.findElement(by);
	}
	
}
