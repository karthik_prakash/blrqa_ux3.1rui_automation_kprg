package com.synchronoss.core;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.HttpCommandExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * Creates RemoteWebDriver instances to execute tests on Selenium Grid.
 */
public class SeleniumGrid extends Base {
	
	/* 
	 * Using "ThreadLocal" typed WebDriver static field to ensure that each TestNG
	 * test thread handles only 1 driver session, which is bound to a specific 
	 * browser on Selenium Gird node machines.
	 */
	public static InheritableThreadLocal<RemoteWebDriver> driver = new InheritableThreadLocal<RemoteWebDriver>();
	
	
	/*************************************************************************
	 * @Method Name : getDriver
	 * @Author : Alkesh Dagade
	 * @Created On : 08/29/2012
	 * @Description : This method is to get WebDriver instance.
	 * @parameter 1 : NA
	 *************************************************************************/	
	public WebDriver getDriver(){
		return SeleniumGrid.driver.get();
	}
	
	
	/*************************************************************************
	 * @Method Name : SeleniumGrid (constructor)
	 * @Author : Alkesh Dagade
	 * @Created On : 08/29/2012
	 * @Description : This method is to create RemoteWebDriver and Selenium instance.
	 * @parameter 1 : browser: Browser name on which suite to be executed e.g Firefox
	 * @parameter 2 : url: application URL on which suite to be executed
	 *************************************************************************/	
	public SeleniumGrid(String hub, String browser) throws MalformedURLException {
		
		DesiredCapabilities capability = new DesiredCapabilities();
		capability.setBrowserName(browser);
		
		if (browser.equalsIgnoreCase("internet explorer")) {
			capability = DesiredCapabilities.internetExplorer();
		    capability.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, false);
		    capability.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, true);
		    capability.setCapability(InternetExplorerDriver.NATIVE_EVENTS, true);
		    capability.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, false);
		}
		
		if (browser.equalsIgnoreCase("chrome")) {
			// set download path, disable the download popup
			String downloadFilepath = "C:\\Automation";
			HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
			chromePrefs.put("profile.default_content_settings.popups", 0);
			chromePrefs.put("download.default_directory", downloadFilepath);
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-extensions");
			options.addArguments("disable-infobars");
			options.setExperimentalOption("prefs", chromePrefs);

			capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			capability.setCapability(ChromeOptions.CAPABILITY, options);
		}
		
		if (browser.equalsIgnoreCase("edge")) {
			capability = DesiredCapabilities.edge();
			capability.setBrowserName("MicrosoftEdge");
		}
		
		//Throw exception when Css Tracker enalbed on Edge test as it's not supported yet.
		/*if (Base.enableCssTracker && browser.equalsIgnoreCase("edge")) {
			throw new IllegalStateException("Css Tracker feature is not supported on Edge test currently.");
		}
		
		// Use polymorphism to instantiate Remote Webdriver.
		if (Base.enableCssTracker) {
			SeleniumGrid.driver.set(new CustomizedRemoteWebDriver(new URL(hub), capability));
		} else if (browser.equalsIgnoreCase("edge")) {
			SeleniumGrid.driver.set(new RemoteWebDriverForEdge(new URL(hub), capability));
		} else {
			SeleniumGrid.driver.set(new RemoteWebDriver(new URL(hub), capability));
		}
		*/
		System.out.println("Capability info: ");
		System.out.println(SeleniumGrid.driver.get().getCapabilities());
	}
	
	
	@Override
	protected void startSession(String url) {		
		// Get and print out WebDriver session info.
		String ipOfNode = getIPOfNode(SeleniumGrid.driver.get());
		String sessionData = getDriver().toString();
		Logging.info("STARTED SESSION ON: " + ipOfNode + " using " + sessionData);
		
		// Navigating to test site.
		Logging.info("Navigating to: " + url);
		SeleniumGrid.driver.get().get(url);
		driver.get().manage().window().maximize();
	}
	
	
  /**
   * get the current node ip
   * 
   * @return String - node ip
   */
  @Override
  public String getIpOfNode() {
    String ipOfNode = getIPOfNode(SeleniumGrid.driver.get());
    Logging.info("The ip of the node is " + ipOfNode);
    return ipOfNode;
  }

	
	/*************************************************************************
	 * @Method Name : getIPOfNode 
	 * @Author : Ravid Te
	 * @Created On : 10/30/2013
	 * @Description : This method will extract the node data from the hub
	 * @parameter 1 : remoteDriver: this is the remoteDriver instance
	 *************************************************************************/	
	public String getIPOfNode(RemoteWebDriver remoteDriver) { 
		String hostFound = null; 
		try { 
			HttpCommandExecutor ce = (HttpCommandExecutor) remoteDriver.getCommandExecutor(); 
			String hostName = ce.getAddressOfRemoteServer().getHost(); 
			int port = ce.getAddressOfRemoteServer().getPort(); 
			HttpHost host = new HttpHost(hostName, port); 
			DefaultHttpClient client = new DefaultHttpClient(); 
			URL sessionURL = new URL("http://" + hostName + ":" + port + "/grid/api/testsession?session=" + remoteDriver.getSessionId()); 
			BasicHttpEntityEnclosingRequest r = new BasicHttpEntityEnclosingRequest( "POST", sessionURL.toExternalForm()); 
			HttpResponse response = client.execute(host, r); 
			JSONObject object = extractObject(response); 
			URL myURL = new URL(object.getString("proxyId")); 
			if ((myURL.getHost() != null) && (myURL.getPort() != -1)) { 
				hostFound = myURL.getHost(); 
			} 
		} catch (Exception e) { 
			System.err.println(e); 
		} 
		return hostFound; 
	} 
	
	
	/***************************************************************************************
	 * @Method Name : extractObject
	 * @Author : Ravid Te
	 * @Created On : 10/30/2013
	 * @Description : This method will get the http response into json format for consumption
	 * @parameter 1 : httpResponse: the return value from the http request to hub
	 ***************************************************************************************/	
	private static JSONObject extractObject(HttpResponse resp) throws IOException, JSONException { 
		InputStream contents = resp.getEntity().getContent(); 
		StringWriter writer = new StringWriter(); 
		IOUtils.copy(contents, writer, "UTF8"); 
		JSONObject objToReturn = new JSONObject(writer.toString()); 
		return objToReturn; 
	}
	
}
