package com.synchronoss.core;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class Retry implements IRetryAnalyzer{
    private int retryCount = 0;
    public boolean retry(ITestResult result) {
        int maxRetryCount = 0;
        if (retryCount < maxRetryCount) {
            retryCount++;
            System.out.println("Retry # " + retryCount + " for this test.");
            return true;
        }
        return false;
    }
}
