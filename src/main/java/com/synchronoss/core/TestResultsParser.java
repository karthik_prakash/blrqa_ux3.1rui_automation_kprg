package com.synchronoss.core;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.testng.xml.XmlClass;

/**
 * Parses existing testng-results.xml with methods to extract class names of 
 * skipped and failed tests.
 * 
 * @author Jerry Zhang
 */
public class TestResultsParser {
	
	private String testResultsPath = System.getProperty("user.dir") 
			+ File.separator + "test-output"
			+ File.separator + "testng-results.xml";
	
	private Document document;
	
	private final String FAILED_CLASS_XPATH 
		= "//suite//test//test-method[@name='run' and @status='FAIL']/parent::class";
	private final String SKIPPED_CLASS_XPATH 
		= "//suite//test//test-method[@name='run' and @status='SKIP']/parent::class";
	
	/**
	 * Creates a document object from existing testng-results.xml.
	 * @throws DocumentException
	 */
	public TestResultsParser() throws DocumentException {
		File inputFile = new File(this.testResultsPath);
		SAXReader reader = new SAXReader();
		this.document = reader.read(inputFile);
	}
	
	/**
	 * Gets a collection of class names.
	 * @param xpath
	 */
	@SuppressWarnings("unchecked")
	private List<String> getClassNamesByXpath(String xpath) {
		ArrayList<String> classNames = new ArrayList<String>();
		List<Node> classNodes = this.document.selectNodes(xpath);
		
		// Extracts class names.
		for (Node node : classNodes) {
			classNames.add(node.valueOf("@name"));
		}
		return classNames;
	}
	
	/**
	 * Directly forms a collection of TestNG XmlClasses instead of names.
	 * @param xpath
	 */
	@SuppressWarnings("unchecked")
	private List<XmlClass> getXmlClassesByXpath(String xpath) {
		ArrayList<XmlClass> xmlClasses = new ArrayList<XmlClass>();
		List<Node> classNodes = this.document.selectNodes(xpath);
		
		// Extracts  XmlClasses.
		for (Node node : classNodes) {
			xmlClasses.add(new XmlClass(node.valueOf("@name")));
		}
		return xmlClasses;
	}
	
	public List<XmlClass> getSkippedXmlClasses() {
		List<XmlClass> xmlClasses = this.getXmlClassesByXpath(SKIPPED_CLASS_XPATH);
		Logging.info(this.getClass().getSimpleName() + ": " 
				+ xmlClasses.size() + " skipped tests extracted from testng-results.xml" );
		return xmlClasses;
	}
	
	public List<XmlClass> getFailedXmlClasses() {
		List<XmlClass> xmlClasses = this.getXmlClassesByXpath(FAILED_CLASS_XPATH);
		Logging.info(this.getClass().getSimpleName() + ": " 
				+ xmlClasses.size() + " failed tests extracted from testng-results.xml" );
		return xmlClasses;
	}

}
