package com.synchronoss.core;

import com.synchronoss.pageobject.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.testng.SkipException;
import org.testng.annotations.*;

import java.net.MalformedURLException;


/**
 * Base class for WebDriver instances and test case classes:
 *
 * 1. Uses polymorphism to instantiate either local or remote WebDrivers.
 * 2. Defines framework configuration methods via TestNG annotations.
 * 3. Instantiates PageObject objects and act as base class for test cases.
 */
public class Base {

    public static Base base = null;
    public static final String suiteExecutionType = PropertyHelper.configParams.getPropertyValue("suite_execution_type");
    public static final String hub = PropertyHelper.configParams.getPropertyValue("selenium_hub");
    public static final String browserName = PropertyHelper.configParams.getPropertyValue("browser_name");
    public static final String url = PropertyHelper.configParams.getPropertyValue("qa_env_url");
    public static final String usingPool = PropertyHelper.configParams.getPropertyValue("userPool");
    public static final String userLocal = PropertyHelper.configParams.getPropertyValue("userLocal");
    public static final String userPoolFile = PropertyHelper.configParams.getPropertyValue("userPoolFile");
    public static final String resetHub = PropertyHelper.configParams.getPropertyValue("reset_Hub");
    public static final String userInterface = PropertyHelper.configParams.getPropertyValue("reset_Hub");
    public static final String mxosURL = PropertyHelper.configParams.getPropertyValue("mxOS");
    public static final String mxosPort = PropertyHelper.configParams.getPropertyValue("mxPort");

    public WebDriver driver = null;
    public static String hubName = null;

    //public static CssClassTracker cssTracker = new CssClassTracker();
    public static boolean userLocalExcel = false;

    /**
     * @Method Name : getDriver
     * @Author : Alkesh Dagade
     * @Created On : 08/29/2012
     * @Description : This method is an abstract method to get webdriver
     *              instance.
     * @parameter 1 : NA
     */
    public WebDriver getDriver() {
        return null;
    }


    /**
     * @Method Name : setBaseInstance
     * @Author : Alkesh Dagade
     * @Created On : 08/29/2012
     * @Description : This method is to create instance of child class based on
     *              SuiteExecutionType variable value.
     * @parameter 1 : SuiteExecutionType->its value should be "Grid"->if suite
     *            to be executed using Grid on remote machine "Local"->If suite
     *            to be executed on local machine using webdriver
     * @parameter 2 : hub->URL of hub where Jenkins is installed
     * @parameter 3 : browser->browser type, possible values are:
     *            "firefox"->Mozilla firefox "chrome"->Google Chrome
     *            "iexplore"->Internet Explorer
     */
    public void setBaseInstance() throws MalformedURLException {
        if (suiteExecutionType.equalsIgnoreCase("grid")) {
            base = new SeleniumGrid(hub, browserName);
        } else {
            base = new SeleniumLocal(browserName);
        }
    }


    /*
     * Starts a WebDriver session. Should be overridden in driver instantiation classes.
     */
    protected void startSession(String url) {
        // To be overridden.
    }



    //#########################################################################
    //							Utility Fields and Methods
    //#########################################################################

    // Logging Duty
    public Logging frameworkLog = new Logging();

    // Tools Helper
    public Tools tools = new Tools();

    // User pool utilities
    public static boolean usingUserPool;
    public static String userPoolFileName;
    public UserPool mxosPool;
    public static UserPool pool;

    // Fields and method records of login user/pass for reference in test
    protected String loginUser = "";
    protected String loginPass = "";

    // Login Latency always captured
    //public static long startTime;
    //public static long endTime;
    //public static long totalTime;
    // Configurable Timeout
    public static String timeout = "15";
    // BrowserName
    //public static String browserName="Chrome";
    //public static boolean enableCssTracker;

    // Timeout value definitions in seconds
    public static final int timeoutShort = 15;
    public static final int timeoutMedium = 60;
    public static final int timeoutLong = 90;


    // Getters and setters for user pool manipulation.
	public  UserPool getPoolByMxos() {
		return this.mxosPool;
	}
	public  void setMxosPool(UserPool userPoolParemeter) {
		mxosPool = userPoolParemeter;
	}

	// Getters and setters for user pool manipulation.
	public static UserPool getPool() {
		return Base.pool;
	}
	public static void setPool(UserPool userPool) {
		Base.pool = userPool;
	}




    //#########################################################################
    //							Core Page Objects
    //#########################################################################

    public CoreLogin login = new CoreLogin();
    public CoreNavigation navigation = new CoreNavigation();
    public CoreEmail email = new CoreEmail();
    public CoreContacts contacts = new CoreContacts();
    public CoreSettings settings = new CoreSettings();
    public CoreCalendar calendar = new CoreCalendar();
    public CoreTasks tasks = new CoreTasks();



    //#########################################################################
    //						TestNG Configuration Methods
    //#########################################################################

	/*@BeforeSuite(alwaysRun = true)
	@Parameters({ "userPool", "resetHub", "hub.name", "userPoolFile", "userLocal" })
	public void beforeSuite(@Optional("false") String usingPool, String resetHub, String hub,
			@Optional("userPool.xlsx") String userPoolFile, String userLocal) {
       Logging.info("userLocal="+userLocal);

       // check if use local excel file
		if(userLocal.equals("true"))
			BaseTest.userLocalExcel = true;
		else
			BaseTest.userLocalExcel = false;

		Logging.info("usingPool="+ usingPool +" and userLocal="+userLocal );
		if (usingPool.equalsIgnoreCase("true")) {
			BaseTest.usingUserPool = true;

			if(userLocalExcel){
				BaseTest.userPoolFileName = userPoolFile;
				BaseTest.setPool(new UserPool(userPoolFile));
				Logging.info("@BeforeSuite: User pool initialized successfully");
			}
		} else {
			BaseTest.usingUserPool = false;
		}
		this.hubName = hub;
	    if (resetHub.equals("true"))
           Tools.resetHub(hub);
	    System.out.println("jfang base.before suite");
	}
	*/


    @BeforeSuite
    public void beforeSuite() throws MalformedURLException {
        Logging.info("userLocal="+userLocal);

        // check if use local excel file
        if(userLocal.equals("true"))
            Base.userLocalExcel = true;
        else
            Base.userLocalExcel = false;
        if(userLocalExcel){
            Base.userPoolFileName = userPoolFile;
            Base.setPool(new UserPool(userPoolFile));
            Logging.info("@BeforeSuite: User pool initialized successfully");
        }

        Logging.info("usingPool="+ usingPool +" and userLocal="+userLocal );
        if (usingPool.equalsIgnoreCase("true")) {
            Base.usingUserPool = true;
             if (usingUserPool && !userLocalExcel) {
                // Create user pool instance.
                try {
                    mxosPool = new UserPool(mxosURL, mxosPort);
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                    throw new SkipException("user pool create failed");
                }
                this.setMxosPool(mxosPool);
            }
        }
        else {
            Base.usingUserPool = false;
        }
        this.setBaseInstance();
        Base.base.startSession(url);
        this.hubName = hub;
        if (resetHub.equals("true"))
            Tools.resetHub(hub);
        System.out.println("Base - Before Suite");
    }

    @BeforeClass
    public void beforeClass()
    {
        this.loginUser = PropertyHelper.configParams.getPropertyValue("username1");
        this.loginPass = PropertyHelper.configParams.getPropertyValue("password1");

        //Maximize browser window before testing.
        Base.base.getDriver().manage().window().maximize();
        login.waitForLoginPageToLoad(Base.base.timeoutLong);

        PropertyFiles.setUserInterface(userInterface);
        Logging.info("@BeforeClass: Logged in to the application successfully.");

        Logging.info("usingUserPool="+usingUserPool+"userLocalExcel"+userLocalExcel);
        // using local excel and userpool
        if (usingUserPool && userLocalExcel) {
            boolean userObtained = Base.getPool().obtainUserByDriver(Base.base.getDriver());
            if (!userObtained) {
                Logging.error("@BeforeMethod: Fail to obtain user from user pool");
                throw new IllegalStateException(
                        "Fail to obtain user from user pool");
            }
            Logging.info("@BeforeMethod: User obtained by session successfully");
            this.loginUser = Base.getPool().getUserName1();
            this.loginPass = Base.getPool().getPassword1();
        } else if (usingUserPool & !userLocalExcel) {
            Logging.info("@BeforeMethod: User obtained by session successfully");
            this.loginUser = this.getPoolByMxos().getUserName1();
            this.loginPass = this.getPoolByMxos().getPassword1();
        }

        login.typeUsername(loginUser);
        login.typePassword(loginPass);
        login.clickLoginButton();

        Logging.info("@BeforeMethod: User " + loginUser + " logged in");
        System.out.println("jfang base.before method");
    }


    @AfterClass
    public void afterClass(){
        try {
            navigation.clickLogoutButton();
            Logging.info("@AfterClass: User logged out successfully.");
        } catch (WebDriverException e) {
            Logging.warn("@AfterClass: User failed to logout.");
        }
    }

    @AfterSuite
    public void afterSuite(){
        try {
            // Quit driver and close the browser.
            Base.base.getDriver().quit();
            System.out.println("------------------------------------------------");
            System.out.println("------------------------------------------------");
        } catch (WebDriverException e) {
            Logging.warn("@AfterClass: Test case completed .. stopping browser");
        }
    }



	/*@BeforeClass(alwaysRun = true)
	@Parameters({ "selenium.hub", "selenium.browser", "browser.url",
			"timelimit", "SuiteExecutionType", "userInterface",
			"enableCssTracker", "mxosURL", "mxosPort" })
	public void beforeClass(@Optional("selenium.hub") String hub,
			String browser, String url, String timelimit,
			String suiteExecutionType, String userInterface,
			boolean enableTracker, String mxosURL, String mxosPort)
			throws MalformedURLException {

		timeout = timelimit;
		browserName = browser;
		enableCssTracker = enableTracker;

		// Create WebDriver instance using run time polymorphism.
		this.setBaseInstance(suiteExecutionType, hub, browser);
		Logging.info("@BeforeClass: Base instance set up successfully");
		BaseTest.base.startSession(url);
		// not use local excel
		if (usingUserPool && !userLocalExcel) {
			// Create user pool instance.
			try {
				mxosPool = new UserPool(mxosURL, mxosPort);
			} catch (NullPointerException ex) {
				ex.printStackTrace();
				throw new SkipException("user pool create failed");

			}
			this.setMxosPool(mxosPool);
		}
		Logging.info("@BeforeSuite: User pool initialized successfully");
		PropertyFiles.setUserInterface(userInterface);
		System.out.println("jfang base.before class");
	}

	@BeforeMethod(alwaysRun = true)
	@Parameters({ "username1", "password1", "userPool", "browser.url"})
	public void beforeMethod(String username1, String password1, boolean usingUserPool, String url) {

		this.loginUser = username1;
		this.loginPass = password1;

		//Maximize browser window before testing.
		BaseTest.base.getDriver().manage().window().maximize();
		login.waitForLoginPageToLoad(BaseTest.timeoutLong);

		Logging.info("usingUserPool="+usingUserPool+"userLocalExcel"+userLocalExcel);
		// using local excel and userpool
		if (usingUserPool && userLocalExcel) {
			boolean userObtained = BaseTest.getPool().obtainUserByDriver(
					BaseTest.base.getDriver());
			if (!userObtained) {
				Logging.error("@BeforeMethod: Fail to obtain user from user pool");
				throw new IllegalStateException(
						"Fail to obtain user from user pool");
			}
			Logging.info("@BeforeMethod: User obtained by session successfully");
			this.loginUser = BaseTest.getPool().getUserName1();
			this.loginPass = BaseTest.getPool().getPassword1();
		} else if (usingUserPool & !userLocalExcel) {
			Logging.info("@BeforeMethod: User obtained by session successfully");
			this.loginUser = this.getPoolByMxos().getUserName1();
			this.loginPass = this.getPoolByMxos().getPassword1();

		}

		Logging.info("@BeforeMethod: User " + username1 + " logged in");
		System.out.println("jfang base.before method");
	}
	*/


	/*
	@AfterMethod(alwaysRun = true)
	public void afterMethod() {
		try {
			navigation.clickLogoutButton();
			Logging.info("@AfterMethod: User logged out successfully");
		} catch (WebDriverException e) {
			Logging.warn("@AfterMethod: User failed to log out");
		}
		System.out.println("jfang base.after method");
	}


	@AfterClass(alwaysRun = true)
	@Parameters({"userPool"})
	public void afterClass(Boolean usingUserPool) {
		if (usingUserPool) {
			// Reset login user/pass
			this.loginUser = "";
			this.loginPass = "";
		}
		// set free user if using local excel
		if (userLocalExcel) {
			BaseTest.getPool().setUserFree(BaseTest.base.getDriver());
		}

		Logging.info("@AfterClass: Test case completed .. stopping browser");

		// Quit driver and close the browser.
        BaseTest.base.getDriver().quit();
        System.out.println("------------------------------------------------");
        System.out.println("------------------------------------------------");
        System.out.println("jfang base.after class");
	}


	@AfterSuite(alwaysRun = true)
	@Parameters({"enableCssTracker"})
	public void afterSuite(boolean enableTracker) {
		if (enableTracker) {
			BaseTest.cssTracker.saveRecords();
			Logging.info("@AfterSuite: Xpath-CssClass mapping records saved successfully");
		}
		System.out.println("jfang base.after suite");
	}
	*/

    /**
     * get the current browser name
     * @return String
     *  the return value may be  "firefox", "chrome" or "internet explorer"
     */
    public static String getBrowserName () {
        return Base.browserName;
    }

    /**
     * get the current node ip
     *
     * @return String - node ip
     */
    public String getIpOfNode() {
        // TODO Auto-generated method stub
        return null;
    }

    private static int getFibo(int i) {
        if (i == 1 || i == 2)
            return 1;
        else
            return getFibo(i - 1) + getFibo(i - 2);
    }
}

