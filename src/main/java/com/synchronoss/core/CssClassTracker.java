package com.synchronoss.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Keep tracking the Xpath-CssClass mappings during test executions, and write all 
 * records to a spreadsheet when test suite is finished.
 * 
 * @author Jerry Zhang
 */
public class CssClassTracker {
	public static final String NULL_SIGN = "NULL";
	
	private class MappingRecord {
		public MappingRecord(String xpath, String css) {
			this.count = 1;
			this.xpath = xpath;
			this.css = css;
		}
		public int count = 0;
		public String xpath = NULL_SIGN;
		public String css = NULL_SIGN;
	}
	
	private HashMap<String, MappingRecord> records = new HashMap<String, MappingRecord>();
	public boolean isLocked = false;
	
	
	public void add(String xpath, String cssClass) {
		// wait for writing privilege obtained and avoid dead loop by quitting 
		// after 1 second of waiting.
		int i = 0;
		while (this.isLocked && (i < 100)) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			i++;
		}
		
		if (this.isLocked) {
			Logging.warn("Fail to add a Xpath-CssClass mapping record: <xpath:> " 
					+ xpath + "  <css:> " + cssClass);
			return;
		}
		
		// thread lock
		this.isLocked = true;
		if (records.containsKey(xpath)) {
			MappingRecord record = records.get(xpath);
			
			// insert record if css name not null
			if (!cssClass.equals(NULL_SIGN)) {
				record.css = cssClass;
			}
			// increase count whatever css name is
			record.count++;
			
		} else {
			records.put(xpath, new MappingRecord(xpath, cssClass));
		}

		Logging.info("Successfully adding a Xpath-CssClass mapping record: <xpath:> " 
				+ xpath + "  <css:> " + cssClass);
		
		// thread unlock
		this.isLocked = false;
	}
	
	
	public void saveRecords() {
		/* Debugging only
		for (MappingRecord record : records.values()) {
			System.out.println("Count: " + record.count);
			System.out.println("Xpath: " + record.xpath);
			System.out.println("CSS: " + record.css);
			System.out.println();
		}
		*/
		
		/************************ Create spreadsheet file **********************/
		String pathName = System.getProperty("user.dir") + File.separator
				+ "css-mapping" + File.separator;
		
		String fileName = pathName + "css_mapping-" 
				+ new SimpleDateFormat("yyyy_MM_dd_HHmm").format(new Date()) 
				+ ".xlsx";
		
		File pathFile = new File(pathName);
		File cssMapping = new File(fileName);
		
		if (!cssMapping.exists()) {
			try {
				pathFile.mkdirs();
				cssMapping.createNewFile();
			} catch (IOException e) {
				Logging.error("Fail to create CSS mapping spreadsheet: " + e.getMessage());
			}
		}
		
		/************************ Populate spreadsheet with records **********************/
		Workbook wb = new XSSFWorkbook();
		Sheet sheet1 = wb.createSheet("new sheet");
				
		Row titleRow = sheet1.createRow(0);
		titleRow.createCell(0).setCellValue("Frequency of Invoking");
		titleRow.createCell(1).setCellValue("Xpath Value");
		titleRow.createCell(2).setCellValue("CSS Class Value");
		
		int rowIndex = 1;
		for (MappingRecord record : records.values()) {
			Row row = sheet1.createRow(rowIndex);
			
			row.createCell(0).setCellType(Cell.CELL_TYPE_NUMERIC);
			row.getCell(0).setCellValue(record.count);
			row.createCell(1).setCellType(Cell.CELL_TYPE_STRING);
			row.getCell(1).setCellValue(record.xpath);
			row.createCell(2).setCellType(Cell.CELL_TYPE_STRING);
			row.getCell(2).setCellValue(record.css);
			
			rowIndex++;
		}
		
		/************************ Write spreadsheet to file **********************/
	    FileOutputStream fileOut;
		try {
			fileOut = new FileOutputStream(cssMapping);
		    wb.write(fileOut);
		    fileOut.close();
		} catch (FileNotFoundException e) {
			Logging.error("Fail to write CSS mapping spreadsheet: " + e.getMessage());
		} catch (IOException e) {
			Logging.error("I/O exception when writing CSS mapping spreadsheet: " + e.getMessage());
		}

	}

}
