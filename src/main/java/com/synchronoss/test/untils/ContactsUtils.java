package com.synchronoss.test.untils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriverException;

import com.synchronoss.core.Base;
import com.synchronoss.core.Logging;
import com.synchronoss.core.Tools;

public class ContactsUtils extends Base {
	
	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 01, 2015<br>
	 * <b>Description</b><br>
	 * Add a new contact into contacts list.
	 * 
	 * @return void
	 */
	public void addNewContact (ContactsInfo contactsinfo) {
		Logging.info("Adding " + contactsinfo.firstname +  " and " + contactsinfo.email + " into Contacts list");
		// click on "Add contact" button
		contacts.clickContactsToolBarButton("Add contact");
		
		// enter "First Name" data
		contacts.enterContactNameInfo("First Name", contactsinfo.firstname);
		
		// enter "email" data
		contacts.enterContactMainInfo("Email", contactsinfo.email);
		
		// click on "Save" button
		contacts.clickContactSave();
	}
	
	
	
	/**
     * <b>Author</b>: Vivian Xie<br>
     * <b>Date</b>: Nov 14, 2016<br>
     * <b>Description</b><br>
     * Add the specific contact to specific group.<br>
     * <b>parameter</b>: String firstName-- the first name of the contact<br>
     * <b>parameter</b>: String groupName-- the name of the group<br>
     * @return void
     */
    public void addContactToGroup(String firstName, String groupName) {
      Logging.info("Adding contact " + firstName +"To group " +  groupName);
      
      // click on Default Address Book to move focus on Default Address Book 
      contacts.clickOnDefaultAddressBook();
    
      //click on the specific contact
      contacts.clickContactInListByName(firstName);
      //click on "Add to group" button
      contacts.clickContactDetailViewToolbarButton("Add to group");
      //selevt the specific group
      contacts.addUserToGroup(firstName, groupName);
      
    }
    
    /**
     * <b>Author</b>: Vivian Xie<br>
     * <b>Date</b>: Nov 14, 2016<br>
     * <b>Description</b><br>
     * Add a new group with the specific name.
     * <b>parameter: String groupName
     * @return void
     */
    public void addNewGroup(String groupName) {
      Logging.info("Adding group" + groupName );
      
      // click on Default Address Book to move focus on Default Address Book 
      contacts.clickOnDefaultAddressBook();
      contacts.clickOnDefaultAddressBook();
      
      //click on "Add group" button
      contacts.clickContactsToolBarButton("Add Contact group");
      
      //input group name
      contacts.inputGroupName(groupName);
      
      //click "Save" button
      contacts.clickSaveOrCancelGroupButton("Save");
      
      //Thread wait for 1 second
      Tools.waitFor(1, TimeUnit.SECONDS);
      
    }
	
	public void addNewSimpleContact (ContactsInfo contactsinfo) {
		Logging.info("Adding " + contactsinfo.email + " into Contacts list");
		// click on "Add contact" button
		contacts.clickContactsToolBarButton("Add contact");
		
		// enter "email" data
		contacts.enterContactMainInfo("Email", contactsinfo.email);
		
		// click on "Save" button
		contacts.clickContactSave();
	}
	
	public void addNewFullContact (ContactsInfo contactsinfo) {
		Logging.info("Adding " + contactsinfo.email + " , " + contactsinfo.firstname + " and " + contactsinfo.lastname + " into Contacts list");
		// click on "Add contact" button
		contacts.clickContactsToolBarButton("Add contact");
		
		// enter "First and last Name" data
		contacts.enterContactNameInfo("firstname", contactsinfo.firstname);
		contacts.enterContactNameInfo("lastname", contactsinfo.lastname);
		
		// enter "email" data
		contacts.enterContactMainInfo("Email", contactsinfo.email);
		
		// click on "Save" button
		contacts.clickContactSave();
	}
	
	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Jul 01, 2015<br>
	 * <b>Description</b><br>
	 * Edit an exist contact into contacts list.
	 * 
	 * @return void
	 */
	public void editContact (ContactsInfo contact1, ContactsInfo contact2) {
		Logging.info("Editing " + contact1.firstname + " to " + contact2.firstname + " Contacts list");
		
		// verify contact1 exists in Contacts list
		if (!contacts.verifyContactInList(contact1.firstname)) {
			this.addNewContact(contact1);
		}
		
		try {
			Tools.setDriverDefaultValues();
			
			// click on contact1 in list
			contacts.clickContactInListByName(contact1.firstname);
			
			// click "Edit" button in preview window
			contacts.clickContactDetailViewToolbarButton("Edit");
			
			//clear and enter first name with contact2
			contacts.enterContactNameInfo("firstname", contact2.firstname);
			
			//clear and enter email with contact2
			contacts.enterContactMainInfo("Email", contact2.email);

			// click on "Save" button
			contacts.clickContactSave();
	    	
		} catch (WebDriverException ex) {
			Logging.error("Could not edit the contact: " + contact1.firstname );
			throw ex;
		}
	}
}
