package com.synchronoss.test.untils;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriverException;
import org.testng.Assert;

import com.synchronoss.core.Base;
import com.synchronoss.core.Logging;
import com.synchronoss.core.Tools;

public class CalendarUtils extends Base{
	
	/**
	 * <b>Author</b>: Young Zhao<br>
	 * <b>Date</b>: Aug 21, 2015<br>
	 * <b>Description</b><br>
	 * Add a new calendar into calendar list.
	 * 
	 * @return void
	 */
	public void addNewCalendar (String calendarName, int index) {
		Logging.info("Adding " + calendarName + " into calendar list");
		
		try {
			Tools.setDriverDefaultValues();
			
			// verify calendarName exists in calendar list already
			Assert.assertFalse(calendar.verifyCalendarExistenceByNameAndColorIndex(calendarName, index));
			
			// Add a calendar, input name and select color index
			calendar.clickPlusIconAddOrSubscribeCalendar("Add a calendar");
			calendar.typeInCalendarNameFieldInAddCalendarWindow(calendarName);
			calendar.clickColorByIndexInAddCalendarWindow(index);
			calendar.clickToolBarButtonInAddCalendarWindow("Save");
	    	
		} catch (WebDriverException ex) {
			Logging.error("Could not add the calendar into list: " + calendarName );
			throw ex;
		}		
	}
	
	/**
	 * <b>Author</b>: Fiona Zhang<br>
	 * <b>Date</b>: Sep 22, 2015<br>
	 * <b>Description</b><br>
	 * This method will add new event.
	 * 
	 * <b>Parameters</b><br>
	 * EventInfo info: event object
	 */
	public void addNewEvent(EventInfo info,String settingTimeFormat) {
		Logging.info("Start to add event: " + info.getSummary());

		try {
			Tools.setDriverDefaultValues();
			// open new event view
			calendar.clickOnCalendarToolBarButton("New event");
			
			// summary
			calendar.typeInSummaryOrLocationOnNewEventPanel("Summary", info.getSummary());
			// startDate 
			calendar.setStartDateOnEventEditorPanel(info.getStartDateTime());	
			// start and end time
			if(info.getAllDay()){
				calendar.EnableAllDayEvent();
			}else
			{
				calendar.setStartTimeOnEventEditorPanel(info.getStartDateTime(),this.getMappingTime(settingTimeFormat));
				calendar.setEndTimeOnEventEditorPanel(info.getEndDateTime(),this.getMappingTime(settingTimeFormat));
			}
			//endDate
			calendar.setEndDateOnEventEditorPanel(info.getEndDateTime());
			// repeatable
			calendar.setEventRepeatbleTime(info.getRepeatable());
			// reminder and reminder Frequency 
			if(!(info.getReminder().equals("None"))){
//				calendar.setEventReminderValue(info.getReminder());
//				calendar.setEventReminderFrequency(info.getReminderFrequency());
				calendar.setEventReminder(info.getReminder());
			}
			
			// calendar name
			if(!info.getDestCalendar().equalsIgnoreCase("default")){
				calendar.setEventCalendarName(info.getDestCalendar());
			}
			// category
			calendar.setEventCategory(info.getCategory());
			// location
			calendar.typeInSummaryOrLocationOnNewEventPanel("Location",info.getLocation());
			// description
			calendar.setEventDescription(info.getDescription());
			// and attendee and click save button
			if(null == info.getAttendee() || info.getAttendee().size() == 0){
				calendar.clickToolbarButtonOnNewEventPanel("Save");
			} else{
				for (int i =0; i < info.getAttendee().size();i++)
				{
					calendar.typeInAddAttendee(info.getAttendee().get(i));
					calendar.clickAttendeePlusButton();		
				}
				calendar.clickToolbarButtonOnNewEventPanel("Save & Send Invitation");	
			}
			
		} catch (WebDriverException ex) {
			throw ex;
		}
		Logging.info("Finish to add event");

	}
	
	public String getMappingTime(String dataMap) {
		Map<String, String> dateFormatSetting = new HashMap<String, String>();
		dateFormatSetting.put("DD/MM/YYYY", "dd/LL/yyyy");
		dateFormatSetting.put("DD/MM/YY", "dd/LL/yy");
		dateFormatSetting.put("MM/DD/YYYY", "LL/dd/yyyy");
		dateFormatSetting.put("YYYY/MM/DD", "yyyy/LL/dd");
		dateFormatSetting.put("DD-MM-YYYY", "dd-LL-yyyy");
		dateFormatSetting.put("DD.MM.YYYY", "dd.LL.yyyy");
		dateFormatSetting.put("24", "HH:mm");
		dateFormatSetting.put("12", "h:mm a");
		
		return dateFormatSetting.get(dataMap);
	}
			
}
