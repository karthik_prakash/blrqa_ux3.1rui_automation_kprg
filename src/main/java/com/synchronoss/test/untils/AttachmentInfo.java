package com.synchronoss.test.untils;

public class AttachmentInfo {
  private String sender;
  private String mailSubject;
  private String attachmentFile[];
  private String sendDate;
  private String attachmentType;

  public AttachmentInfo() {

  }

  public AttachmentInfo(String attachmentName, String mailSubject, String imageType, String sender) {
    attachmentFile = new String[1];
    this.mailSubject = mailSubject;
    this.attachmentType = imageType;
    this.attachmentFile[0] = attachmentName;
    this.sender = sender;
  }

  public String getSender() {
    return sender;
  }

  public void setSender(String sender) {
    this.sender = sender;
  }

  public String getMailSubject() {
    return mailSubject;
  }

  public void setMailSubject(String mailSubject) {
    this.mailSubject = mailSubject;
  }

  public String getAttachmentFile() {
    return attachmentFile[0];
  }

  public String[] getAttachmentFiles() {
    return attachmentFile;
  }

  public void setAttachmentFile(String[] attachmentFile) {
    this.attachmentFile = attachmentFile;
  }

  public String getSendDate() {
    return sendDate;
  }

  public void setSendDate(String sendDate) {
    this.sendDate = sendDate;
  }

  public String getAttachmentType() {
    return attachmentType;
  }

  public void setAttachmentType(String attachmentType) {
    this.attachmentType = attachmentType;
  }
}
