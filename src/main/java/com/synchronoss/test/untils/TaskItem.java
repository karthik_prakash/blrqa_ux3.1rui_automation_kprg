package com.synchronoss.test.untils;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Represents a task item object that will be used by TaskUtils.
 * @author Jerry Zhang
 */
public class TaskItem {
	
	// Task option fields
	public String title = null;
	public String list = null;
	public String priority = null;
	public String status = null;
	
	public LocalDateTime due = null;
	public String reminder = null;
	
	public String url = null;
	public String description = null;
	
	
	public TaskItem() {
		this.title = "Task_" + UUID.randomUUID().toString().substring(0, 8);
	}

}
