package com.synchronoss.test.untils;

import java.util.UUID;

public class SignatureInfo {
	
	public String name = null;
	public String body = null;
	
	public SignatureInfo() {
		this.name = "SignatureName" + UUID.randomUUID().toString().substring(0, 4);
		this.body = "Signature body " + UUID.randomUUID().toString().substring(0, 16);
	}
}