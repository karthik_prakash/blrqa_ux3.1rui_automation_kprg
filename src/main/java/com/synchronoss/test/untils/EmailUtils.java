package com.synchronoss.test.untils;

import com.synchronoss.core.Base;

public class EmailUtils extends Base {
	
	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Jun 08, 2015<br>
	 * <b>Description</b><br>
	 * Encapsulates the actions of generating a simple conversation 
	 * by reply between 2 users.
	 * 
	 * @return void
	 */
	public void generateConversationByReply (String emailId1, String username1, String password1, 
    		String emailId2, String username2, String password2, EmailMessage msg) {
		
    	// Send an email from user1 to user2.
        email.selectLayout("right");
        email.clickEmailToolBarButton("Compose");
        email.getComposer().typeInToFieldWithValidation(emailId2);
        email.getComposer().typeInSubjectField(msg.subject);
        email.getComposer().typeInMessageBody(msg.body);
        email.getComposer().clickComposerToolBarButton("Send");
        navigation.clickLogoutButton();
        
        // Login to user2 and reply the email.
        login.waitForLoginPageToLoad(Base.timeoutMedium);
        login.typeUsername(username2);
        login.typePassword(password2);
        login.clickLoginButton();
        
        email.selectLayout("right");
        email.clickOnMessageBySubject(msg.subject);
        email.clickEmailToolBarButton("Reply");
        email.getComposer().typeInMessageBody(msg.user2ReplyBody1);
        email.getComposer().clickComposerToolBarButton("Send");
        navigation.clickLogoutButton();
        
        // login to user1 and reply the email again.
        login.waitForLoginPageToLoad(Base.timeoutMedium);
        login.typeUsername(username1);
        login.typePassword(password1);
        login.clickLoginButton();
        
        email.selectLayout("right");
        email.clickOnMessageBySubject(msg.replySubject);
        email.clickEmailToolBarButton("Reply");
        email.getComposer().typeInMessageBody(msg.user1ReplyBody1);
        email.getComposer().clickComposerToolBarButton("Send");
        navigation.clickLogoutButton();
        
        // Login to user2 again and verify conversations.
        login.waitForLoginPageToLoad(Base.timeoutMedium);
        login.typeUsername(username2);
        login.typePassword(password2);
        login.clickLoginButton();
        
        // Enable conversation view.
        email.changeConversationView(true);
	}
	
	
	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Jun 15, 2015<br>
	 * <b>Description</b><br>
	 * Encapsulates the actions of generating a simple conversation 
	 * by reply to sender itself.
	 * 
	 * @return void
	 */
	public void generateConversationByReplyToSelf (String emailId1, String username1, 
			String password1, EmailMessage msg) {
		
    	// Send an email from user1 to self.
        email.selectLayout("right");
        email.clickEmailToolBarButton("Compose");
        email.getComposer().typeInToFieldWithValidation(emailId1);
        email.getComposer().typeInSubjectField(msg.subject);
        email.getComposer().typeInMessageBody(msg.body);
        email.getComposer().clickComposerToolBarButton("Send");
        
        // Refresh and click on the received email.
        email.clickRefresh();
        email.clickOnMessageBySubject(msg.subject);
        email.clickEmailToolBarButton("Reply");
        email.getComposer().typeInMessageBody(msg.user1ReplyBody1);
        email.getComposer().clickComposerToolBarButton("Send");
        
        // Refresh to receive the replied email and then enable conversation view.
        email.clickRefresh();
        email.changeConversationView(true);
	}
	
	
	/**
	 * <b>Author</b>: Jerry Zhang<br>
	 * <b>Date</b>: Jun 15, 2015<br>
	 * <b>Description</b><br>
	 * Encapsulates the actions of generating a simple conversation by reply to 
	 * sender itself and also CC to other.
	 * 
	 * @return void
	 */
	public void generateConversationByReplyToSelfAndCCToOther (String emailId1, String username1, 
			String password1, String emailId2, String username2, String password2, EmailMessage msg) {
		
    	// Send an email from user1 to self and cc to user2.
        email.selectLayout("right");
        email.clickEmailToolBarButton("Compose");
        email.getComposer().typeInToFieldWithValidation(emailId1);
        //email.getComposer().clickOnCcButton();
        email.getComposer().typeInCcFieldWithValidation(emailId2);
        email.getComposer().typeInSubjectField(msg.subject);
        email.getComposer().typeInMessageBody(msg.body);
        email.getComposer().clickComposerToolBarButton("Send");
        
        // Refresh and click on the received email then reply all.
        email.clickRefresh();
        email.clickOnMessageBySubject(msg.subject);
        email.clickEmailToolBarButton("Reply All");
        email.getComposer().typeInMessageBody(msg.user1ReplyBody1);
        email.getComposer().clickComposerToolBarButton("Send");
        
        // Refresh to receive the replied email and then enable conversation view.
        email.clickRefresh();
        email.changeConversationView(true);
	}
	
	
	/**
     * <b>Author</b>: Vivian Xie<br>
     * <b>Date</b>: Nov 25, 2016<br>
     * <b>Description</b><br>
     * move a mail to the specific system folder
     * <b>parameter:</b> folder name--can be Inbox, Sent, Drafts,Trash,Spam
     * @return void
     */
  public void moveEmailToSystemFolder(String folderName) {
    email.clickEmailToolBarButton("More");
    email.moveToMenuItem("Move to", 2);
    if (folderName.equals("Inbox")) {
      email.clickMenuItem(folderName);
    } else {
      email.moveToMenuItem("Inbox", 1);
      email.clickMenuItem(folderName);
    }


  }

}
