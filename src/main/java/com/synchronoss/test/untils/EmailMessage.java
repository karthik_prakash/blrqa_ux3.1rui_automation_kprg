package com.synchronoss.test.untils;

import java.util.UUID;

public class EmailMessage {
	public static String replyPrefix = "Re: ";
	public static String forwardPrefix = "Fwd: ";
	
	public String subject = null;
	public String body = null;
	
	public String replySubject = null;
	public String forwardSubject = null;
	
	public String user1ReplyBody1 = null;
	public String user1ReplyBody2 = null;
	public String user1ForwardBody1 = null;
	
	public String user2ReplyBody1 = null;
	public String user2ReplyBody2 = null;
	public String user2ForwardBody1 = null;
	
	
	public EmailMessage() {
		this.subject = "Test mail " + UUID.randomUUID().toString().substring(0, 8);
		this.body = "Original body " + UUID.randomUUID().toString().substring(0, 16);
		
		this.replySubject = EmailMessage.replyPrefix + this.subject;
		this.forwardSubject = EmailMessage.forwardPrefix + this.subject;
		
		this.user1ReplyBody1 = "Reply body 1 by user 1 " 
				+ UUID.randomUUID().toString().substring(0, 16);
		this.user1ReplyBody2 = "Reply body 2 by user 1 " 
				+ UUID.randomUUID().toString().substring(0, 16);
		this.user1ForwardBody1 = "Forward body 1 by user 1 " 
				+ UUID.randomUUID().toString().substring(0, 16);
		
		this.user2ReplyBody1 = "Reply body 1 by user 2 " 
				+ UUID.randomUUID().toString().substring(0, 16);
		this.user2ReplyBody2 = "Reply body 2 by user 2 " 
				+ UUID.randomUUID().toString().substring(0, 16);
		this.user2ForwardBody1 = "Forward body 1 by user 2 " 
				+ UUID.randomUUID().toString().substring(0, 16);
		
	}
	
}
